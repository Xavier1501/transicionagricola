
<br>
<!--//Pregunta 1-->

<!--Pregunta 2-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
        <label>
            2.- Resumen sobre el trabajo que realiza en la iniciativa o experiencia agroecológica
        </label>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control resumen" rows="3" name="resumen" id="resumen"
                                      value=""><?php echo $resumen = (!empty($entrevistaArray['resumen'])) ? $entrevistaArray['resumen'] : '' ?></textarea>

        <div class="text-right"><span id="caracteres" class="valid-text pt-3" id="txaCount"></span></div>
        <br>

    </div>

</div>
<!--//Pregunta 2-->

<!--Pregunta 3-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
        <label>
            3.- ¿Cómo surgió su experiencia o iniciativa?
        </label>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <textarea class="form-control" rows="3" name="surgimiento"
                                      id="surgimiento"><?php echo $surgimiento = (!empty($entrevistaArray['surgimiento'])) ? $entrevistaArray['surgimiento'] : '' ?></textarea>
        <div class="text-right"><span id="car-sur" class="valid-text pt-3" id="txaCount"></span></div>
        <br>

    </div>

</div>
<!--//Pregunta 3-->

<!--Pregunta 4-->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
        <label>
            4.- Contexto histórico en que surge
        </label>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="historico"
                                      id="historico"><?php echo $his = (!empty($entrevistaArray['his'])) ? $entrevistaArray['his'] : '' ?></textarea>
        <div class="text-right"><span id="car-his" class="valid-text pt-3" id="txaCount"></span></div>

        <br>

    </div>

</div>
<!--//Pregunta 4-->

<!--Pregunta 5-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
        <label>
            5.- Principales actores involucrados
        </label>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="actor"
                                      id="actor"><?php echo $actor = (!empty($entrevistaArray['actor'])) ? $entrevistaArray['actor'] : '' ?></textarea>
        <div class="text-right"><span id="car-ac" class="valid-text pt-3" id="txaCount"></span></div>

        <br>

    </div>

</div>
<!--//Pregunta 5-->

<!--Pregunta 6-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
        <label>
            6.- Propósitos más importantes
        </label>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="proposito"
                                      id="proposito"><?php echo $proposito = (!empty($entrevistaArray['proposito'])) ? $entrevistaArray['proposito'] : '' ?></textarea>
        <div class="text-right"><span id="car-p" class="valid-text pt-3" id="txaCount"></span></div>

        <br>

    </div>

</div>
<!--//Pregunta 6-->

<!--Pregunta 7-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
        <label>
            7.- Problemas a los que se ha enfretado su inciativa o experiencia agroecológica
        </label>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="problemas"
                                      id="problemas"><?php echo $probremas = (!empty($entrevistaArray['problemas'])) ? $entrevistaArray['problemas'] : '' ?></textarea>
        <div class="text-right"><span id="car-pr" class="valid-text pt-3" id="txaCount"></span></div>

        <br>

    </div>

</div>
<!--//Pregunta 7-->

<!--Pregunta 8-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
        <label>
            8.- Soluciones encontradas
        </label>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="soluciones"
                                      id="soluciones"><?php echo $soluciones = (!empty($entrevistaArray['solucion'])) ? $entrevistaArray['solucion'] : '' ?></textarea>
        <div class="text-right"><span id="car-sol" class="valid-text pt-3" id="txaCount"></span></div>

        <br>

    </div>

</div>
<!--//Pregunta 8-->

<!--Pregunta 9-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
        <label>
            9.- Retos futuros
        </label>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="retos"
                                      id="retos"><?php echo $reto = (!empty($entrevistaArray['reto'])) ? $entrevistaArray['reto'] : '' ?></textarea>
        <div class="text-right"><span id="car-retos" class="valid-text pt-3" id="txaCount"></span></div>

        <br>

    </div>

</div>
<!--//Pregunta 9-->

<!--Pregunta 10-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
        <label>
            10.- Si se ha recibido financiamiento, indicar de que fuente
        </label>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="financiamiento"
                                      id="financiamiento"><?php echo $financiamiento = (!empty($entrevistaArray['finaciamiento'])) ? $entrevistaArray['finaciamiento'] : '' ?></textarea>
        <div class="text-right"><span id="car-fin" class="valid-text pt-3" id="txaCount"></span></div>

        <br>

    </div>

</div>
<!--//Pregunta 10-->

<!--Pregunta 11-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label>
            11.- Año en que iniciaron actividades
        </label>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <input type="date" name="fechaAtlas"
            <?php echo $fecha = (!empty($entrevistaArray['fecha'])) ? ''  : 'id="fechaAtlas"' ?>
               value="<?php echo $fecha = (!empty($entrevistaArray['fecha'])) ? $entrevistaArray['fecha'] : '' ?>">
    </div>
</div>
<!--//Pregunta 11-->

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
    <br>
    <label>
        En donde se encuentra ubicada la iniciativa:
    </label>

</div>

<!--Estado-->
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-justify">
    <br>
    <label>
        Estado:
    </label>
    <input type="text" class="form-control" name="estado" id="estados" value="<?php echo$grupo = (!empty($entrevistaArray['estados']) )? $entrevistaArray['estados']:'' ?>">


</div>
<!--/Estado-->

<!--Estado-->
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-justify">
    <br>
    <label>
        Municipio:
    </label>
    <input type="text" class="form-control" name="municipio" id="municipio" value="<?php echo$grupo = (!empty($entrevistaArray['municipio']) )? $entrevistaArray['municipio']:'' ?>">


</div>
<!--/Estado-->

<!--Pregunta 12-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label>
            <br>
            12.- Ubique en el mapa su experiencia agroecológica, arrastrando y acercando el marcador <img
                src=" img/marcador.png" style="

                                                    max-width: 100%;
                                                    width: 2.5%;
                                                    height: auto;
                                                    position: relative;
                                                    right: 3px;">
            en el sitio que corresponda
            <br>
        </label>
        <label>
        </label>
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" onload="initialize()" id="error">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tam" id="map_canvas" style="max-width: 100%;
                          position: relative;
                          overflow: hidden;
                          height: 500px;
                          width: 100%;">
    </div>
    <input id="txtLat" type="hidden" name="txtLat" style="color:red"
           value="<?php echo $lat = (!empty($entrevistaArray['lat'])) ? $entrevistaArray['lat'] : '' ?>" />
    <input id="txtLng" type="hidden" name="txtLng" style="color:red"
           value="<?php echo $actor = (!empty($entrevistaArray['lon'])) ? $entrevistaArray['lon'] : '' ?>" /><br />
    <br />
    <br />
</div>

<!--pregunta 12-->

<!--Pregunta img-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <br>
        <label>
            13.- Cargue las 3 imágenes representativas de su experiencia agroecológica:
            &nbsp;<i id="g" class="fa fa-question-circle-o tro too">


                                    <span class="tool">
                                        <h style="line-height: 0.2em; font-size: 12px;">
                                            1. Deberá cargarlas en formato .png y .jpg
                                        </h>
                                    </span>
                <span class="tool">
                                        <h style="line-height: 0.2em; font-size: 12px;">

                                            1. Deberá cargarlas en formato .png o .jpg<br>
                                            2. El peso de cada imagen no debe exceder de 1 MB.<br>
                                        </h>
                                    </span>

            </i>


        </label>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


        <?php if (!empty($entrevistaArray['urlimg'])) : ?>
            <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert"></button>
                <strong>¡Aviso!</strong> Imagenes ya se encuentran cargadas.
            </div>
        <?php else : ?>
            <a class="btn btn-primary" data-toggle="modal" href='#modal-id2'>Cargar imagenes</a>

            <?php include_once 'modal-img.php'?>

        <?php endif; ?>


    </div>
</div>
<!--//Pregunta img-->

<!--//Pregunta url-->
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <br>
        <label class="">
            14.- Si cuenta con un video de la experiencia agroecólogica ingrese el url:
        </label>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <input type="text" class="form-control" name="url" id="url"
               value="<?php echo $url = (!empty($entrevistaArray['url'])) ? $entrevistaArray['url'] : '' ?>"></input>
        <br>
    </div>
</div>
</div>
<!--//Pregunta url
</form>





</fieldset>


</form>


</fieldset









