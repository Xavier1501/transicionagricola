<?php


include_once '../ConexionBD.php';
include_once '../Modal/MetodosIncuesta.php';

session_start();
$objMetodo = new MetodosIncuesta();



$id = $conexion->real_escape_string((!empty($_POST['idfase4_1'])? $_POST['idfase4_1']: 0));
$genero_1 = $conexion -> real_escape_string($_POST['NMujeres']);
$genero_2 = $conexion -> real_escape_string($_POST['NHombres']);
$genero_3 = $conexion -> real_escape_string($_POST['NOtrosGeneros']);
$genero_4 = $conexion -> real_escape_string($_POST['IntegranteNinos']);
$genero_5 = $conexion-> real_escape_string($_POST['IntegranteAdolecentes']);
$genero_6 =$conexion-> real_escape_string($_POST['IntegranteAdultos']);
$genero_7 =$conexion-> real_escape_string($_POST['IntegrantesAdultosMayores']);
$genero_8 =$conexion-> real_escape_string($_POST['$PorcentajeHombresDirectivos']);
$genero_9 =$conexion-> real_escape_string($_POST['PorcentajeMujeresDirectivos']);
$genero_10 =$conexion-> real_escape_string($_POST['PorcentajeHombresCoodinados']);
$genero_11 =$conexion-> real_escape_string($_POST['PorcentajeMujeresCoordinados']);
$genero_12 =$conexion-> real_escape_string((!empty($_POST['IgualdaSalario'])? $_POST['IgualdaSalario']: 'No'));
$genero_13 =$conexion-> real_escape_string($_POST['IgualdaPorque']);
$genero_14 =$conexion-> real_escape_string($_POST['PoliticaEquidad']);
$genero_15 =$conexion-> real_escape_string((!empty($_POST['DerechoMaternal'])? $_POST['DerechoMaternal']: 'No'));
$genero_16 =$conexion-> real_escape_string($_POST['DerechoMaternalPorQue']);
$genero_17 =$conexion-> real_escape_string($_POST['AcosoSexual']);
$genero_18 =$conexion-> real_escape_string((!empty($_POST['Discriminacion'])? $_POST['Discriminacion']: 'No'));
$genero_19 =$conexion-> real_escape_string($_POST['DiscriminacionPorQue']);
$genero_20 =$conexion-> real_escape_string((!empty($_POST['MadreSoltera'])? $_POST['MadreSoltera']: 'No'));
$genero_21 =$conexion-> real_escape_string((!empty($_POST['MujeresParticipanteProyecto'])? $_POST['MujeresParticipanteProyecto']: 'No'));
$genero_22 =$conexion-> real_escape_string($_POST['MujeresParticipanteProyectoporQue']);
$genero_23 =$conexion-> real_escape_string((!empty($_POST['DependenciasEconomicos'])? $_POST['DependenciasEconomicos']: 'No'));
$genero_24 =$conexion-> real_escape_string((!empty($_POST['EnfermosCuidadosEspeciales'])? $_POST['DependenciasEconomicos']: 'No'));
$genero_25 =$conexion-> real_escape_string($_POST['EnfermosCuidadosEspeciapesPorQue']);

//$_POST['EnfermosCuidadosEspeciales']
$data = array(
    'id' => $id,
    '1' => $genero_1,
    '2' => $genero_2,
    '3' => $genero_3,
    '4' => $genero_4,
    '5' => $genero_5,
    '6' => $genero_6,
    '7' => $genero_7,
    '8' => $genero_8,
    '9' => $genero_9,
    '10' => $genero_10,
    '11' => $genero_11,
    '12' => $genero_12,
    '13' => $genero_13,
    '14' => $genero_14,
    '15' => $genero_15,
    '16' => $genero_16,
    '17' => $genero_17,
    '18' => $genero_18,
    '19' => $genero_19,
    '20' => $genero_20,
    '21' => $genero_21,
    '22' => $genero_22,
    '23' => $genero_23,
    '24' => $genero_24,
    '25' => $genero_25,

);

//var_dump($data);

echo $objMetodo-> fase4_1($data);