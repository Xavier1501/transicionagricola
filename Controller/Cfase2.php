<?php

include_once '../ConexionBD.php';
include_once '../Modal/MetodosIncuesta.php';

session_start();


/*Agricultura Urbana*/
$idAgricultura = $conexion->real_escape_string((!empty($_POST['idAgrArb'])? $_POST['idAgrArb']: 0));
$urbanFarming = $conexion->real_escape_string(isset($_POST['AgriUr1'])? '1':'0');
$urbanFarming2 = $conexion->real_escape_string(isset($_POST['AgriUr2'])? '1':'0');
$urbanFarming3 = $conexion->real_escape_string(isset($_POST['AgriUr3'])? '1':'0');
$urbanFarming4 = $conexion->real_escape_string(isset($_POST['AgriUr4'])? '1':'0');
$urbanFarming5 = $conexion->real_escape_string(isset($_POST['AgriUr5'])? '1':'0');

/*Agricultura urban -> huertos*/
$urbanFarmingHuerto_1 = $conexion->real_escape_string(isset($_POST['AgroUrbaHuerto1'])? '1':'0');
$urbanFarmingHuerto_2 = $conexion->real_escape_string(isset($_POST['AgroUrbaHuerto2'])? '1':'0');
$urbanFarmingHuerto_3 = $conexion->real_escape_string(isset($_POST['AgroUrbaHuerto3'])? '1':'0');
$urbanFarmingHuerto_4 = $conexion->real_escape_string(isset($_POST['AgroUrbaHuerto4'])? '1':'0');
$urbanFarmingHuerto_5 = $conexion->real_escape_string($_POST['AgroUrbaHuerto5']);


/*Salud y plantas medicinales*/
$idSaludPlantasMedicinales = $conexion->real_escape_string((!empty($_POST['idSaludPlantasMedicinales'])? $_POST['idSaludPlantasMedicinales']: 0));
$health_1 = $conexion->real_escape_string(isset($_POST['salud1'])? '1':'0');
$health_2 = $conexion->real_escape_string(isset($_POST['salud2'])? '1':'0');
$health_3 = $conexion->real_escape_string(isset($_POST['salud3'])? '1':'0');
$health_4 = $conexion->real_escape_string(isset($_POST['salud4'])? '1':'0');
$health_5 = $conexion->real_escape_string(isset($_POST['salud5'])? '1':'0');
$health_6 = $conexion->real_escape_string($_POST['salud6']);

/*Actividades no agrícolas*/

$idActividadNoAgricola = $conexion->real_escape_string((!empty($_POST['idActividadNoAgricola'])? $_POST['idActividadNoAgricola']: 0));
$non_agricultural_1 = $conexion->real_escape_string(isset($_POST['ActividadNoAgro1'])? '1':'0');
$non_agricultural_2 = $conexion->real_escape_string(isset($_POST['ActividadNoAgro2'])? '1':'0');
$non_agricultural_3 = $conexion->real_escape_string(isset($_POST['ActividadNoAgro3'])? '1':'0');
$non_agricultural_4 = $conexion->real_escape_string(isset($_POST['ActividadNoAgro4'])? '1':'0');
$non_agricultural_5 = $conexion->real_escape_string(isset($_POST['ActividadNoAgro5'])? '1':'0');
$non_agricultural_6 = $conexion->real_escape_string(isset($_POST['ActividadNoAgro6'])? '1':'0');
$non_agricultural_7 = $conexion->real_escape_string(isset($_POST['ActividadNoAgro7'])? '1':'0');
$non_agricultural_8 = $conexion->real_escape_string(isset($_POST['ActividadNoAgro8'])? '1':'0');
$non_agricultural_9 = $conexion->real_escape_string($_POST['ActividadNoAgro9']);

/*Desarrollo Local*/
$idDesarrolloLocal = $conexion->real_escape_string((!empty($_POST['idDesarrolloLocal'])? $_POST['idDesarrolloLocal']: 0));
$deve_local_1 = $conexion->real_escape_string(isset($_POST['DesarrolloLocal1'])? '1':'0');
$deve_local_2 = $conexion->real_escape_string(isset($_POST['DesarrolloLocal2'])? '1':'0');
$deve_local_3 = $conexion->real_escape_string(isset($_POST['DesarrolloLocal3'])? '1':'0');
$deve_local_4 = $conexion->real_escape_string(isset($_POST['DesarrolloLocal4'])? '1':'0');
$deve_local_5 = $conexion->real_escape_string(isset($_POST['DesarrolloLocal5'])? '1':'0');
$deve_local_6 = $conexion->real_escape_string($_POST['DesarrolloLocal6']);



/*economia*/
$IdEconomia = $conexion->real_escape_string((!empty($_POST['IdEconomia'])? $_POST['IdEconomia']: 0));
$economy_1=$conexion -> real_escape_string(isset($_POST['Economia1'])? '1':'0');
$economy_2 =$conexion -> real_escape_string(isset($_POST['Economia2'])? '1':'0');
$economy_3 =$conexion -> real_escape_string(isset($_POST['Economia3'])? '1':'0');
$economy_4 =$conexion -> real_escape_string(isset($_POST['Economia4'])? '1':'0');
$economy_5 =$conexion -> real_escape_string(isset($_POST['Economia5'])? '1':'0');
$economy_6 =$conexion -> real_escape_string(isset($_POST['Economia6'])? '1':'0');
$economy_7 =$conexion -> real_escape_string(isset($_POST['Economia7'])? '1':'0');
$economy_8 =$conexion -> real_escape_string(isset($_POST['Economia8'])? '1':'0');
$economy_9 =$conexion -> real_escape_string(isset($_POST['Economia9'])? '1':'0');
$economy_10 =$conexion -> real_escape_string(isset($_POST['Economia10'])? '1':'0');
$economy_11 =$conexion -> real_escape_string($_POST['Economia11']);

/*economia->cooperativas*/
$idCooperativa = $conexion->real_escape_string((!empty($_POST['idCooperativa'])? $_POST['idCooperativa']: 0));
$economy_cooperatives_1 = $conexion -> real_escape_string(isset($_POST['Cooperativa1'])? '1':'0');
$economy_cooperatives_2 = $conexion -> real_escape_string(isset($_POST['Cooperativa2'])? '1':'0');
$economy_cooperatives_3 = $conexion -> real_escape_string(isset($_POST['Cooperativa3'])? '1':'0');


/*economia -> cooperativas -> produccion*/
$idProduccion = $conexion->real_escape_string((!empty($_POST['idProduccion'])? $_POST['idProduccion']: 0));
$idCooperativa = $conexion->real_escape_string((!empty($_POST['idCooperativa'])? $_POST['idCooperativa']: 0));
$economy_cooperatives_production_1 = $conexion -> real_escape_string(isset($_POST['producion01'])? '1':'0');
$economy_cooperatives_production_2 = $conexion -> real_escape_string(isset($_POST['producion02'])? '1':'0');

/*Educacion*/
$idEducacion = $conexion->real_escape_string((!empty($_POST['idEducacion'])? $_POST['idEducacion']: 0));
$education_1 = $conexion -> real_escape_string(isset($_POST['Educacion1'])? '1':'0');
$education_2 = $conexion -> real_escape_string(isset($_POST['Educacion2'])? '1':'0');
$education_3 = $conexion -> real_escape_string(isset($_POST['Educacion3'])? '1':'0');
$education_4 = $conexion -> real_escape_string(isset($_POST['Educacion4'])? '1':'0');
$education_5 = $conexion -> real_escape_string(isset($_POST['Educacion5'])? '1':'0');
$education_6 = $conexion -> real_escape_string(isset($_POST['Educacion6'])? '1':'0');
$education_7 = $conexion -> real_escape_string($_POST['Educacion7']);

$education_8 = $conexion -> real_escape_string(isset($_POST['EduFormal1'])? '1':'0');
$education_9 = $conexion -> real_escape_string(isset($_POST['EduFormal2'])? '1':'0');
$education_10 = $conexion -> real_escape_string(isset($_POST['EduFormal3'])? '1':'0');

$idDerehosHumanos = $conexion->real_escape_string((!empty($_POST['idDerehosHumanos'])? $_POST['idDerehosHumanos']: 0));
$DH = $conexion -> real_escape_string(isset($_POST['DerechosH1'])? '1':'0');
$DH2 = $conexion -> real_escape_string(isset($_POST['DerechosHD2'])? '1':'0');
$DH3 = $conexion -> real_escape_string(isset($_POST['DerechosHD3'])? '1':'0');


/* Trabajo con niños / joventud */
$idTrabajoNinoJuventd =$conexion->real_escape_string((!empty($_POST['idTrabajoNinoJuventd'])? $_POST['idTrabajoNinoJuventd']: 0));
$work_children_1 = $conexion -> real_escape_string(isset($_POST['TrabajoNJ1'])? '1':'0');
$work_children_2 = $conexion -> real_escape_string(isset($_POST['TrabajoNJ2'])? '1':'0');
$work_children_3 = $conexion -> real_escape_string($_POST['TrabajoNJ4']);



/* Investigación agrícola y Extensión rural */
$investigation_1 = $conexion -> real_escape_string(isset($_POST['Investigacion1'])? '1':'0');
$investigation_2 = $conexion -> real_escape_string(isset($_POST['Investigacion2'])? '1':'0');
$investigation_3 = $conexion -> real_escape_string(isset($_POST['Investigacion3'])? '1':'0');
$investigation_4 = $conexion -> real_escape_string(isset($_POST['Investigacion4'])? '1':'0');
$investigation_5 = $conexion -> real_escape_string(isset($_POST['Investigacion5'])? '1':'0');
$investigation_6 = $conexion -> real_escape_string(isset($_POST['Investigacion6'])? '1':'0');
$investigation_7 = $conexion -> real_escape_string($_POST['Investigacion7']);
$idInvestigacionAgricolaRural = $conexion->real_escape_string((!empty($_POST['idInvestigacionAgricolaRural'])? $_POST['idInvestigacionAgricolaRural']: 0));

/*  Manejo de recursos hídricos */

$driving_resources_1 = $conexion -> real_escape_string(isset($_POST['ManejoRecursos1'])? '1':'0');
$driving_resources_2 = $conexion -> real_escape_string(isset($_POST['ManejoRecursos2'])? '1':'0');
$driving_resources_3 = $conexion -> real_escape_string(isset($_POST['ManejoRecursos3'])? '1':'0');
$driving_resources_4 = $conexion -> real_escape_string(isset($_POST['ManejoRecursos4'])? '1':'0');
$driving_resources_5 = $conexion -> real_escape_string(isset($_POST['ManejoRecursos5'])? '1':'0');
$driving_resources_6 = $conexion -> real_escape_string(isset($_POST['ManejoRecursos6'])? '1':'0');
$driving_resources_7 = $conexion -> real_escape_string($_POST['ManejoRecursos7']);
$idManejoRecursosHidricos = $conexion->real_escape_string((!empty($_POST['idManejoRecursosHidricos'])? $_POST['idManejoRecursosHidricos']: 0));


/* Manejo poscosecha*/
$driving_harvest_1  = $conexion -> real_escape_string(isset($_POST['ManejoPoscosecha1'])? '1':'0');
$driving_harvest_2  = $conexion -> real_escape_string(isset($_POST['ManejoPoscosecha2'])? '1':'0');
$driving_harvest_3  = $conexion -> real_escape_string(isset($_POST['ManejoPoscosecha3'])? '1':'0');
$driving_harvest_4  = $conexion -> real_escape_string($_POST['ManejoPoscosecha4']);
$idManejoPosCosecha = $conexion->real_escape_string((!empty($_POST['idManejoPosCosecha'])? $_POST['idManejoPosCosecha']: 0));



/*Semillas*/
$idSemillas = $conexion->real_escape_string((!empty($_POST['idSemillas'])? $_POST['idSemillas']: 0));
$seeds_1 = $conexion -> real_escape_string(isset($_POST['Semillas1'])? '1':'0');
$seeds_2 = $conexion -> real_escape_string(isset($_POST['Semillas2'])? '1':'0');
$seeds_3 = $conexion -> real_escape_string(isset($_POST['Semillas3'])? '1':'0');
$seeds_4 = $conexion -> real_escape_string(isset($_POST['Semillas4'])? '1':'0');
$seeds_5 = $conexion -> real_escape_string(isset($_POST['Semillas5'])? '1':'0');
$seeds_6 = $conexion -> real_escape_string($_POST['Semillas6']);

/*Sistemas agroforestales y Arborizacion*/
$idSistemasAgroforestales = $conexion->real_escape_string((!empty($_POST['idSistemasAgroforestales'])? $_POST['idSistemasAgroforestales']: 0));
$system_agroforestry_1 = $conexion -> real_escape_string(isset($_POST['sistemaAgro1'])? '1':'0');
$system_agroforestry_2 = $conexion -> real_escape_string(isset($_POST['sistemaAgro2'])? '1':'0');
$system_agroforestry_3 = $conexion -> real_escape_string($_POST['sistemaAgro3']);
$system_agroforestry_4 = $conexion -> real_escape_string(isset($_POST['arbo1'])? '1':'0');
$system_agroforestry_5 = $conexion -> real_escape_string(isset($_POST['arbo2'])? '1':'0');
$system_agroforestry_6 = $conexion -> real_escape_string(isset($_POST['arbo3'])? '1':'0');

/*Productos forestales*/
$proucts_forestry_1 = $conexion ->real_escape_string(isset($_POST['prof1'])? '1':'0');
$proucts_forestry_2 = $conexion ->real_escape_string(isset($_POST['prof2'])? '1':'0');
$proucts_forestry_3 = $conexion ->real_escape_string($_POST['prof3']);
$idProductosForestales = $conexion->real_escape_string((!empty($_POST['idProductosForestales'])? $_POST['idProductosForestales']: 0));


/*  Sistemas de producción animal */
$system_production_animal_1 = $conexion ->real_escape_string(isset($_POST['sisProAn1'])? '1':'0');
$system_production_animal_2 = $conexion ->real_escape_string(isset($_POST['sisProAn2'])? '1':'0');
$system_production_animal_3 = $conexion ->real_escape_string(isset($_POST['sisProAn3'])? '1':'0');
$system_production_animal_4 = $conexion ->real_escape_string(isset($_POST['sisProAn4'])? '1':'0');
$system_production_animal_5 = $conexion ->real_escape_string(isset($_POST['sisProAn5'])? '1':'0');
$system_production_animal_6 = $conexion ->real_escape_string(isset($_POST['sisProAn6'])? '1':'0');
$system_production_animal_7 = $conexion ->real_escape_string(isset($_POST['sisProAn7'])? '1':'0');
$system_production_animal_8 = $conexion ->real_escape_string(isset($_POST['sisProAn8'])? '1':'0');
$system_production_animal_9 = $conexion ->real_escape_string(isset($_POST['sisProAn9'])? '1':'0');
$system_production_animal_10 = $conexion ->real_escape_string(isset($_POST['sisProAn10'])? '1':'0');
$system_production_animal_11 = $conexion ->real_escape_string(isset($_POST['sisProAn11'])? '1':'0');
$system_production_animal_12 = $conexion ->real_escape_string(isset($_POST['sisProAn12'])? '1':'0');
$system_production_animal_13 = $conexion ->real_escape_string(isset($_POST['sisProAn13'])? '1':'0');
$system_production_animal_14 = $conexion ->real_escape_string(isset($_POST['sisProAn14'])? '1':'0');
$system_production_animal_15 = $conexion ->real_escape_string(isset($_POST['sisProAn15'])? '1':'0');
$system_production_animal_16 = $conexion ->real_escape_string(isset($_POST['sisProAn16'])? '1':'0');
$system_production_animal_17 = $conexion ->real_escape_string(isset($_POST['sisProAn17'])? '1':'0');
$system_production_animal_18 = $conexion ->real_escape_string($_POST['sisProAn18']);
$system_production_animal_19 = $conexion ->real_escape_string(isset($_POST['sisProAn19'])? '1':'0');
$system_production_animal_20 = $conexion ->real_escape_string(isset($_POST['sisProAn20'])? '1':'0');
$idSistemaProduccionAnimal = $conexion->real_escape_string((!empty($_POST['idSistemaProduccionAnimal'])? $_POST['idSistemaProduccionAnimal']: 0));

/*Sistemas de producción agrícola */
$idSistemaProduccionAgricola = $conexion->real_escape_string((!empty($_POST['idSistemaProduccionAgricola'])? $_POST['idSistemaProduccionAgricola']: 0));
$system_agricola_1 = $conexion ->real_escape_string(isset($_POST['estrategia1'])? '1':'0');
$system_agricola_2 = $conexion ->real_escape_string(isset($_POST['estrategia2'])? '1':'0');
$system_agricola_3 = $conexion ->real_escape_string(isset($_POST['estrategia3'])? '1':'0');
$system_agricola_4 = $conexion ->real_escape_string(isset($_POST['estrategia4'])? '1':'0');
$system_agricola_5 = $conexion ->real_escape_string(isset($_POST['estrategia5'])? '1':'0');
$system_agricola_6 = $conexion ->real_escape_string(isset($_POST['estrategia6'])? '1':'0');
$system_agricola_7 = $conexion ->real_escape_string(isset($_POST['estrategia7'])? '1':'0');
$system_agricola_8 = $conexion ->real_escape_string(isset($_POST['estrategia8'])? '1':'0');
$system_agricola_9 = $conexion ->real_escape_string(isset($_POST['estrategia9'])? '1':'0');
$system_agricola_10 = $conexion ->real_escape_string(isset($_POST['estrategia10'])? '1':'0');
$system_agricola_11 = $conexion ->real_escape_string(isset($_POST['estrategia11'])? '1':'0');
$system_agricola_12 = $conexion ->real_escape_string(isset($_POST['estrategia12'])? '1':'0');
$system_agricola_13 = $conexion ->real_escape_string(isset($_POST['estrategia13'])? '1':'0');
$system_agricola_14 = $conexion ->real_escape_string(isset($_POST['estrategia14'])? '1':'0');
$system_agricola_15 = $conexion ->real_escape_string(isset($_POST['estrategia15'])? '1':'0');
$system_agricola_16 = $conexion ->real_escape_string(isset($_POST['estrategia16'])? '1':'0');
$system_agricola_17 = $conexion ->real_escape_string(isset($_POST['estrategia17'])? '1':'0');
$system_agricola_18 = $conexion ->real_escape_string(isset($_POST['estrategia18'])? '1':'0');
$system_agricola_19 = $conexion ->real_escape_string(isset($_POST['estrategia19'])? '1':'0');
$system_agricola_20 = $conexion ->real_escape_string(isset($_POST['estrategia20'])? '1':'0');
$system_agricola_21 = $conexion ->real_escape_string(isset($_POST['estrategia21'])? '1':'0');
$system_agricola_22 = $conexion ->real_escape_string(isset($_POST['estrategia22'])? '1':'0');
$system_agricola_23 = $conexion ->real_escape_string(isset($_POST['estrategia23'])? '1':'0');
$system_agricola_24 = $conexion ->real_escape_string(isset($_POST['estrategia24'])? '1':'0');
$system_agricola_25 = $conexion ->real_escape_string(isset($_POST['estrategia25'])? '1':'0');
$system_agricola_26 = $conexion ->real_escape_string(isset($_POST['estrategia26'])? '1':'0');
$system_agricola_27 = $conexion ->real_escape_string(isset($_POST['estrategia27'])? '1':'0');
$system_agricola_28 = $conexion ->real_escape_string(isset($_POST['estrategia28'])? '1':'0');
$system_agricola_29 = $conexion ->real_escape_string(isset($_POST['estrategia29'])? '1':'0');
$system_agricola_30 = $conexion ->real_escape_string(isset($_POST['estrategia30'])? '1':'0');
$system_agricola_31 = $conexion ->real_escape_string(isset($_POST['estrategia31'])? '1':'0');
$system_agricola_32 = $conexion ->real_escape_string($_POST['estrategia32']);


$user = $_SESSION['Usuario'];

//var_dump('idi Avtividad agricola'.$idActividadNoAgricola);
$data = array(
    'usuario'=> $user,
    'idAgrArb'=>$idAgricultura,
    'Azotea' =>$urbanFarming,
    'Balcon' =>$urbanFarming2,
    'Traspatio' => $urbanFarming3,
    'Parques' => $urbanFarming4,
    'Huertos' => array(
        'Hcomunitarios' =>$urbanFarmingHuerto_1,
        'HSociales' => $urbanFarmingHuerto_2,
        'HInstitucionales' => $urbanFarmingHuerto_3,
        'Hempresas' => $urbanFarmingHuerto_4,
        'HOtros' =>$urbanFarmingHuerto_5
    ),
    'health' => array(
        'idSaludPlantasMedicinales' => $idSaludPlantasMedicinales,
        '1' => $health_1 ,
        '2' => $health_2 ,
        '3' => $health_3 ,
        '4' => $health_4 ,
        '5' => $health_5,
        '6' => $health_6
    ),

    'non_agricultural' => array(
        'id'=> $idActividadNoAgricola,
        '1' => $non_agricultural_1 ,
        '2' => $non_agricultural_2 ,
        '3' => $non_agricultural_3 ,
        '4' => $non_agricultural_4 ,
        '5' => $non_agricultural_5,
        '6' => $non_agricultural_6,
        '7' => $non_agricultural_7,
        '8' => $non_agricultural_8,
        '9' => $non_agricultural_9
    ),
    'localDevelopment' => array(
        'id' =>$idDesarrolloLocal,
        '1' => $deve_local_1,
        '2' => $deve_local_2,
        '3' => $deve_local_3,
        '4' => $deve_local_4,
        '5' => $deve_local_5,
        '6' => $deve_local_6

    ),

    'Economy' => array(
        '1' => $economy_1 ,
        '2' => $economy_2 ,
        '3' => $economy_3 ,
        '4' => $economy_4 ,
        '5' => $economy_5 ,
        '6' => $economy_6 ,
        '7' => $economy_7 ,
        '8' => $economy_8 ,
        '9' => $economy_9 ,
        '10' => $economy_10,
        '11' => $economy_11
    ),

    'economyCooperatives' => array(
        '1' => $economy_cooperatives_1,
        '2' => $economy_cooperatives_2,
        '3' => $economy_cooperatives_3
    ),

    'economyProduction' => array(
        '1' => $economy_cooperatives_production_1,
        '2' => $economy_cooperatives_production_2
    ),

    'education' => array(
        '1' => $education_1,
        '2' => $education_2,
        '3' => $education_3,
        '4' => $education_4,
        '5' => $education_5,
        '6' => $education_6,
        '7' => $education_7,
        '8' => $education_8,
        '9' => $education_9,
        '10' => $education_10
    ),

    'DH' => array(
        '1' => $DH,
        '2' => $DH2,
        '3' => $DH
    ),
    'wordChildren'=> array(
        '1' =>$work_children_1,
        '2' =>$work_children_2,
        '3' =>$work_children_3,
    ),

    'investigation' => array(
        '1' => $investigation_1,
        '2' => $investigation_2,
        '3' => $investigation_3,
        '4' => $investigation_4,
        '5' => $investigation_5,
        '6' => $investigation_6,
        '7' => $investigation_7
    ),

    'drivingResources' => array(
        '1' => $driving_resources_1,
        '2' => $driving_resources_2,
        '3' => $driving_resources_3,
        '4' => $driving_resources_4,
        '5' => $driving_resources_5,
        '6' => $driving_resources_6,
        '7' => $driving_resources_7
    ),

    'drivingHarvest' => array(
        '1' => $driving_harvest_1,
        '2' => $driving_harvest_2,
        '3' => $driving_harvest_3,
        '4' => $driving_harvest_4
    ),

    'seeds' =>array(
        '1' => $seeds_1 ,
        '2' => $seeds_2 ,
        '3' => $seeds_3 ,
        '4' => $seeds_4 ,
        '5' => $seeds_5 ,
        '6' => $seeds_6

    ),

    'systemAgroforestry' => array(
        '1' => $system_agroforestry_1,
        '2' => $system_agroforestry_2,
        '3' => $system_agroforestry_6,
        '4' => $system_agroforestry_4,
        '5' => $system_agroforestry_5,
        '6' => $system_agroforestry_3
    ),

    'productsForestry' => array(
        '1' => $proucts_forestry_1,
        '2' => $proucts_forestry_2,
        '3' => $proucts_forestry_3,
        '3' => $proucts_forestry_3
    ),

    'systemProductionAnimal' => array(
        '1' => $system_production_animal_1,
        '2' => $system_production_animal_2,
        '3' => $system_production_animal_3,
        '4' => $system_production_animal_4,
        '5' => $system_production_animal_5,
        '6' => $system_production_animal_6,
        '7' => $system_production_animal_7,
        '8' => $system_production_animal_8,
        '9' => $system_production_animal_9,
        '10' => $system_production_animal_10,
        '11' => $system_production_animal_11,
        '12' => $system_production_animal_12,
        '13' => $system_production_animal_13,
        '14' => $system_production_animal_14,
        '15' => $system_production_animal_15,
        '16' => $system_production_animal_16,
        '17' => $system_production_animal_17,
        '18' => $system_production_animal_18,
        '19' => $system_production_animal_19,
        '20' => $system_production_animal_20,
    ),

    'systemAgricola' => array(

        '1'=>$system_agricola_1,
        '2'=>$system_agricola_2,
        '3'=>$system_agricola_3,
        '4'=>$system_agricola_4,
        '5'=>$system_agricola_5,
        '6'=>$system_agricola_6,
        '7'=>$system_agricola_7,
        '8'=>$system_agricola_8,
        '9'=>$system_agricola_9,
        '10'=>$system_agricola_10,
        '11'=>$system_agricola_11,
        '12'=>$system_agricola_12,
        '13'=>$system_agricola_13,
        '14'=>$system_agricola_14,
        '15'=>$system_agricola_15,
        '16'=>$system_agricola_16,
        '17'=>$system_agricola_17,
        '18'=>$system_agricola_18,
        '19'=>$system_agricola_19,
        '20'=>$system_agricola_20,
        '21'=>$system_agricola_21,
        '22'=>$system_agricola_22,
        '23'=>$system_agricola_23,
        '24'=>$system_agricola_24,
        '25'=>$system_agricola_25,
        '26'=>$system_agricola_26,
        '27'=>$system_agricola_27,
        '28'=>$system_agricola_28,
        '29'=>$system_agricola_29,
        '30'=>$system_agricola_30,
        '31'=>$system_agricola_31,
        '32'=>$system_agricola_32

    ),

    'ids' => array(
        'idAgrArb'=>$idAgricultura,
        'idSaludPlantasMedicinales' => $idSaludPlantasMedicinales,
        'idActividadNoAgricola' => $idActividadNoAgricola,
        'idDesarrolloLocal' =>$idDesarrolloLocal,
        'IdEconomia' => $IdEconomia,
        'idProduccion' => $idProduccion,
        'idCooperativa' =>$idCooperativa,
        'idEducacion' => $idEducacion,
        'idDerehosHumanos' => $idDerehosHumanos,
        'idTrabajoNinoJuventd' => $idTrabajoNinoJuventd,
        'idInvestigacionAgricolaRural' => $idInvestigacionAgricolaRural,
        'idManejoRecursosHidricos' => $idManejoRecursosHidricos,
        'idManejoPosCosecha' => $idManejoPosCosecha,
        'idSemillas'=>$idSemillas,
        'idSistemasAgroforestales' => $idSistemasAgroforestales,
        'idProductosForestales' => $idProductosForestales,
        'idSistemaProduccionAnimal' => $idSistemaProduccionAnimal,
        'idSistemaProduccionAgricola' => $idSistemaProduccionAgricola
    )


);







$obj = new MetodosIncuesta();
$insert= $obj->Fase2($data);






//var_dump($insert);


