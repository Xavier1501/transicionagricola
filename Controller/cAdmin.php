<?php

session_start();
include_once '../ConexionBD.php';
include_once '../Modal/mAdmin.php';
$objMetodo = new mAdmin();

$grupo = $_REQUEST['grupo'];
$iniciativa = $_REQUEST['iniciativa'];
$correo = $_REQUEST['correo'];
$resumen = $_REQUEST['resumen'];
$idiniciativa = $_REQUEST['idiniciativa'];
$identrevista = $_REQUEST['identrevista'];
$estado = $_REQUEST['estado'];
$municipio = $_REQUEST['municipio'];
$atlas_iniciatives_Lat = $conexion -> real_escape_string($_POST['txtLat']);
$atlas_iniciatives_Lng = $conexion -> real_escape_string($_POST['txtLng']);

$data = array(
    'idEn' => $identrevista,
    'idIn' => $idiniciativa,
    'nameGroup' => $grupo,
    'email' => $correo,
    'resumen' => $resumen,
    'iniciativa' => $iniciativa,
    'estado' => $estado,
    'municipio' => $municipio,
    'lat' => $atlas_iniciatives_Lat,
    'lng' => $atlas_iniciatives_Lng

);

$editarEn = $objMetodo->adminEditarEntrevista($data);
$editarIn = $objMetodo->adminEditarIniciativa($data);


if ($editarIn && $editarEn){
    echo 1;
}
