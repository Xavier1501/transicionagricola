<?php

include_once '../ConexionBD.php';
include_once '../Modal/MetodosIncuesta.php';

session_start();

/*Nucleo Agrario*/
$idNucleoAgrario = $conexion->real_escape_string((!empty($_POST['idNucleoAgrario'])? $_POST['idNucleoAgrario']: 0));
$nucAgri1 = $conexion->real_escape_string(isset($_POST['nucAgri1'])? '1':'0');
$nucAgri2 = $conexion->real_escape_string(isset($_POST['nucAgri2'])? '1':'0');

/*Fundación*/
$idFundacion = $conexion->real_escape_string((!empty($_POST['idFundacion'])? $_POST['idFundacion']: 0));
$fundacion1 = $conexion->real_escape_string(isset($_POST['fundacion1'])? '1':'0');
$fundacion2 = $conexion->real_escape_string(isset($_POST['fundacion2'])? '1':'0');

/*Gobierno*/
$idGobierno = $conexion->real_escape_string((!empty($_POST['idGobierno'])? $_POST['idGobierno']: 0));
$gobierno1 = $conexion->real_escape_string(isset($_POST['gobierno1'])? '1':'0');
$gobierno2 = $conexion->real_escape_string(isset($_POST['gobierno2'])? '1':'0');
$gobierno3 = $conexion->real_escape_string(isset($_POST['gobierno3'])? '1':'0');

/*Instituciones Educativas*/
$idInstitucionEducativa = $conexion->real_escape_string((!empty($_POST['idInstitucionEducativa'])? $_POST['idInstitucionEducativa']: 0));
$intEduc1 = $conexion->real_escape_string(isset($_POST['intEduc1'])? '1':'0');
$intEduc2 = $conexion->real_escape_string(isset($_POST['intEduc2'])? '1':'0');
$intEduc3 = $conexion->real_escape_string(isset($_POST['intEduc3'])? '1':'0');
$intEduc4 = $conexion->real_escape_string(isset($_POST['intEduc4'])? '1':'0');
$intEduc5 = $conexion->real_escape_string(isset($_POST['intEduc5'])? '1':'0');
$intEduc6 = $conexion->real_escape_string(isset($_POST['intEduc6'])? '1':'0');
$intEduc7 = $conexion->real_escape_string(isset($_POST['intEduc7'])? '1':'0');
$intEduc8 = $conexion->real_escape_string(isset($_POST['intEduc8'])? '1':'0');

/*Tipo Organización*/
$idTipoOrganizacion = $conexion->real_escape_string((!empty($_POST['idTipoOrganizacion'])? $_POST['idTipoOrganizacion']: 0));
$TipoOrga1 = $conexion->real_escape_string(isset($_POST['TipoOrga1'])? '1':'0');
$TipoOrga2 = $conexion->real_escape_string(isset($_POST['TipoOrga2'])? '1':'0');
$TipoOrga3 = $conexion->real_escape_string($_POST['TipoOrga3']);

/*Cooperativa*/
$idCooperativaColectivo = $conexion->real_escape_string((!empty($_POST['idCooperativaColectivo'])? $_POST['idCooperativaColectivo']: 0));
$coop1 = $conexion->real_escape_string(isset($_POST['coop1'])? '1':'0');
$coop2 = $conexion->real_escape_string(isset($_POST['coop2'])? '1':'0');
$coop3 = $conexion->real_escape_string($_POST['coop3']);

/*Tipo Empresa*/
$idEmpresaColectivo = $conexion->real_escape_string((!empty($_POST['idEmpresaColectivo'])? $_POST['idEmpresaColectivo']: 0));
$empSoc1 = $conexion->real_escape_string(isset($_POST['empSoc1'])? '1':'0');
$empSoc2 = $conexion->real_escape_string(isset($_POST['empSoc2'])? '1':'0');

/*Grupos Organizados*/
$idGruopoOrganizado = $conexion->real_escape_string((!empty($_POST['idGruopoOrganizado'])? $_POST['idGruopoOrganizado']: 0));
$grupOrga1 = $conexion->real_escape_string(isset($_POST['grupOrga1'])? '1':'0');
$grupOrga2 = $conexion->real_escape_string(isset($_POST['grupOrga2'])? '1':'0');
$grupOrga3 = $conexion->real_escape_string(isset($_POST['grupOrga3'])? '1':'0');
$grupOrga4 = $conexion->real_escape_string(isset($_POST['grupOrga4'])? '1':'0');
$grupOrga5 = $conexion->real_escape_string(isset($_POST['grupOrga5'])? '1':'0');
$grupOrga6 = $conexion->real_escape_string($_POST['grupOrga6']);


/*Programa de Extensión*/
$idTipoColectivoOrganizacion = $conexion->real_escape_string((!empty($_POST['idTipoColectivoOrganizacion'])? $_POST['idTipoColectivoOrganizacion']: 0));
$progExt1 = $conexion->real_escape_string(isset($_POST['progExt1'])? '1':'0');
/*Otro*/
$otro_Colec1 = $conexion->real_escape_string($_POST['otro_Colec1']);

$data = array(

    'idTipoColectivoOrganizacion'=>$idTipoColectivoOrganizacion,
    '$progExt1'=>$progExt1,
    'otro_Colec1'=>$otro_Colec1,

    'nucleoAgrario' => array(
        'idNucleoAgrario' => $idNucleoAgrario,
        'nucAgri1' => $nucAgri1 ,
        'nucAgri2' => $nucAgri2
    ),

    'idFundacion' => array(
        'idFundacion'=> $idFundacion,
        'fundacion1' => $fundacion1 ,
        'fundacion2' => $fundacion2
    ),

    'Gobierno' => array(
        'idGobierno' =>$idGobierno,
        'gobierno1' => $gobierno1,
        'gobierno2' => $gobierno2,
        'gobierno3' => $gobierno3

    ),

    'InstitucionEdicativa' => array(
        'idInstitucionEdicativa' => $idInstitucionEducativa ,
        'intEduc1' => $intEduc1 ,
        'intEduc2' => $intEduc2 ,
        'intEduc3' => $intEduc3 ,
        'intEduc4' => $intEduc4 ,
        'intEduc5' => $intEduc5 ,
        'intEduc6' => $intEduc6 ,
        'intEduc7' => $intEduc7 ,
        'intEduc8' => $intEduc8

    ),

    'TipoOrganizacion' => array(
        'idTipoOrganizacion' => $idTipoOrganizacion ,
        'TipoOrga1' => $TipoOrga1 ,
        'TipoOrga2' => $TipoOrga2 ,
        'TipoOrga3' => $TipoOrga3
    ),
    'Cooperativa' => array(
        'idCooperativa' => $idCooperativaColectivo ,
        'coop1' => $coop1 ,
        'coop2' => $coop2 ,
        'coop3' => $coop3
    ),

    'TipoEmpresa' => array(
        'idTipoEmpresa' => $idEmpresaColectivo ,
        'empSoc1' => $empSoc1 ,
        'empSoc2' => $empSoc2
    ),

    'GruposOrganizados' => array(
        'idTipoOrganizados' => $idGruopoOrganizado ,
        'grupOrga1' => $grupOrga1 ,
        'grupOrga2' => $grupOrga2 ,
        'grupOrga3' => $grupOrga3 ,
        'grupOrga4' => $grupOrga4 ,
        'grupOrga5' => $grupOrga5 ,
        'grupOrga6' => $grupOrga6
    ),



    'ids' => array(
        'idNucleoAgrario' => $idNucleoAgrario,
        'idFundacion'=> $idFundacion,
        'idGobierno' =>$idGobierno,
        'idInstitucionEducativa' => $idInstitucionEducativa,
        'idTipoOrganizacion' => $idTipoOrganizacion,
        'idCooperativaColectivo' => $idCooperativaColectivo,
        'idTipoEmpresa' => $idEmpresaColectivo ,
        'idGruopoOrganizado' => $idGruopoOrganizado


    ),

);

//var_dump($data);


$obj = new MetodosIncuesta();

$insert= $obj->Fase3($data);

