<?php


include_once '../ConexionBD.php';
include_once '../Modal/MetodosIncuesta.php';
session_start();
$objMetodo = new MetodosIncuesta();



/*Formacion*/
$id = $conexion->real_escape_string((!empty($_POST['idfase7'])? $_POST['idfase7']: 0));
$training_1 = $conexion->real_escape_string($_POST['formacion1']);
$training_2 = $conexion->real_escape_string($_POST['formacion2']);
$training_3 = $conexion->real_escape_string(!isset($_POST['formacion3'])?'NO':$_POST['formacion3']);
$training_5 = $conexion->real_escape_string($_POST['formacion5']);
$training_6 = $conexion->real_escape_string(!isset($_POST['formacion6'])? 'NO':$_POST['formacion6']);
$training_8 = $conexion->real_escape_string($_POST['formacion8']);
$training_9 = $conexion->real_escape_string($_POST['formacion9']);
$training_10 = $conexion->real_escape_string($_POST['formacion10']);
$training_11 = $conexion->real_escape_string($_POST['formacion11']);

$data = array(
    'id' => $id,
    'ApoyoColectivo' => $training_1,
    'Fortaleza' => $training_2,
    'si_Programas' => $training_3,
    'cuales_programas' => $training_5,
    'si_taller' => $training_6,
    'Cuales_taller' => $training_8,
    'PropuestaEductiva' =>$training_9,
    'Capacitacion' => $training_10,
    'TemasApoyo' => $training_11
);


//var_dump($data);

echo $objMetodo -> fase7($data);
