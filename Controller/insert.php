<?php

//error_reporting(0);
include_once '../ConexionBD.php';
include_once '../Modal/MetodosIncuesta.php';

$objMetodo = new MetodosIncuesta();



$nameGroup = $conexion->real_escape_string($_POST['name']);
$nameRepresentative = $conexion->real_escape_string($_POST['representante']);
$email = $conexion->real_escape_string($_POST['correo']);
$urlSocial = $conexion->real_escape_string($_POST['urlSocial']);
$state = $conexion->real_escape_string($_POST['cbx_estado']);
$municipality = $conexion->real_escape_string($_POST['cbx_municipio']);
$address = $conexion ->real_escape_string($_POST['direccion']);
$cp = $conexion ->real_escape_string($_POST['cp']);

/*Traemos los check */






/*Agricultura Urbana*/
$urbanFarming = $conexion->real_escape_string(isset($_POST['AgriUr1'])? '1':'0');




//var_dump('Agricultura Urbana:'. $urbanFarming);


/*Salud y plantas medicinales*/
$health_1 = $conexion->real_escape_string(isset($_POST['salud1'])? '1':'0');
$health_2 = $conexion->real_escape_string(isset($_POST['salud2'])? '1':'0');
$health_3 = $conexion->real_escape_string(isset($_POST['salud3'])? '1':'0');
$health_4 = $conexion->real_escape_string(isset($_POST['salud4'])? '1':'0');
$health_5 = $conexion->real_escape_string($_POST['salud5']);


//var_dump('salud:'. $health_2);


/*Actividades no agrícolas*/
$non_agricultural_1 = $conexion->real_escape_string(isset($_POST['ActividadNoAgro1'])? '1':'0');
$non_agricultural_2 = $conexion->real_escape_string(isset($_POST['ActividadNoAgro2'])? '1':'0');
$non_agricultural_3 = $conexion->real_escape_string(isset($_POST['ActividadNoAgro3'])? '1':'0');
$non_agricultural_4 = $conexion->real_escape_string(isset($_POST['ActividadNoAgro4'])? '1':'0');
$non_agricultural_5 = $conexion->real_escape_string($_POST['ActividadNoAgro5']);

/* Derechos Humanos*/
$Human_Right_1 = $conexion->real_escape_string(isset($_POST['DerechosH1'])? '1':'0');

/*Desarrollo Local*/
$deve_local_1 = $conexion->real_escape_string(isset($_POST['DesarrolloLocal1'])? '1':'0');
$deve_local_2 = $conexion->real_escape_string(isset($_POST['DesarrolloLocal2'])? '1':'0');
$deve_local_3 = $conexion->real_escape_string(isset($_POST['DesarrolloLocal3'])? '1':'0');
$deve_local_4 = $conexion->real_escape_string(isset($_POST['DesarrolloLocal4'])? '1':'0');
$deve_local_5 = $conexion->real_escape_string(isset($_POST['DesarrolloLocal5'])? '1':'0');
$deve_local_6 = $conexion->real_escape_string($_POST['DesarrolloLocal6']);


/*economia*/
$economy_1=$conexion -> real_escape_string(isset($_POST['Economia1'])? '1':'0');
$economy_2 =$conexion -> real_escape_string(isset($_POST['Economia2'])? '1':'0');
$economy_3 =$conexion -> real_escape_string(isset($_POST['Economia3'])? '1':'0');
$economy_4 =$conexion -> real_escape_string(isset($_POST['Economia4'])? '1':'0');
$economy_5 =$conexion -> real_escape_string(isset($_POST['Economia5'])? '1':'0');
$economy_6 =$conexion -> real_escape_string(isset($_POST['Economia6'])? '1':'0');
$economy_7 =$conexion -> real_escape_string(isset($_POST['Economia7'])? '1':'0');
$economy_8 =$conexion -> real_escape_string(isset($_POST['Economia8'])? '1':'0');
$economy_9 =$conexion -> real_escape_string(isset($_POST['Economia9'])? '1':'0');
$economy_10 =$conexion -> real_escape_string(isset($_POST['Economia10'])? '1':'0');
$economy_11 =$conexion -> real_escape_string($_POST['Economia11']);




/*economia->cooperativas*/
$economy_cooperatives_1 = $conexion -> real_escape_string(isset($_POST['Cooperativa1'])? '1':'0');
$economy_cooperatives_2 = $conexion -> real_escape_string(isset($_POST['Cooperativa2'])? '1':'0');
$economy_cooperatives_3 = $conexion -> real_escape_string(isset($_POST['Cooperativa3'])? '1':'0');


/*economia -> cooperativas -> produccion*/
$economy_cooperatives_production_1 = $conexion -> real_escape_string(isset($_POST['producion01'])? '1':'0');
$economy_cooperatives_production_2 = $conexion -> real_escape_string(isset($_POST['producion02'])? '1':'0');


/*Educacion*/
$education_1 = $conexion -> real_escape_string(isset($_POST['Educacion1'])? '1':'0');
$education_2 = $conexion -> real_escape_string(isset($_POST['Educacion2'])? '1':'0');
$education_3 = $conexion -> real_escape_string(isset($_POST['Educacion3'])? '1':'0');
$education_4 = $conexion -> real_escape_string(isset($_POST['Educacion4'])? '1':'0');
$education_5 = $conexion -> real_escape_string(isset($_POST['Educacion5'])? '1':'0');
$education_6 = $conexion -> real_escape_string($_POST['Educacion6']);


/*Derechos Humanos*/
$human_rights_1 = $conexion -> real_escape_string( isset($_POST['DerechosH1'])? '1':'0');

/* Trabajo con niños / joventud */
$work_children_1 = $conexion -> real_escape_string(isset($_POST['TrabajoNJ1'])? '1':'0');
$work_children_2 = $conexion -> real_escape_string(isset($_POST['TrabajoNJ2'])? '1':'0');
$work_children_3 = $conexion -> real_escape_string(isset($_POST['TrabajoNJ3'])? '1':'0');
$work_children_4 = $conexion -> real_escape_string($_POST['TrabajoNJ4']);


/* Investigación agrícola y Extensión rural */
$investigation_1 = $conexion -> real_escape_string(isset($_POST['Investigacion1'])? '1':'0');
$investigation_2 = $conexion -> real_escape_string(isset($_POST['Investigacion2'])? '1':'0');
$investigation_3 = $conexion -> real_escape_string(isset($_POST['Investigacion3'])? '1':'0');
$investigation_4 = $conexion -> real_escape_string(isset($_POST['Investigacion4'])? '1':'0');
$investigation_5 = $conexion -> real_escape_string(isset($_POST['Investigacion5'])? '1':'0');
$investigation_6 = $conexion -> real_escape_string(isset($_POST['Investigacion6'])? '1':'0');
$investigation_7 = $conexion -> real_escape_string($_POST['Investigacion7']);


/*  Manejo de recursos hídricos */

$driving_resources_1 = $conexion -> real_escape_string(isset($_POST['ManejoRecursos1'])? '1':'0');
$driving_resources_2 = $conexion -> real_escape_string(isset($_POST['ManejoRecursos2'])? '1':'0');
$driving_resources_3 = $conexion -> real_escape_string(isset($_POST['ManejoRecursos3'])? '1':'0');
$driving_resources_4 = $conexion -> real_escape_string(isset($_POST['ManejoRecursos4'])? '1':'0');
$driving_resources_5 = $conexion -> real_escape_string(isset($_POST['ManejoRecursos5'])? '1':'0');
$driving_resources_6 = $conexion -> real_escape_string(isset($_POST['ManejoRecursos6'])? '1':'0');
$driving_resources_7 = $conexion -> real_escape_string($_POST['ManejoRecursos7']);


/* Manejo poscosecha*/
$driving_harvest_1  = $conexion -> real_escape_string(isset($_POST['ManejoPoscosecha1'])? '1':'0');
$driving_harvest_2  = $conexion -> real_escape_string(isset($_POST['ManejoPoscosecha2'])? '1':'0');
$driving_harvest_3  = $conexion -> real_escape_string(isset($_POST['ManejoPoscosecha3'])? '1':'0');
$driving_harvest_4  = $conexion -> real_escape_string($_POST['ManejoPoscosecha4']);



/*Semillas*/
$seeds_1 = $conexion -> real_escape_string(isset($_POST['Semillas1'])? '1':'0');
$seeds_2 = $conexion -> real_escape_string(isset($_POST['Semillas2'])? '1':'0');
$seeds_3 = $conexion -> real_escape_string(isset($_POST['Semillas3'])? '1':'0');
$seeds_4 = $conexion -> real_escape_string(isset($_POST['Semillas4'])? '1':'0');
$seeds_5 = $conexion -> real_escape_string(isset($_POST['Semillas5'])? '1':'0');
$seeds_6 = $conexion -> real_escape_string($_POST['Semillas6']);


/*Genero*/
$gender  = $conexion ->real_escape_string( isset($_POST['genero1'])? '1':'0');

/*Sistemas agroforestales*/
$system_agroforestry_1 = $conexion -> real_escape_string(isset($_POST['sistemaAgro1'])? '1':'0');
$system_agroforestry_2 = $conexion -> real_escape_string(isset($_POST['sistemaAgro2'])? '1':'0');
$system_agroforestry_3 = $conexion -> real_escape_string($_POST['sistemaAgro3']);

/*Sistemas agroforestales -> Arborizacion*/
$system_agroforestry_tree_1 = $conexion -> real_escape_string(isset($_POST['arbo1'])? '1':'0');
$system_agroforestry_tree_2 = $conexion -> real_escape_string(isset($_POST['arbo2'])? '1':'0');
$system_agroforestry_tree_3 = $conexion -> real_escape_string(isset($_POST['arbo3'])? '1':'0');

/*Productos forestales*/
$proucts_forestry_1 = $conexion ->real_escape_string(isset($_POST['prof1'])? '1':'0');
$proucts_forestry_2 = $conexion ->real_escape_string(isset($_POST['prof2'])? '1':'0');
$proucts_forestry_3 = $conexion ->real_escape_string($_POST['prof3']);

/*  Sistemas de producción animal */
$system_production_animal_1 = $conexion ->real_escape_string(isset($_POST['sisProAn1'])? '1':'0');
$system_production_animal_2 = $conexion ->real_escape_string(isset($_POST['sisProAn2'])? '1':'0');
$system_production_animal_3 = $conexion ->real_escape_string(isset($_POST['sisProAn3'])? '1':'0');
$system_production_animal_4 = $conexion ->real_escape_string(isset($_POST['sisProAn4'])? '1':'0');
$system_production_animal_5 = $conexion ->real_escape_string(isset($_POST['sisProAn5'])? '1':'0');
$system_production_animal_6 = $conexion ->real_escape_string(isset($_POST['sisProAn6'])? '1':'0');
$system_production_animal_7 = $conexion ->real_escape_string(isset($_POST['sisProAn7'])? '1':'0');
$system_production_animal_8 = $conexion ->real_escape_string(isset($_POST['sisProAn8'])? '1':'0');
$system_production_animal_9 = $conexion ->real_escape_string(isset($_POST['sisProAn9'])? '1':'0');
$system_production_animal_10 = $conexion ->real_escape_string(isset($_POST['sisProAn10'])? '1':'0');
$system_production_animal_11 = $conexion ->real_escape_string(isset($_POST['sisProAn11'])? '1':'0');
$system_production_animal_12 = $conexion ->real_escape_string(isset($_POST['sisProAn12'])? '1':'0');
$system_production_animal_13 = $conexion ->real_escape_string(isset($_POST['sisProAn13'])? '1':'0');
$system_production_animal_14 = $conexion ->real_escape_string(isset($_POST['sisProAn14'])? '1':'0');
$system_production_animal_15 = $conexion ->real_escape_string($_POST['sisProAn15']);
//var_dump($system_production_animal_15);

/*  Sistemas de producción animal -> Apicultura */
$system_beekeeping_1 = $conexion ->real_escape_string(isset($_POST['apicultura1'])? '1':'0');
$system_beekeeping_2 = $conexion ->real_escape_string(isset($_POST['apicultura2'])? '1':'0');

/*Sistemas de producción agrícola -> Estrategias alternativas de producción:*/
$system_agricola_1 = $conexion ->real_escape_string(isset($_POST['estAlPro1'])? '1':'0');
$system_agricola_2 = $conexion ->real_escape_string(isset($_POST['estAlPro2'])? '1':'0');
$system_agricola_3 = $conexion ->real_escape_string(isset($_POST['estAlPro3'])? '1':'0');
$system_agricola_4 = $conexion ->real_escape_string(isset($_POST['estAlPro4'])? '1':'0');
$system_agricola_5 = $conexion ->real_escape_string(isset($_POST['estAlPro5'])? '1':'0');
$system_agricola_6 = $conexion ->real_escape_string(isset($_POST['estAlPro6'])? '1':'0');

/*Sistemas de producción agrícola -> Manejo de plagas y enfermedades::*/
$driving_pests_diseases_1 = $conexion -> real_escape_string(isset($_POST['manPlEn1'])? '1':'0');
$driving_pests_diseases_2 = $conexion -> real_escape_string(isset($_POST['manPlEn2'])? '1':'0');
$driving_pests_diseases_3 = $conexion -> real_escape_string(isset($_POST['manPlEn3'])? '1':'0');

/*Sistemas de producción agrícola -> Manejo de suelos:*/
$system_driving_soils_1 = $conexion -> real_escape_string(isset($_POST['mandSue1'])? '1':'0');
$system_driving_soils_2 = $conexion -> real_escape_string(isset($_POST['mandSue2'])? '1':'0');
$system_driving_soils_3 = $conexion -> real_escape_string(isset($_POST['mandSue3'])? '1':'0');
$system_driving_soils_4 = $conexion -> real_escape_string(isset($_POST['mandSue4'])? '1':'0');
$system_driving_soils_5 = $conexion -> real_escape_string(isset($_POST['mandSue5'])? '1':'0');
$system_driving_soils_6 = $conexion -> real_escape_string(isset($_POST['mandSue6'])? '1':'0');
$system_driving_soils_7 = $conexion -> real_escape_string(isset($_POST['mandSue7'])? '1':'0');
$system_driving_soils_8 = $conexion -> real_escape_string(isset($_POST['mandSue8'])? '1':'0');
$system_driving_soils_9 = $conexion -> real_escape_string(isset($_POST['mandSue9'])? '1':'0');
$system_driving_soils_10 = $conexion -> real_escape_string(isset($_POST['mandSue10'])? '1':'0');
$system_driving_soils_11 = $conexion -> real_escape_string(isset($_POST['mandSue11'])? '1':'0');
$system_driving_soils_12 = $conexion -> real_escape_string(isset($_POST['mandSue12'])? '1':'0');
$system_driving_soils_13 = $conexion -> real_escape_string(isset($_POST['mandSue13'])? '1':'0');

/*Sistemas de producción agrícola -> Sistemas de producción agrícola:*/

$system_production_agricola_1 = $conexion -> real_escape_string(isset($_POST['sisdProdAgri1'])? '1':'0');
$system_production_agricola_2 = $conexion -> real_escape_string(isset($_POST['sisdProdAgri2'])? '1':'0');
$system_production_agricola_3 = $conexion -> real_escape_string(isset($_POST['sisdProdAgri3'])? '1':'0');
$system_production_agricola_4 = $conexion -> real_escape_string(isset($_POST['sisdProdAgri4'])? '1':'0');
$system_production_agricola_5 = $conexion -> real_escape_string(isset($_POST['sisdProdAgri5'])? '1':'0');
$system_production_agricola_6 = $conexion -> real_escape_string(isset($_POST['sisdProdAgri6'])? '1':'0');
$system_production_agricola_7 = $conexion -> real_escape_string(isset($_POST['sisdProdAgri7'])? '1':'0');
$system_production_agricola_8 = $conexion -> real_escape_string(isset($_POST['sisdProdAgri8'])? '1':'0');
$system_production_agricola_9 = $conexion -> real_escape_string(isset($_POST['sisdProdAgri9'])? '1':'0');


/*Sistemas de producción agrícola -> Sistemas de producción agrícola:*/
$system_production_1 = $conexion -> real_escape_string($_POST['sisdProdAgri10']);



//--------Colectivo & Organización---//

/* Nucelo Agrario*/
$Agriculture_Nucleo1 = $conexion -> real_escape_string(isset($_POST['nucAgri1'])? '1':'0');
$Agriculture_Nucleo2 = $conexion -> real_escape_string(isset($_POST['nucAgri2'])? '1':'0');

/*Fundación*/
$Fundation1 = $conexion -> real_escape_string(isset($_POST['fundacion1'])? '1':'0');
$Fundation2 = $conexion -> real_escape_string(isset($_POST['fundacion2'])? '1':'0');
//$Fundation3 = $conexion -> real_escape_string($_POST['fundacion3']);


/* Gobierno*/
$Goverment1 = $conexion -> real_escape_string(isset($_POST['gobierno1'])? '1':'0');
$Goverment2 = $conexion -> real_escape_string(isset($_POST['gobierno2'])? '1':'0');
$Goverment3 = $conexion -> real_escape_string(isset($_POST['gobierno3'])? '1':'0');

/* Institucuines Educativas*/
$Education_Institute1 = $conexion -> real_escape_string(isset($_POST['intEduc1'])? '1':'0');
$Education_Institute2 = $conexion -> real_escape_string(isset($_POST['intEduc2'])? '1':'0');
$Education_Institute3 = $conexion -> real_escape_string(isset($_POST['intEduc3'])? '1':'0');
$Education_Institute4 = $conexion -> real_escape_string(isset($_POST['intEduc4'])? '1':'0');
$Education_Institute5 = $conexion -> real_escape_string(isset($_POST['intEduc5'])? '1':'0');
$Education_Institute6 = $conexion -> real_escape_string(isset($_POST['intEduc6'])? '1':'0');
$Education_Institute7 = $conexion -> real_escape_string(isset($_POST['intEduc7'])? '1':'0');
$Education_Institute8 = $conexion -> real_escape_string(isset($_POST['intEduc8'])? '1':'0');

/* Organización Campesina*/
$Farm_Organization = $conexion -> real_escape_string(isset($_POST['orgCamp1'])? '1':'0' );

/*Colectivo Mujeres*/
$Women_Colective = $conexion -> real_escape_string(isset($_POST['colMuj1'])? '1':'0' );

/*Cooperativa*/
$Cooperative = $conexion -> real_escape_string(isset($_POST['coop1'])? '1':'0' );

/*Cooperativa de Consumo*/
$Consume_Cooperative = $conexion -> real_escape_string(isset($_POST['coopCon1'])? '1':'0' );

/*Cooperativa de Producción*/
$Production_Coperative = $conexion -> real_escape_string(isset($_POST['cooProd1'])? '1':'0' );

/*Ejido*/
$Ejido = $conexion -> real_escape_string(isset($_POST['eji1'])? '1':'0' );

/*Empresa Social*/
$Social_Company = $conexion -> real_escape_string(isset($_POST['empSoc1'])? '1':'0' );

/*Empresa Privada*/
$Company_Private = $conexion -> real_escape_string(isset($_POST['empPriv1'])? '1':'0' );

/*Grupo de Asesores Tecnicos*/
$Technical_Grup = $conexion -> real_escape_string(isset($_POST['grupAse'])? '1':'0' );

/*Grupo de consumidores*/
$Consumer_Group = $conexion -> real_escape_string(isset($_POST['grupCons1'])? '1':'0' );

/*Grupo de investigación*/
$Investigation_Group = $conexion -> real_escape_string(isset($_POST['grupoInve1'])? '1':'0' );

/*Grupo Estudiantil*/
$Student_Group = $conexion -> real_escape_string(isset($_POST['grupEst1'])? '1':'0' );

/*Grupo de la Iglesia*/
$Church_Group = $conexion -> real_escape_string(isset($_POST['grupIgl1'])? '1':'0' );

/*Organización de la Sociedad Civil*/
$Organization_Civil = $conexion -> real_escape_string(isset($_POST['orgSocCiv1'])? '1':'0' );

/*Progama de extension*/
$Extension_Group = $conexion -> real_escape_string(isset($_POST['progExt1'])? '1':'0' );

/* Otro-Colectivo*/
$Other_Colectivo = $conexion -> real_escape_string($_POST['otro_Colec1'] );


//--------Seguridad---//

$Security1 = $conexion -> real_escape_string(isset($_POST['seguridad1'])? '1':'0' );
$Security2 = $conexion -> real_escape_string(isset($_POST['seguridad2'])? '1':'0' );
$Security3 = $conexion -> real_escape_string(isset($_POST['seguridad3'])? '1':'0' );
$Security4 = $conexion -> real_escape_string(isset($_POST['seguridad4'])? '1':'0' );

/* Atlas de iniciativas*/

$atlas_iniciatives_1 = $conexion -> real_escape_string($_POST['nombreIncExp']);
$atlas_iniciatives_2 = $conexion -> real_escape_string($_POST['resIni']);
$atlas_iniciatives_3 = $conexion -> real_escape_string($_POST['fechaAtlas']);
$atlas_iniciatives_Lat_4 = $conexion -> real_escape_string($_POST['txtLat']);
$atlas_iniciatives_Lng_5 = $conexion -> real_escape_string($_POST['txtLng']);

/*Hacia una nueva política pública*/
$public_politics_1 = $conexion ->real_escape_string($_POST['politicas1']);
$public_politics_2 = $conexion ->real_escape_string($_POST['politicas2']);
$public_politics_3 = $conexion ->real_escape_string($_POST['politicas3']);
$public_politics_4 = $conexion ->real_escape_string($_POST['politicas4']);

/*Tejiendo redes*/

$weaving_nets_1 = $conexion -> real_escape_string($_POST['redes1']);
$weaving_nets_2 = $conexion -> real_escape_string($_POST['redes2']);
$weaving_nets_3 = $conexion -> real_escape_string($_POST['redes3']);
$weaving_nets_4 = $conexion -> real_escape_string($_POST['redes4']);

/*Formacion*/

$training_1 = $conexion -> real_escape_string($_POST['formacion1']);
$training_2 = $conexion -> real_escape_string($_POST['formacion2']);

/*Nota*/
$note_1 = $conexion -> real_escape_string($_POST['nota1']);
$note_2 = $conexion -> real_escape_string($_POST['nota2']);
$note_3 = $conexion -> real_escape_string($_POST['nota3']);

/*carlos */
$data = array(
    'nameGroup' => $nameGroup,
    'nameRepresentative' => $nameRepresentative,
    'email' => $email,
    'urlSocial' => $urlSocial,
    'state' => $state,
    'municipality' => $municipality,
    'address' => $address,
    'cp' => $cp,
    'urbanFarming' => $urbanFarming,
    'systemProductionAgricola' =>$system_production_1,

    'health' => array(
        '1' => $health_1 ,
        '2' => $health_2 ,
        '3' => $health_3 ,
        '4' => $health_4 ,
        '5' => $health_5
    ),
    'non_agricultural' => array(
        '1' => $non_agricultural_1 ,
        '2' => $non_agricultural_2 ,
        '3' => $non_agricultural_3 ,
        '4' => $non_agricultural_4 ,
        '5' => $non_agricultural_5
    ),
    'localDevelopment' => array(
        '1' => $deve_local_1,
        '2' => $deve_local_2,
        '3' => $deve_local_3,
        '4' => $deve_local_4,
        '5' => $deve_local_5,
        '6' => $deve_local_6

    ),

    'Economy' => array(
        '1' => $economy_1 ,
        '2' => $economy_2 ,
        '3' => $economy_3 ,
        '4' => $economy_4 ,
        '5' => $economy_5 ,
        '6' => $economy_6 ,
        '7' => $economy_7 ,
        '8' => $economy_8 ,
        '9' => $economy_9 ,
        '10' => $economy_10,
        '11' => $economy_11

    ),

    'economyCooperatives' => array(

        '1' => $economy_cooperatives_1,
        '2' => $economy_cooperatives_2,
        '3' => $economy_cooperatives_3
    ),



    'economyProduction' => array(
        '1' => $economy_cooperatives_production_1,
        '2' => $economy_cooperatives_production_2

    ),



    'education' => array(
        '1' => $education_1,
        '2' => $education_2,
        '3' => $education_3,
        '4' => $education_4,
        '5' => $education_5,
        '6' => $education_6

    ),



    'wordChildren' => array(
        '1' => $work_children_1,
        '2' => $work_children_2,
        '3' => $work_children_3,
        '4' => $work_children_4
    ),

    'investigation' => array(
        '1' => $investigation_1,
        '2' => $investigation_2,
        '3' => $investigation_3,
        '4' => $investigation_4,
        '5' => $investigation_5,
        '6' => $investigation_6,
        '7' => $investigation_7
    ),

    'drivingResources' => array(
        '1' => $driving_resources_1,
        '2' => $driving_resources_2,
        '3' => $driving_resources_3,
        '4' => $driving_resources_4,
        '5' => $driving_resources_5,
        '6' => $driving_resources_6,
        '7' => $driving_resources_7
    ),

    'drivingHarvest' => array(
        '1' => $driving_harvest_1,
        '2' => $driving_harvest_2,
        '3' => $driving_harvest_3,
        '4' => $driving_harvest_4

    ),

    'seeds' =>array(
        '1' => $seeds_1 ,
        '2' => $seeds_2 ,
        '3' => $seeds_3 ,
        '4' => $seeds_4 ,
        '5' => $seeds_5 ,
        '6' => $seeds_6

    ),
    'gender' => $gender,

    'systemAgroforestry' => array(
        '1' => $system_agroforestry_1,
        '2' => $system_agroforestry_2,
        '3' => $system_agroforestry_3
    ),

    'systemAgroforestryTree' => array(
        '1' => $system_agroforestry_tree_1,
        '2' => $system_agroforestry_tree_2,
        '3' => $system_agroforestry_tree_3
    ),

    'productsForestry' => array(
        '1' => $proucts_forestry_1,
        '2' => $proucts_forestry_2,
        '3' => $proucts_forestry_3
    ),

    'systemProductionAnimal' =>array(
        '1' => $system_production_animal_1,
        '2' => $system_production_animal_2,
        '3' => $system_production_animal_3,
        '4' => $system_production_animal_4,
        '5' => $system_production_animal_5,
        '6' => $system_production_animal_6,
        '7' => $system_production_animal_7,
        '8' => $system_production_animal_8,
        '9' => $system_production_animal_9,
        '10' => $system_production_animal_10,
        '11' => $system_production_animal_11,
        '12' => $system_production_animal_12,
        '13' => $system_production_animal_13,
        '14' => $system_production_animal_14,
        '15' => $system_production_animal_15
    ),

    'systemBeekeeping' => array(
        '1' => $system_beekeeping_1,
        '2' => $system_beekeeping_2
    ),

    'systemAgriculaProduction' => array(
        '1' => $system_agricola_1,
        '2' => $system_agricola_2,
        '3' => $system_agricola_3,
        '4' => $system_agricola_4,
        '5' => $system_agricola_5,
        '6' => $system_agricola_6
    ),


    'drivingPestsDiseases' => array(
        '1' => $driving_pests_diseases_1,
        '2' => $driving_pests_diseases_2,
        '3' => $driving_pests_diseases_3

    ),

    'systemDrivingSoils' => array(
        '1' => $system_driving_soils_1,
        '2' => $system_driving_soils_2,
        '3' => $system_driving_soils_3,
        '4' => $system_driving_soils_4,
        '5' => $system_driving_soils_5,
        '6' => $system_driving_soils_6,
        '7' => $system_driving_soils_7,
        '8' => $system_driving_soils_8,
        '9' => $system_driving_soils_9,
        '10' => $system_driving_soils_10,
        '11' => $system_driving_soils_11,
        '12' => $system_driving_soils_12,
        '13' => $system_driving_soils_13

    ),

    'systemProductionAgricola_1' => array(
        '1' => $system_production_agricola_1,
        '2' => $system_production_agricola_2,
        '3' => $system_production_agricola_3,
        '4' => $system_production_agricola_4,
        '5' => $system_production_agricola_5,
        '6' => $system_production_agricola_6,
        '7' => $system_production_agricola_7,
        '8' => $system_production_agricola_8,
        '9' => $system_production_agricola_9
    ),

    'systemProductionAgricolaRaiz' => $system_production_1,

    'agroNucelo' => array(
        '1' => $Agriculture_Nucleo1,
        '2' => $Agriculture_Nucleo2
    ),

    'fundation' => array(
        '1' => $Fundation1,
        '2' => $Fundation2
        //'3' => $Fundation3 = ($Fundation3 == 'on')? '1':'0'
    ),

    'goberment' => array(
        '1' => $Goverment1,
        '2' => $Goverment2,
        '3' => $Goverment3
    ),

    'educative' => array(
        '1' => $Education_Institute1 ,
        '2' => $Education_Institute2 ,
        '3' => $Education_Institute3 ,
        '4' => $Education_Institute4 ,
        '5' => $Education_Institute5 ,
        '6' => $Education_Institute6 ,
        '7' => $Education_Institute7 ,
        '8' => $Education_Institute8

    ),

    'farmOrganization' => $Farm_Organization,

    'womenColective' => $Women_Colective,

    'cooperative' => $Cooperative,

    'consumeCooperative' => $Consume_Cooperative,

    'production_Coperative' => $Production_Coperative,

    'ejido' => $Ejido,

    'socialCompany' => $Social_Company,

    'privateCompany' => $Company_Private,

    'technicalGroup' => $Technical_Grup,

    'consumerGroup' => $Consumer_Group,

    'investigationGroup' => $Investigation_Group,

    'studentGroup' => $Student_Group,

    'ChurchGroup' => $Church_Group,

    'organizationCivil' => $Organization_Civil,

    'extensionGroup' => $Extension_Group,

    'otherColectivo' => $Other_Colectivo,
    'humanRight' => $human_rights_1,


    'atlas' => array(
        '1' => $atlas_iniciatives_1,
        '2' => $atlas_iniciatives_2,
        '3' => $atlas_iniciatives_3,
        '4' => $atlas_iniciatives_Lat_4,
        '5' => $atlas_iniciatives_Lng_5
    ),

    'politics' => array(
        '1' => $public_politics_1,
        '2' => $public_politics_2,
        '3' => $public_politics_3,
        '4' => $public_politics_4
    ),

    'networks' => array(
        '1' => $weaving_nets_1,
        '2' => $weaving_nets_2,
        '3' => $weaving_nets_3,
        '4' => $weaving_nets_4
    ),
    'training' => array(
        '1'=> $training_1,
        '2'=> $training_2
    ),

    'security' => array(
        '1' => $Security1,
        '2' => $Security2,
        '3' => $Security3,
        '4' => $Security4

    ),

    'note' => array(
        '1' => $note_1,
        '2' => $note_2,
        '3' => $note_3
    )


);
//var_dump($data);
echo $objMetodo ->mainInsertFile($data);

$flang = $objMetodo -> mainInsertFile($data);

if ($flang  == 1){
    //echo 'guardado';
    return 1;

}else{
    echo  'No se guardaron';
}

















