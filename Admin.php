<?php
include 'ConexionBD.php';

session_start();
$user = $_SESSION['Usuario'];

if (isset($user)) {
    include_once 'Vistas/Include/encabezadoAdmin.php';
    include_once 'Vistas/Include/menuAdmin.php';
    include_once 'Vistas/Admin/principalAdmin.php';
    include_once 'Vistas/Include/pieAdmin.php';
} else {
    header("Location:index.php");
}