<style>
    .menu-img-mapa-uv {
        position: absolute;
        right: 6%;
        top: 25px;
        max-width: 100%;
        width: 40% !important;
        z-index: 4;

    }

    #menu-salud-plantas-medicinales {
        display: none;
    }

    @media (min-width: 768px) {
        .menu-img-mapa-uv {
            top: 30px;
            width: 40% !important;
        }
    }
</style>



<div class="menu-img-mapa-uv d-sm-none" id="menu-salud-plantas-medicinales">
    <img class="img-thumbnail" src="img/SPM.png" alt="">
</div>
