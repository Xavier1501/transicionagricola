<?php
include './Modal/Mapping.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mapa</title>


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.css">

    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://kit.fontawesome.com/a076d05399.js"></script>




    <!--script type="text/javascript" src="js/graficas.js"></script-->
    <!--libreria para el sobreposicionamiento-->



    <script src="JS/marcadoresAgriculturaUrbana.js"></script>
    <script src="JS/marcadoresSaludPlantasMedicinales.js"></script>
    <script src="JS/marcadorActividadesNoAgricolas.js"></script>
    <script src="JS/desarrolloLocal.js"></script>
    <script src="JS/derechosHumanos.js"></script>
    <script src="JS/trabajoNiños.js"></script>
    <script src="JS/investigacionAgriculturaRural.js"></script>
    <script src="JS/ManejoRecursosHibridos.js"></script>
    <script src="JS/ManejoPoscosecha.js"></script>
    <script src="JS/semillas.js"></script>
    <script src="JS/Mapping.js"></script>
    <script src="JS/sistemaProduccionAgricola.js"></script>
    <script src="JS/economia.js"></script>
    <script src="JS/Educacion.js"></script>
    <script src="JS/SistemaAgroforestales.js"></script>
    <script src="JS/ProductosForestales.js"></script>
    <script src="JS/todas.js"></script>
    <script src="JS/Sistemas_producción_animal.js"></script>


</head>

<body>


    <style>
    * {
        padding: 0;
        margin: 0;
    }

    .bg-color-uv-mapa {
        background-color: rgba(0, 153, 50, 0.8);
    }

    .h3-mapa-uv {
        padding: 1rem;
        color: cornsilk;
    }



    .bg-color-uv-mapa-logo {
        background-color: crimson;
    }

    .menu-vertical {
        z-index: 5;
    }


    /*menu*/

    #check {
        display: none;
    }

    #check:checked~.sidebar {
        left: 0;
    }

    #check:checked~.sidebar {
        left: 0;
    }

    #check:checked~label #btn {
        left: 250px;
        opacity: 0;
        pointer-events: none;
    }

    #check:checked~label #cancel {
        left: 250px;
    }

    #check:checked~section {
        margin-left: 250px;
    }

    label #btn,
    label #cancel {
        position: absolute;
        background: #042331;
        border-radius: 3px;
        cursor: pointer;
    }

    label #btn {
        left: 0;
        top: 250px;
        font-size: 1rem;
        color: white;
        padding: 6px 12px;
        transition: all .5s;
        z-index: 3;
    }

    label #cancel {
        z-index: 1111;
        left: -195px;
        top: 17px;
        font-size: 30px;
        color: #0a5275;
        padding: 4px 9px;
        transition: all .5s ease;
    }

    ul li:hover a {
        padding-left: 50px;
    }

    .sidebar ul a i {
        margin-right: 16px;
    }

    .sidebar {
        position: fixed;
        left: -350px;
        width: 350px;
        top: 0;
        height: 100%;
        background: #042331;
        transition: all .5s ease;
        overflow-y: scroll;
        z-index: 5;
    }

    .sidebar header {
        font-size: 22px;
        color: white;
        line-height: 70px;
        text-align: center;
        background: #063146;
        user-select: none;
    }

    .sidebar ul a {
        display: contents;
        text-align: center;
        height: 100%;
        width: 100%;
        line-height: 65px;
        font-size: 20px;
        color: white;
        padding-left: 40px;
        box-sizing: border-box;
        border-bottom: 1px solid black;
        border-top: 1px solid rgba(255, 255, 255, .1);
        transition: .4s;
    }

    .title-category-uv {
        color: #fff;
        text-align: center;
        padding: 1rem;
        text-decoration: none;
    }

    .op-category-uv li {
        text-align: left;
        color: aliceblue;
        padding: .5rem;
    }

    .text-category-uv {
        color: #fff;
    }

    .informe-mapa {
        margin-top: -50px;
    }

    .capa-uv {
        padding: .3rem;
        text-align: justify;
        color: white;

    }

    li:hover {
        background-color: rgba(192, 192, 192, 0.3);
    }


    .contenedor {
        display: flex;
    }

    .item {

        justify-content: space-between;
    }

    .img-uv-informe {
        max-width: 100%;
        width: 12.9%;
        height: auto;
        text-align: center;
        margin: 1rem;
        padding: .5rem;

    }

    .banner{
        display: none;
    }

    .boton{
        width: 28% !important;
        background-color: white;
        position: absolute !important;
        top: 16.5%;
        z-index: 5;
    }
    .img-banner{
        max-width: 100%;
        width: 40%;
        height:auto;
        padding: 1rem;
    }
    .hover-uv{
        display: none;
    }
    .boton:hover + .hover-uv{
        display: block;
        cursor: pointer;
    }


    @media (min-width: 576px) {
        .img-uv-informe {
            max-width: 100%;
            width: 6.5%;
            height: auto;
            text-align: center;
            margin: 1rem;

        }

        .img-banner{
            max-width: 100%;
            width: 40%;
            height:auto;
            padding: 1rem;
        }

        .boton{
            width: 15% !important;
            top: 11.5%;

        }


    }



    </style>

    <header class="menu-informe">
        <div class="container-fluid">
            <div class="row">



                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center bg-color-uv-mapa">
                    <h3 class="h3-mapa-uv">Atlas de Transiciones Agroecológicas en México</h3>
                </div>

                <div class="col-xs-1 text-center boton">
                   <strong> Ver participantes </strong>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center hover-uv ">
                    <img class="img-fluid img-uv-informe " src="img/uv-null.png" alt="">
                    <img class="img-fluid img-uv-informe " src="img/logos/agr.png" alt="">
                    <img class="img-fluid img-uv-informe " src="img/logos/idiomas.png" alt="">
                    <img class="img-fluid img-uv-informe " src="img/logos/Logo-Clacso-2019-esp.png" alt="">
                    <img class="img-fluid img-uv-informe " src="img/logos/UdeG.png" alt="">
                    <img class="img-fluid img-uv-informe " src="img/logos/Conacyt.png" alt="">
                    <img class="img-fluid img-uv-informe " src="img/logos/CUCSur.png" alt="">
                    <img class="img-fluid img-uv-informe " src="img/logos/RASa_Logo.png" alt="">
                    <img class="img-fluid img-uv-informe " src="img/logos/vILzzGbg.png" alt="">
                    <img class="img-fluid img-uv-informe " src="img/logos/FCPySggg.jpg" alt="">
                </div>
            </div>
        </div>

    </header>

    <section class="menu-vertical" >
        <input type="checkbox" id="check" />
        <label for="check">
            <i class="fas fa-bars" id="btn"></i>
            <i class="fas fa-times" id="cancel"></i>
        </label>

        <div class="sidebar">
            <header>Iniciativas</header>
            <ul>



                <li class="capa-uv">
                    <input name="op" id="todas" value="1" type="radio" checked class="form-check-input">
                    <label>Todas las iniciativas</label>
                </li>




                <li class="capa-uv">
                    <input name="op" id="agriculturaUrbana" type="radio" class="form-check-input"
                        value="Agricultura Urbana">
                    <label>Agricultura Urbana</label>
                </li>
                <li class="capa-uv">
                    <input name="op" id="saludPlantasMedicinales" value="1" type="radio" class="form-check-input">
                    <label>Salud plantas medicinales</label>
                </li>
                <li class="capa-uv">
                    <input name="op" id="actividadeNoAgricolas" value="1" type="radio" class="form-check-input">
                    <label>Actividades no agrícolas</label>
                </li>

                <li class="capa-uv">
                    <input name="op" id="desarrolloLocal" value="1" type="radio" class="form-check-input">
                    <label>Desarrollo Local</label>
                </li>

                <li class="capa-uv">
                    <input name="op" id="economia" value="1" type="radio" class="form-check-input">
                    <label>Economia</label>
                </li>

                <li class="capa-uv">
                    <input name="op" id="educacion" value="1" type="radio" class="form-check-input">
                    <label>Educacion</label>

                </li>

                <li class="capa-uv">
                    <input name="op" id="derechosHumanos" value="1" type="radio" class="form-check-input">
                    <label>Derechos Humanos</label>
                </li>

                <li class="capa-uv">

                    <input name="op" id="trabajoNiñosJoventud" value="1" type="radio" class="form-check-input">
                    <label>Trabajo con niños/juventud</label>
                </li>

                <li class="capa-uv">
                    <input name="op" id="invAgricolaExtension" value="1" type="radio" class="form-check-input">
                    <label>Investigación agrícola y Extensión rural</label>
                </li>

                <li class="capa-uv">
                    <input name="op" id="manejoRecursosHibridos" value="1" type="radio" class="form-check-input">
                    <label>Manejo de recursos hídricos</label>
                </li>

                <li class="capa-uv">
                    <input name="op" id="manejoPoscosecha" value="1" type="radio" class="form-check-input">
                    <label>Manejo poscosecha</label>
                </li>

                <li class="capa-uv">
                    <input name="op" id="semillas" value="1" type="radio" class="form-check-input">
                    <label>Semillas</label>
                </li>

                <li class="capa-uv">
                    <input name="op" id="sistemasAgroforestales" value="1" type="radio" class="form-check-input">
                    <label>Sistemas agroforestales</label>
                </li>

                <li class="capa-uv">
                    <input name="op" id="productosForestales" value="1" type="radio" class="form-check-input">
                    <label>Productos forestales</label>
                </li>

                <li class="capa-uv">
                    <input name="op" id="sistemaProduccionAnimal" value="1" type="radio" class="form-check-input">
                    <label>Sistemas de producción animal</label>
                </li>

                <li class="capa-uv">
                    <input name="op" id="sistemaProduccionAgricola" value="1" type="radio" class="form-check-input">
                    <label>Sistemas de producción agrícola</label>
                </li>








            </ul>
        </div>
    </section>


    <section class="informe-mapa">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <?php
                    include 'menu-categoria/MenuCategoriaAgriculturaUrbana.php';
                    include 'menu-categoria/MenuCategoriaSaludPlantasMedicinales.php';
                    include 'menu-categoria/MenuCategoriaSistsmeaProduccionAgricola.php';
                    include 'menu-categoria/MenuCategoriaActividadesNoAgricolas.php';
                    include 'menu-categoria/MenuCategoriaDesarrollo.php';
                    include 'menu-categoria/MenuCategoriaDH.php';
                    include 'menu-categoria/MenuCategoriaTrabajoNiños.php';
                    include 'menu-categoria/MenuCategoriaIAR.php';
                    include 'menu-categoria/MenuCategoriaMRH.php';
                    include 'menu-categoria/MenuCategoriaMP.php';
                    include 'menu-categoria/MenuCategoriaSemillas.php';
                    include 'menu-categoria/MenuCategoriaEconomia.php';
                    include 'menu-categoria/MenuCategoriaEducacion.php';
                    include 'menu-categoria/MenuSistemasAgroforestales.php';
                    include 'menu-categoria/MenuProductosforestales.php';
                    include 'menu-categoria/MenuSistemasProducciónAnimal.php';
                    ?>
                </div>


                <style>

                    #map{

                        overflow:hidden;
                        padding-bottom:56.25%;
                        position:relative;
                        width: 100%;
                        height:600px;

                    }


                    @media (min-width: 576px) {
                        #map{
                         height: auto;
                        }
                    }







                </style>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="map" >
                </div>


            </div>
        </div>


    </section>


    <?php
    include_once 'Vistas/modal/categoria/AgriculturaUrbana/Modal-Agricultura-Urbana-HCumunitarios.php';
    include_once 'Vistas/modal/categoria/AgriculturaUrbana/Modal-Agricultura-Urbana-HEmpresas.php';
    include_once 'Vistas/modal/categoria/AgriculturaUrbana/Modal-Agricultura-Urbana-Traspatio.php';
    include_once 'Vistas/modal/categoria/AgriculturaUrbana/Modal-Agricultura-Urbana-Parques.php';
    include_once 'Vistas/modal/categoria/AgriculturaUrbana/Modal-Agricultura-Urbana-Balcon.php';
    include_once 'Vistas/modal/categoria/AgriculturaUrbana/Modal-Agricultura-Urbana-Azotea.php';
    include_once 'Vistas/modal/categoria/AgriculturaUrbana/Modal-Agricultura-Urbana-Hsociales.php';
    include_once 'Vistas/modal/categoria/AgriculturaUrbana/Modal-Agricultura-Urbana-Histitucionales.php';
    include_once 'Vistas/modal/categoria/AgriculturaUrbana/Modal-Agricultura-Urbana-Hotro.php';
    include_once 'Vistas/modal/categoria/saludPlantasMedicinales/Modal-SaludPlantasMedicionales-homeopatia.php';
    include_once 'Vistas/modal/categoria/saludPlantasMedicinales/Modal-SaludPlantasMedicionales-Medicina.php';
    include_once 'Vistas/modal/categoria/saludPlantasMedicinales/Modal-SaludPlantasMedicionales-preparativos.php';
    include_once 'Vistas/modal/categoria/saludPlantasMedicinales/Modal-SaludPlantasMedicionales-remedio.php';
    include_once 'Vistas/modal/categoria/saludPlantasMedicinales/Modal-SaludPlantasMedicionales-nutricion.php';
    include_once 'Vistas/modal/categoria/saludPlantasMedicinales/Modal-SaludPlantasMedicionales-otro.php';
    include_once 'Vistas/modal/categoria/ActividadesNoAgricolas/Modal-Actividades-no-Agricolas-Artesania.php';
    include_once 'Vistas/modal/categoria/ActividadesNoAgricolas/Modal-Actividades-no-Agricolas-Caja.php';
    include_once 'Vistas/modal/categoria/ActividadesNoAgricolas/Modal-Actividades-no-Agricolas-Turismo.php';
    include_once 'Vistas/modal/categoria/ActividadesNoAgricolas/Modal-Actividades-no-Agricolas-Culturales.php';
    include_once 'Vistas/modal/categoria/ActividadesNoAgricolas/Modal-Actividades-no-Agricolas-Comedor.php';
    include_once 'Vistas/modal/categoria/ActividadesNoAgricolas/Modal-Actividades-no-Agricolas-ComedorEscolar.php';
    include_once 'Vistas/modal/categoria/ActividadesNoAgricolas/Modal-Actividades-no-Agricolas-Otro.php';
    include_once 'Vistas/modal/categoria/ActividadesNoAgricolas/Modal-Actividades-no-Agricolas-restaurante.php';
    include_once 'Vistas/modal/categoria/DesarrolloLocal/Modal-proyectos.php';
    include_once 'Vistas/modal/categoria/DesarrolloLocal/Modal-desarrollo.php';
    include_once 'Vistas/modal/categoria/DesarrolloLocal/Modal-nucleo.php';
    include_once 'Vistas/modal/categoria/DesarrolloLocal/Modal-Organizacion.php';
    include_once 'Vistas/modal/categoria/DesarrolloLocal/Modal-otro.php';
    include_once 'Vistas/modal/categoria/DesarrolloLocal/Modal-programa.php';
    include_once 'Vistas/modal/categoria/DerechosHumanos/Modal-DH-da.php';
    include_once 'Vistas/modal/categoria/DerechosHumanos/Modal-DH-de.php';
    include_once 'Vistas/modal/categoria/DerechosHumanos/Modal-DH-dh.php';
    include_once 'Vistas/modal/categoria/TrabajosNiños/Modal-TNJ-h.php';
    include_once 'Vistas/modal/categoria/TrabajosNiños/Modal-TNJ-ea.php';
    include_once 'Vistas/modal/categoria/TrabajosNiños/Modal-TNJ-o.php';
    include_once 'Vistas/modal/categoria/InvestigacionAgricolaRural/Modal-otro.php';
    include_once 'Vistas/modal/categoria/InvestigacionAgricolaRural/Modal-comunicacion.php';
    include_once 'Vistas/modal/categoria/InvestigacionAgricolaRural/Modal-Diagnostico.php';
    include_once 'Vistas/modal/categoria/InvestigacionAgricolaRural/Modal-experimentacion.php';
    include_once 'Vistas/modal/categoria/InvestigacionAgricolaRural/Modal-fromacion.php';
    include_once 'Vistas/modal/categoria/InvestigacionAgricolaRural/Modal-ordenamientos.php';
    include_once 'Vistas/modal/categoria/InvestigacionAgricolaRural/Modal-investigacion.php';
    include_once 'Vistas/modal/categoria/ManejoRecursosHibridos/Modal-otro.php';
    include_once 'Vistas/modal/categoria/ManejoRecursosHibridos/Modal-captacion.php';
    include_once 'Vistas/modal/categoria/ManejoRecursosHibridos/Modal-enfoque.php';
    include_once 'Vistas/modal/categoria/ManejoRecursosHibridos/Modal-implementacion.php';
    include_once 'Vistas/modal/categoria/ManejoRecursosHibridos/Modal-Manejo.php';
    include_once 'Vistas/modal/categoria/ManejoRecursosHibridos/Modal-Riego.php';
    include_once 'Vistas/modal/categoria/ManejoRecursosHibridos/Modal-tratamiento.php';
    include_once 'Vistas/modal/categoria/ManejoPoscosecha/Modal-Almacenamiento.php';
    include_once 'Vistas/modal/categoria/ManejoPoscosecha/Modal-comercializacion.php';
    include_once 'Vistas/modal/categoria/ManejoPoscosecha/Modal-Procesamiento.php';
    include_once 'Vistas/modal/categoria/ManejoPoscosecha/Modal-otroMP.php';
    include_once 'Vistas/modal/categoria/Semillas/Almacenamiento_conservación_semillas.php';
    include_once 'Vistas/modal/categoria/Semillas/Bancos_comunitarios_semillas.php';
    include_once 'Vistas/modal/categoria/Semillas/Intercambio_Semillas.php';
    include_once 'Vistas/modal/categoria/Semillas/Mejoramiento_plantas_animales.php';
    include_once 'Vistas/modal/categoria/Semillas/Producción_semillas_nativas_criollas.php';
    include_once 'Vistas/modal/categoria/Semillas/otro_semillas.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Agricultura_biodinámica.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Agricultura_natural.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Agricultura_orgánica.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Agrohomeopatía.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Permacultura.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Transición_sustitución_insumos.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Sustitución_agroquímicos.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Manejo_biológico.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Manejo_integrado.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Abonos_verdes.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Barbecho_descanso.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Uso_harinas_roca.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/coberteras.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Compostaje.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Mejoramiento_fertilidad.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Producción_biofertilizantes.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Producción_abonos_sólidos.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Práticas_conservación_suelos.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Siembra_directa_cultivo.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Siembra_cultivo_tracción_animal.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Siembra_cultivo_con.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Siembra_directa.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Cereales.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Cultivos_asociados.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Forrajes.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Frutales.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Hortalizas.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Leguminosas.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Medicinales.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Oleaginosas.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/Textiles.php';
    include_once 'Vistas/modal/categoria/SistemasProduccionAgricola/otro.php';
    include_once 'Vistas/modal/categoria/Economia/Autoconsumo.php';
    include_once 'Vistas/modal/categoria/Economia/Certificación_participativa.php';
    include_once 'Vistas/modal/categoria/Economia/Certificación_tercera_parte.php';
    include_once 'Vistas/modal/categoria/Economia/Comercialización_local.php';
    include_once 'Vistas/modal/categoria/Economia/Comercialización_regional.php';
    include_once 'Vistas/modal/categoria/Economia/Consumo.php';
    include_once 'Vistas/modal/categoria/Economia/Crédito.php';
    include_once 'Vistas/modal/categoria/Economia/Empresa_productora_insumos.php';
    include_once 'Vistas/modal/categoria/Economia/Iniciativa_intercambio.php';
    include_once 'Vistas/modal/categoria/Economia/Insumos.php';
    include_once 'Vistas/modal/categoria/Economia/Mercado_alternativo.php';
    include_once 'Vistas/modal/categoria/Economia/Otro.php';
    include_once 'Vistas/modal/categoria/Economia/Semillas.php';
    include_once 'Vistas/modal/categoria/Economia/Servicios.php';
    include_once 'Vistas/modal/categoria/Economia/Sistema_cestas.php';
    include_once 'Vistas/modal/categoria/Economia/Tianguis_orgánico.php';
    include_once 'Vistas/modal/categoria/Educacion/Basica.php';
    include_once 'Vistas/modal/categoria/Educacion/Comunidad_aprendizaje.php';
    include_once 'Vistas/modal/categoria/Educacion/Educación_ambiental.php';
    include_once 'Vistas/modal/categoria/Educacion/Escuela_campesina.php';
    include_once 'Vistas/modal/categoria/Educacion/Escuela_indígena.php';
    include_once 'Vistas/modal/categoria/Educacion/Formación_técnicos.php';
    include_once 'Vistas/modal/categoria/Educacion/Iniciativa_campesino_campesino.php';
    include_once 'Vistas/modal/categoria/Educacion/Media_Superior.php';
    include_once 'Vistas/modal/categoria/Educacion/Otro.php';
    include_once 'Vistas/modal/categoria/Educacion/Superior.php';
    include_once 'Vistas/modal/categoria/SistemasAgroforestales/Barreras_rompevientos.php';
    include_once 'Vistas/modal/categoria/SistemasAgroforestales/Cercas_vivas.php';
    include_once 'Vistas/modal/categoria/SistemasAgroforestales/Barreras_rompevientos.php';
    include_once 'Vistas/modal/categoria/SistemasAgroforestales/Montes_áreas_comunes_destinadas_conservación_sistemas_silvopastoriles.php';
    include_once 'Vistas/modal/categoria/SistemasAgroforestales/Sistemas_Agroforestales_Tradicionales.php';
    include_once 'Vistas/modal/categoria/SistemasAgroforestales/Otro.php';
    include_once 'Vistas/modal/categoria/Productos_forestales/Otro.php';
    include_once 'Vistas/modal/categoria/Productos_forestales/Productos_forestables_maderables.php';
    include_once 'Vistas/modal/categoria/Productos_forestales/Productos_forestales_no_maderables.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Abejas_europeas.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Abejas_nativas.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Acuacultura.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Aves.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Bovinos.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Conejo.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Caprinos.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Cunicultura.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Equinos.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Lombricultura.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Ovinos.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Pesca.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Porcino.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Mejoramiento genético.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Nutrición_Animal.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Prácticas_Salud_Animal.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Veterinaria_Natural.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Homeopatia.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Construcción_mantenimiento_instalaciones.php';
    include_once 'Vistas/modal/categoria/SistemasProducciónAnimal/Otro.php';
    include_once 'Vistas/modal/categoria/todos/Todos.php';




    ?>

    <?php
    include_once './JS/Mapping.php';

    ?>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!--Nueva libreria para carrusel -->
    <script src="https://cdn.jsdelivr.net/npm/glider-js@1.7.3/glider.min.js"></script>


</body>



</html>