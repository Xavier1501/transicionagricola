<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Informe de resultados </title>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <!--p> templete a utiliza  -- https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_webpage&stacked=h</p-->
    <!--script type="text/javascript" src="js/cargarmapa.js"></script-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>
    <script id="marc" type="text/javascript" src="js/marcadores.js"></script>
    <script type="text/javascript" src="js/evento_publico.js"></script>
    <script src="js/markerclusterer.min.js"></script>
    <!--script type="text/javascript" src="js/graficas.js"></script-->
    <!--libreria para el sobreposicionamiento-->
    <!--script  id="oms" src="https://jawj.github.io/OverlappingMarkerSpiderfier/bin/oms.min.js"></script-->

    <link rel="stylesheet" href="css/styles.css">
   

</head>

<body>



<style>
    .bg-uv{
        background-color: #18529D !important ;
    }

    .img-uv{
        max-width: 100%;
        width: 8%;

    }


    /*Estilos para el menu */
    .menu-perfil ul {
        position: relative;
        display: flex;
        justify-content: flex-end;
        top: 11px;
    }

    .menu-perfil ul li{
        text-decoration: none;
        display: inline-block;
        padding: 1rem;
    }
    .menu-perfil ul li a{
        text-decoration: none;
        color: #ffffff;
    }

    .font-uv{
        font-size: 1.2rem;
        text-align: justify;
    }



    /*

    Codigo proyecto
    */

    @import url("https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
    /*cargar mapa*/
    #map{
        height: 570px;
        /*height: 550px;*/
        /*width: 1046px;*/
        background-color: #B3B3B3;
        box-sizing: content-box;
        width: 103%;
        border:1px solid;
        margin: 0px 15px 0px -15px;
        /*margin: 0px 0px 0px -15px;*/
    }

    body{margin-top:50px;}
    .glyphicon { margin-right:10px; }
    .panel-body { padding:0px; }
    .panel-body table tr td { padding-left: 15px }
    .panel-body .table {margin-bottom: 0px;
    }

    #contenido{
        /*border-top-style: solid;
       border-right-style: solid;
       border-bottom-style: solid;
       border-left-style: solid;*/
    }

    /*estilos de los checkbox*/
    label{
        position: relative;
        cursor: pointer;
        color: #666;
        font-size: 15px;
    }

    input[type="checkbox"], input[type="radio"]{
        position: absolute;
        right: 9000px;
    }

    input[type="checkbox"] + .label-text:before{
        content: "\f096";
        font-family: "FontAwesome";
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing:antialiased;
        width: 1em;
        display: inline-block;
        margin-right: 5px;
    }

    input[type="checkbox"]:checked + .label-text:before{
        content: "\f14a";
        color: #2980b9;
        animation: effect 250ms ease-in;
    }

    input[type="checkbox"]:disabled + .label-text{
        color: #aaa;
    }

    input[type="checkbox"]:disabled + .label-text:before{
        content: "\f0c8";
        color: #ccc;
    }




    /*img{
      height: 60px;
      width: 50%;
    }*/


    /*tamaño de las ventanas modales*/
    #tamano{
        /*width: 70% !important;*/
        width: 80% !important;
    }
    #modalinstrucciones{
        width: 65% !important;
    }
    /*estilos del header y su contenido*/
    header{
        /*background: #333;*/
        /*color: white;*/
        /*height: 80px;
        width: 100%;
        left: 0;
        top: 0;
        position: fixed;*/
        box-shadow: 3px 3px 0px #F7F7F7;
        border-bottom-color: #F8F8F8;

        margin-top: -70px;
    }
    nav{
        /*border: 1px solid #ECC2C2;*/
        display: inline-block;
        width: 100%;
    }
    nav ul{
        /*border: 1px solid #90DFFC;*/
        list-style-type: none;
        width: 100%;
    }
    nav li{
        /*border: 1px solid #A8E9C7;*/
        display: inline-block;
    }


    /*estilo de las graficas*/
    .card{
        /*margin-top: 10px;*/
        border-top-style: 1px solid #E3E3E3;
        border-right-style: 1px solid;
        border-bottom-style: 1px solid;
        border-left-style: 1px solid
    }
    .card-header{
        text-align: center;
    }
    #chartContainer{
        /*height: 400px;
        width: 400px;*/
        border: white 1px groove;
        /*margin-top: 10px;
        border-top-style: 1px solid;
        border-right-style: 1px solid;
        border-bottom-style: 1px solid;
        border-left-style: red 5px solid;*/
    }
    #chartContainer2{
        margin-top: 10px;
        border: white 1px groove;
        /*border-top-style: solid;
        border-right-style: solid;
        border-bottom-style: solid;
        border-left-style: solid;*/
        /* height: 300px;*/
    }
    #municip{
        margin-top: 10px;
        border: white 1px groove;
        /*border-top-style: solid;
        border-right-style: solid;
        border-bottom-style: solid;
        border-left-style: solid;*/
        /* height: 300px;*/
    }
    #chartContainer3{
        margin-top: 10px;
        /*border-top-style: solid;
        border-right-style: solid;
        border-bottom-style: solid;
        border-left-style: solid;*/
        /* height: 300px;*/
        border: white 1px groove;
    }
    .card-body{
        /*height: 400px;
        width: 400px*/
        margin-top: 10px;
        border-top-style: solid;
        border-right-style: solid;
        border-bottom-style: solid;
        border-left-style: solid
    }
    .btn-sm{
        width: 50%;
    }
    #titulo{
        text-align: center;

    }
    #cuo{
        height: 50px;
        margin-top: 13px;
    }
    #recult{
        width: 35%;
    }
    #tab{
        border-collapse: separate;
    }
    #divcont{
        /*altura*/
        height: 435px;
        /*ancho*/
        width: 100%;
        /*background-color: #DADADA;*/
        border: white 1px groove;
    }

    #divcont3{
        /*altura*/
        /*height: 473px;
        /*ancho*/
        width: 100%;
        /*background-color: #DADADA;*/
        border: white 1px groove;
    }

    #bd{

        text-decoration: none;
        background-color: #FEFEFE;
    }
    #bd:hover{
        /*color: #F8F8F8;
        background-color: #545353;*/
        /*opacity: 0.6;*/
        color: #F9FEFD;
        background-color: rgba(92, 192, 164, 1);

        /*color: rgba(92, 192, 164, 1);*/
    }

    #accordion{
        background-color: #3D3C3C;
        border: white 1px groove;

    }
    tr{

    }
    tr:hover{
        color: #08A070;
        background-color: #EDECEC;
    }
    #dom{
        color: #0F0E0E;
        text-decoration: none;
        /* background-color: #0A0909;*/
    }

    #g,#n,#e,#a,#i,#j,#m,#am,#pg,#o,#d,#ed,#ex,#cl,#pr,#ot,#ct,#fes,#for,#pre,#prod,#rcion,#tro,#tt{
        font-size: 20px;
        cursor: pointer;
    }

    #cambioanio{
        /*border: 2px solid transparent;*/
        width: 100px;
        /*background-color: #C6C5C5;*/
        border-radius: 5px;
        border: 1px solid #39c;
    }
    #btn_anio{
        width: 60px;
        /*background-color: #C6C5C5;*/
        border-radius: 5px;
        border: 1px solid #39c;
    }

    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        /*height: 40px;*/
        background-color:#EFF2F3;
        color:;
        text-align: center;
    }
    #foo{
        text-decoration: none;
    }

    /*Aqui empiezan los estios para los mensajes emergentes*/
    .too {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;

    }

    .too .tool {
        visibility: hidden;
        width: 318px;
        height: 190px;
        background-color: black;
        color: #fff;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;
        text-align: center;
        position: absolute;
        z-index: 1;
    }

    .too:hover .tool {
        visibility: visible;
    }
    /*//////////////////*/
    .tro {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .tro .toot {
        visibility: hidden;
        width: 318px;
        height: 100px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;
        /*text-align: left;
      /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .tro:hover .toot {
        visibility: visible;
    }
    /*//////////////////*/
    .n {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .n .np {
        visibility: hidden;
        width: 318px;
        height: 70px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;
        /*text-align: left;
      /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .n:hover .np {
        visibility: visible;
    }
    /*//////////////////*/
    .n {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .n .np {
        visibility: hidden;
        width: 318px;
        height: 70px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;
        /* text-align: left;
       /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .n:hover .np {
        visibility: visible;
    }
    /*//////////////////*/
    .e {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .e .ed {
        visibility: hidden;
        width: 318px;
        height: 90px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;
        /* text-align: left;
       /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .e:hover .ed {
        visibility: visible;
    }
    /*//////////////////*/
    .a {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .a .aa {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .a:hover .aa {
        visibility: visible;
    }
    /*//////////////////*/
    .i {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .i .ii {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .i:hover .ii {
        visibility: visible;
    }

    /*//////////////////*/
    .j {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .j .jj {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .j:hover .jj {
        visibility: visible;
    }
    /*//////////////////*/
    .m {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .m .mm {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .m:hover .mm {
        visibility: visible;
    }
    /*//////////////////*/
    .am {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .am .amy {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .am:hover .amy {
        visibility: visible;
    }
    /*//////////////////*/
    .pg {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .pg .pgp {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .pg:hover .pgp {
        visibility: visible;
    }

    /*//////////////////*/
    .o {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .o .ol {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .o:hover .ol {
        visibility: visible;
    }

    /*//////////////////*/
    .d {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .d .df {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .d:hover .df {
        visibility: visible;
    }

    /*//////////////////*/
    .edt {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .edt .edn {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .edt:hover .edn {
        visibility: visible;
    }

    /*//////////////////*/
    .ex {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .ex .ec {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .ex:hover .ec {
        visibility: visible;
    }

    /*//////////////////*/
    .cl {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .cl .clc {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .cl:hover .clc {
        visibility: visible;
    }


    /*//////////////////*/
    .pr {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .pr .pq {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .pr:hover .pq {
        visibility: visible;
    }

    /*//////////////////*/
    .ot {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .ot .oto {
        visibility: hidden;
        width: 318px;
        height: 70px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .ot:hover .oto {
        visibility: visible;
    }

    /*//////////////////*/
    .ct {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .ct .cte {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .ct:hover .cte {
        visibility: visible;
    }

    /*//////////////////*/
    .fes {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .fes .fess {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .fes:hover .fess {
        visibility: visible;
    }

    /*//////////////////*/
    .for {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .for .fos {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .for:hover .fos {
        visibility: visible;
    }

    /*//////////////////*/
    .pre {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .pre .prp {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .pre:hover .prp {
        visibility: visible;
    }

    /*//////////////////*/
    .prod {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .prod .produ {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .prod:hover .produ {
        visibility: visible;
    }

    /*//////////////////*/
    .rcion {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .rcion .rciono {
        visibility: hidden;
        width: 318px;
        height: 40px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 150%;
        font-family: "Palatino", sans-serif;
        font-size: 13;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .rcion:hover .rciono {
        visibility: visible;
    }

</style>








<header class="banner">
    <?php
    include_once 'menu.php'
    ?>

    <div class="container-fliud">
        <div class="row">
            <nav>
                <center>
                    <ul>
                        <li>
                            <p><a href="#" id=""><img src="img/Luv.jpg" height="78" width="88"></a></p>
                        </li>
                        <!--li ><div><p style="border: 1px solid;" height="90"><h5>Cartografía de la gestión cultural.</h5>México, Red de Cultura Viva Comunitaria</p></div></li-->
                        <li>
                            <div>
                                <div height="90" width="280">
                                    <p>
                                    <h3><strong>Atlas de Iniciativas Agroecológicas en México</strong></h3>
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <p><a href="#" title="ir a la página"><img src="img/fao.png"
                                                                                           height="60" width="78"></a></p>
                        </li>
                        <li>
                            <p><a href="#"
                                  title="ir a la página"><img src="img/medioAmb.png" height="89" width="100"></a>
                            </p>
                        </li>
                        
                    </ul>
                </center>
            </nav>
        </div>
    </div>
</header>





<section class="menuHorizontal">

    <div class="container" id="m_2018">
        <div id=""></div>
        <div class="row" style="width: 100%;" id="">
            <div id="comunicacion"></div>
            <div id="div_008"></div>
            <div class="col-sm-3 col-md-3" style="background-color:" id="menu2018">
                <!--select name="cambioanio" id="cambioanio">
                     <option>2018</option>
                     <option>2019</option>
                   </select-->

                <div class="panel-group" id="accordion">


                    <!--p class="text-center" style="font-size: 11pt;color: #F5FFF6;"><strong>Eventos de los miembros de la Red 2018</strong></p-->
                    <div class="panel panel-default">
                        <div class="panel-heading" id="bd">
                            <h4 class="panel-title">
                                <a data-toggle="modal" href="#" data-target="#graf" id="dom"><span class="glyphicon glyphicon-list-alt" style="color: #088D4A;">
                      </span><strong>Instrucciones</strong></a>
                            </h4>
                        </div>
                        <div id="collapse6" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">

                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" id="bd">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" id="dom"><span class="glyphicon glyphicon-th" style="color: #088D4A;">
                      </span><strong>Experiencia Agroecologica</strong></a>&nbsp;
                                <!--i class="fa fa-question-circle-o" data-toggle="tooltip" title="Tipo" id="tooltip" style="  font-size: 20px; cursor: pointer;"></i-->
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table">
                                    <tr id="" class="">
                                        <td>
                                            <div class="form-check">
                                                <label>
                                                    <input type="checkbox" id="intangible" name="check"> <span class="">Definiciones</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>

                                    </tr>

                                    <tr>

                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>




                </div>
            </div>
            <!--Aqui esta el html donde se ejecutan las ventanas modales con las graficas de cada uno de los estados-->
            <div class="col-sm-9 col-md-9" id="contenido">
                <div id="map"></div>
            </div>
        </div>
    </div>



</section>







<script>
    var customLabel = {
        restaurant: {
            label: '1'
        },
        bar: {
            label: '2'
        }
    };

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(19.5214634, -96.9179886),
            zoom: 5
        });
        var infoWindow = new google.maps.InfoWindow;

        // Change this depending on the name of your PHP or XML file
        downloadUrl('resultado.php', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
                var name = markerElem.getAttribute('nombre');
                var address = markerElem.getAttribute('address');
                var type = markerElem.getAttribute('nombre');
                var point = new google.maps.LatLng(
                    parseFloat(markerElem.getAttribute('lat')),
                    parseFloat(markerElem.getAttribute('lng')));

                var infowincontent = document.createElement('div');
                var strong = document.createElement('strong');
                strong.textContent = name
                infowincontent.appendChild(strong);
                infowincontent.appendChild(document.createElement('br'));

                var text = document.createElement('text');
                text.textContent = address
                infowincontent.appendChild(text);
                var icon = customLabel[type] || {};
                var marker = new google.maps.Marker({
                    map: map,
                    position: point,
                    label: icon.label
                });
                marker.addListener('click', function() {
                    infoWindow.setContent(infowincontent);
                    infoWindow.open(map, marker);
                });
            });
        });
    }



    function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
            if (request.readyState == 4) {
                request.onreadystatechange = doNothing;
                callback(request, request.status);
            }
        };

        request.open('GET', url, true);
        request.send(null);
    }

    function doNothing() {}
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKrV88GHRaVGDBAmgj2_KmGnFnFOl4zvs&callback=initMap">
</script>



</body>

</html>



