<?php

//session_start();
include_once 'ConexionBD.php';
$user = $_SESSION['Usuario'];
$id = $_SESSION['idEntrevista'];

//var_dump($user);
//var_dump($id);



$sql_id = "select cae.idEntrevista as id from co_agr_entrevista cae where cae.nombreUsuario = '$user'";

$query_id = $conexion->query($sql_id);
$array = $query_id->fetch_array(MYSQLI_ASSOC);
$result_id = $array['id'];

//var_dump($result_id);

$sql = "
select 
cas.Opciones as siemple,
cas.idEntrevista as idE,
cas.idSeguridad as id
from co_agr_entrevista cae
left join co_agr_seguridad cas
on cae.idEntrevista = cas.idEntrevista where cas.idEntrevista = '$result_id'
";





$query = $conexion->query($sql);
$entrevistaArray = $query->fetch_array(MYSQLI_ASSOC);
//var_dump($entrevistaArray);
?>

<style>

    a{
        text-decoration: none;
        color: #fff;
    }
</style>

<fieldset id="8">

    <form id="register_form" novalidate method="post">
        <div class="container">
            <div class="row">

                <input type="hidden" class="form-control" name="idfase8" id="idfase8" value="<?php echo $id = (!empty($entrevistaArray['id'])) ? $entrevistaArray['id'] : '' ?>">


                <br>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:80%">
                        90%
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <h2 class="text-center">Seguridad</h2>
                </div>
                <!--Pregunta_1_seguridad-->
                <br>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                        <label>
                            40.- ¿El contexto de seguridad en su región permite que se lleven a cabo intercambios y/o visitas de otras experiencias?
                        </label>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 seguridad">
                        <ul>
                            <li><label><input type="radio" name="seguridad" id="seguridad1" value="Siempre" <?php echo $siemple = ($entrevistaArray['siemple'] === 'Siempre') ? 'checked' : '' ?>> Siempre</label></li>
                            <li><label><input type="radio" name="seguridad" id="seguridad2" value="Ocasionalmente" <?php echo $siemple = ($entrevistaArray['siemple'] === 'Ocasionalmente') ? 'checked' : '' ?>> Ocasionalmente</label></li>
                            <li><label><input type="radio" name="seguridad" id="seguridad3" value="Nunca" <?php echo $siemple = ($entrevistaArray['siemple'] === 'Nunca') ? 'checked' : '' ?>> Nunca</label></li>

                        </ul>
                    </div>
                    <hr>
                </div>
                <!--//Pregunta_1_seguridad-->
            </div>
        </div>

    </form>
    <div class="container">
        <div class="row">

            <a href="Preguntas-fase7.php" class="btn btn-default"> Regresar </a>

            <a href="#modal-id" data-toggle="modal">
                <button type="button" class="btn btn-info" id="guardarFase8">Siguiente</a></button>

            <?php include 'Modal-Cargado.php' ?>

            <?php include 'btn-subir.php' ?>

        </div>
    </div>
</fieldset>