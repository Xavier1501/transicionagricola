
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2 class="text-center">Gracias por Participar</h2>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <a href="index.php">
                <div class="card">
                    <img class="card-img-top img-responsive" src="./img/login.png" alt="">
                    <div class="card-body">
                        <h2 class="card-title text-center">Ingreso al sistema</h2>

                    </div>
                </div>
            </a>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <a href="informe.php">
                <div class="card">
                    <img class="card-img-top img-responsive" src="./img/informe.png" alt="">
                    <div class="card-body">
                        <h2 class="card-title text-center">Información del Mapa</h2>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>