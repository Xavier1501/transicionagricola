<?php
include_once 'ConexionBD.php';
$user = $_SESSION['Usuario'];





$sql_id = "select cae.idEntrevista as id from co_agr_entrevista cae where cae.nombreUsuario = '$user'";

$query_id = $conexion->query($sql_id);
$array = $query_id->fetch_array(MYSQLI_ASSOC);
$result_id = $array['id'];

//var_dump($result_id);

$sql = "

select cae.NombreGrupo as grupo, 
cae.NombreRepresentante as representante,
cae.Correo as correo,
cae.SitioWebPerfil as internet,
cae.SitioFacebook as facebook,
cae.instagram as instagram,
cae.Direccion as direccion,
cae.estado as estado,
cae.municipio as municipio,
cae.CodigoPostal as cp,
cae.idEntrevista as id
from co_agr_usuario cau 
left join co_agr_entrevista cae 
on cau.idUsuario = cae.idUser 
where cae.nombreUsuario = '$user'
";




$query = $conexion->query($sql);
$entrevistaArray = $query->fetch_array(MYSQLI_ASSOC);
//var_dump($entrevistaArray);
//var_dump($sql);


?>

<style>

    a{
        text-decoration: none;
        color: #fff;
    }
</style>


<div class="container">
    <div class="row">





        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <fieldset id="1">
                <br>

                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar"
                        aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:10%">
                        10%
                    </div>
                </div>


                <form id="register_form" novalidate method="post">
                    <div class="col-md-12 text-center">
                        <h2>Proceso de llenado de la ficha de Sistematización</h2>
                        <p class="text-center">
                            <h3>
                            Datos de identificación

                        </h3>


                            <br>

                        </p>
                        <input type="hidden" class="form-control" name="id" id="id"  value="<?php echo $id = (!empty($entrevistaArray['id']) )? $entrevistaArray['id']:'' ?>" required>



                    </div>
                    <br>
                    <!---Nombre Grupo--->
                    <div class="form-group row border-bottom pb-2" >
                        <label for="name" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                            Nombre del grupo:
                        </label>
                        <div class="col-sm-12 col-md-8 col-lg-9" id="nameDiv">
                                <input type="text" class="form-control" name="name" id="name" value="<?php echo$grupo = (!empty($entrevistaArray['grupo']) )? $entrevistaArray['grupo']:'' ?>" required>
                        </div>
                    </div>
                    <!---Nombre representante---->
                    <div class="form-group row border-bottom pb-2" id="">
                        <label for="representante" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                            Nombre de la persona representante:
                        </label>
                        <div class="col-sm-12 col-md-8 col-lg-9" id="repreDiv">
                            <input type="text" class="form-control" name="representante" id="representante" value="<?php echo $representante = (!empty($entrevistaArray['representante']) )? $entrevistaArray['representante']:'' ?>" required>
                        </div>
                    </div>

                    <!---Correo Electronico--->
                    <div class="form-group row border-bottom pb-2" >
                        <label for="correoElectronico" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                            Correo electrónico:
                        </label>
                        <div class="col-sm-12 col-md-8 col-lg-9" id="correoDiv" >
                            <input type="email" class="form-control" name="correo" id="correo" placeholder="correo@ejemplo.com" value="<?php echo$correo = (!empty($entrevistaArray['correo']) )? $entrevistaArray['correo']:'' ?>" required>
                        </div>
                    </div>
                    <!---Sitio Internet--->
                    <div class="form-group row border-bottom pb-2" >
                        <label for="sitioInternet" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                            Sitio de Internet:
                        </label>
                        <div class="col-sm-12 col-md-8 col-lg-9" id="InterDiv">
                            <input type="text" class="form-control" name="urlInter" id="urlInter" placeholder="https://www.google.com.mx/" value="<?php echo $internet = (!empty($entrevistaArray['internet']) )? $entrevistaArray['internet']:'' ?>" required>
                        </div>


                    </div>

                    <div class="form-group row border-bottom pb-2">
                        <label for="sitioInternet" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                            Página de Facebook:
                        </label>
                        <div class="col-sm-12 col-md-8 col-lg-9" id="faceDiv">
                            <input type="text" class="form-control" name="urlSocial" id="urlSocial" placeholder="https://www.facebook.com/" value="<?php echo $facebook = (!empty($entrevistaArray['facebook']) )? $entrevistaArray['facebook']:'' ?>" required>
                        </div>
                    </div>

                    <div class="form-group row border-bottom pb-2">
                        <label for="sitioInternet" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                            Instagram:
                        </label>
                        <div class="col-sm-12 col-md-8 col-lg-9" id="IntamDiv">
                            <input type="text" class="form-control" name="ins" id="ins" placeholder="https://www.instagram.com/" value="<?php echo $facebook = (!empty($entrevistaArray['instagram']) )? $entrevistaArray['instagram']:'' ?>" required>
                        </div>
                    </div>


                    <div class="form-group row border-bottom pb-2">
                        <label for="sitioInternet" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                            Estado:
                        </label>
                        <div class="col-sm-12 col-md-8 col-lg-9" id="estadoDiv">
                            <input type="text" class="form-control" name="estado" id="estado" value="<?php echo$grupo = (!empty($entrevistaArray['estado']) )? $entrevistaArray['estado']:'' ?>" required>
                        </div>
                    </div>

                    <div class="form-group row border-bottom pb-2">
                        <label for="sitioInternet" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                            Municipio:
                        </label>
                        <div class="col-sm-12 col-md-8 col-lg-9" id="municipioDiv">
                            <input type="text" class="form-control" name="municipio" id="municipio" value="<?php echo$grupo = (!empty($entrevistaArray['municipio']) )? $entrevistaArray['municipio']:'' ?>" required>
                        </div>
                    </div>


















                    <!--Direccion-->
                    <div class="form-group col-md-6" id="direccionDiv">
                        <label for="exampleInputName2">Dirección:</label>
                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder=""
                               value="<?php echo $direccion = (!empty($entrevistaArray['direccion']) )? $entrevistaArray['direccion']:'' ?>">
                    </div>
                    <!--/Direccion-->

                    <!--Codigo Postal-->
                    <div class="form-group col-md-6" id="cpDiv">
                        <label for="exampleInputName2">Código Postal:</label>
                        <input type="text" class="form-control" id="cp" name="cp" maxlength="5" placeholder=""
                        value="<?php echo $cp = (!empty($entrevistaArray['cp']) )? $entrevistaArray['cp']:'' ?>">
                    </div>
                    <!--/Codigo Postal-->

                </form>

                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 " >

                                <button type="button" class="btn btn-info " id="guardarFase1">Siguiente</button>



                        </div>

                    </div>
                </div>



            </fieldset>
        </div>


    </div>
</div>





<?php include 'Modal-Cargado.php' ?>
<?php include 'modal-Conexion.php' ?>