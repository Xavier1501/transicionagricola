<?php
//session_start();
include_once 'ConexionBD.php';
$user = $_SESSION['Usuario'];
$id = $_SESSION['idEntrevista'];

//var_dump($user);
//var_dump($id);

$sql_id = "select cae.idEntrevista as id from co_agr_entrevista cae where cae.nombreUsuario = '$user'";

$query_id = $conexion->query($sql_id);
$array = $query_id->fetch_array(MYSQLI_ASSOC);
$result_id = $array['id'];

//var_dump($result_id);

$sql = "
select * from 
co_agr_entrevista cae 
LEFT JOIN co_agr_nucleoagrario can ON cae.idEntrevista = can.IdTipoOrganizacion
LEFT JOIN co_agr_fundacion caf ON cae.idEntrevista = caf.IdTipoOrganizacion
left join co_agr_gobierno cag on cae.idEntrevista = cag.IdTipoOrganizacion
left join co_agr_institucioneducativa caie on cae.idEntrevista = caie.IdTipoOrganizacion
left join co_agr_tipoorganizacion catp on cae.idEntrevista = catp.idTipoColectivo
left join co_agr_cooperativa_colectivo cacc on cae.idEntrevista = cacc.idTipoColectivo
left join co_agr_empresa_colectivo caec on cae.idEntrevista = caec.idTipoColectivo
left join co_agr_grupos_organizados cagu on cae.idEntrevista = cagu.idTipoColectivo
left join co_agr_tipocolectivoorganizacion catpo on cae.idEntrevista = catpo.idEntrevista 
where cae.idEntrevista = '$result_id'
";
$query = $conexion->query($sql);
$entrevistaArray = $query->fetch_array(MYSQLI_ASSOC);
//var_dump($entrevistaArray);

?>

<style>

    a{
        text-decoration: none;
        color: #fff;
    }
</style>



<fieldset id="3">
    <form id="register_form" novalidate method="post">
    <br>
    <div class="container">
        <div class="row">

            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar"
                     aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:30%">
                    30%
                </div>
            </div>
            <h2 class="text-center">  Tipo de colectivo y/o organización</h2>

            <div class="col-md-6">
                <div class="panel-group">

                    <input type="hidden" class="form-control"  name="idNucleoAgrario" id="idNucleoAgrario" value="<?php echo $idNucleoAgrario = (!empty($entrevistaArray['idNucleoAgrario']) )? $entrevistaArray['idNucleoAgrario']:'' ?>">
                    <input type="hidden" class="form-control"  name="idFundacion" id="idFundacion" value="<?php echo $idFundacion = (!empty($entrevistaArray['idFundacion']) )? $entrevistaArray['idFundacion']:'' ?>">
                    <input type="hidden" class="form-control"  name="idGobierno" id="idGobierno" value="<?php echo $idGobierno = (!empty($entrevistaArray['idGobierno']) )? $entrevistaArray['idGobierno']:'' ?>">
                    <input type="hidden" class="form-control"  name="idInstitucionEducativa" id="idInstitucionEducativa" value="<?php echo $idInstitucionEducativa = (!empty($entrevistaArray['idInstitucionEducativa']) )? $entrevistaArray['idInstitucionEducativa']:'' ?>">
                    <input type="hidden" class="form-control"  name="idTipoOrganizacion" id="idTipoOrganizacion" value="<?php echo $idTipoOrganizacion= (!empty($entrevistaArray['idTipoOrganizacion']) )? $entrevistaArray['idTipoOrganizacion']:'' ?>">
                    <input type="hidden" class="form-control"  name="idCooperativaColectivo" id="idCooperativaColectivo" value="<?php echo $idCooperativaColectivo= (!empty($entrevistaArray['idCooperativaColectivo']) )? $entrevistaArray['idCooperativaColectivo']:'' ?>">
                    <input type="hidden" class="form-control"  name="idEmpresaColectivo" id="idEmpresaColectivo" value="<?php echo $idEmpresaColectivo= (!empty($entrevistaArray['idEmpresaColectivo']) )? $entrevistaArray['idEmpresaColectivo']:'' ?>">
                    <input type="hidden" class="form-control"  name="idGruopoOrganizado" id="idGruopoOrganizado" value="<?php echo $idGruopoOrganizado= (!empty($entrevistaArray['idGruopoOrganizado']) )? $entrevistaArray['idGruopoOrganizado']:'' ?>">
                    <input type="hidden" class="form-control"  name="idTipoColectivoOrganizacion" id="idTipoColectivoOrganizacion" value="<?php echo $idTipoColectivoOrganizacion= (!empty($entrevistaArray['idTipoColectivoOrganizacion']) )? $entrevistaArray['idTipoColectivoOrganizacion']:'' ?>">


                    <!--Nucleo Agrario-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="" href="#20">
                                    Núcleo Agrario
                                </a>
                            </h4>
                        </div>
                        <div id="20" class="panel-collapse">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="nucAgri1" id="nucAgri1" <?php echo $BienesComunales =  (!empty($entrevistaArray['bienesComunales']) )? 'checked':'' ?> > Bienes Comunales </li>
                                    <li> <input type="checkbox" name="nucAgri2" id="nucAgri2" <?php echo $Ejido =  (!empty($entrevistaArray['Ejido']) )? 'checked':'' ?> > Ejido </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Nucleo AAgrario-->

                    <!--Fundación-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="" href="#21">
                                    Fundación</a>
                            </h4>
                        </div>
                        <div id="21" class="panel-collapse">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="fundacion1" id="fundacion1" <?php echo $Nacional =  (!empty($entrevistaArray['Nacional']) )? 'checked':'' ?> > Nacional </li>
                                    <li> <input type="checkbox" name="fundacion2" id="fundacion2" <?php echo $Internacional =  (!empty($entrevistaArray['Internacional']) )? 'checked':'' ?> > Internacional </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Fundación-->

                    <!--Gobierno-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="" href="#23">Gobierno</a>
                            </h4>
                        </div>
                        <div id="23" class="panel-collapse">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="gobierno1" id="gobierno1" <?php echo $Municipio =  (!empty($entrevistaArray['Municipio']) )? 'checked':'' ?> > Municipal </li>
                                    <li> <input type="checkbox" name="gobierno2" id="gobierno2" <?php echo $Estatal =  (!empty($entrevistaArray['Estatal']) )? 'checked':'' ?> > Estatal </li>
                                    <li> <input type="checkbox" name="gobierno3" id="gobierno3" <?php echo $Federal =  (!empty($entrevistaArray['Federal']) )? 'checked':'' ?> > Federal </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Gobierno-->

                    <!--Instituciones Educativas-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="" href="#24">Instituciones Educativas</a>
                            </h4>
                        </div>
                        <div id="24" class="panel-collapse">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="intEduc1" id="intEduc1" <?php echo $EscuelaCampesina =  (!empty($entrevistaArray['EscuelaCampesina']) )? 'checked':'' ?> > Escuela Campesina</li>
                                    <li> <input type="checkbox" name="intEduc2" id="intEduc2" <?php echo $EscuelaPrimaria =  (!empty($entrevistaArray['EscuelaPrimaria']) )? 'checked':'' ?> > Escuela Primaria</li>
                                    <li> <input type="checkbox" name="intEduc3" id="intEduc3" <?php echo $EscuelaSecundaria =  (!empty($entrevistaArray['EscuelaSecundaria']) )? 'checked':'' ?> > Escuela Secundaria</li>
                                    <li> <input type="checkbox" name="intEduc4" id="intEduc4" <?php echo $Bachillerato =  (!empty($entrevistaArray['Bachillerato']) )? 'checked':'' ?> > Bachillerato</li>
                                    <li> <input type="checkbox" name="intEduc5" id="intEduc5" <?php echo $Universidad =  (!empty($entrevistaArray['Universidad']) )? 'checked':'' ?> > Universidad</li>
                                    <li> <input type="checkbox" name="intEduc6" id="intEduc6" <?php echo $UniversidadIntercultural =  (!empty($entrevistaArray['UniversidadIntercultural']) )? 'checked':'' ?> > Universidad Intercultural</li>
                                    <li> <input type="checkbox" name="intEduc7" id="intEduc7" <?php echo $UniversidadAlternativa =  (!empty($entrevistaArray['UniversidadAlternativa']) )? 'checked':'' ?> > Universidad Alternativa</li>
                                    <li> <input type="checkbox" name="intEduc8" id="intEduc8" <?php echo $CentroFormacionPrivada =  (!empty($entrevistaArray['CentroFormacionPrivada']) )? 'checked':'' ?> > Centro de Formación privado</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Instutciones Educativas-->

                    <!--Tipo de Organización-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="" href="#23">Tipo de Organización</a>
                            </h4>
                        </div>
                        <div id="23" class="panel-collapse">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="TipoOrga1" id="TipoOrga1" <?php echo $OrganizacionCampesina =  (!empty($entrevistaArray['OrganizacionCampesina']) )? 'checked':'' ?> > Organización Campesina </li>
                                    <li> <input type="checkbox" name="TipoOrga2" id="TipoOrga2" <?php echo $SociedadCivil =  (!empty($entrevistaArray['SociedadCivil']) )? 'checked':'' ?> > De la Sociedad Civil Organizada </li>
                                    <li> <input type="text" name="TipoOrga3" id="TipoOrga3" value="<?php echo $OtroTipoOrganizacion = (!empty($entrevistaArray['OtroTipoOrganizacion']) )? $entrevistaArray['OtroTipoOrganizacion']:'' ?>" > Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-6">
                <div class="panel-group">

                    <!--Cooperativa-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="" href="#23">Cooperativa</a>
                            </h4>
                        </div>
                        <div id="23" class="panel-collapse">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="coop1" id="coop1" <?php echo $Consumo =  (!empty($entrevistaArray['Consumo']) )? 'checked':'' ?> > De Consumo </li>
                                    <li> <input type="checkbox" name="coop2" id="coop2" <?php echo $Produccion =  (!empty($entrevistaArray['Produccion']) )? 'checked':'' ?> > De Producción </li>
                                    <li> <input type="text" name="coop3" id="coop3"  value="<?php echo $OtroCooperativaColectivo = (!empty($entrevistaArray['OtroCooperativaColectivo']) )? $entrevistaArray['OtroCooperativaColectivo']:'' ?>" > Otro: (Especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Cooperativa-->

                    <!--Tipo Empresa-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="" href="#23"> Tipo de Empresa </a>
                            </h4>
                        </div>
                        <div id="23" class="panel-collapse">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="empSoc1" id="empSoc1" <?php echo $Social =  (!empty($entrevistaArray['Social']) )? 'checked':'' ?> > Empresa Social </li>
                                    <li> <input type="checkbox" name="empSoc2" id="empSoc2" <?php echo $Privada =  (!empty($entrevistaArray['Privada']) )? 'checked':'' ?> > Empresa Privada </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Tipo Empresa-->


                    <!--Grupos organizados-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="" href="#23">Grupos Organizados</a>

                            </h4>
                        </div>
                        <div id="23" class="panel-collapse">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="grupOrga1" id="grupOrga1" <?php echo $AsesorTecnico =  (!empty($entrevistaArray['AsesorTecnico']) )? 'checked':'' ?> > Grupo de Asesores Tecnicos </li>
                                    <li> <input type="checkbox" name="grupOrga2" id="grupOrga2" <?php echo $Consumidor =  (!empty($entrevistaArray['Consumidor']) )? 'checked':'' ?> > Grupo de Consumidores </li>
                                    <li> <input type="checkbox" name="grupOrga3" id="grupOrga3" <?php echo $investigacion =  (!empty($entrevistaArray['investigacion']) )? 'checked':'' ?> > Grupo de Investigación </li>
                                    <li> <input type="checkbox" name="grupOrga4" id="grupOrga4" <?php echo $Estudiantil =  (!empty($entrevistaArray['Estudiantil']) )? 'checked':'' ?> > Grupo Estudiantil </li>
                                    <li> <input type="checkbox" name="grupOrga5" id="grupOrga5" <?php echo $Eclesiastico =  (!empty($entrevistaArray['Eclesiastico ']) )? 'checked':'' ?> > Grupo Eclesiástico </li>
                                    <li> <input type="text" name="grupOrga6" id="grupOrga6" value="<?php echo $OtroGrupoOrganizacion= (!empty($entrevistaArray['OtroGrupoOrganizacion']) )? $entrevistaArray['OtroGrupoOrganizacion']:'' ?>" > Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Grupos Organizados-->

                    <!--Programa de extensión-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="" href="#23">Programa de Extensión</a>
                            </h4>
                        </div>
                        <div id="23" class="panel-collapse">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="progExt1" id="progExt1" <?php echo $PorgramaExtensionUniversitaria =  (!empty($entrevistaArray['PorgramaExtensionUniversitaria']) )? 'checked':'' ?> > Programa de Extensión </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Programa de extensión-->

                    <!--Otro-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="" href="#23">Otro</a>
                            </h4>
                        </div>
                        <div id="23" class="panel-collapse">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="text" name="otro_Colec1" id="otro_Colec1"  value="<?php echo $Otro= (!empty($entrevistaArray['Otro']) )? $entrevistaArray['Otro']:'' ?>" > Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Otro-->
                </div>
            </div>
        </div>
    </div>
    </form>


    <div class="container">
        <div class="row">

            <a href="Preguntas-fase2.php" class="btn btn-default"> Regresar </a>
            <!--<a href="Preguntas-fase4.php" class="btn btn-info"> siguiente </a>-->
            <a href="#modal-id" data-toggle="modal"><button type="button" class="btn btn-info" id="guardarFase3">Siguiente</a></button>

            <?php include 'Modal-Cargado.php' ?>
            <?php include 'btn-subir.php' ?>

        </div>
    </div>
</fieldset>