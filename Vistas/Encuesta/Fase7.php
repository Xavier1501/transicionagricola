<?php

//session_start();
include_once 'ConexionBD.php';
$user = $_SESSION['Usuario'];
$id = $_SESSION['idEntrevista'];

//var_dump($user);
//var_dump($id);



$sql_id = "select cae.idEntrevista as id from co_agr_entrevista cae where cae.nombreUsuario = '$user'";

$query_id = $conexion->query($sql_id);
$array = $query_id->fetch_array(MYSQLI_ASSOC);
$result_id = $array['id'];

//var_dump($result_id);

$sql = "
select 
*
from co_agr_entrevista cae
left join co_agr_formacion caf
on cae.idEntrevista = caf.idEntrevista where caf.idEntrevista = '$result_id'
";

$query = $conexion->query($sql);
$entrevistaArray = $query->fetch_array(MYSQLI_ASSOC);
//ar_dump($entrevistaArray);
?>

<style>

    a{
        text-decoration: none;
        color: #fff;
    }
</style>


<fieldset id="7">

    <form id="register_form" novalidate method="post">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <br>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                            80%
                        </div>
                    </div>
                    <h2 class="text-center">Formación</h2>
                </div>
                <!--Pregunta_1_Formacion-->
                <br>
                <input type="hidden" class="form-control" name="idfase7" id="idfase7" value="<?php echo $id = (!empty($entrevistaArray['id_formacion'])) ? $entrevistaArray['id_formacion'] : '' ?>">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                        <label>
                            33.- ¿En qué temas considera que su colectivo puede apoyar para formar otros grupos?
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <textarea class="form-control" rows="3" name="formacion1" id="formacion1"><?php echo $apoyo = (!empty($entrevistaArray['ApoyoColectivo'])) ? $entrevistaArray['ApoyoColectivo'] : '' ?></textarea>
                        <div class="text-right"><span id="car-for1" class="valid-text pt-3" id="txaCount"></span></div>

                        <br>
                    </div>

                </div>
                <!--//Pregunta_1_Formacion-->
                <!--Pregunta_2_Formacion-->
                <br>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12text-justify">
                        <label>
                            34.- ¿Qué temas de formación considera que son importantes para fortalecer el trabajo de su colectivo?
                        </label>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <textarea class="form-control" rows="3" name="formacion2" id="formacion2"><?php echo $fortaleza = (!empty($entrevistaArray['Fortaleza'])) ? $entrevistaArray['Fortaleza'] : '' ?></textarea>
                        <div class="text-right"><span id="car-for2" class="valid-text pt-3" id="txaCount"></span></div>

                        <br>
                    </div>

                </div>
                <!--//Pregunta_2_formacion-->
                <!--Pregunta_3_Formacion-->
                <br>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12text-justify">
                        <label>
                            35.- ¿Conoce los programas educativos que se implementan en las universidades públicas,
                            los bachilleratos y otras organizaciones educativas vinculadas con la agroecología?
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">



                        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 text-center">
                            <input type="radio" name="formacion3" id="formacion3" value="SI" <?php echo $siPrograma = ($entrevistaArray['si_Programas']=== 'SI' )? 'checked':''?>> Si

                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 text-center">
                            <input type="radio" name="formacion3" id="formacion3" value="NO" <?php echo $siPrograma = ($entrevistaArray['si_Programas']=== 'NO' )? 'checked':''?>> No
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">

                            <p id="vista5"
                                <?php echo $vista5 = ($entrevistaArray['si_Programas']=== 'NO' )?'style="display: none"':''?>
                            >
                                <label>Cuáles</label>
                                <textarea  class="form-control" name="formacion5" id="formacion5"><?php echo $EnfermosCuidadosEspeciapesPorQue = (!empty($entrevistaArray['cuales_programas']) )? $entrevistaArray['cuales_programas']:'' ?></textarea>
                            <div class="text-right"><span id="car-for5" class="valid-text pt-3" id="txaCount"></span></div>

                            <br>
                            </p>


                        </div>






                    </div>
                </div>
                <!--//Pregunta_3_formacion-->
                <!--Pregunta_4_Formacion-->
                <br>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12text-justify">
                        <label>
                            36.- ¿Ha tomado un curso o taller sobre agroecología?
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">

                        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 text-center">
                            <input type="radio" name="formacion6" id="formacion6" value="SI" <?php echo $si_taller = ($entrevistaArray['si_taller']=== 'SI ' )? 'checked':''?>> Si

                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 text-center">
                            <input type="radio" name="formacion6" id="formacion6" value="NO" <?php echo $i_taller = ($entrevistaArray['si_taller']=== 'NO ' )? 'checked':''?>> No
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">
                            <p id="vista6"
                                <?php echo $vista5 = ($entrevistaArray['si_taller']=== 'NO ' )?'style="display: none"':''?>
                            >
                                <label>¿En dónde y quién lo impartió?</label>
                                <textarea  class="form-control" name="formacion8" id="formacion8"><?php echo $cualesTaller = (!empty($entrevistaArray['Cuales_taller'])) ? $entrevistaArray['Cuales_taller'] : '' ?></textarea>
                            <div class="text-right"><span id="car-for8" class="valid-text pt-3" id="txaCount"></span></div>
                            <br>
                            </p>

                        </div>


                    </div>
                </div>

                <!--//Pregunta_4_formacion-->
                <!--Pregunta_5_Formacion-->
                <br>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12text-justify">
                        <label>
                            37.- ¿Qué propuestas educativas propondría para el fomento de la agroecología?
                        </label>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <textarea class="form-control" rows="3" name="formacion9" id="formacion9"><?php echo $educativa = (!empty($entrevistaArray['PropuestaEductiva'])) ? $entrevistaArray['PropuestaEductiva'] : '' ?></textarea>
                        <div class="text-right"><span id="car-for9" class="valid-text pt-3" id="txaCount"></span></div>

                        <br>
                    </div>

                </div>
                <!--//Pregunta_5_formacion-->
                <!--Pregunta_6_Formacion-->
                <br>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12text-justify">
                        <label>
                            38.- ¿Qué temas considera que su organización o colectivo puede apoyar en la capacitación de
                            otros grupos?
                        </label>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <textarea class="form-control" rows="3" name="formacion10" id="formacion10"><?php echo $temas = (!empty($entrevistaArray['Capacitacion'])) ? $entrevistaArray['Capacitacion'] : '' ?></textarea>
                        <div class="text-right"><span id="car-for10" class="valid-text pt-3" id="txaCount"></span></div>

                        <br>
                    </div>

                </div>
                <!--//Pregunta_6_formacion-->

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12text-justify">
                        <label>
                            39.- ¿Qué temas de capacitación considera que son importantes para fortalecer el trabajo de su organización o colectivo?
                        </label>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <textarea class="form-control" rows="3" name="formacion11" id="formacion11"><?php echo $capacitacion = (!empty($entrevistaArray['TemasApoyo'])) ? $entrevistaArray['TemasApoyo'] : '' ?></textarea>
                        <div class="text-right"><span id="car-for11" class="valid-text pt-3" id="txaCount"></span></div>

                        <br>
                    </div>

                </div>
            </div>
        </div>

    </form>
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                <a href="Preguntas-fase6.php" class="btn btn-default"> Regresar </a>


                <a href="#modal-id" data-toggle="modal"><button type="button" class="btn btn-info" id="guardarFase7">Siguiente</a></button>

                <?php include 'Modal-Cargado.php' ?>
                <br>

            </div>


            <?php include 'btn-subir.php' ?>

        </div>
    </div>
</fieldset>