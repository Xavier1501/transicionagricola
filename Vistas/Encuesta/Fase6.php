<?php

//session_start();
include_once 'ConexionBD.php';
$user = $_SESSION['Usuario'];
$id = $_SESSION['idEntrevista'];

//var_dump($user);
//var_dump($id);



$sql_id = "select cae.idEntrevista as id from co_agr_entrevista cae where cae.nombreUsuario = '$user'";

$query_id = $conexion->query($sql_id);
$array = $query_id->fetch_array(MYSQLI_ASSOC);
$result_id = $array['id'];

//var_dump($result_id);

$sql = "
select 
car.RelacionTrabajo as relacion,
car.FortalezaTrabajo as fortaleza,
car.TransicionEscala as escala,
car.ObstaculosTransicion as obstaculo,
car.idEntrevista as idFk,
car.id_redes as id
from co_agr_entrevista cae
left join co_agr_redes car
on cae.idEntrevista = car.idEntrevista where car.idEntrevista = '$result_id'
";

$query = $conexion->query($sql);
$entrevistaArray = $query->fetch_array(MYSQLI_ASSOC);
//var_dump($entrevistaArray);
?>

<style>

    a{
        text-decoration: none;
        color: #fff;
    }
</style>


<fieldset id="6">

    <form id="register_form" novalidate method="post">
    <div class="container">
        <div class="row">

            <br>
            <div class="progress">
                <div class="progress-bar progress-bar-striped active text-center" role="progressbar "
                     aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:60%">
                    70%
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                <h2 class="text-center">Tejiendo redes</h2>
            </div>

            <!--Pregunta_1_redes-->

            <input type="hidden" class="form-control"  name="idfase6" id="idfase6" value="<?php echo $id = (!empty($entrevistaArray['id']) )? $entrevistaArray['id']:'' ?>">

            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        29.- ¿Con quién se relaciona para realizar su trabajo, desde un enfoque agroecológico? (por ejemplo:
                        otros agricultores/as, otras organizaciones, escuelas o instancias de gobierno)
                    </label>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <textarea class="form-control" rows="3" name="redes1" id="redes1"><?php echo $relacion = (!empty($entrevistaArray['relacion']) )? $entrevistaArray['relacion']:'' ?></textarea>
                    <div class="text-right"><span id="car-rd1" class="valid-text pt-3" id="txaCount"></span></div>

                    <br>
                </div>

            </div>
            <!--//Pregunta_1_redes-->

            <!--Pregunta_2_redes-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-justify">
                    <label>
                       30.-  ¿Qué propondrían para fortalecer su trabajo en torno a la agroecología?
                    </label>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <textarea class="form-control" rows="3" name="redes2" id="redes2"><?php echo $fortaleza = (!empty($entrevistaArray['fortaleza']) )? $entrevistaArray['fortaleza']:'' ?></textarea>
                    <div class="text-right"><span id="car-rd2" class="valid-text pt-3" id="txaCount"></span></div>

                    <br>
                </div>

            </div>
            <!--//Pregunta_2_redes-->


            <!--Pregunta_3_redes-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        31.- ¿Qué se podría hacer para una transición agroecológica a escala de país/estado/municipio?
                    </label>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <textarea class="form-control" rows="3" name="redes3" id="redes3"><?php echo $escala = (!empty($entrevistaArray['escala']) )? $entrevistaArray['escala']:'' ?></textarea>
                    <div class="text-right"><span id="car-rd3" class="valid-text pt-3" id="txaCount"></span></div>

                    <br>
                </div>

            </div>
            <!--//Pregunta_3_redes-->

            <!--Pregunta_4_redes-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                       32.-  ¿Cuáles son los obstáculos para lograr una transición agroecológica nacional?
                    </label>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <textarea class="form-control" rows="3" name="redes4" id="redes4"><?php echo $obstaculo = (!empty($entrevistaArray['obstaculo']) )? $entrevistaArray['obstaculo']:'' ?></textarea>
                    <div class="text-right"><span id="car-rd4" class="valid-text pt-3" id="txaCount"></span></div>

                    <br>
                </div>

            </div>
            <!--//Pregunta_4_redes-->
        </div>
    </div>
    </form>
    <div class="container">
        <div class="row">
            <a href="Preguntas-fase5.php" class="btn btn-default"> Regresar </a>

            <!--<button type="" class=" btn btn-info" id="guardarFase5">Siguiente</button> -->

            <a href="#modal-id" data-toggle="modal"><button type="button" class="btn btn-info" id="guardarFase6">Siguiente</a></button>

            <?php include 'Modal-Cargado.php' ?>
            <?php include 'btn-subir.php' ?>
        </div>
    </div>

</fieldset>
