<?php

//session_start();
include_once 'ConexionBD.php';
$user = $_SESSION['Usuario'];
$id = $_SESSION['idEntrevista'];

//var_dump($user);
//var_dump($id);



$sql_id = "select cae.idEntrevista as id from co_agr_entrevista cae where cae.nombreUsuario = '$user'";

$query_id = $conexion->query($sql_id);
$array = $query_id->fetch_array(MYSQLI_ASSOC);
$result_id = $array['id'];

//var_dump($result_id);

$sql = "
select 
cap.idPolitica as idP,
cap.PoliticaActualFavor as pFavor,
cap.PoliticaActualLimitada as pActL,
cap.PoliticaFaltante as pFal,
cap.idEntrevista as idE
from co_agr_entrevista cae
left join co_agr_politica cap 
on cae.idEntrevista = cap.idEntrevista where cap.idEntrevista = '$result_id';


";
$query = $conexion->query($sql);
$entrevistaArray = $query->fetch_array(MYSQLI_ASSOC);
//var_dump($entrevistaArray);
?>



<style type="text/css">

    /*Aqui empiezan los estios para los mensajes emergentes*/
    .too {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;

    }

    .too .tool {
        visibility: hidden;
        width: 300px;
        height: 190px;
        background-color: black;
        color: #fff;
        border-radius: 6px;
        padding: 5px 5px 5px 5px;
        line-height: 90%;
        font-family: "Palatino", sans-serif;
        font-size: 13;
        text-align: center;
        position: absolute;
        z-index: 1;
    }

    .too:hover .tool {
        visibility: visible;
    }
    /*//////////////////*/
    .tro {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .tro .toot {
        visibility: hidden;
        width: 445px;
        height: 140px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 100%;
        font-family: "Palatino", sans-serif;
        font-size: 13px !importan;
        /*text-align: left;
      /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .tro:hover .toot {
        visibility: visible;
    }


     a{
         text-decoration: none;
         color: #fff;
     }


</style>

<fieldset id="5">
    <form id="register_form" novalidate method="post">
    <div class="container">
        <div class="row">
            <br>


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar"
                     aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                    60%
                </div>
            </div>
            </div>

            <h2 class="text-center"> Hacia una nueva política pública</h2>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                <strong>
                   Nos interesa mucho el poder compartir su palabra sobre sus experiencias y retos, por ello le rogaríamos nos ayude  contestando las siguientes preguntas:

                </strong>
            </div>

            <br>

            <!--Pregunta_1_politicas-->
            <br>

            <input type="hidden" class="form-control"  name="idfase5" id="idfase5" value="<?php echo $id = (!empty($entrevistaArray['idP']) )? $entrevistaArray['idP']:'' ?>">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                            26.- ¿Qué políticas públicas actuales considera que favorecen el impulso de la agroecología en su región y en el país?
                    </label>

                    &nbsp;<i id="g" class="fa fa-question-circle-o tro too">
                        <span class="tool"><h style="line-height: 0.2em; font-size: 12px;">“Las políticas públicas pueden ser entendidas como el conjunto de planes y programas de acción gubernamental destinados a la intervención en el dominio social (interés público), por medio de los cuales son trazadas las directrices y metas a ser fomentadas por el Estado, sobre todo en la implementación de los objetivos y derechos fundamentales dispuestos en la constitución” (Cristovam, 2005)</h></span></i>


                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <textarea class="form-control" rows="3" name="politicas1" id="politicas1"><?php echo $pFavor = (!empty($entrevistaArray['pFavor']) )? $entrevistaArray['pFavor']:'' ?></textarea>
                    <div class="text-right"><span id="car-pol1" class="valid-text pt-3" id="txaCount"></span></div>

                    <br>
                </div>

            </div>
            <!--//Pregunta_1_politicas-->

            <!--Pregunta_2_politicas-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        27.- ¿Qué políticas públicas actuales considera que pueden estar limitando el impulso de la agroecología en su región y en el país?
                    </label>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <textarea class="form-control" rows="3" name="politicas2" id="politicas2"><?php echo $pActL = (!empty($entrevistaArray['pActL']) )? $entrevistaArray['pActL']:'' ?></textarea>
                    <div class="text-right"><span id="car-pol2" class="valid-text pt-3" id="txaCount"></span></div>

                    <br>
                </div>

            </div>
            <!--//Pregunta_2_politicas-->

            <!--Pregunta_3_politicas-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        28.- ¿Qué faltaría hacer para implementar una política pública para fortalecer la transición agroecológica en el país?
                    </label>

                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <textarea class="form-control" rows="3" name="politicas3" id="politicas3"><?php echo $pFal = (!empty($entrevistaArray['pFal']) )? $entrevistaArray['pFal']:'' ?></textarea>
                    <div class="text-right"><span id="car-pol3" class="valid-text pt-3" id="txaCount"></span></div>

                    <br>
                </div>

            </div>
            <!--//Pregunta_3_politicas-->

        </div>
    </div>



    </form>

    <div class="container">
        <div class="row">

            <a href="Preguntas-fase4_1.php" class="btn btn-default"> Regresar </a>
            <!--<a href="Preguntas-fase6.php" class="btn btn-info"> siguiente </a>-->



            <a href="#modal-id" data-toggle="modal"><button type="button" class="btn btn-info" id="guardarFase5">Siguiente</a></button>

            <?php include 'Modal-Cargado.php' ?>

            <?php include 'btn-subir.php' ?>

        </div>
    </div>

</fieldset>
