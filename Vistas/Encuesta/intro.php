<style>
.img-intro-uv {
    max-width: 100%;
    height: auto;
    padding: 1rem;
    /*border-radius: 1.5em;*/
}

    .enlace{
        padding: 1rem;
    }
</style>

<section class="Introduccion">
    <div class="container">
        <div class="row">


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <h2>
                    Atlas de Transiciones Agroecológicas en México<br>
                </h2>
                <h3 style="font-weight: bold;">Sistema de registro de experiencias e iniciativas agroecológicas en
                    México</h3>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <img src="img/login/granos.jpg" class="img-intro-uv text-center" alt="img/login/granos.jpg">
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  text-center">
                    <a href="registro.php?valor=grupo" type="button" class="btn btn-primary "
                       style="font-size: 14px;">Registro  para participar como grupo</a>

                <a href="registro.php?valor=individual" type="button" class="btn btn-primary  "
                   style="font-size: 14px;">Registro para participar como iniciativa familiar</a>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 enlace text-center">
                <a href="inicio.php" type="button" class="btn btn-primary" style="font-size: 14px;">Inicie o continúe
                    <br>el registro de su experiencia o iniciativa </a>
            </div>
            <br>
            <br>


        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header text-center" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link  text-center " data-toggle="collapse"
                                    data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <h5 style="font-size: 16px;">Introducción y Objetivos</h5>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse in" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body" style="height: 750px; overflow: auto; padding: 3% 3% 3% 3%;">
                                <p class="text-justify">
                                    En el país se vienen generando las condiciones para contar con una política pública
                                    que
                                    favorezca procesos de transición agroecológica, la cual propone construirse de forma
                                    sinérgica
                                    entre el gobierno y la sociedad. Para ello se ha constituido un grupo técnico que
                                    elaborará una
                                    propuesta que contribuirá a la construcción de un <h
                                        style="color: #ccccc; font-weight: bold; "> Programa Nacional de Transición
                                        Agroecológica</h>
                                    intentando tomar en cuenta el mayor número de voces que están vinculadas con la
                                    agroecología o
                                    buscan hacerlo. Proponemos conocernos, re-conocernos y enredarnos entre las
                                    iniciativas que
                                    miran en la agroecología, la posibilidad de articular los procesos de producción y
                                    consumo
                                    pensando en el bien común para alcanzar la soberanía alimentaria y de salud. Es que
                                    desde esta
                                    iniciativa proponemos fortalecer el trabajo de redes, iniciativas o personas que de
                                    manera
                                    formal e informal vienen implementando propuestas con una perspectiva agroecológica
                                    y hacerlas
                                    visibles en un mapa de manera que facilite la articulación entre actores, mejore las
                                    acciones
                                    comunes y contribuya a encontrar visiones conjuntas hacia una política pública más
                                    cercana con
                                    la gente. Queremos que su experiencia, si así lo desea aparezca en este gran mapa de
                                    iniciativas agroecológicas, y pueda servir para:<br><br>
                                    <div class="col-lg-1 col-xs-1 text-right">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </div>
                                    <div class="col-lg-11 col-xs-11">
                                        Contribuir desde su experiencia y palabra a la construcción de una política
                                        pública a nivel nacional sobre agroecología.<br><br>
                                    </div>

                                    <div class="col-lg-1 col-xs-1 text-right">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </div>
                                    <div class="col-lg-11 col-xs-11">
                                        Identificar y promover experiencias clave -agronómicas, socioeconómicas,
                                        políticas- que contribuyan al planteamiento de soluciones a necesidades comunes
                                        en la búsqueda de la consolidación y el avance del movimiento agroecológico a
                                        nivel local, regional y nacional. Entre ellas las de los pueblos
                                        originarios-campesinos, como aporte a las estrategias de escalamiento hacia
                                        sistemas agroalimentarios sustentables.<br><br>
                                    </div>




                                    <div class="col-lg-1 col-xs-1 text-right">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </div>
                                    <div class="col-lg-11 col-xs-11">
                                        Identificar las iniciativas femeninas y su contribución al proceso de transición
                                        agroecológica.<br><br>
                                    </div>

                                    <div class="col-lg-1 col-xs-1 text-right">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </div>
                                    <div class="col-lg-11 col-xs-11">
                                        Ser un espacio de reflexión-acción en torno a la construcción de horizontes
                                        comunes hacia modelos de producción y consumo más sustentables, con miras a
                                        fortalecer los canales cortos de comercialización y generación de valor agregado
                                        a nivel local y regional.<br><br>
                                    </div>

                                    <div class="col-lg-1 col-xs-1 text-right">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </div>
                                    <div class="col-lg-11 col-xs-11">
                                        Servir como un medio de difusión, intercambio, encuentro y propuestas para la
                                        promoción y creación de alianzas de las experiencias agroecológicas en el
                                        país.<br><br>
                                    </div>

                                    <div class="col-lg-1 col-xs-1 text-right">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </div>
                                    <div class="col-lg-11 col-xs-11">
                                        Promover la articulación de las diferentes iniciativas de educación formal, no
                                        formal e informal, para consolidar un sistema mexicano de educación en
                                        agroecología.<br><br>
                                    </div>

                                    <div class="col-lg-1 col-xs-1 text-right">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </div>
                                    <div class="col-lg-11 col-xs-11">
                                        Co-construir un mapa y un sitio web que permita intercambiar publicaciones y
                                        boletines informativos que permitan dar a conocer los resultados del propio
                                        proceso de sistematización. Pensamos que si compartimos nuestras experiencias
                                        entre comunes, podremos aprender juntos sobre qué prácticas y estrategias se
                                        considera tienen valor para un proceso de transición agroecológica, porque han
                                        funcionado y han sido útiles y también señalar qué cosas necesitan mejorarse,
                                        qué cosas han salido mal, qué cosa pensamos que es importante incluir y qué
                                        elementos serían necesarios para construir una política pública incluyente y
                                        democrática.
                                    </div>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="accordion">

                    <div class="card">
                        <div class="card-header text-center" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed in" data-toggle="collapse"
                                    data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <h style="font-size: 16px;">Instrucciones generales</h>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse in" aria-labelledby="headingThree"
                            data-parent="#accordion">
                            <div class="card-body" style="height: 360px; overflow: auto;padding: 3% 3% 3% 3%;">
                                <p class="text-justify">
                                    <div class="col-lg-1 col-xs-1 text-right">
                                        <span class="glyphicon glyphicon-menu-right"></span>
                                    </div>
                                    <div class="col-lg-11 col-xs-11">
                                        El registro de las experiencias e iniciativas será totalmente voluntario y el
                                        llenado podrá ser desde la página del proyecto, a través de las redes sociales o
                                        vía correo electrónico.<br><br>
                                    </div>

                                    <div class="col-lg-1 col-xs-1 text-right">
                                        <span class="glyphicon glyphicon-menu-right"></span>
                                    </div>
                                    <div class="col-lg-11 col-xs-11">
                                        Sólo habrá un registro por experiencia o iniciativa, y se generará una clave de
                                        usuario y una contraseña para poder actualizar su propia información.<br><br>
                                    </div>

                                    <div class="col-lg-1 col-xs-1 text-right">
                                        <span class="glyphicon glyphicon-menu-right"></span>
                                    </div>
                                    <div class="col-lg-11 col-xs-11">

                                        Una vez que la información sea subida a la plataforma se asume que las personas
                                        responsables de la misma autorizan para que sea compartida a través de la
                                        plataforma, respetando siempre datos personales.<br><br>
                                    </div>

                                    <div class="col-lg-1 col-xs-1 text-right">
                                        <span class="glyphicon glyphicon-menu-right"></span>
                                    </div>
                                    <div class="col-lg-11 col-xs-11">
                                        <strong>
                                            Se dará todo el crédito a las personas, colectivos, organizaciones e
                                            instituciones que se sumen a esta iniciativa y, se garantizará el uso
                                            responsable de la información mediante el consenso, en cada caso que el uso de
                                            la misma, difiera de los fines especificados del proyecto.

                                        </strong>
                                        <br><br>
                                    </div>

                                    <div class="col-lg-1 col-xs-1 text-right">
                                        <span class="glyphicon glyphicon-menu-right"></span>
                                    </div>
                                    <div class="col-lg-11 col-xs-11">
                                        Puede escoger más de una opción a la vez, buscando reflejar la integralidad de
                                        sus experiencias en transición agroecológica.
                                    </div>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="accordion">

                    <div class="card">
                        <div class="card-header text-center" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse"
                                    data-target="#collapseThree1" aria-expanded="false" aria-controls="collapseThree1">
                                    <h style="font-size: 16px;">Contacto</h>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree1" class="collapse in" aria-labelledby="headingThree"
                            data-parent="#accordion">
                            <div class="card-body text-center">
                                <p class="">
                                    Dr. Miguel Ángel Escalona Aguilar
                                </p>
                                <p class="">
                                    Universidad Veracruzana
                                </p>
                                <p class="">
                                    Correo: mescalona@uv.mx
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</section>