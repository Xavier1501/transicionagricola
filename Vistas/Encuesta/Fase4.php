<?php
//session_start();
include_once 'ConexionBD.php';
$user = $_SESSION['Usuario'];
$id = $_SESSION['idEntrevista'];

//var_dump($user);
//var_dump($id);
$sql_id = "
select cae.idEntrevista as id from co_agr_entrevista cae 

where cae.nombreUsuario = '$user';
                ";
$query_id = $conexion->query($sql_id);
$array = $query_id->fetch_array(MYSQLI_ASSOC);
$result_id = $array['id'];
//var_dump($result_id);
$sql = "

select 
cai.idEntrevista as idEntrevista,
cai.NombreIniciativa as nombre,
cai.AnoInicio as fecha,
cai.Resumen as resumen,
cai.Latitud as lat,
cai.Longitud as lon,
cai.Surgimiento as surgimiento,
cai.historico as his,
cai.principalActor as actor,
cai.proposito as proposito,
cai.problemas as problemas,
cai.soluciiones as solucion,
cai.retos as reto,
cai.financiamiento as finaciamiento,
cai.municipio as municipio,
cai.estados as estados,
cae.nombreUsuario as usuario,
caur.Url as url,
caur.idUrl as idurl,
caim.url as urlimg,
cai.idIniciativa as id
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista =cai.idEntrevista
left join co_agr_img caim on cae.idEntrevista = caim.idAreaTrabajo 
left join co_agr_url caur on cae.idEntrevista =caur.idAreaTrabajo where cae.idEntrevista = '$result_id'


";
$query = $conexion->query($sql);
$entrevistaArray = $query->fetch_array(MYSQLI_ASSOC);

//var_dump($entrevistaArray);
?>

<style>
/*Aqui empiezan los estios para los mensajes emergentes*/
.too {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;

}

.too .tool {
    visibility: hidden;
    width: 300px;
    height: auto;
    background-color: black;
    color: #fff;
    border-radius: 6px;
    padding: 5px 5px 5px 5px;
    line-height: 90%;
    font-family: "Palatino", sans-serif;
    font-size: 13;
    text-align: center;
    position: absolute;
    z-index: 1;
}

.too:hover .tool {
    visibility: visible;
}

/*//////////////////*/
.tro {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
    font-size: 20px;
    cursor: pointer;
}

.tro .toot {
    visibility: hidden;
    width: 445px;
    height: 140px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    line-height: 100%;
    font-family: "Palatino", sans-serif;
    font-size: 13px !importan;
    /*text-align: left;
      /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

.tro:hover .toot {
    visibility: visible;
}

a {
    text-decoration: none;
    color: #fff;
}
</style>


<fieldset id="4">


    <form id="register_form" novalidate method="post">
        <div class="container">
            <br>

            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="10"
                    aria-valuemin="0" aria-valuemax="100" style="width:40%">
                    40%
                </div>
            </div>

            <h2 class="text-center">Atlas Transiciones</h2>
            <input type="hidden" class="form-control" name="idfase4" id="idfase4"
                value="<?php echo $id = (!empty($entrevistaArray['id'])) ? $entrevistaArray['id'] : '' ?>"></input>
            <input type="hidden" class="form-control" name="idurl" id="idurl"
                value="<?php echo $idurl = (!empty($entrevistaArray['idurl'])) ? $entrevistaArray['idurl'] : '' ?>"></input>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">

                    <!--Pregunta 1-->


                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="">
                                1.- Nombre de la iniciativa o experiencia
                            </label>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <input type="text" class="form-control" name="nombreIncExp" id="nombreIncExp"
                                value="<?php echo $nombre = (!empty($entrevistaArray['nombre'])) ? $entrevistaArray['nombre'] : '' ?>"></input>
                            <br>
                        </div>

                    </div>


                    <!--//Pregunta 1-->

                    <!--Pregunta 2-->
                    <br>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                            <label>
                                2.- Resumen sobre el trabajo que realiza en la iniciativa o experiencia agroecológica
                            </label>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control resumen" rows="3" name="resumen" id="resumen"
                                value=""><?php echo $resumen = (!empty($entrevistaArray['resumen'])) ? $entrevistaArray['resumen'] : '' ?></textarea>
                            <div class="text-right"><span id="caracteres" class="valid-text pt-3" id="txaCount"></span></div>
                            <br>

                        </div>

                    </div>
                    <!--//Pregunta 2-->

                    <!--Pregunta 5-->
                    <br>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                            <label>
                                3.- ¿Cómo surgió su experiencia o iniciativa?
                            </label>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="surgimiento"
                                id="surgimiento"><?php echo $surgimiento = (!empty($entrevistaArray['surgimiento'])) ? $entrevistaArray['surgimiento'] : '' ?></textarea>
                            <div class="text-right"><span id="car-sur" class="valid-text pt-3" id="txaCount"></span></div>
                            <br>

                        </div>

                    </div>
                    <!--//Pregunta 5-->

                    <!--Pregunta 6-->
                    <br>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                            <label>
                                4.- Contexto histórico en que surge
                            </label>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="historico"
                                id="historico"><?php echo $his = (!empty($entrevistaArray['his'])) ? $entrevistaArray['his'] : '' ?></textarea>
                            <div class="text-right"><span id="car-his" class="valid-text pt-3" id="txaCount"></span></div>

                            <br>

                        </div>

                    </div>
                    <!--//Pregunta 6-->

                    <!--Pregunta 7-->
                    <br>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                            <label>
                                5.- Principales actores involucrados
                            </label>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="actor"
                                id="actor"><?php echo $actor = (!empty($entrevistaArray['actor'])) ? $entrevistaArray['actor'] : '' ?></textarea>
                            <div class="text-right"><span id="car-ac" class="valid-text pt-3" id="txaCount"></span></div>

                            <br>

                        </div>

                    </div>
                    <!--//Pregunta 7-->

                    <!--Pregunta 8-->
                    <br>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                            <label>
                                6.- Propósitos más importantes
                            </label>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="proposito"
                                id="proposito"><?php echo $proposito = (!empty($entrevistaArray['proposito'])) ? $entrevistaArray['proposito'] : '' ?></textarea>
                            <div class="text-right"><span id="car-p" class="valid-text pt-3" id="txaCount"></span></div>

                            <br>

                        </div>

                    </div>
                    <!--//Pregunta 8-->

                    <!--Pregunta 9-->
                    <br>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                            <label>
                                7.- Problemas a los que se ha enfretado su inciativa o experiencia agroecológica
                            </label>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="problemas"
                                id="problemas"><?php echo $probremas = (!empty($entrevistaArray['problemas'])) ? $entrevistaArray['problemas'] : '' ?></textarea>
                            <div class="text-right"><span id="car-pr" class="valid-text pt-3" id="txaCount"></span></div>

                            <br>

                        </div>

                    </div>
                    <!--//Pregunta 9-->

                    <!--Pregunta 10-->
                    <br>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                            <label>
                                8.- Soluciones encontradas
                            </label>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="soluciones"
                                id="soluciones"><?php echo $soluciones = (!empty($entrevistaArray['solucion'])) ? $entrevistaArray['solucion'] : '' ?></textarea>
                            <div class="text-right"><span id="car-sol" class="valid-text pt-3" id="txaCount"></span></div>

                            <br>

                        </div>

                    </div>
                    <!--//Pregunta 10-->

                    <!--Pregunta 11-->
                    <br>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                            <label>
                                9.- Retos futuros
                            </label>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="retos"
                                id="retos"><?php echo $reto = (!empty($entrevistaArray['reto'])) ? $entrevistaArray['reto'] : '' ?></textarea>
                            <div class="text-right"><span id="car-retos" class="valid-text pt-3" id="txaCount"></span></div>

                            <br>

                        </div>

                    </div>
                    <!--//Pregunta 11-->

                    <!--Pregunta 12-->
                    <br>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                            <label>
                                10.- Si se ha recibido financiamiento, indicar de que fuente
                            </label>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="form-control" rows="3" name="financiamiento"
                                id="financiamiento"><?php echo $financiamiento = (!empty($entrevistaArray['finaciamiento'])) ? $entrevistaArray['finaciamiento'] : '' ?></textarea>
                            <div class="text-right"><span id="car-fin" class="valid-text pt-3" id="txaCount"></span></div>

                            <br>

                        </div>

                    </div>
                    <!--//Pregunta 12-->


                    <br>
                    <br>




                    <!--Pregunta 3-->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label>
                                11.- Año en que iniciaron actividades
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <input type="date" name="fechaAtlas"
                                <?php echo $fecha = (!empty($entrevistaArray['fecha'])) ? ''  : 'id="fechaAtlas"' ?>
                                value="<?php echo $fecha = (!empty($entrevistaArray['fecha'])) ? $entrevistaArray['fecha'] : '' ?>">
                        </div>
                    </div>
                    <!--//Pregunta 3-->
                    <br>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                        <br>
                        <label>
                            En donde se encuentra ubicada la iniciativa:
                        </label>

                    </div>


                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="">
                                Estado:
                            </label>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <input type="text" class="form-control" name="estado" id="estados" value="<?php echo$grupo = (!empty($entrevistaArray['estados']) )? $entrevistaArray['estados']:'' ?>">

                            <br>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="">
                                Municipio:
                            </label>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <input type="text" class="form-control" name="municipio" id="municipio" value="<?php echo$grupo = (!empty($entrevistaArray['municipio']) )? $entrevistaArray['municipio']:'' ?>">

                            <br>
                        </div>

                    </div>

                    

                    <br> <br>

                    <!--Pregunta 4-->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label>
                                <br>
                                12.- Ubique en el mapa su experiencia agroecológica, arrastrando y acercando el marcador <img
                                    src=" img/marcador.png" style="

                                                    max-width: 100%;
                                                    width: 2.5%;
                                                    height: auto;
                                                    position: relative;
                                                    right: 3px;">
                                en el sitio que corresponda
                                <br>
                            </label>
                            <label>
                            </label>
                        </div>
                    </div>
                    <!--pregunta 4-->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" onload="initialize()" id="error">

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tam" id="map_canvas" style="max-width: 100%;
                          position: relative;
                          overflow: hidden;
                          height: 500px;
                          width: 100%;">
                        </div>
                        <input id="txtLat" type="hidden" name="txtLat" style="color:red"
                            value="<?php echo $lat = (!empty($entrevistaArray['lat'])) ? $entrevistaArray['lat'] : '' ?>" />
                        <input id="txtLng" type="hidden" name="txtLng" style="color:red"
                            value="<?php echo $actor = (!empty($entrevistaArray['lon'])) ? $entrevistaArray['lon'] : '' ?>" /><br />
                        <br />
                        <br />
                    </div>
                    <!--//pregunta4-->
                    <br>
                    <br>

                    <!--Pregunta 3-->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <br>
                            <label>
                                13.- Cargue las 3 imágenes representativas de su experiencia agroecológica:
                                &nbsp;<i id="g" class="fa fa-question-circle-o tro too">


                                    <span class="tool">
                                        <h style="line-height: 0.2em; font-size: 12px;">
                                            1. Deberá cargarlas en formato .png y .jpg
                                        </h>
                                    </span>
                                    <span class="tool">
                                        <h style="line-height: 0.2em; font-size: 12px;">

                                            1. Deberá cargarlas en formato .png o .jpg<br>
                                            2. El peso de cada imagen no debe exceder de 1 MB.<br>
                                        </h>
                                    </span>

                                </i>


                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


                            <?php if (!empty($entrevistaArray['urlimg'])) : ?>
                            <div class="alert alert-warning alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <strong>¡Aviso!</strong> Imagenes ya se encuentran cargadas.
                            </div>
                            <?php else : ?>
                            <a class="btn btn-primary" data-toggle="modal" href='#modal-id2'>Cargar imagenes</a>

                            <?php include_once 'modal-img.php'?>

                            <?php endif; ?>


                        </div>
                    </div>
                    <!--//Pregunta 3-->

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <br>
                            <label class="">
                                14.- Si cuenta con un video de youtube o en facebook de la experiencia agroecólogica ingrese el url:
                            </label>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <input type="text" class="form-control" name="url" id="url"
                                value="<?php echo $url = (!empty($entrevistaArray['url'])) ? $entrevistaArray['url'] : '' ?>"></input>
                            <br>
                        </div>



                    </div>
                </div>
            </div>
            <br>
    </form>


    <?php

    //var_dump($user);
    $sqlUser = "   
   select * from co_agr_usuario cau where cau.usuario = '$user'
   ";


    $queryUser = $conexion->query($sqlUser);
    $rowUser = $queryUser ->fetch_array(MYSQLI_ASSOC);
    //var_dump($rowUser);


    $sqlC = "   
                select  cai.Continuarfases from co_agr_entrevista cae
LEFT JOIN co_agr_iniciativa cai ON cae.idEntrevista =cai.idEntrevista
where cae.nombreUsuario = '$user'
                 ";


    $queryC = $conexion->query($sqlC);
    $rowC= $queryC ->fetch_array(MYSQLI_ASSOC);
    //var_dump($rowC);

    ?>

    <?php if ($rowUser['tipo'] ==='grupo'):?>
        <a href="Preguntas-fase3.php" class="btn btn-default"> Regresar </a>
    <?php else:?>
        <a href="Preguntas-fase2.php" class="btn btn-default"> Regresar </a>
    <?php endif;?>
        <a href="#modal-id-sig-fase-2" data-toggle="modal"><button type="button" class="btn btn-info" id="guardarFase4">Siguiente</button></a>
        <?php include_once 'modal-guardar.php' ?>
</fieldset>