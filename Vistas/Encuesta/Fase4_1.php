<?php

//session_start();
include_once 'ConexionBD.php';
$user = $_SESSION['Usuario'];
$id = $_SESSION['idEntrevista'];

//var_dump($user);
//var_dump($id);



$sql_id = "select cae.idEntrevista as id from co_agr_entrevista cae where cae.nombreUsuario = '$user'";

$query_id = $conexion->query($sql_id);
$array = $query_id->fetch_array(MYSQLI_ASSOC);
$result_id = $array['id'];

//var_dump($result_id);


$sql_id = "
select 
*
from co_agr_entrevista cae 
left join co_agr_genero c on cae.idEntrevista = c.idAreaTrabajo 
where cae.idEntrevista ='$result_id'";
$query_id = $conexion->query($sql_id);
$array = $query_id->fetch_array(MYSQLI_ASSOC);
//$result_id = $array['id'];

//var_dump($result_id);

$sql = "
select 
*
from co_agr_entrevista cae 
left join co_agr_genero c on cae.idEntrevista = c.idAreaTrabajo 
where cae.idEntrevista = '$result_id';


";
$query = $conexion->query($sql);
$entrevistaArray = $query->fetch_array(MYSQLI_ASSOC);
//var_dump($entrevistaArray);
?>



<style type="text/css">

    /*Aqui empiezan los estios para los mensajes emergentes*/
    .too {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;

    }

    .too .tool {
        visibility: hidden;
        width: 300px;
        height: 190px;
        background-color: black;
        color: #fff;
        border-radius: 6px;
        padding: 5px 5px 5px 5px;
        line-height: 90%;
        font-family: "Palatino", sans-serif;
        font-size: 13;
        text-align: center;
        position: absolute;
        z-index: 1;
    }

    .too:hover .tool {
        visibility: visible;
    }
    /*//////////////////*/
    .tro {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        font-size: 20px;
        cursor: pointer;
    }

    .tro .toot {
        visibility: hidden;
        width: 445px;
        height: 140px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        line-height: 100%;
        font-family: "Palatino", sans-serif;
        font-size: 13px !importan;
        /*text-align: left;
      /* Position the tooltip */
        position: absolute;
        z-index: 1;
    }

    .tro:hover .toot {
        visibility: visible;
    }




     a{
         text-decoration: none;
         color: #fff;
     }





</style>

<fieldset id="3_1">
    <form id="register_form" novalidate method="post">
    <div class="container">
        <div class="row">
            <br>


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar"
                     aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                    50%
                </div>
            </div>
            </div>

            <h2 class="text-center">Género y agroecología</h2>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                <strong>
                   Con la idea de identificar las iniciativas femeninas y su contribución al proceso de transición agroecológica, de tal forma que el análisis de los datos obtenidos nos permita conocer acerca de los proyectos y sus dinámicas en relación a la perspectiva de Género, le pedimos nos ayudes a contestar las siguientes preguntas:
                </strong>
            </div>
            <br><br>
            <!--Pregunta_1_género-->
            <br>

            <input type="hidden" class="form-control"  name="idfase4_1" id="idfase4_1" value="<?php echo $idGenero = (!empty($entrevistaArray['idGenero']) )? $entrevistaArray['idGenero']:'' ?>">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                            14.- Número de integrantes de la iniciativa o experiencia agroecológica
                    </label>                 
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <label>Número de mujeres</label>
                    <input type="number" class="form-control" name="NMujeres" id="NMujeres" value="<?php echo $NMujeres = (!empty($entrevistaArray['NMujeres']) )? $entrevistaArray['NMujeres']:'' ?>">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <label class="text-center">Número de hombres</label>
                    <input type="number" class="form-control" name="NHombres" id="NHombres" value="<?php echo $NHombres = (!empty($entrevistaArray['NHombres']) )? $entrevistaArray['NHombres']:'' ?>">
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <label>Número en otros géneros (LGBTTTIQ)</label>
                    <input type="number" class="form-control" name="NOtrosGeneros" id="NOtrosGeneros" value="<?php echo $NOtrosGeneros = (!empty($entrevistaArray['NOtrosGeneros']) )? $entrevistaArray['NOtrosGeneros']:'' ?>"><br>
                </div>

            </div>

            <!--Pregunta_2_género-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        15.- Integrantes por rango de edades
                    </label>

                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <label>Niños</label>
                    <input  type="number" class="form-control" name="IntegranteNinos" id="IntegranteNinos" value="<?php echo $IntegranteNinos = (!empty($entrevistaArray['IntegranteNinos']) )? $entrevistaArray['IntegranteNinos']:'' ?>">
                    <br>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <label>Adolescentes</label>
                    <input  type="number" class="form-control" name="IntegranteAdolecentes" id="IntegranteAdolecentes" value="<?php echo $IntegranteAdolecentes = (!empty($entrevistaArray['IntegranteAdolecentes']) )? $entrevistaArray['IntegranteAdolecentes']:'' ?>"></input>
                    <br>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <label>Adultos</label>
                    <input  type="number" class="form-control" name="IntegranteAdultos" id="IntegranteAdultos" value="<?php echo $IntegranteAdultos = (!empty($entrevistaArray['IntegranteAdultos']) )? $entrevistaArray['IntegranteAdultos']:'' ?>"></input>
                    <br>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <label>Adultos mayores</label>
                    <input  type="number" class="form-control" name="IntegrantesAdultosMayores" id="IntegrantesAdultosMayores" value="<?php echo $IntegrantesAdultosMayores = (!empty($entrevistaArray['IntegrantesAdultosMayores']) )? $entrevistaArray['IntegrantesAdultosMayores']:'' ?>"></input>
                    <br>
                </div>

            </div>

            <!--Pregunta_3_género-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        16.- Porcentaje en puestos directivos, de representación, coordinación (que permita evidenciar, aunque sea numéricamente la brecha de género)
                    </label>

                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <label>Hombres en puesto directivos</label>
                    <input  type="number" class="form-control" name="$PorcentajeHombresDirectivos" id="$PorcentajeHombresDirectivos" value="<?php echo $PorcentajeHombresDirectivos = (!empty($entrevistaArray['PorcentajeHombresDirectivos']) )? $entrevistaArray['PorcentajeHombresDirectivos']:'' ?>"></input>
                    <br>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <label>Mujeres en puestos directivos</label>
                    <input  type="number" class="form-control" name="PorcentajeMujeresDirectivos" id="PorcentajeMujeresDirectivos" value="<?php echo $PorcentajeMujeresDirectivos = (!empty($entrevistaArray['PorcentajeMujeresDirectivos']) )? $entrevistaArray['PorcentajeMujeresDirectivos']:'' ?>"></input>
                    <br>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <label>Hombres en puestos de coordinación</label>
                    <input  type="number" class="form-control" name="PorcentajeHombresCoodinados" id="PorcentajeHombresCoodinados" value="<?php echo $PorcentajeHombresCoodinados = (!empty($entrevistaArray['PorcentajeHombresCoodinados']) )? $entrevistaArray['PorcentajeHombresCoodinados']:'' ?>">
                    <br>
                </div>
                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <label>Mujeres en puestos de coordinación</label>
                    <input  type="number" class="form-control" name="PorcentajeMujeresCoordinados" id="PorcentajeMujeresCoordinados" value="<?php echo $PorcentajeMujeresCoordinados = (!empty($entrevistaArray['PorcentajeMujeresCoordinados']) )? $entrevistaArray['PorcentajeMujeresCoordinados']:'' ?>"></input>
                    <br>
                </div>

            </div>







            <!--Pregunta_4_género-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label>
                        17.- ¿Hay igualdad de salarios para puestos equivalentes?
                    </label>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>Sí&nbsp;&nbsp;</label> <input type="radio" class="i-radio" name="IgualdaSalario" id="IgualdaSalario" value="Si" <?php echo $IgualdaSalario = ($entrevistaArray['IgualdaSalario']=== 'Si' )? 'checked':'' ?>>
                    <br>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>No&nbsp;&nbsp;</label> <input type="radio" name="IgualdaSalario" id="IgualdaSalario" value="No" <?php echo $IgualdaSalario = ($entrevistaArray['IgualdaSalario']=== 'No' )? 'checked':''?>>
                    <br>
                </div>


            <p id="vista"


                <?php echo $IgualdaSalario = ($entrevistaArray['IgualdaSalario']=== 'No' )?'':'style="display: none"'?>

            >
                <label>Por qué</label>
                <textarea class="form-control " name="IgualdaPorque" id="IgualdaPorque"><?php echo $SalEqResp = (!empty($entrevistaArray['IgualdaPorque']) )? $entrevistaArray['IgualdaPorque']:'' ?></textarea>
            <div class="text-right"><span id="car-igpq" class="valid-text pt-3" id="txaCount"></span></div>

            <br>

            </>
            
            </div>
            <!--Pregunta_5_género-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        18.- ¿Tiene una política de equidad de género en su colectivo u organización? Favor de describirla brevemente
                    </label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <textarea  class="form-control" name="PoliticaEquidad" id="PoliticaEquidad"><?php echo $PoliticaEquidad = (!empty($entrevistaArray['PoliticaEquidad']) )? $entrevistaArray['PoliticaEquidad']:'' ?></textarea>
                    <div class="text-right"><span id="car-peq" class="valid-text pt-3" id="txaCount"></span></div>

                    <br>
                </div>

            </div>
            <!--Pregunta_6_género-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        19.- ¿Se respetan los derechos de la maternidad?
                    </label>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>Sí&nbsp;&nbsp;</label> <input type="radio" name="DerechoMaternal" id="DerechoMaternal" value="Si" <?php echo $DerechoMaternal = ($entrevistaArray['DerechoMaternal']=== 'Si' )? 'checked':''?>>
                    <br>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>No&nbsp;&nbsp;</label> <input type="radio" name="DerechoMaternal" id="DerechoMaternal" value="No" <?php echo $DerechoMaternal = ($entrevistaArray['DerechoMaternal']=== 'No' )? 'checked':''?>>
                    <br>
                </div>

                <p id="vista2"
                    <?php echo $vista2 = ($entrevistaArray['DerechoMaternal']=== 'No' )?'':'style="display: none"'?>

                >
                    <label>Por qué</label>
                    <textarea  class="form-control" name="DerechoMaternalPorQue" id="DerechoMaternalPorQue"><?php echo $DerechoMaternalPorQue = (!empty($entrevistaArray['DerechoMaternalPorQue']) )? $entrevistaArray['DerechoMaternalPorQue']:'' ?></textarea>
                <div class="text-right"><span id="car-dmpq" class="valid-text pt-3" id="txaCount"></span></div>


                </p>




            
            </div>
            <!--Pregunta_7_género-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        20.- ¿Como se manejan los casos de acoso sexual? 
                    </label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <textarea  class="form-control" name="AcosoSexual" id="AcosoSexual"><?php echo $AcosoSexual = (!empty($entrevistaArray['AcosoSexual']) )? $entrevistaArray['AcosoSexual']:'' ?></textarea>
                    <div class="text-right"><span id="car-fin" class="valid-text pt-3" id="txaCount"></span></div>

                    <br>
                </div>

            </div>
            <!--Pregunta_8_género-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        21.- ¿Se promueve la no discriminación?
                    </label>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>Sí&nbsp;&nbsp;</label> <input type="radio" name="Discriminacion" id="Discriminacion" value="Si" <?php echo $Discriminacion = ($entrevistaArray['Discriminacion']=== 'Si' )? 'checked':'' ?>>
                    <br>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>No&nbsp;&nbsp;</label> <input type="radio" name="Discriminacion" id="Discriminacion" value="No" <?php echo $Discriminacion = ($entrevistaArray['Discriminacion']=== 'No' )? 'checked':''?>>
                    <br>
                </div>


                <p id="vista3"
                    <?php echo $vista3 = ($entrevistaArray['Discriminacion']=== 'No' )?'':'style="display: none"'?>
                >
                    <label>Por qué</label>
                    <textarea  class="form-control" name="DiscriminacionPorQue" id="DiscriminacionPorQue"><?php echo $DiscriminacionPorQue = (!empty($entrevistaArray['DiscriminacionPorQue']) )? $entrevistaArray['DiscriminacionPorQue']:'' ?></textarea>
                <div class="text-right"><span id="car-dispq" class="valid-text pt-3" id="txaCount"></span></div>

                <br>
                </p>


            
            </div>
            <!--Pregunta_9_género-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        22.- ¿Existe apoyo para madres solteras?
                    </label>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>Sí&nbsp;&nbsp;</label> <input type="radio" name="MadreSoltera" id="MadreSoltera" value="Si" <?php echo $MadreSoltera = ($entrevistaArray['MadreSoltera']=== 'Si' )? 'checked':''?>>
                    <br>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>No&nbsp;&nbsp;</label> <input type="radio" name="MadreSoltera" id="MadreSoltera" value="No" <?php echo $MadreSoltera = ($entrevistaArray['MadreSoltera']=== 'No' )? 'checked':'' ?>>
                    <br><br>
                </div>

            </div>
            <!--Pregunta_10_género-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        23.- ¿Sabe acerca de las necesidades de las mujeres que participan en el proyecto?
                    </label>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>Sí&nbsp;&nbsp;</label> <input type="radio" name="MujeresParticipanteProyecto" id="MujeresParticipanteProyecto" value="Si" <?php echo $MujeresParticipanteProyecto = ($entrevistaArray['MujeresParticipanteProyecto']=== 'Si' )? 'checked':''?>>
                    <br>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>No&nbsp;&nbsp;</label> <input type="radio" name="MujeresParticipanteProyecto" id="MujeresParticipanteProyecto" value="No" <?php echo $MujeresParticipanteProyecto = ($entrevistaArray['MujeresParticipanteProyecto']=== 'No' )? 'checked':''?>>
                    <br>
                </div>


                <p id="vista4"
                    <?php echo $vista4 = ($entrevistaArray['MujeresParticipanteProyecto']=== 'No' )?'':'style="display: none"'?>
                >
                    <label>Por qué</label>
                    <textarea  class="form-control" name="MujeresParticipanteProyectoporQue" id="MujeresParticipanteProyectoporQue"><?php echo $MujeresParticipanteProyectoporQue = (!empty($entrevistaArray['MujeresParticipanteProyectoporQue']) )? $entrevistaArray['MujeresParticipanteProyectoporQue']:'' ?></textarea>
                <div class="text-right"><span id="car-mpppq" class="valid-text pt-3" id="txaCount"></span></div>

                </p>


            
            </div>
            <!--Pregunta_11_género-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        24.- ¿Tiene dependientes económicos?
                    </label>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>Sí&nbsp;&nbsp;</label> <input type="radio" name="DependenciasEconomicos" id="DependenciasEconomicos" value="Si" <?php echo $DependenciasEconomicos = ($entrevistaArray['DependenciasEconomicos']=== 'Si' )? 'checked':''?>>
                    <br>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>No&nbsp;&nbsp;</label> <input type="radio" name="DependenciasEconomicos" id="DependenciasEconomicos" value="No" <?php echo $DependenciasEconomicos = ($entrevistaArray['DependenciasEconomicos']=== 'No' )? 'checked':''?>>
                    <br><br>
                </div>

            </div>
            <!--Pregunta_12_género-->
            <br>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <label>
                        25.- ¿Tiene enfermos que requieren cuidados especiales?
                    </label>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>Sí&nbsp;&nbsp;</label> <input type="radio" name="EnfermosCuidadosEspeciales" id="EnfermosCuidadosEspeciales" value="Si" <?php echo $EnfermosCuidadosEspeciales = ($entrevistaArray['EnfermosCuidadosEspeciales']=== 'Si' )? 'checked':'' ?>>
                    <br>
                </div>
                <div class="col-xs-6 col-sm-1 col-md-1 col-lg-1">
                    <label>No&nbsp;&nbsp;</label> <input type="radio" name="EnfermosCuidadosEspeciales" id="EnfermosCuidadosEspeciales" value="No" <?php echo $EnfermosCuidadosEspeciales = ($entrevistaArray['EnfermosCuidadosEspeciales']=== 'No' )? 'checked':''?>>
                    <br>
                </div>


                <p id="vista5"
                    <?php echo $vista5 = ($entrevistaArray['EnfermosCuidadosEspeciales']=== 'No' )?'style="display: none"':''?>
                >
                    <label>Cuáles</label>
                    <textarea  class="form-control" name="EnfermosCuidadosEspeciapesPorQue" id="EnfermosCuidadosEspeciapesPorQue"><?php echo $EnfermosCuidadosEspeciapesPorQue = (!empty($entrevistaArray['EnfermosCuidadosEspeciapesPorQue']) )? $entrevistaArray['EnfermosCuidadosEspeciapesPorQue']:'' ?></textarea>
                <div class="text-right"><span id="car-ecepq" class="valid-text pt-3" id="txaCount"></span></div>

                <br>
                </p>

            
            </div>

        </div>
    </div>



    </form>

    <div class="container">
        <div class="row">

            <a href="Preguntas-fase4.php" class="btn btn-default"> Regresar </a>
            <a href="#modal-id" data-toggle="modal"><button type="button" class="btn btn-info" id="guardarFase4_1">Siguiente</a></button>

            <?php include 'Modal-Cargado.php' ?>

            <?php include 'btn-subir.php' ?>

        </div>
    </div>

</fieldset>
