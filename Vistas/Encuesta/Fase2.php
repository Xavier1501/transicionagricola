<?php
//session_start();
include_once 'ConexionBD.php';
$user = $_SESSION['Usuario'];
$id = $_SESSION['idEntrevista'];

//var_dump($user);
//var_dump($id);



$sql_id = "select cae.idEntrevista as id from co_agr_entrevista cae where cae.nombreUsuario = '$user'";

$query_id = $conexion->query($sql_id);
$array = $query_id->fetch_array(MYSQLI_ASSOC);
$result_id = $array['id'];

//var_dump($result_id);


$sql = "
select 
*
from co_agr_entrevista cae 
left join co_agr_agricultura_urbana c on cae.idEntrevista = c.idEntrevistaFK
left join co_agr_saludplantasmedicinales ca on cae.idEntrevista = ca.idAreaTrabajo
left join co_agr_actividadnoagricola caa on cae.idEntrevista = caa.idAreaTrabajo
left join co_agr_desarollolocal cad on cae.idEntrevista = cad.idAreaTrabajo
left join co_agr_economia caec on cae.idEntrevista = caec.idAreaTrabajo
left join co_agr_educacion caed on cae.idEntrevista = caed.idAreaTrabajo  
left join co_agr_derechos_humanos cadh on cae.idEntrevista = cadh.idAreaTrabajo 
left join co_agr_trabajoninojuventud catnj on cae.idEntrevista = catnj.idAreaTrabajo
left join co_agr_investigacionagricolarural caiar on cae.idEntrevista = caiar.idAreaTrabajo
left join co_agr_manejorecursoshidricos camch on cae.idEntrevista = camch.idAreaTrabajo
left join co_agr_manejoposcosecha caampc on cae.idEntrevista = caampc.idAreaTrabajo
left join co_agr_semillas cas on cae.idEntrevista = cas.idAreaTrabajo
left join co_agr_sistemasagroforestales casaf on cae.idEntrevista = casaf.idAreaTrabajo
left join co_agr_productosforestales capf on cae.idEntrevista = capf.idAreaTrabajo 
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where cae.idEntrevista ='$result_id'";

$query = $conexion->query($sql);
$entrevistaArray = $query->fetch_array(MYSQLI_ASSOC);
//var_dump($entrevistaArray['idSistemaProduccionAgricola']);


?>

    <style>

        .a-btn{
            text-decoration: none;
            color: #fff;
        }

        #menu * {
            list-style: none;
        }

        #menu li {
            line-height: 180%;
        }

        #menu li a {
            color: #222;
            text-decoration: none;
        }

        #menu li a:before {
            content: "\025b8";
            color: #ddd;
            margin-right: 4px;
        }

        #menu input[name="list"] {
            position: absolute;
            left: -1000em;
        }

        #menu label:before {
            content: "\025b8";
            margin-right: 4px;
        }

        #menu input:checked~label:before {
            content: "\025be";
        }

        #menu .interior {
            display: none;
        }

        #menu input:checked~ul {
            display: block;
        }
    </style>
<fieldset id="2">

    <br>

    <form id="register_form" novalidate method="post">
    <div class="container">
        <div class="row">


            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar"
                     aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:20%">
                    20%
                </div>
            </div>

            <h2 class="text-center"> Área(s) en las que trabajan </h2>
            <div class="col-md-4">
                <div class="panel-group">
                    <input type="hidden" class="form-control"  name="idAgrArb" id="idAgrArb" value="<?php echo $idAgrArb = (!empty($entrevistaArray['idAgrArb']) )? $entrevistaArray['idAgrArb']:'' ?>">
                    <input type="hidden" class="form-control"  name="idSaludPlantasMedicinales" id="idSaludPlantasMedicinales" value="<?php echo $idSaludPlantasMedicinales = (!empty($entrevistaArray['idSaludPlantasMedicinales']) )? $entrevistaArray['idSaludPlantasMedicinales']:'' ?>">
                    <input type="hidden" class="form-control"  name="idActividadNoAgricola" id="idActividadNoAgricola" value="<?php echo $idActividadNoAgricola = (!empty($entrevistaArray['idActividadNoAgricola']) )? $entrevistaArray['idActividadNoAgricola']:'0' ?>">
                    <input type="hidden" class="form-control"  name="idDesarrolloLocal" id="idDesarrolloLocal" value="<?php echo $idDesarrolloLocal = (!empty($entrevistaArray['idDesarrolloLocal']) )? $entrevistaArray['idDesarrolloLocal']:'' ?>">
                    <input type="hidden" class="form-control"  name="IdEconomia" id="IdEconomia" value="<?php echo $IdEconomia = (!empty($entrevistaArray['IdEconomia']) )? $entrevistaArray['IdEconomia']:'' ?>">
                    <input type="hidden" class="form-control"  name="idCooperativa" id="idCooperativa" value="<?php echo $IdEconomia = (!empty($entrevistaArray['idCooperativa']) )? $entrevistaArray['idCooperativa']:'' ?>">
                    <input type="hidden" class="form-control"  name="idProduccion" id="idProduccion" value="<?php echo $idProduccion = (!empty($entrevistaArray['idProduccion']) )? $entrevistaArray['idProduccion']:'' ?>">
                    <input type="hidden" class="form-control"  name="idEducacion" id="idEducacion" value="<?php echo $idEducacion = (!empty($entrevistaArray['idEducacion']) )? $entrevistaArray['idEducacion']:'' ?>">
                    <input type="hidden" class="form-control"  name="idDerehosHumanos" id="idDerehosHumanos" value="<?php echo $idDerehosHumanos = (!empty($entrevistaArray['idDerehosHumanos']) )? $entrevistaArray['idDerehosHumanos']:'' ?>">
                    <input type="hidden" class="form-control"  name="idTrabajoNinoJuventd" id="idTrabajoNinoJuventd" value="<?php echo $idDerehosHumanos = (!empty($entrevistaArray['idTrabajoNinoJuventd']) )? $entrevistaArray['idTrabajoNinoJuventd']:'' ?>">
                    <input type="hidden" class="form-control"  name="idManejoRecursosHidricos" id="idManejoRecursosHidricos" value="<?php echo $idDerehosHumanos = (!empty($entrevistaArray['idManejoRecursosHidricos']) )? $entrevistaArray['idManejoRecursosHidricos']:'' ?>">
                    <input type="hidden" class="form-control"  name="idManejoPosCosecha" id="idManejoPosCosecha" value="<?php echo $idManejoPosCosecha = (!empty($entrevistaArray['idManejoPosCosecha']) )? $entrevistaArray['idManejoPosCosecha']:'' ?>">
                    <input type="hidden" class="form-control"  name="idSemillas" id="idSemillas" value="<?php echo $idSemillas = (!empty($entrevistaArray['idSemillas']) )? $entrevistaArray['idSemillas']:'' ?>">
                    <input type="hidden" class="form-control"  name="idSemillas" id="idSemillas" value="<?php echo $idSemillas = (!empty($entrevistaArray['idSemillas']) )? $entrevistaArray['idSemillas']:'' ?>">
                    <input type="hidden" class="form-control"  name="idSistemasAgroforestales" id="idSistemasAgroforestales" value="<?php echo $idSistemasAgroforestales = (!empty($entrevistaArray['idSistemasAgroforestales']) )? $entrevistaArray['idSistemasAgroforestales']:'' ?>">
                    <input type="hidden" class="form-control"    name="idInvestigacionAgricolaRural" id="idInvestigacionAgricolaRural" value="<?php echo $idProductosForestales = (!empty($entrevistaArray['idInvestigacionAgricolaRural']) )? $entrevistaArray['idInvestigacionAgricolaRural']:'' ?>">
                    <input type="hidden" class="form-control"    name="idProductosForestales" id="idProductosForestales" value="<?php echo $idProductosForestales = (!empty($entrevistaArray['idProductosForestales']) )? $entrevistaArray['idProductosForestales']:'' ?>">
                    <input type="hidden" class="form-control"    name="idSistemaProduccionAnimal" id="idSistemaProduccionAnimal" value="<?php echo $idProductosForestales = (!empty($entrevistaArray['idSistemaProduccionAnimal']) )? $entrevistaArray['idSistemaProduccionAnimal']:'' ?>">
                    <input type="hidden" class="form-control"    name="idSistemaProduccionAgricola" id="idSistemaProduccionAgricola" value="<?php echo $idSistemaProduccionAgricola = (!empty($entrevistaArray['idSistemaProduccionAgricola']) )? $entrevistaArray['idSistemaProduccionAgricola']:'' ?>">

                    <!--Agricultura Urbana  -->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <div data-toggle="collapse" href="#">
                                    Agricultura urbana
                                </div>
                            </h4>
                        </div>
                        <div id="" class="panel-collapse">
                            <div class="panel-body">
                                <ul>
                                    <!--<li> <input type="checkbox" name="AgriUr1" id="AgriUr1" > Agricultura Urbana </li>-->
                                    <li> <input type="checkbox" name="AgriUr1" id="AgriUr1" <?php echo $Azotea = (!empty($entrevistaArray['Azotea']) )? 'checked':'' ?> > Azotea </li>
                                    <li> <input type="checkbox" name="AgriUr2" id="AgriUr2" <?php echo $Balcon = (!empty($entrevistaArray['Balcon']) )? 'checked':'' ?>> Balcón </li>
                                    <li> <input type="checkbox" name="AgriUr3" id="AgriUr3" <?php echo $Traspatio = (!empty($entrevistaArray['Traspatio']) )? 'checked':'' ?>> Traspatio </li>
                                    <li> <input type="checkbox" name="AgriUr4" id="AgriUr4" <?php echo $Parques = (!empty($entrevistaArray['Parques']) )? 'checked':'' ?>> Parques o espacios públicos </li>

                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            Huertos
                                            <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li> <input type="checkbox" name="AgroUrbaHuerto1" id="AgroUrbaHuerto1" <?php echo $Hcomunitarios = (!empty($entrevistaArray['Hcomunitarios']) )? 'checked':'' ?>> Huertos comunitarios</li>
                                            <li> <input type="checkbox" name="AgroUrbaHuerto2" id="AgroUrbaHuerto2" <?php echo $HSociales = (!empty($entrevistaArray['HSociales']) )? 'checked':'' ?>> Huertos sociales</li>
                                            <li> <input type="checkbox" name="AgroUrbaHuerto3" id="AgroUrbaHuerto3" <?php echo $HInstitucionales = (!empty($entrevistaArray['HInstitucionales']) )? 'checked':'' ?>> Huertos institucionales</li>
                                            <li> <input type="checkbox" name="AgroUrbaHuerto4" id="AgroUrbaHuerto4" <?php echo $Hempresas = (!empty($entrevistaArray['Hempresas']) )? 'checked':'' ?>> Huertos de empresas</li>
                                            <li> <input type="text" name="AgroUrbaHuerto5" id="AgroUrbaHuerto5" value="<?php echo $HOtros = (!empty($entrevistaArray['OtroActNoAgri']) )? $entrevistaArray['OtroActNoAgri']:'' ?>"> Otro (especificar)</li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Agricultura Urbana  -->

                    <!--Salud y plantas medicinales-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#">
                                    Salud y plantas medicinales
                                </a>
                            </h4>
                        </div>
                        <div id="" class="panel-collapse  ">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="salud1" id="salud1" <?php echo $Homepatia = (!empty($entrevistaArray['Homepatia']) )? 'checked':'' ?>> Homeopatía </li>
                                    <li> <input type="checkbox" name="salud2" id="salud2" <?php echo $Medicina = (!empty($entrevistaArray['Medicina']) )? 'checked':'' ?>> Medicina tradicional </li>
                                    <li> <input type="checkbox" name="salud3" id="salud3" <?php echo $PreparadosBotanicos = (!empty($entrevistaArray['PreparadosBotanicos']) )? 'checked':'' ?>> Preparados botánicos </li>
                                    <li> <input type="checkbox" name="salud4" id="salud4" <?php echo $RemedioCasero = (!empty($entrevistaArray['RemedioCasero']) )? 'checked':'' ?>> Remedios caseros</li>
                                    <li> <input type="checkbox" name="salud5" id="salud5" <?php echo $NutricionAgri = (!empty($entrevistaArray['NutricionAgri']) )? 'checked':'' ?>> Nutrición agroecológica</li>
                                    <li> <input type="text" name="salud6" id="salud6" value="<?php echo $OtrosaludPlantas = (!empty($entrevistaArray['OtrosaludPlantas']) )? $entrevistaArray['OtrosaludPlantas']:'' ?>"> Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Salud y plantas medicinales-->

                    <!--Actividades no agrícolas-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#">Actividades no agrícolas</a>
                            </h4>
                        </div>
                        <div id="" class="panel-collapse ">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="ActividadNoAgro1" id="ActividadNoAgro1" <?php echo $Artesania = (!empty($entrevistaArray['Artesania']) )? 'checked':'' ?>> Artesanías </li>
                                    <li> <input type="checkbox" name="ActividadNoAgro2" id="ActividadNoAgro2" <?php echo $CajaAhorro = (!empty($entrevistaArray['CajaAhorro']) )? 'checked':'' ?>> Cajas de ahorro y crédito </li>
                                    <li> <input type="checkbox" name="ActividadNoAgro3" id="ActividadNoAgro3" <?php echo $Turismo = (!empty($entrevistaArray['Turismo']) )? 'checked':'' ?>> Turismo </li>
                                    <li> <input type="checkbox" name="ActividadNoAgro4" id="ActividadNoAgro4" <?php echo $Culturales = (!empty($entrevistaArray['Culturales']) )? 'checked':'' ?>> Culturales</li>
                                    <li> <input type="checkbox" name="ActividadNoAgro5" id="ActividadNoAgro5" <?php echo $ComedorComuni= (!empty($entrevistaArray['ComedorComuni']) )? 'checked':'' ?>> Comedores comunitarios </li>
                                    <li> <input type="checkbox" name="ActividadNoAgro6" id="ActividadNoAgro6" <?php echo $ComedorSocial = (!empty($entrevistaArray['ComedorSocial']) )? 'checked':'' ?>> Comedores sociales </li>
                                    <li> <input type="checkbox" name="ActividadNoAgro7" id="ActividadNoAgro4" <?php echo $ComedorEscolar = (!empty($entrevistaArray['ComedorEscolar']) )? 'checked':'' ?>> Comedores escolares </li>
                                    <li> <input type="checkbox" name="ActividadNoAgro8" id="ActividadNoAgro8" <?php echo $Restaurant = (!empty($entrevistaArray['Restaurant']) )? 'checked':'' ?>> Restaurantes, fondas y/o cocinas que
                                        ofrecen comida agroecológica </li>
                                    <li> <input type="text" name="ActividadNoAgro9" id="ActividadNoAgro9"  value="<?php echo $OtroActNoAgri = (!empty($entrevistaArray['OtroActNoAgri']) )? $entrevistaArray['OtroActNoAgri']:'' ?>"> Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Actividades no agrícolas-->

                    <!--Desarrollo Local-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#">Desarrollo Local</a>
                            </h4>
                        </div>
                        <div id="" class="panel-collapse ">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="DesarrolloLocal1" id="DesarrolloLocal1" <?php echo $AcomProyectos = (!empty($entrevistaArray['AcomProyectos']) )? 'checked':'' ?> > Acompañamiento de proyectos </li>
                                    <li> <input type="checkbox" name="DesarrolloLocal2" id="DesarrolloLocal2" <?php echo $Desarollo = (!empty($entrevistaArray['Desarollo']) )? 'checked':'' ?>> Desarrollo de proyectos productivos </li>
                                    <li> <input type="checkbox" name="DesarrolloLocal3" id="DesarrolloLocal3" <?php echo $OrganizacionAgriculturales = (!empty($entrevistaArray['OrganizacionAgriculturales']) )? 'checked':'' ?>> Organización de agricultores/as </li>
                                    <li> <input type="checkbox" name="DesarrolloLocal4" id=DesarrolloLocal4"  <?php echo $ProgramaGobierno = (!empty($entrevistaArray['ProgramaGobierno']) )? 'checked':'' ?>> Programas gubernamentales</li>
                                    <li> <input type="checkbox" name="DesarrolloLocal5" id="DesarrolloLocal5" <?php echo $NucleoAgrario = (!empty($entrevistaArray['NucleoAgrario']) )? 'checked':'' ?>> Planes de núcleos agrarios (ejidos,
                                        bienes
                                        comunales)</li>
                                    <li> <input type="text" name="DesarrolloLocal6" id="DesarrolloLocal6" value="<?php echo $OtroDesaLocal = (!empty($entrevistaArray['OtroDesaLocal']) )? $entrevistaArray['OtroDesaLocal']:'' ?>"> Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Desarrollo Local-->

                    <style>
                        .arbol-uv{
                            color: #337ab7;
                        }

                    </style>

                    <!--Economia-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#">Economia</a>
                            </h4>
                        </div>
                        <div id="" class="panel-collapse ">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="Economia1" id="Economia1" <?php echo $AutoConsumo = (!empty($entrevistaArray['AutoConsumo']) )? 'checked':'' ?>> Autoconsumo </li>
                                    <li> <input type="checkbox" name="Economia2" id="Economia2" <?php echo $CertificaParticipativa = (!empty($entrevistaArray['CertificaParticipativa']) )? 'checked':'' ?>> Certificación participativa (Sistema
                                        participativo de garantía) </li>
                                    <li> <input type="checkbox" name="Economia3" id="Economia3" <?php echo $CertificacionTerceraParte = (!empty($entrevistaArray['CertificacionTerceraParte']) )? 'checked':'' ?>> Certificación de tercera parte </li>
                                    <li> <input type="checkbox" name="Economia4" id="Economia4" <?php echo $ComercioLocal = (!empty($entrevistaArray['ComercioLocal']) )? 'checked':'' ?>> Comercialización local</li>
                                    <li> <input type="checkbox" name="Economia5" id="Economia5" <?php echo $ComercioRegional = (!empty($entrevistaArray['ComercioRegional']) )? 'checked':'' ?>> Comercialización regional</li>
                                    <ul id="menu">
                                        <li><input type="checkbox"  name="list" id="nivel1-1"><label class="arbol-uv" for="nivel1-1">Coperativas de Consumo</label>
                                            <ul class="interior">
                                                <li> <input type="checkbox" name="Cooperativa1" id="Cooperativa1" <?php echo $Consumo = (!empty($entrevistaArray['Consumo']) )? 'checked':'' ?>> Consumo</li>
                                                <li><input type="checkbox" name="list" id="nivel2-1"><label class="arbol-uv" for="nivel2-1">Produccion de:</label>
                                                    <ul class="interior">
                                                        <li> <input type="checkbox" name="producion01" id="producion01" <?php echo $Semillas = (!empty($entrevistaArray['Semillas']) )? 'checked':'' ?>>Semillas</li>
                                                        <li> <input type="checkbox" name="producion02" id="producion02" <?php echo $Insumos = (!empty($entrevistaArray['Insumos']) )? 'checked':'' ?>>Insumos</li>
                                                    </ul>
                                                </li>
                                                <li> <input type="checkbox" name="Cooperativa2" id="Cooperativa2" <?php echo $Credito = (!empty($entrevistaArray['Credito']) )? 'checked':'' ?>> Crédito</li>
                                                <li> <input type="checkbox" name="Cooperativa3" id="Cooperativa3" <?php echo $Servicios = (!empty($entrevistaArray['Servicios']) )? 'checked':'' ?>> Servicios</li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <li><input type="checkbox" name="Economia6" id="Economia6" <?php echo $EmpresaProducInsumo = (!empty($entrevistaArray['EmpresaProducInsumo']) )? 'checked':'' ?>>Empresa productora de insumos</li>
                                    <li><input type="checkbox" name="Economia7" id="Economia7" <?php echo $IniciativaIntercambio = (!empty($entrevistaArray['IniciativaIntercambio']) )? 'checked':'' ?>>Iniciativa de intercambio (trueque)</li>
                                    <li><input type="checkbox" name="Economia8" id="Economia8" <?php echo $MercadoAlternativo = (!empty($entrevistaArray['MercadoAlternativo']) )? 'checked':'' ?>>Mercado alternativo (agroecológico)</li>
                                    <li><input type="checkbox" name="Economia9" id="Economia9" <?php echo $SistemaGesta = (!empty($entrevistaArray['SistemaGesta']) )? 'checked':'' ?>>Sistema de cestas</li>
                                    <li><input type="checkbox" name="Economia10" id="Economia10" <?php echo $TianguisOrganico = (!empty($entrevistaArray['TianguisOrganico']) )? 'checked':'' ?>>Tianguis orgánico</li>
                                    <li> <input type="text" name="Economia11" id="Economia11" value="<?php echo $OtroEconomia = (!empty($entrevistaArray['OtroEconomia']) )? $entrevistaArray['OtroEconomia']:'' ?>" > Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Economia-->
                    <!--Educación-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#">Educación</a>
                            </h4>
                        </div>
                        <div id="" class="panel-collapse ">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="Educacion1" id="Educacion1" <?php echo $EduAmbiental = (!empty($entrevistaArray['EduAmbiental']) )? 'checked':'' ?>> Educación ambiental </li>
                                    <li> <input type="checkbox" name="Educacion2" id="Educacion2" <?php echo $EscuelaCampesina = (!empty($entrevistaArray['EscuelaCampesina']) )? 'checked':'' ?>> Escuela campesina </li>
                                    <li> <input type="checkbox" name="Educacion3" id="Educacion3" <?php echo $EscuelaIndigena = (!empty($entrevistaArray['EscuelaIndigena']) )? 'checked':'' ?>> Escuela indígena </li>
                                    <li> <input type="checkbox" name="Educacion4" id="Educacion4" <?php echo $FormacionTecnico = (!empty($entrevistaArray['FormacionTecnico']) )? 'checked':'' ?>> Formación de técnicos </li>
                                    <li> <input type="checkbox" name="Educacion5" id="Educacion5" <?php echo $ComuniAprende = (!empty($entrevistaArray['ComuniAprende']) )? 'checked':'' ?>> Comunidad de aprendizaje </li>
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            Educación Formal
                                            <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li> <input type="checkbox" name="EduFormal1" id="EduFormal1" <?php echo $Basicas = (!empty($entrevistaArray['Basicas']) )? 'checked':'' ?>> Básica </li>
                                            <li> <input type="checkbox" name="EduFormal2" id="EduFormal2" <?php echo $MediaSuperior = (!empty($entrevistaArray['MediaSuperior']) )? 'checked':'' ?>> Media Superior </li>
                                            <li> <input type="checkbox" name="EduFormal3" id="EduFormal3" <?php echo $Superior = (!empty($entrevistaArray['Superior']) )? 'checked':'' ?>> Superior </li>
                                        </ul>
                                    </li>
                                    <li><input type="checkbox" name="Educacion6" id="Educacion6" <?php echo $IniciativaCampesina = (!empty($entrevistaArray['IniciativaCampesina']) )? 'checked':'' ?>>Iniciativa de campesino a campesino</li>
                                    <li> <input type="text" name="Educacion7" id="Educacion7" value="<?php echo $OtroEcudacion = (!empty($entrevistaArray['OtroEcudacion']) )? $entrevistaArray['OtroEcudacion']:'' ?>"> Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Educación-->
                </div>
            </div>

            <div class="col-md-4">
                <!--Derechos humanos  -->
                <div class="panel panel-default">
                    <div class="panel-heading style-uv text-center">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#">
                                Derechos humanos
                            </a>
                        </h4>
                    </div>
                    <div id="" class="panel-collapse ">
                        <div class="panel-body">
                            <ul>
                                <li> <input type="checkbox" name="DerechosH1" id="DerechosH1" <?php echo $DerechosHumanos = (!empty($entrevistaArray['DerechosHumanos']) )? 'checked':'' ?>>
                                    Derechos humanos
                                </li>
                                <li> <input type="checkbox" name="DerechosHD2" id="DerechosHDh2" <?php echo $DerechosHumanos = (!empty($entrevistaArray['DerechosAmbientales']) )? 'checked':'' ?>>
                                    Derechos ambientales
                                </li>
                                <li> <input type="checkbox" name="DerechosHD3" id="DerechosHD3" <?php echo $DerechosHumanos = (!empty($entrevistaArray['DerechosEconomicos']) )? 'checked':'' ?>>
                                    Derechos económicos y de género
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--//Derechos humanos-->

                <div class="panel-group">
                    <!--Trabajo con niños/juventud-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#">
                                    Trabajo con niños/juventud
                                </a>
                            </h4>
                        </div>
                        <div id="" class="panel-collapse ">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="TrabajoNJ1" id="TrabajoNJ1" <?php echo $HuertoEscolar = (!empty($entrevistaArray['HuertoEscolar']) )? 'checked':'' ?>> Huertos-parcelas escolares </li>
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            Proyectos pedagógicos
                                            <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li> <input type="checkbox" name="TrabajoNJ2" id="TrabajoNJ2" <?php echo $EducacionAlimenticia = (!empty($entrevistaArray['EducacionAlimenticia']) )? 'checked':'' ?> > Educación
                                              Alimentaria y nutricional  </li>
                                        </ul>
                                    <!--<li> <input type="checkbox" name="TrabajoNJ3" id="TrabajoNJ3"> Otro (especificar) </li> -->
                                    <li> <input type="text" name="TrabajoNJ4" id="TrabajoNJ4" value="<?php echo $OtroTrabajoNiñoJoventud = (!empty($entrevistaArray['OtroTNJ']) )? $entrevistaArray['OtroTNJ']:'' ?>"> Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Trabajo con niños/juventud-->

                    <!--Investigación agrícola y Extensión rural-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#">
                                    Investigación agrícola y Extensión rural
                                </a>
                            </h4>
                        </div>
                        <div id="" class="panel-collapse ">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="Investigacion1" id="Investigacion1" <?php echo $Comunicacion = (!empty($entrevistaArray['Comunicacion']) )? 'checked':'' ?>> Comunicación</li>
                                    <li> <input type="checkbox" name="Investigacion2" id="Investigacion2" <?php echo $DiagAgroecosistema = (!empty($entrevistaArray['DiagAgroecosistema']) )? 'checked':'' ?>> Diagnóstico de agroecosistemas </li>
                                    <li> <input type="checkbox" name="Investigacion3" id="Investigacion3" <?php echo $Experimentacion = (!empty($entrevistaArray['Experimentacion']) )? 'checked':'' ?>> Experimentación </li>
                                    <li> <input type="checkbox" name="Investigacion4" id="Investigacion4" <?php echo $FormacionCapacitacion = (!empty($entrevistaArray['FormacionCapacitacion']) )? 'checked':'' ?>> Formación y capacitación </li>
                                    <li> <input type="checkbox" name="Investigacion5" id="Investigacion5" <?php echo $Investigacion = (!empty($entrevistaArray['Investigacion']) )? 'checked':'' ?>> Investigación </li>
                                    <li> <input type="checkbox" name="Investigacion6" id="Investigacion7" <?php echo $OrdenTerritoral = (!empty($entrevistaArray['OrdenTerritoral']) )? 'checked':'' ?>> Ordenamientos territoriales </li>
                                    <li> <input type="text" name="Investigacion7" id="Investigacion7" value="<?php echo $OtroTrabajoNiñoJoventud = (!empty($entrevistaArray['OtroInvestigacionRural']) )? $entrevistaArray['OtroInvestigacionRural']:'' ?>"> Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Investigación agrícola y Extensión rural-->

                    <!--Manejo de recursos hídricos-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#">
                                    Manejo de recursos hídricos
                                </a>
                            </h4>
                        </div>
                        <div id="" class="panel-collapse ">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="ManejoRecursos1" id="ManejoRecursos1" <?php echo $AlmacenAguaLluvia = (!empty($entrevistaArray['AlmacenAguaLluvia']) )? 'checked':'' ?>> Captación y almacenamiento de agua de lluvias</li>
                                    <li> <input type="checkbox" name="ManejoRecursos2" id="ManejoRecursos2" <?php echo $EnfoqueMicrocuenca = (!empty($entrevistaArray['EnfoqueMicrocuenca']) )? 'checked':'' ?>> Enfoque de microcuencas </li>
                                    <li> <input type="checkbox" name="ManejoRecursos3" id="ManejoRecursos3" <?php echo $ImplementacionEcotecnologicas = (!empty($entrevistaArray['ImplementacionEcotecnologicas']) )? 'checked':'' ?>> Implementación de ecotecnologías </li>
                                    <li> <input type="checkbox" name="ManejoRecursos4" id="ManejoRecursos4" <?php echo $ManejoComuniAgua = (!empty($entrevistaArray['ManejoComuniAgua']) )? 'checked':'' ?>> Manejo comunitario del agua </li>
                                    <li> <input type="checkbox" name="ManejoRecursos5" id="ManejoRecursos5" <?php echo $RiesgoDrenaje = (!empty($entrevistaArray['RiesgoDrenaje']) )? 'checked':'' ?>> Riego y drenaje </li>
                                    <li> <input type="checkbox" name="ManejoRecursos6" id="ManejoRecursos6" <?php echo $TratamientoAgua = (!empty($entrevistaArray['TratamientoAgua']) )? 'checked':'' ?>> Tratamiento de aguas </li>
                                    <li> <input type="text" name="ManejoRecursos7" id="ManejoRecursos7" value="<?php echo $OtroManejoH = (!empty($entrevistaArray['OtroManejoH']) )? $entrevistaArray['OtroManejoH']:'' ?>"> Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Manejo de recursos hídricos-->

                    <!--Manejo poscosecha-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#">
                                    Manejo poscosecha
                                </a>
                            </h4>
                        </div>
                        <div id="" class="panel-collapse ">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="ManejoPoscosecha1" id="ManejoPoscosecha1"  <?php echo $Almacenamiento = (!empty($entrevistaArray['Almacenamiento']) )? 'checked':'' ?> > Almacenamientos</li>
                                    <li> <input type="checkbox" name="ManejoPoscosecha2" id="ManejoPoscosecha2" <?php echo $Comercializacion = (!empty($entrevistaArray['Comercializacion']) )? 'checked':'' ?>> Comercialización </li>
                                    <li> <input type="checkbox" name="ManejoPoscosecha3" id="ManejoPoscosecha3" <?php echo $ProcesamientoTransfo = (!empty($entrevistaArray['ProcesamientoTransfo']) )? 'checked':'' ?>> Procesamiento o transformación </li>
                                    <li> <input type="text" name="ManejoPoscosecha4" id="ManejoPoscosecha4" value="<?php echo $OtroPosCos= (!empty($entrevistaArray['OtroPosCos']) )? $entrevistaArray['OtroPosCos']:'' ?>"> Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Manejo poscosecha-->

                    <!--Semillas-->
                    <div class="panel panel-default">
                        <div class="panel-heading style-uv text-center">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#">
                                    Semillas
                                </a>
                            </h4>
                        </div>
                        <div id="" class="panel-collapse ">
                            <div class="panel-body">
                                <ul>
                                    <li> <input type="checkbox" name="Semillas1" id="Semillas1" <?php echo $AlmacenConservacionSemillas = (!empty($entrevistaArray['AlmacenConservacionSemillas']) )? 'checked':'' ?>> Almacenamiento y conservación de semillas</li>
                                    <li> <input type="checkbox" name="Semillas2" id="Semillas2" <?php echo $BancosComuniSemilla = (!empty($entrevistaArray['BancosComuniSemilla']) )? 'checked':'' ?>> Bancos comunitarios de semillas</li>
                                    <li> <input type="checkbox" name="Semillas3" id="Semillas3" <?php echo $IntercambioSemilla = (!empty($entrevistaArray['IntercambioSemilla']) )? 'checked':'' ?>> Intercambio de Semillas</li>
                                    <li> <input type="checkbox" name="Semillas4" id="Semillas4" <?php echo $MejoraPlantaAnimal = (!empty($entrevistaArray['MejoraPlantaAnimal']) )? 'checked':'' ?>> Mejoramiento de plantas y animales </li>
                                    <li> <input type="checkbox" name="Semillas5" id="Semillas5" <?php echo $ProduccionSemilla = (!empty($entrevistaArray['ProduccionSemilla']) )? 'checked':'' ?>> Producción de semillas nativas y criollas </li>
                                    <li> <input type="text" name="Semillas6" id="Semillas6" value="<?php echo $OtroSemillas= (!empty($entrevistaArray['OtroSemillas']) )? $entrevistaArray['OtroSemillas']:'' ?>"> Otro (especificar) </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--//Semillas-->
                </div>
            </div>

            <div class="col-md-4">

                <!--Sistemas agroforestales-->
                <div class="panel panel-default">
                    <div class="panel-heading style-uv text-center">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#">
                                Sistemas agroforestales
                            </a>
                        </h4>
                    </div>
                    <div id="" class="panel-collapse ">
                        <div class="panel-body">
                            <ul>
                                <li> <input type="checkbox" name="sistemaAgro1" id="sistemaAgro1" <?php echo $ProduccionSemilla = (!empty($entrevistaArray['AreaDestinoConservaSilvo']) )? 'checked':'' ?>> Montes y áreas comunes destinadas a la conservación de sistemas silvopastoriles</li>
                                <li> <input type="checkbox" name="sistemaAgro2" id="sistemaAgro2" <?php echo $SistemaAgroforestal = (!empty($entrevistaArray['SistemaAgroforestal']) )? 'checked':'' ?>> Sistemas Agroforestales Tradicionales</li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        Arborización:
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li> <input type="checkbox" name="arbo1" id="arbo1" <?php echo $BosqueComestible = (!empty($entrevistaArray['BosqueComestible']) )? 'checked':'' ?>> Bosques comestibles</li>
                                        <li> <input type="checkbox" name="arbo2" id="arbo2" <?php echo $CercasVivas = (!empty($entrevistaArray['CercasVivas']) )? 'checked':'' ?>> Cercas vivas</li>
                                        <li> <input type="checkbox" name="arbo3" id="arbo3" <?php echo $BarreraRompeVientos = (!empty($entrevistaArray['BarreraRompeVientos']) )? 'checked':'' ?>> Barreras rompevientos</li>
                                    </ul>

                                </li>

                                <li> <input type="text" name="sistemaAgro3" id="sistemaAgro3" value="<?php echo $OtrosistemaAgro= (!empty($entrevistaArray['OtrosistemaAgro']) )? $entrevistaArray['OtrosistemaAgro']:'' ?>"> Otro (especificar) </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--//Sistemas agroforestales-->

                <!--Productos forestales-->
                <div class="panel panel-default">
                    <div class="panel-heading style-uv text-center">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#">
                                Productos forestales
                            </a>
                        </h4>
                    </div>
                    <div id="" class="panel-collapse ">
                        <div class="panel-body">
                            <ul>
                                <li> <input type="checkbox" name="prof1" id="prof1" <?php echo $ProductoMaderable = (!empty($entrevistaArray['ProductoMaderable']) )? 'checked':'' ?>> Productos forestables maderables</li>
                                <li> <input type="checkbox" name="prof2" id="prof2" <?php echo $ProductoNoMaderable = (!empty($entrevistaArray['OtroProductoForestales']) )? 'checked':'' ?>> Productos forestales no maderables</li>
                                <li> <input type="text" name="prof3" id="prof3" value="<?php echo $OtroProductoForestales= (!empty($entrevistaArray['OtroProductoForestales']) )? $entrevistaArray['OtroProductoForestales']:'' ?>"> Otro (especificar) </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--//Productos forestales-->

                <!--Sistemas de producción animal-->
                <div class="panel panel-default">
                    <div class="panel-heading style-uv text-center">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#">
                                Sistemas de producción animal
                            </a>
                        </h4>
                    </div>
                    <div id="" class="panel-collapse ">
                        <div class="panel-body">
                            <ul>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        Apicultura:
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li> <input type="checkbox" name="sisProAn1" id="sisProAn1" <?php echo $AbejaEuropeas = (!empty($entrevistaArray['AbejaEuropeas']) )? 'checked':'' ?>> Abejas europeas</li>
                                        <li> <input type="checkbox" name="sisProAn2" id="sisProAn2" <?php echo $AbejasNativas = (!empty($entrevistaArray['AbejasNativas']) )? 'checked':'' ?>> Abejas nativas</li>
                                    </ul>

                                </li>
                                <li> <input type="checkbox" name="sisProAn3" id="sisProAn3" <?php echo $Acuacultura = (!empty($entrevistaArray['Acuacultura']) )? 'checked':'' ?>> Acuacultura</li>
                                <li> <input type="checkbox" name="sisProAn4" id="sisProAn4" <?php echo $Aves = (!empty($entrevistaArray['Aves']) )? 'checked':'' ?>> Aves</li>
                                <li> <input type="checkbox" name="sisProAn5" id="sisProAn5" <?php echo $Bovinos = (!empty($entrevistaArray['Bovinos']) )? 'checked':'' ?>> Bovinos</li>
                                <li> <input type="checkbox" name="sisProAn20" id="sisProAn20" <?php echo $conejo = (!empty($entrevistaArray['Conejo']) )? 'checked':'' ?>> Conejo</li>
                                <li> <input type="checkbox" name="sisProAn6" id="sisProAn6" <?php echo $Caprinos = (!empty($entrevistaArray['Caprinos']) )? 'checked':'' ?>> Caprinos</li>
                                <li> <input type="checkbox" name="sisProAn7" id="sisProAn7" <?php echo $Cuniculturas = (!empty($entrevistaArray['Cuniculturas']) )? 'checked':'' ?>> Cunicultura</li>
                                <li> <input type="checkbox" name="sisProAn8" id="sisProAn8" <?php echo $Equinos = (!empty($entrevistaArray['Equinos']) )? 'checked':'' ?>> Equinos</li>
                                <li> <input type="checkbox" name="sisProAn9" id="sisProAn9" <?php echo $Lombricultura = (!empty($entrevistaArray['Lombricultura']) )? 'checked':'' ?>> Lombricultura</li>
                                <li> <input type="checkbox" name="sisProAn10" id="sisProAn10" <?php echo $Ovinos = (!empty($entrevistaArray['Ovinos']) )? 'checked':'' ?>> Ovinos</li>
                                <li> <input type="checkbox" name="sisProAn11" id="sisProAn11" <?php echo $Pesca = (!empty($entrevistaArray['Pesca']) )? 'checked':'' ?>> Pesca</li>
                                <li> <input type="checkbox" name="sisProAn12" id="sisProAn12" <?php echo $Porcinos = (!empty($entrevistaArray['Porcinos']) )? 'checked':'' ?>> Porcino</li>


                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        Prácticas
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li> <input type="checkbox" name= "sisProAn13" id="sisProAn13" <?php echo $MejoramientoGenetico = (!empty($entrevistaArray['MejoramientoGenetico']) )? 'checked':'' ?>> Mejoramiento genético</li>
                                        <li> <input type="checkbox" name= "sisProAn14" id="sisProAn14" <?php echo $NuticionAnimal = (!empty($entrevistaArray['NutricionAnimal']) )? 'checked':'' ?>> Nutrición Animal</li>
                                        <li> <input type="checkbox" name= "sisProAn15" id="sisProAn15" <?php echo $PracticasSalud = (!empty($entrevistaArray['PracticasSalud']) )? 'checked':'' ?>> Prácticas de Salud Animal</li>
                                        <li> <input type="checkbox" name= "sisProAn16" id="sisProAn16" <?php echo $VeterinariaNatural = (!empty($entrevistaArray['VeterinariaNatural']) )? 'checked':'' ?>> Veterinaria Natural</li>
                                        <li> <input type="checkbox" name= "sisProAn17" id="sisProAn17" <?php echo $Homeopatía = (!empty($entrevistaArray['Homeopatia']) )? 'checked':'' ?>> Homeopatia </li>

                                    </ul>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        Producción e intercambio de instalaciones
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                       <li> <input type="text" name="sisProAn18" id="sisProAn18" value="<?php echo $OtroProduccion= (!empty($entrevistaArray['OtroProduccion']) )? $entrevistaArray['OtroProduccion']:'' ?>"> Otro: (especificar) </li>

                                    </ul>

                                <li> <input type="checkbox" name="sisProAn19" id="sisProAn19" <?php echo $Construccio = (!empty($entrevistaArray['Construccio']) )? 'checked':'' ?>> Construcción y mantenimiento de instalaciones </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--//Sistemas de producción animal-->

                <!--Sistemas de producción agrícola-->
                <div class="panel panel-default">
                    <div class="panel-heading style-uv text-center">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#">
                                Sistemas de producción agrícola
                            </a>
                        </h4>
                    </div>
                    <div id="" class="panel-collapse ">
                        <div class="panel-body">
                            <ul>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        Estrategias alternativas de producción:
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li> <input type="checkbox" name="estrategia1" id="estrategia1" <?php echo $Permacultura = (!empty($entrevistaArray['Permacultura']) )? 'checked':'' ?>> Permacultura</li>
                                        <li> <input type="checkbox" name="estrategia2" id="estrategia2" <?php echo $AgriculturaBiodinamica = (!empty($entrevistaArray['AgriculturaBiodinamica']) )? 'checked':'' ?>> Agricultura biodinámica</li>
                                        <li> <input type="checkbox" name="estrategia3" id="estrategia3" <?php echo $AgriculturaNatural = (!empty($entrevistaArray['AgriculturaNatural']) )? 'checked':'' ?>> Agricultura natural</li>
                                        <li> <input type="checkbox" name="estrategia4" id="estrategia4" <?php echo $AgriculturaOrganica = (!empty($entrevistaArray['AgriculturaOrganica']) )? 'checked':'' ?>> Agricultura orgánica</li>
                                        <li> <input type="checkbox" name="estrategia5" id="estrategia5" <?php echo $Agrohomeopatia = (!empty($entrevistaArray['Agrohomeopatia']) )? 'checked':'' ?>> Agrohomeopatía</li>
                                        <li> <input type="checkbox" name="estrategia6" id="estrategia6" <?php echo $TransicionSustiInsumo = (!empty($entrevistaArray['TransicionSustiInsumo']) )? 'checked':'' ?>> Transición en sustitución de insumos</li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        Manejo de plagas y enfermedades:
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li> <input type="checkbox" name="estrategia7" id="estrategia7" <?php echo $SustitucionAgroquimica = (!empty($entrevistaArray['SustitucionAgroquimica']) )? 'checked':'' ?>> Sustitución de agroquímicos</li>
                                        <li> <input type="checkbox" name="estrategia8" id="estrategia8" <?php echo $ManejoBioologico = (!empty($entrevistaArray['ManejoBioologico']) )? 'checked':'' ?>> Manejo biológico</li>
                                        <li> <input type="checkbox" name="estrategia9" id="estrategia9" <?php echo $ManejoIntegrado = (!empty($entrevistaArray['ManejoIntegrado']) )? 'checked':'' ?>> Manejo integrado</li>

                                    </ul>
                                </li>

                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        Manejo de suelos:
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li> <input type="checkbox" name="estrategia10" id="estrategia10" <?php echo $AbonoVerde = (!empty($entrevistaArray['AbonoVerde']) )? 'checked':'' ?>> Abonos verdes</li>
                                        <li> <input type="checkbox" name="estrategia11" id="estrategia11" <?php echo $BarbechoDescanso = (!empty($entrevistaArray['BarbechoDescanso']) )? 'checked':'' ?>> Barbecho o descanso</li>
                                        <li> <input type="checkbox" name="estrategia12" id="estrategia12" <?php echo $HarinaRoca = (!empty($entrevistaArray['HarinaRoca']) )? 'checked':'' ?>> Uso de harinas de roca</li>
                                        <li> <input type="checkbox" name="estrategia13" id="estrategia13" <?php echo $Cobertura = (!empty($entrevistaArray['Cobertura']) )? 'checked':'' ?>> Coberteras</li>
                                        <li> <input type="checkbox" name="estrategia14" id="estrategia14" <?php echo $Compostaje = (!empty($entrevistaArray['Compostaje']) )? 'checked':'' ?>> Compostaje</li>
                                        <li> <input type="checkbox" name="estrategia15" id="estrategia15" <?php echo $MejoraFertilidad = (!empty($entrevistaArray['MejoraFertilidad']) )? 'checked':'' ?>> Mejoramiento de la fertilidad</li>
                                        <li> <input type="checkbox" name="estrategia16" id="estrategia16" <?php echo $ProduccionBiofertilizante = (!empty($entrevistaArray['ProduccionBiofertilizante']) )? 'checked':'' ?>> Producción de biofertilizantes</li>
                                        <li> <input type="checkbox" name="estrategia17" id="estrategia17" <?php echo $ProduccionAbonoSolido = (!empty($entrevistaArray['ProduccionAbonoSolido']) )? 'checked':'' ?>> Producción de abonos sólidos</li>
                                        <li> <input type="checkbox" name="estrategia18" id="estrategia18" <?php echo $PracticaConservacionSuelo = (!empty($entrevistaArray['PracticaConservacionSuelo']) )? 'checked':'' ?>> Práticas de conservación de suelos</li>
                                        <li> <input type="checkbox" name="estrategia19" id="estrategia19" <?php echo $SiembraDirectaCultivo = (!empty($entrevistaArray['SiembraDirectaCultivo']) )? 'checked':'' ?>> Siembra directa y cultivo</li>
                                        <li> <input type="checkbox" name="estrategia21" id="estrategia20" <?php echo $SiembraCultivoAnimal = (!empty($entrevistaArray['SiembraCultivoAnimal']) )? 'checked':'' ?>> Siembra y cultivo con tracción animal</li>
                                        <li> <input type="checkbox" name="estrategia20" id="estrategia21" <?php echo $SiembraContivoConRotu = (!empty($entrevistaArray['SiembraContivoConRotu']) )? 'checked':'' ?>> Siembra y cultivo con (roturacion tractor, maquinaria)</li>
                                        <li> <input type="checkbox" name="estrategia22" id="estrategia22" <?php echo $SiembraDirecta = (!empty($entrevistaArray['SiembraDirecta']) )? 'checked':'' ?>> Siembra directa</li>

                                    </ul>
                                </li>

                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                        Sistemas de producción agrícola:
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li> <input type="checkbox" name="estrategia23" id="estrategia23" <?php echo $Cereales = (!empty($entrevistaArray['Cereales']) )? 'checked':'' ?>> Cereales</li>
                                        <li> <input type="checkbox" name="estrategia24" id="estrategia24" <?php echo $CultivoAsociado = (!empty($entrevistaArray['CultivoAsociado']) )? 'checked':'' ?>> Cultivos asociados</li>
                                        <li> <input type="checkbox" name="estrategia25" id="estrategia25" <?php echo $Forraje = (!empty($entrevistaArray['Forraje']) )? 'checked':'' ?>> Forrajes</li>
                                        <li> <input type="checkbox" name="estrategia26" id="estrategia26" <?php echo $Frutales = (!empty($entrevistaArray['Frutales']) )? 'checked':'' ?>> Frutales</li>
                                        <li> <input type="checkbox" name="estrategia27" id="estrategia27" <?php echo $Hortaliza = (!empty($entrevistaArray['Hortaliza']) )? 'checked':'' ?>> Hortalizas</li>
                                        <li> <input type="checkbox" name="estrategia28" id="estrategia28" <?php echo $Leguminosa = (!empty($entrevistaArray['Leguminosa']) )? 'checked':'' ?>> Leguminosas</li>
                                        <li> <input type="checkbox" name="estrategia29" id="estrategia29" <?php echo $Medicinales = (!empty($entrevistaArray['Medicinales']) )? 'checked':'' ?>> Medicinales</li>
                                        <li> <input type="checkbox" name="estrategia30" id="estrategia30" <?php echo $Oleaginosa = (!empty($entrevistaArray['Oleaginosa']) )? 'checked':'' ?>> Oleaginosas</li>
                                        <li> <input type="checkbox" name="estrategia31" id="estrategia31" <?php echo $Textil = (!empty($entrevistaArray['Textil']) )? 'checked':'' ?>> Textiles</li>
                                    </ul>
                                </li>
                                <li> <input type="text" name="estrategia32" id="estrategia32" value="<?php echo $OtroAgricola= (!empty($entrevistaArray['OtroAgricola']) )? $entrevistaArray['OtroAgricola']:'' ?>"> Otro (especificar) </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--//Sistemas de producción agrícola-->

            </div>
        </div>
    </div>

    </form>

    <div class="container">
        <div class="row">



            <a href="Form.php" class="btn btn-default"> Regresar </a>
            <a href="#modal-id" class="a-btn" data-toggle="modal"><button type="button" class="btn btn-info" id="guardarFase2">Siguiente</a></button>

            <?php include 'Modal-Cargado.php' ?>

            <?php include 'btn-subir.php' ?>

        </div>
    </div>


</fieldset>

<?php include 'Modal-Cargado.php' ?>