<?php

//session_start();
include_once 'ConexionBD.php';
$user = $_SESSION['Usuario'];
$id = $_SESSION['idEntrevista'];

//var_dump($user);
//var_dump($id);



$sql_id = "select cae.idEntrevista as id from co_agr_entrevista cae where cae.nombreUsuario = '$user'";

$query_id = $conexion->query($sql_id);
$array = $query_id->fetch_array(MYSQLI_ASSOC);
$result_id = $array['id'];

//var_dump($result_id);

$sql = "
select 
cac.idContacto as id,
cac.Nombre as nombre,
cac.Telefono as telefono,
cac.Correo as correo,
cac.idEntrevista as idE
from co_agr_entrevista cae
left join co_agr_contacto cac
on cae.idEntrevista = cac.idEntrevista where cac.idEntrevista ='$result_id'
";

$query = $conexion->query($sql);
//$entrevistaArray = $query->fetch_array(MYSQLI_ASSOC);
$bandera = $query->num_rows;
//var_dump($entrevistaArray);
//var_dump($bandera);
?>
<fieldset id="9">
    <form id="register_form9" novalidate method="post">
        <div class="container">
            <div class="row">
                <br>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                        100%
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                    <h2 class="text-center">Nota</h2>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    <label>
                        ¿Si conoces a alguien que pueda estar interesada/o en llenar esta ficha? ¿Nos puedes compartir su contacto?
                    </label>
                    <hr>
                </div>
                <?php if ($bandera > 0): ?>
                <?php while ($resul_nota = $query->fetch_array(MYSQLI_ASSOC)) : ?>
                        <input type="hidden" class="form-control" name="idfase9[]" id="idfase9" value="<?php echo $id = (!empty($resul_nota['id'])) ? $resul_nota['id'] : '' ?>">

                        <div class="inputs">
                    <br>
                    <br>

                        <!--nombre-nota-->
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 paddin-uv">
                        <label for="exampleInputName2">Nombre</label>
                        <input type="text" name="nota1[]" id="nota1" class="form-control" placeholder="" value="<?php echo $nombre = (!empty($resul_nota['nombre'])) ? $resul_nota['nombre'] : '' ?>" required="required" pattern="" title="">
                    </div>
                    <!--correo-nota-->
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 paddin-uv">
                        <label for="exampleInputName2">Correo</label>
                        <input type="email" name="nota2[]" id="nota2" class="form-control" placeholder="" value="<?php echo $correo = (!empty($resul_nota['correo'])) ? $resul_nota['correo'] : '' ?>" required="required" pattern="" title="">
                    </div>
                    <!--correo-telefono-->
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 paddin-uv">
                        <label for="exampleInputName2">Teléfono</label>
                        <input type="number" name="nota3[]" id="nota3" class="form-control" placeholder="" value="<?php echo $telefono = (!empty($resul_nota['telefono'])) ? $resul_nota['telefono'] : '' ?>" required="required" pattern="" title="">
                    </div>

                    <br>
                    <br>
                    <?php endwhile; ?>
                <?php else: ?>
                    <div class="inputs">
                        <br>
                        <br>
                        <!--nombre-nota-->
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 paddin-uv">
                            <label for="exampleInputName2">Nombre</label>
                            <input type="text" name="nota1[]" id="nota1" class="form-control" placeholder="Nombre" value="" required="required" pattern="" title="">
                        </div>
                        <!--correo-nota-->
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 paddin-uv">
                            <label for="exampleInputName2">Correo</label>
                            <input type="Email" name="nota2[]" id="nota2" class="form-control" placeholder="@" value="" required="required" pattern="" title="">
                        </div>
                        <!--correo-telefono-->
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 paddin-uv">
                            <label for="exampleInputName2">Telefono</label>
                            <input type="text" name="nota3[]" id="nota3" class="form-control" maxlength="13" placeholder="Teléfono" value="" required="required" pattern="" title="">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 padding-uv-botton">
                            <button type="button" class="btn btn-large btn-info" onclick="nuevo();"><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </div>
                        <br>
                        <br>
                    </div>
                    <div name="enviar"></div>
                    <script>
                        let nuevo = function() {
                            $("<section/>").insertBefore("[name='enviar']")
                                .append($(".inputs").html())
                                .find("button")
                                .attr("onclick", "eliminar(this)")
                                .text("x");
                        }

                        let eliminar = function(obj) {
                            $(obj).closest("section").remove();
                        }
                    </script>
                    </div>
                <?php endif; ?>
            </div>
            <br>
        </div>
        </div>
    </form>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 paddin-uv">
                <br>
            <a href="Preguntas-fase8.php" class="btn btn-default"> Regresar </a>
            <a href="#modal-id-sig-fase-9" data-toggle="modal"><button type="" class=" submit btn btn-success" id="guardarFase9">Finalizar</button></a>
            <?php include_once 'modal-guardar-final.php'?>

            <?php include 'btn-subir.php' ?>
            </div>

        </div>
    </div>
</fieldset>