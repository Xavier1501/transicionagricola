<style>
    .uv-img-login {
        max-width: 100%;
        width: 80%;
    }

    .img-contenedor {
        position: relative;
        top: 10px;
    }


    @media (min-width: 992px) {
        .img-contenedor {
            position: relative;
            top: 70px;
        }
    }
</style>
<div class="container">
    <div class="row">

        

        <br><br>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center img-contenedor">
            <a href="https://www.uv.mx/apps/cuo/cosustenta/transicion_agricola/"><img src="img/reg_iniciativas_agroec_pnta.png" alt="" class="uv-img-login "></a>

        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 " style=" position: relative; top: 40px;">
                <h4 class="text-center">Registro de usuario</h4>
                <p class="text-justify">
                    Para iniciar su participación, deberá generar un nombre de usuario y una contraseña, los cuales deberá de recordar o anotar,
                    ya que le permitirán acceder al sistema de registro de su experiencia o iniciativa agroecológica.
                    <br><br>

                    El registro es bastante largo y podrá parar cuando así lo requiera, podrá concluir su llenado
                    utilizando el nombre de usuario y contraseña que generó.
                    <br><br>


                    NOTA: Al momento del llenando del registro de su experiencia, se le solicitará que nos
                    comparta 3 imágenes representativas de su iniciativa en formato jpg o png cada una no mayor de 1 MB.

                </p><br>
                <br>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  bg-login-uv">
                <form id="registroFormUser">
                    <fieldset>
                        <div id="Error"></div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group" id="userDiv">
                                <input type="hidden" id="grupo" name="grupo" class="form-control" value="<?php echo (!empty($_GET['valor'])? $_GET['valor']: 'grupo')?>" placeholder="tipogrupo">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group" id="userDiv">
                                <input type="text" id="user" name="user" class="form-control" placeholder="Usuario">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group" id="passDiv">
                                <input type="password" id="password" name="password" class="form-control" placeholder="Contraseña (8 Caracteres)">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group" id="passDiv2">
                                <input type="password" id="password2" class="form-control" placeholder="Escriba nuevamente la contraseña">
                            </div>
                        </div>

                    </fieldset>
                </form>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" id="registrar" value="Registrar" id="guardar"> Registrar </button>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                <p>Inicie o continúe su registro de su experiencia agroecológica, proporcionando su nombre de usuario y contraseña que generó:</p>
                <a href="inicio.php" type="hidden" class="btn btn-primary" id="botonCrear" name="botonCrear" value="Crear Cuenta">Iniciar Sesión</a>
                <br><br><br>

            </div>

        </div>
    </div>
</div>