<?php
include '../../ConexionBD.php';
$id = $_GET['id'];


$sql = "
select
cae.idEntrevista ,
cae.NombreGrupo,
cae.Correo, 
cai.Resumen,
cai.NombreIniciativa,
cai.Longitud,
cai.Latitud,
cai.estados,
cai.municipio
from co_agr_entrevista cae
LEFT JOIN co_agr_iniciativa cai ON cae.idEntrevista =cai.idEntrevista
where cai.idIniciativa = '$id'
";


$query = $conexion->query($sql);
$row = $query ->fetch_array(MYSQLI_ASSOC);
$lan = $row['Latitud'];
$lon = json_decode($row['Longitud']);

//var_dump($row);

$result_id = $row['idEntrevista'];


?>


<div class="container-fluid">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form action="" method="POST" role="form" id="editar">
            <div class="form-group col-sm-12">
                <label for="">Grupo</label>
                <input type="hidden" class="form-control" name="idiniciativa" id="idiniciativa" value="<?php echo $id?>" placeholder="Input field">
                <input type="hidden" class="form-control" name="identrevista" id="identrevista" value="<?php echo $row['idEntrevista']?>" placeholder="Input field">

                <input type="text" class="form-control" name="grupo" id="grupo" value="<?php echo $row['NombreGrupo']?>" placeholder="Input field">
            </div>
            <div class="form-group col-sm-12">
                <label for="">iniciativa</label>
                <input type="text" class="form-control" name="iniciativa" id="iniciativa" value="<?php echo $row['NombreIniciativa']?>" placeholder="Input field">
            </div>
            <div class="form-group col-sm-12">
                <label for="">Correo</label>
                <input type="email" class="form-control" name="correo" id="correo" value="<?php echo $row['Correo']?>" placeholder="Input field">

            </div>
            <div class="form-group col-sm-12">
                <label for="">Resumen</label>
                <textarea name="resumen" id="resumen" style="width: 98%;"><?php echo $row['Resumen']?></textarea>
            </div>

            <div class="form-group col-sm-12">
                <label for="">Estado</label>
                <input type="text" class="form-control" name="estado" id="estado"  value="<?php echo $row['estados']?>" placeholder="Input field">
            </div>

            <div class="form-group col-sm-12">
                <label for="">Municipio</label>
                <input type="text" class="form-control" name="municipio" id="municipio"  value="<?php echo $row['municipio']?>" placeholder="Input field">
            </div>





            



            <hr>


            <!--pregunta 4-->
            <label for="">Ubicacion</label>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" onload="initialize()" id="error">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tam" id="map_canvas" style="max-width: 100%;
                          position: relative;
                          overflow: hidden;
                          height: 500px;
                          width: 100%;">
                </div>
                <input id="txtLat" type="hidden" name="txtLat" style="color:red"
                       value="<?php echo $lan; ?>" />
                <input id="txtLng" type="hidden" name="txtLng" style="color:red"
                       value="<?php echo $lon; ?>" /><br />
                <br />
                <br />
            </div>
            <!--//pregunta4-->

        </form>


    </div>

</div>


<script>

    let lan = <?php echo json_encode($lan);?>;
    let lon = <?php echo json_encode($lon);?>;


    /*funcion del mapa*/
    function initialize() {
        // Creamos el objeto
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 5,
            center: new google.maps.LatLng(lan, lon),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        // creates a draggable marker to the given coords
        var vMarker = new google.maps.Marker({
            position: new google.maps.LatLng(lan, lon),
            draggable: true
        });

        // adds a listener to the marker
        // gets the coords when drag event ends
        // then updates the input with the new coords
        google.maps.event.addListener(vMarker, 'dragend', function (evt) {
            $("#txtLat").val(evt.latLng.lat().toFixed(6));
            $("#txtLng").val(evt.latLng.lng().toFixed(6));
            map.panTo(evt.latLng);
        });

        // centers the map on markers coords
        map.setCenter(vMarker.position);

        // adds the marker on the map
        vMarker.setMap(map);
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKrV88GHRaVGDBAmgj2_KmGnFnFOl4zvs&callback=initialize"
        async defer></script>