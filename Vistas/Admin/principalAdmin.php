<?php


$sql = '
select * from co_agr_entrevista cae 
left join co_agr_usuario cau on cae.idUser = cau.idUsuario
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa desc ;
';
$result =  $conexion->query($sql);
//var_dump($sql);
?>
<div class="container">
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1 class="text-center">Administrador</h1>
        </div>
        <br>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 table-responsive">
            <table id="example" class="table table-striped table-bordered ">
                <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>Contraseña Asignada</th>
                        <th>Fecha de creación</th>
                        <th>Nombre del grupo</th>
                        <th>Nombre del representante</th>
                        <th>Nombre de la iniciativa</th>
                        <th>Modificacion encuesta</th>
                        <th>Modificar</th>
                        <th>Eliminar</th>
                        <th>Activar</th>



                    </tr>
                </thead>
                <tbody>

                    <?php
                    while ($rowUser = $result->fetch_array(MYSQLI_ASSOC)) :
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $rowUser["usuario"] ?></td>
                        <td class="text-center"><?php echo $rowUser["Contrasena"] ?></td>
                        <td class="text-center"><?php echo $rowUser["Fechacreacion"] ?></td>
                        <td class="text-center"><?php echo $rowUser["NombreGrupo"] ?></td>
                        <td class="text-center"><?php echo $rowUser['NombreRepresentante'] ?></td>
                        <td class="text-center"><?php echo $rowUser["NombreIniciativa"] ?></td>
                        <td class="text-center"><?php echo $rowUser["actualizacion_datos"] ?></td>
                        <td class="text-center">
                            <button class="btn btn-primary " id="editar-modal"
                                    value="<?php echo  $rowUser['idIniciativa']?>">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                            </button>
                        </td>
                        <td class="text-center">
                            <button class="btn btn-danger " id="eliminar-modal"
                                    value="<?php echo  $rowUser['idEntrevista']?>">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </button>
                        </td>
                        <td class="text-center">
                            <form id="activar">
                                <input type="checkbox" name="activo" id="activo" value="<?php echo  $rowUser['idIniciativa']?>"
                                    <?php echo ($rowUser["activo"] == 1) ? 'checked' : ''  ?>>
                            </form>
                        </td>

                    </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>

        </div>
    </div>
</div>

<?php
include_once 'modal-Admin-Editar.php';
include_once 'modal-Admin-eliminar.php';
?>





