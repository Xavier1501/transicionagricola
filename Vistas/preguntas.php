

<section class="registro">

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3>Proceso de llenado de la ficha de Sistematización</h3>

                <p class="text-justify ">
                    Datos de identificación, (funciona como el primero elemento de ingreso a la plataforma
                    y permite que el usuario puede tener una clase de acceso).
                </p>
            </div>

            <div class="col-12">
                <form class="needs-validation" method="POST">

                    <!---Nombre Grupo--->
                    <div class="form-group row border-bottom pb-2">
                        <label for="name" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                            Nombre del grupo:
                        </label>
                        <div class="col-sm-12 col-md-8 col-lg-9">
                            <input type="text" class="form-control" name="name" id="name" value="" required>
                        </div>
                    </div>

                    <!---Nombre representante---->
                    <div class="form-group row border-bottom pb-2">
                        <label for="representante" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                            Nombre del representante:
                        </label>
                        <div class="col-sm-12 col-md-8 col-lg-9">
                            <input type="text" class="form-control" name="representante" id="representante" value=""
                                   required>
                        </div>
                    </div>

                    <!---Correo Electronico--->
                    <div class="form-group row border-bottom pb-2">
                        <label for="correoElectronico" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                            Correo Electronico:
                        </label>
                        <div class="col-sm-12 col-md-8 col-lg-9">
                            <input type="email" class="form-control" name="correoElectronico" id="correoElectronico"
                                   placeholder="correo@ejemplo.com" value="" required>
                        </div>
                    </div>

                    <!---Sitio Internet--->
                    <div class="form-group row border-bottom pb-2">
                        <label for="sitioInternet" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                            Sitio de internet o página de Facebook:
                        </label>
                        <div class="col-sm-12 col-md-8 col-lg-9">
                            <input type="text" class="form-control" name="SitioInternet" id="SitioInternet" value=""
                                   required>
                        </div>
                    </div>

                    <!--Ubicacion-->


            </div>
        </div>
    </div>

</section>
