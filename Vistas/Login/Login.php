<div class="container-fluid">
    <div class="row">


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center conte-login">

            <br><br><br>
            <div class=" col-lg-1 "></div>  
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-5 uv-padding text-center">
                <img src="img/reg_iniciativas_agroec_pnta.png" alt="" class="uv-img-login">

            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-5 posicion-uv-login text-center">
                <div class="" id="error" role="alert"></div>
                <!--<h1 class="text-center">Atlas de Iniciativas Agroecológicas en México</h1>-->
               
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 " style=" position: relative; top: 10px;">
                    <h3>Bienvenidos</h3 >
                    <p class="text-justify">
                        Muchas gracias por ser parte de este proyecto que intenta visibilizar la gran riqueza de experiencias agroecológicas de nuestro país, su aportación será de gran ayuda para avanzar hacia una política pública a nivel nacional, construida entre todas y todos nosotros. Los datos que ingrese serán de carácter confidencial y se usarán exclusivamente para los fines del proyecto.
                    </p>
                    <br>
                    <br>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  bg-login-uv text-center">
                    <form id="user_session">
                        <fieldset>                           
                            <div id="Error"></div>
                            <h4 class="text-left" style="font-family:'Arial','Open Sans';">Iniciar Sesión</h4><hr>
                            <div class="form-group" id="userDiv">
                                <input type="text" id="username" name="username" class="form-control" placeholder="Nombre de usuario" name="username" autofocus="" required="">
                            </div>
                            <div class="form-group" id="userDiv">
                                <input type="password" id="userPassword" name="userPassword" class="form-control" placeholder="contraseña" name="username" autofocus="" required="">
                            </div>
                            <div class="form-group">
                                <div class="for-check"></div>
                            </div>
                        </fieldset>

                    </form>                  

                    <button type="submit" class="btn btn-primary" id="ingresar" value="Registrar"  > Ingresar </button>

                   </div>

                    <p>Si aún no cuenta con su usuario y contraseña, vaya al siguiente enlace para su registro: </p>
                    
                    <a href="registro.php" type="hidden"  class="btn btn-primary text-left" id="botonCrear" name="botonCrear" value="Crear Cuenta" formtarget="_blank"> Crear Cuenta </a><br><br><br>
                </div>
                <div class=" col-lg-1 "></div>   
            

        </div>

    </div>
</div>