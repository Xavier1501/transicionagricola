<?php
include_once 'Vistas/menu.php';
include_once 'ConexionBD.php';
?>


<style>
    .style-uv {
        border: solid 1px #28AD56 !important;
    }

    hr {
        background-color: #28AD56;
    }

    .seguridad ul li {
        list-style: none;
    }

    .panel-default>.panel-heading {
        background-color: rgba(128, 128, 128, .8);
        color: #ffffff;

    }
</style>




<section class="form">

    <div class="container">

        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
        </div>


        <div class="alert alert-danger hide"></div>


        <form id="register_form" novalidate method="post">
            <fieldset>
                <div class="container">
                    <div class="row">
                        <form id="register_form" novalidate method="post">
                            <fieldset>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <h3>Proceso de llenado de la ficha de Sistematización</h3>
                                            <p class="text-justify">
                                                <strong>
                                                    Datos de identificación, (funciona como el primero elemento de ingreso a la plataforma
                                                    y permite que el usuario puede tener una clase de acceso).
                                                </strong>

                                            </p>
                                        </div>
                                        <!---Nombre Grupo--->
                                        <div class="form-group row border-bottom pb-2">
                                            <label for="name" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                                                &nbsp; &nbsp; Nombre del grupo:
                                            </label>
                                            <div class="col-sm-12 col-md-8 col-lg-9">
                                                <input type="text" class="form-control" name="name" id="name" value="" required>
                                            </div>
                                        </div>
                                        <!---Nombre representante---->
                                        <div class="form-group row border-bottom pb-2">
                                            <label for="representante" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                                                &nbsp; &nbsp;Nombre del representante:
                                            </label>
                                            <div class="col-sm-12 col-md-8 col-lg-9">
                                                <input type="text" class="form-control" name="representante" id="representante" value="" required>
                                            </div>
                                        </div>
                                        <!---Correo Electronico--->
                                        <div class="form-group row border-bottom pb-2">
                                            <label for="correoElectronico" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                                                &nbsp; &nbsp; Correo electrónico:
                                            </label>
                                            <div class="col-sm-12 col-md-8 col-lg-9">
                                                <input type="email" class="form-control" name="correo" id="correo" placeholder="correo@ejemplo.com" value="" required>
                                            </div>
                                        </div>
                                        <!---Sitio Internet--->
                                        <div class="form-group row border-bottom pb-2">
                                            <label for="sitioInternet" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
                                                Sitio de internet o página de Facebook:
                                            </label>
                                            <div class="col-sm-12 col-md-8 col-lg-9">
                                                <input type="text" class="form-control" name="urlSocial" id="urlSocial" value="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <!--Estado-->
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName2">Estado</label>
                                    <select class="form-control" id="estado" name="estado">
                                        <option selected value="">Seleccionar</option>

                                        <?php
                                        $sql = "select * from co_agr_estado";
                                        $result = $conexion->query($sql);
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['cve_ent'] . '">' . $row['opcion'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <!--/Estado-->

                                <!--Municipio-->
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName2">Municipio</label>
                                    <select class="form-control" id="municipio" name="municipio">
                                        <option selected value="">Seleccionar</option>
                                        <?php
                                        $sql = "select * from co_agr_municipio";
                                        $result = $conexion->query($sql);
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['cve_ent'] . '">' . $row['opcion'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <!--/Municipio-->

                                <!--Direccion-->
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName2">Direccion</label>
                                    <input type="text" class="form-control" id="direccion" name="direccion" placeholder="">
                                </div>
                                <!--/Direccion-->

                                <!--Codigo Postal-->
                                <div class="form-group col-md-6">
                                    <label for="exampleInputName2">Codigo Postal</label>
                                    <input type="number" class="form-control" id="cp" name="cp" placeholder="">
                                </div>
                                <!--/Codigo Postal-->


                                <input type="button" class="next-form btn btn-info" value="siguiente" />
                            </fieldset>


                            <fieldset>
                                <h2> Área(s) en las que trabajan </h2>
                                <br>
                                <div class="container">
                                    <div class="row">



                                        <div class="col-md-4">
                                            <div class="panel-group">

                                                <!--Agricultura Urbana  -->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#">
                                                                Agricultura Urbana
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="AgriUr1" id="AgriUr1">
                                                                    Agricultura Urbana
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Agricultura Urbana  -->

                                                <!--Salud y plantas medicinales-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#">
                                                                Salud y plantas medicinales
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="" class="panel-collapse  ">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="salud1" id="salud1"> Homeopatía </li>
                                                                <li> <input type="checkbox" name="salud2" id="salud2"> Medicina tradicional </li>
                                                                <li> <input type="checkbox" name="salud3" id="salud3"> Preparados botánicos </li>
                                                                <li> <input type="checkbox" name="salud4" id="salud4"> Remedios caseros</li>
                                                                <li> <input type="text" name="salud5" id="salud5"> Otro (especificar) </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Salud y plantas medicinales-->
                                                <!--Actividades no agrícolas-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#">Actividades no agrícolas</a>
                                                        </h4>
                                                    </div>
                                                    <div id="" class="panel-collapse ">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="ActividadNoAgro1" id="ActividadNoAgro1"> Artesanías </li>
                                                                <li> <input type="checkbox" name="ActividadNoAgro2" id="ActividadNoAgro2"> Crédito </li>
                                                                <li> <input type="checkbox" name="ActividadNoAgro3" id="ActividadNoAgro3"> Turismo </li>
                                                                <li> <input type="checkbox" name="ActividadNoAgro4" id="ActividadNoAgro4"> Culturales</li>
                                                                <li> <input type="text" name="ActividadNoAgro5" id="ActividadNoAgro5"> Otro (especificar) </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Actividades no agrícolas-->
                                                <!--Desarrollo Local-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#">Desarrollo Local</a>
                                                        </h4>
                                                    </div>
                                                    <div id="" class="panel-collapse ">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="DesarrolloLocal1" id="DesarrolloLocal1"> Acompañamiento de proyectos </li>
                                                                <li> <input type="checkbox" name="DesarrolloLocal2" id="DesarrolloLocal2"> Desarrollo de proyectos productivos </li>
                                                                <li> <input type="checkbox" name="DesarrolloLocal3" id="DesarrolloLocal3"> Organización de agricultores/as </li>
                                                                <li> <input type="checkbox" name="DesarrolloLocal4" id=DesarrolloLocal4> Programas gubernamentales</li>
                                                                <li> <input type="checkbox" name="DesarrolloLocal5" id="DesarrolloLocal5"> Planes de núcleos agrarios (ejidos,
                                                                    bienes
                                                                    comunales)</li>
                                                                <li> <input type="text" name="DesarrolloLocal6" id="DesarrolloLocal6"> Otro (especificar) </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Desarrollo Local-->
                                                <!--Economia-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#">Economia</a>
                                                        </h4>
                                                    </div>
                                                    <div id="" class="panel-collapse ">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="Economia1" id="Economia1"> Autoconsumo </li>
                                                                <li> <input type="checkbox" name="Economia2" id="Economia2"> Certificación participativa (Sistema
                                                                    participativo de garantía) </li>
                                                                <li> <input type="checkbox" name="Economia3" id="Economia3"> Certificación de tercera parte </li>
                                                                <li> <input type="checkbox" name="Economia4" id="Economia4"> Comercialización local</li>
                                                                <li> <input type="checkbox" name="Economia5" id="Economia5"> Comercialización reginal</li>
                                                                <li class="dropdown">
                                                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                                        Cooperativas de:
                                                                        <span class="caret"></span></a>
                                                                    <ul class="dropdown-menu">
                                                                        <li> <input type="checkbox" name="Cooperativa1" id="Cooperativa1"> Consumo</li>

                                                                        <li class="dropdown">
                                                                            <a class="" data-toggle="dropdown" href="#">
                                                                                Produccion de:
                                                                                <span class="caret"></span></a>
                                                                            <ul class="dropdown-menu">
                                                                                <li> <input type="checkbox" name="producion01" id="producion01">Semillas</li>
                                                                                <li> <input type="checkbox" name="producion02" id="producion02">Insumos</li>

                                                                            </ul>

                                                                        <li> <input type="checkbox" name="Cooperativa2" id="Cooperativa2"> Crédito</li>
                                                                        <li> <input type="checkbox" name="Cooperativa3" id="Cooperativa3"> Servicios</li>
                                                                    </ul>

                                                                </li>
                                                                <li><input type="checkbox" name="Economia6" id="Economia6">Empresa productora de insumos</li>
                                                                <li><input type="checkbox" name="Economia7" id="Economia7">Iniciativa de intercambio (trueque)</li>
                                                                <li><input type="checkbox" name="Economia8" id="Economia8">Mercado alternativo (agroecológico)</li>
                                                                <li><input type="checkbox" name="Economia9" id="Economia9">Sistema de gestas</li>
                                                                <li><input type="checkbox" name="Economia10" id="Economia10">Tianguis orgánico</li>
                                                                <li> <input type="text" name="Economia11" id="Economia11"> Otro (especificar) </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Economia-->
                                                <!--Educación-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#">Educación</a>
                                                        </h4>
                                                    </div>
                                                    <div id="" class="panel-collapse ">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="Educacion1" id="Educacion1"> Educación ambiental </li>
                                                                <li> <input type="checkbox" name="Educacion2" id="Educacion2"> Escuela campesina </li>
                                                                <li> <input type="checkbox" name="Educacion3" id="Educacion3"> Formación de técnicos </li>
                                                                <li> <input type="checkbox" name="Educacion4" id="Educacion4"> Formación universitaria</li>
                                                                <li><input type="checkbox" name="Educacion5" id="Educacion5">Iniciativa de campesino a campesino</li>
                                                                <li> <input type="text" name="Educacion6" id="Educacion6"> Otro (especificar) </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Educación-->
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <!--Derechos humanos  -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading style-uv text-center">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" href="#">
                                                            Derechos humanos
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="" class="panel-collapse ">
                                                    <div class="panel-body">
                                                        <ul>
                                                            <li> <input type="checkbox" name="DerechosH1" id="DerechosH1">
                                                                Derechos humanos
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--//Derechos humanos-->

                                            <div class="panel-group">
                                                <!--Trabajo con niños/juventud-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#">
                                                                Trabajo con niños/juventud
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="" class="panel-collapse ">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="TrabajoNJ1" id="TrabajoNJ1"> Huertos-parcelas escolares </li>
                                                                <li> <input type="checkbox" name="TrabajoNJ2" id="TrabajoNJ2"> Proyectos pedagógicos </li>
                                                                <li> <input type="checkbox" name="TrabajoNJ3" id="TrabajoNJ3"> Otro (especificar) </li>
                                                                <li> <input type="text" name="TrabajoNJ4" id="TrabajoNJ4"> Otro (especificar) </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Trabajo con niños/juventud-->

                                                <!--Investigación agrícola y Extensión rural-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#">
                                                                Investigación agrícola y Extensión rural
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="" class="panel-collapse ">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="Investigacion1" id="Investigacion1"> Comunicación</li>
                                                                <li> <input type="checkbox" name="Investigacion2" id="Investigacion2"> Diagnóstico de agroecosistemas </li>
                                                                <li> <input type="checkbox" name="Investigacion3" id="Investigacion3"> Experimentación </li>
                                                                <li> <input type="checkbox" name="Investigacion4" id="Investigacion4"> Formación y capacitación </li>
                                                                <li> <input type="checkbox" name="Investigacion5" id="Investigacion5"> Investigación </li>
                                                                <li> <input type="checkbox" name="Investigacion6" id="Investigacion7"> Ordenamientos territoriales </li>
                                                                <li> <input type="text" name="Investigacion7" id="Investigacion7"> Otro (especificar) </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Investigación agrícola y Extensión rural-->

                                                <!--Manejo de recursos hídricos-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#">
                                                                Manejo de recursos hídricos
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="" class="panel-collapse ">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="ManejoRecursos1" id="ManejoRecursos1"> Capacitación y almacenamiento de agua de lluvias</li>
                                                                <li> <input type="checkbox" name="ManejoRecursos2" id="ManejoRecursos2"> Enfoque de microcuencas </li>
                                                                <li> <input type="checkbox" name="ManejoRecursos3" id="ManejoRecursos3"> Implementación de ecotecnologías </li>
                                                                <li> <input type="checkbox" name="ManejoRecursos4" id="ManejoRecursos4"> Manejo comunitario del agua </li>
                                                                <li> <input type="checkbox" name="ManejoRecursos5" id="ManejoRecursos5"> Riego y drenaje </li>
                                                                <li> <input type="checkbox" name="ManejoRecursos6" id="ManejoRecursos6"> Tratamiento de aguas </li>
                                                                <li> <input type="text" name="ManejoRecursos7" id="ManejoRecursos7"> Otro (especificar) </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Manejo de recursos hídricos-->

                                                <!--Manejo poscosecha-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#">
                                                                Manejo poscosecha
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="" class="panel-collapse ">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="ManejoPoscosecha1" id="ManejoPoscosecha1"> Almacenamientos</li>
                                                                <li> <input type="checkbox" name="ManejoPoscosecha2" id="ManejoPoscosecha2"> Comercialización </li>
                                                                <li> <input type="checkbox" name="ManejoPoscosecha3" id="ManejoPoscosecha3"> Procesamiento o transformación </li>
                                                                <li> <input type="text" name="ManejoPoscosecha4" id="ManejoPoscosecha4"> Otro (especificar) </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Manejo poscosecha-->

                                                <!--Semillas-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#">
                                                                Semillas
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="" class="panel-collapse ">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="Semillas1" id="Semillas1"> Almacenamiento y conservación de semillas</li>
                                                                <li> <input type="checkbox" name="Semillas2" id="Semillas2"> Bancos comunitarios de semillas</li>
                                                                <li> <input type="checkbox" name="Semillas3" id="Semillas3"> Intercambio de Semillas</li>
                                                                <li> <input type="checkbox" name="Semillas4" id="Semillas4"> Mejoramiento de plantas y animales </li>
                                                                <li> <input type="checkbox" name="Semillas5" id="Semillas5"> Producción de semillas nativas y criollas </li>
                                                                <li> <input type="text" name="Semillas6" id="Semillas6"> Otro (especificar) </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Semillas-->
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <!--Genero-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading style-uv text-center">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" href="#">
                                                            Genero
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="" class="panel-collapse ">
                                                    <div class="panel-body">
                                                        <ul>
                                                            <li> <input type="checkbox" name="genero1" id="genero1"> Genero</li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--//Genero-->


                                            <!--Sistemas agroforestales-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading style-uv text-center">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" href="#">
                                                            Sistemas agroforestales
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="" class="panel-collapse ">
                                                    <div class="panel-body">
                                                        <ul>
                                                            <li> <input type="checkbox" name="sistemaAgro1" id="sistemaAgro1"> Montes y áreas comunes destinadas a la conservación de sistemas silvopastoriles</li>
                                                            <li> <input type="checkbox" name="sistemaAgro2" id="sistemaAgro2"> Sistemas Agroforestales Tradicionales</li>
                                                            <li class="dropdown">
                                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                                    Arborizacion:
                                                                    <span class="caret"></span></a>
                                                                <ul class="dropdown-menu">
                                                                    <li> <input type="checkbox" name="arbo1" id="arbo1"> Bosques comestibles</li>
                                                                    <li> <input type="checkbox" name="arbo2" id="arbo2"> Cercas vivas</li>
                                                                    <li> <input type="checkbox" name="arbo3" id="arbo3"> Barreras rompevientos</li>
                                                                </ul>

                                                            </li>

                                                            <li> <input type="text" name="sistemaAgro3" id="sistemaAgro3"> Otro (especificar) </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--//Sistemas agroforestales-->

                                            <!--Productos forestales-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading style-uv text-center">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" href="#">
                                                            Productos forestales
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="" class="panel-collapse ">
                                                    <div class="panel-body">
                                                        <ul>
                                                            <li> <input type="checkbox" name="prof1" id="prof1"> Productos forestables maderables</li>
                                                            <li> <input type="checkbox" name="prof2" id="prof2"> Productos forestales no maderables</li>
                                                            <li> <input type="text" name="prof3" id="prof3"> Otro (especificar) </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--//Productos forestales-->

                                            <!--Sistemas de producción animal-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading style-uv text-center">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" href="#">
                                                            Sistemas de producción animal
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="" class="panel-collapse ">
                                                    <div class="panel-body">
                                                        <ul>
                                                            <li class="dropdown">
                                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                                    Apicultura:
                                                                    <span class="caret"></span></a>
                                                                <ul class="dropdown-menu">
                                                                    <li> <input type="checkbox" name="apicultura1" id="apicultura1"> Abejas europeas</li>
                                                                    <li> <input type="checkbox" name="apicultura2" id="apicultura2"> Abejas nativas</li>
                                                                </ul>

                                                            </li>
                                                            <li> <input type="checkbox" name="sisProAn1" id="sisProAn1"> Acuacultura</li>
                                                            <li> <input type="checkbox" name="sisProAn2" id="sisProAn2"> Aves</li>
                                                            <li> <input type="checkbox" name="sisProAn3" id="sisProAn3"> Bovinos</li>
                                                            <li> <input type="checkbox" name="sisProAn4" id="sisProAn4"> Caprinos</li>
                                                            <li> <input type="checkbox" name="sisProAn5" id="sisProAn5"> Cunicultura</li>
                                                            <li> <input type="checkbox" name="sisProAn6" id="sisProAn6"> Equinos</li>
                                                            <li> <input type="checkbox" name="sisProAn7" id="sisProAn7"> Lombricultura</li>
                                                            <li> <input type="checkbox" name="sisProAn8" id="sisProAn8"> Manejo de Instalaciones</li>
                                                            <li> <input type="checkbox" name="sisProAn9" id="sisProAn9"> Mejoramiento genético de animales</li>
                                                            <li> <input type="checkbox" name="sisProAn10" id="sisProAn10"> Nutrición animal</li>
                                                            <li> <input type="checkbox" name="sisProAn11" id="sisProAn11"> Ovinos</li>
                                                            <li> <input type="checkbox" name="sisProAn12" id="sisProAn12"> Pesca</li>
                                                            <li> <input type="checkbox" name="sisProAn13" id="sisProAn13"> Porcinos</li>
                                                            <li> <input type="checkbox" name="sisProAn14" id="sisProAn14"> Prácticas de salud, veterinaria natural</li>
                                                            <li> <input type="text" name="sisProAn15" id="sisProAn15"> Otro (especificar) </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--//Sistemas de producción animal-->

                                            <!--Sistemas de producción agrícola-->
                                            <div class="panel panel-default">
                                                <div class="panel-heading style-uv text-center">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" href="#">
                                                            Sistemas de producción agrícola
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="" class="panel-collapse ">
                                                    <div class="panel-body">
                                                        <ul>
                                                            <li class="dropdown">
                                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                                    Estrategias alternativas de producción:
                                                                    <span class="caret"></span></a>
                                                                <ul class="dropdown-menu">
                                                                    <li> <input type="checkbox" name="estAlPro1" id="estAlPro1"> Permacultura</li>
                                                                    <li> <input type="checkbox" name="estAlPro2" id="estAlPro2"> Agricultura biodinámica</li>
                                                                    <li> <input type="checkbox" name="estAlPro3" id="estAlPro3"> Agricultura natural</li>
                                                                    <li> <input type="checkbox" name="estAlPro4" id="estAlPro4"> Agricultura orgánica</li>
                                                                    <li> <input type="checkbox" name="estAlPro5" id="estAlPro5"> Agrohomeopatía</li>
                                                                    <li> <input type="checkbox" name="estAlPro6" id="estAlPro6"> Transición en sustitución de insumos</li>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown">
                                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                                    Manejo de plagas y enfermedades:
                                                                    <span class="caret"></span></a>
                                                                <ul class="dropdown-menu">
                                                                    <li> <input type="checkbox" name="manPlEn1" id="manPlEn1"> Sustitución de agroquímicos</li>
                                                                    <li> <input type="checkbox" name="manPlEn2" id="manPlEn2"> Manejo biológico</li>
                                                                    <li> <input type="checkbox" name="manPlEn3" id="manPlEn3"> Manejo integrado</li>

                                                                </ul>
                                                            </li>

                                                            <li class="dropdown">
                                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                                    Manejo de suelos:
                                                                    <span class="caret"></span></a>
                                                                <ul class="dropdown-menu">
                                                                    <li> <input type="checkbox" name="mandSue1" id="mandSue1"> Abonos verdes</li>
                                                                    <li> <input type="checkbox" name="mandSue2" id="mandSue2"> Barbecho o descanso</li>
                                                                    <li> <input type="checkbox" name="mandSue3" id="mandSue3"> Uso de harinas de roca</li>
                                                                    <li> <input type="checkbox" name="mandSue4" id="mandSue4"> Coberteras</li>
                                                                    <li> <input type="checkbox" name="mandSue5" id="mandSue5"> Compostaje</li>
                                                                    <li> <input type="checkbox" name="mandSue6" id="mandSue6"> Mejoramiento de la fertilidad</li>
                                                                    <li> <input type="checkbox" name="mandSue7" id="mandSue7"> Producción de biofertilizantes</li>
                                                                    <li> <input type="checkbox" name="mandSue8" id="mandSue8"> Producción de abonos sólidos</li>
                                                                    <li> <input type="checkbox" name="mandSue9" id="mandSue9"> Práticas de conservación de suelos</li>
                                                                    <li> <input type="checkbox" name="mandSue10" id="mandSue10"> Siembra directa y cultivo</li>
                                                                    <li> <input type="checkbox" name="mandSue11" id="mandSue11"> Siembra y cultivo con tracción animal</li>
                                                                    <li> <input type="checkbox" name="mandSue12" id="mandSue12"> Siembra y cultivo con (roturacion tractor, maquinaria)</li>
                                                                    <li> <input type="checkbox" name="mandSue13" id="mandSue13"> Siembra directa</li>

                                                                </ul>
                                                            </li>

                                                            <li class="dropdown">
                                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                                    Sistemas de producción agrícola:
                                                                    <span class="caret"></span></a>
                                                                <ul class="dropdown-menu">
                                                                    <li> <input type="checkbox" name="sisdProdAgri1" id="sisdProdAgri1"> Cereales</li>
                                                                    <li> <input type="checkbox" name="sisdProdAgri2" id="sisdProdAgri2"> Cultivos asociados</li>
                                                                    <li> <input type="checkbox" name="sisdProdAgri3" id="sisdProdAgri3"> Forrajes</li>
                                                                    <li> <input type="checkbox" name="sisdProdAgri4" id="sisdProdAgri4"> Frutales</li>
                                                                    <li> <input type="checkbox" name="sisdProdAgri5" id="sisdProdAgri5"> Hortalizas</li>
                                                                    <li> <input type="checkbox" name="sisdProdAgri6" id="sisdProdAgri6"> Leguminosas</li>
                                                                    <li> <input type="checkbox" name="sisdProdAgri7" id="sisdProdAgri7"> Medicinales</li>
                                                                    <li> <input type="checkbox" name="sisdProdAgri8" id="sisdProdAgri8"> Oleaginosas</li>
                                                                    <li> <input type="checkbox" name="sisdProdAgri9" id="sisdProdAgri9"> Textiles</li>

                                                                </ul>
                                                            </li>
                                                            <li> <input type="text" name="sisdProdAgri10" id="sisdProdAgri10"> Otro (especificar) </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--//Sistemas de producción agrícola-->

                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <input type="button" name="previous" class="previous-form btn btn-default" value="Regresar" />
                                <input type="button" name="next" class="next-form btn btn-info" value="Siguente" />
                            </fieldset>

                            <fieldset>
                                <h2> Tipo de colectivo y/o organización</h2>
                                <br>
                                <div class="container">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="panel-group">

                                                <!--Nucleo Agrario-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#20">
                                                                Nucelo Agrario
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="20" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="nucAgri1" id="nucAgri1"> Bienes Comunales </li>
                                                                <li> <input type="checkbox" name="nucAgri2" id="nucAgri2"> Ejido </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Nucleo AAgrario-->

                                                <!--Fundación-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#21">
                                                                Fundación</a>
                                                        </h4>
                                                    </div>
                                                    <div id="21" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="fundacion1" id="fundacion1"> Nacional </li>
                                                                <li> <input type="checkbox" name="fundacion2" id="fundacion2"> Internacional </li>
                                                                
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Fundación-->

                                                <!--Gobierno-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Gobierno</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="gobierno1" id="gobierno1"> Municipal </li>
                                                                <li> <input type="checkbox" name="gobierno2" id="gobierno2"> Estatal </li>
                                                                <li> <input type="checkbox" name="gobierno3" id="gobierno3"> Federal </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Gobierno-->

                                                <!--Instituciones Educativas-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#24">Instituciones Educativas</a>
                                                        </h4>
                                                    </div>
                                                    <div id="24" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="intEduc1" id="intEduc1"> Escuela Campesina</li>
                                                                <li> <input type="checkbox" name="intEduc2" id="intEduc2"> Eascuela Primaria</li>
                                                                <li> <input type="checkbox" name="intEduc3" id="intEduc3"> Escuela Secundaria</li>
                                                                <li> <input type="checkbox" name="intEduc4" id="intEduc4"> Bachillerato</li>
                                                                <li> <input type="checkbox" name="intEduc5" id="intEduc5"> Universidad</li>
                                                                <li> <input type="checkbox" name="intEduc6" id="intEduc6"> Universidad Intercultural</li>
                                                                <li> <input type="checkbox" name="intEduc7" id="intEduc7"> Universidad Alternativa</li>
                                                                <li> <input type="checkbox" name="intEduc8" id="intEduc8"> Centro de Formación privado</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Fundación-->

                                                <!--Organización Campesina-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Organización Campesina</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="orgCamp1" id="orgCamp1"> Organización Campesina </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="panel-group">
                                                <!--Colectivo Mujeres-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Colectivo Mujeres</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="colMuj1" id="colMuj1"> Colectivo Mujeres </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Colectivo Mujeres-->

                                                <!--Cooperativa-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Cooperativa</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="coop1" id="coop1"> Cooperativa </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Cooperativa-->

                                                <!--Cooperativa de Consumo-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Cooperativa de consumo</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="coopCon1" id="coopCon1"> Cooperativa de Consumo </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Cooperativa de Consumo-->

                                                <!--Cooperativa de Producción-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Cooperativa de producción</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="cooProd1" id="cooProd1"> Cooperativa de producción </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Cooperativa de producción-->
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="panel-group">
                                                <!--Ejido-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Ejido</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="eji1" id="eji1"> Ejido </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Ejido-->

                                                <!--Empresa Social-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23"> Empresa Social </a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="empSoc1" id="empSoc1"> Empresa Social </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Empresa Social-->

                                                <!--Empresa Privada-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Empresa Privada</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="empPriv1" id="empPriv1"> Empresa Privada </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Empresa Privada-->

                                                <!--Grupo de Asesores Tecnicos-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Grupo de Asesores Tecnicos</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="grupAse" id="grupAse"> Grupo de Asesores Tecnicos </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Grupo de Asesores Tecnicos-->
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="panel-group">
                                                <!--Grupo de Consumidores-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Grupo de Consumidores</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="grupCons1" id="grupCons1"> Grupo de Consumidores </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Grupo de Consumidores-->

                                                <!--Grupo de investigación-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Grupo de Investigación</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="grupoInve1" id="grupoInve1"> Grupo de Investigación </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Grupo de Investigación-->

                                                <!--Grupo Estudiantil-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Grupo Estudiantil</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="grupEst1" id="grupEst1">Grupo Estudiantil</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Grupo Estudiantil-->

                                                <!--Grupo de la Iglesia-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Grupo de la Iglesia</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="grupIgl1" id="grupIgl1"> Grupo de la Iglesia </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Grupo de la Iglesia-->
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="panel-group">
                                                <!--Organización de la Sociedad Civil-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Organización de la Sociedad Civil</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="orgSocCiv1" id="orgSocCiv1"> Organización de la Sociedad Civil </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Organización de la Sociedad Civil-->

                                                <!--Programa de extensión-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Programa de Extensión</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="checkbox" name="progExt1" id="progExt1"> Programa de Extensión </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Programa de extensión-->

                                                <!--Otro-->
                                                <div class="panel panel-default">
                                                    <div class="panel-heading style-uv text-center">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="" href="#23">Otro</a>
                                                        </h4>
                                                    </div>
                                                    <div id="23" class="panel-collapse">
                                                        <div class="panel-body">
                                                            <ul>
                                                                <li> <input type="text" name="otro_Colec1" id="otro_Colec1"> Otro (especificar) </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--//Otro-->
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <hr>

                                <input type="button" name="previous" class="previous-form btn btn-default" value="Regresar" />
                                <input type="button" name="next" class="next-form btn btn-info" value="Siguente" />
                            </fieldset>

                            <fieldset>
                                <h2>Atlas de iniciativas</h2>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                            <h3> Procedimiento: </h3>
                                            <p>
                                                <strong>
                                                    Buscamos contar con información importante de tu iniciativa para ser compartida por lo que te agradeceríamos mucho nos ayudes con la siguiente información.
                                                </strong>
                                            </p>
                                            <hr>
                                        </div>
                                        <br>

                                        <!--Pregunta 1-->
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <label class="">
                                                            Nombre de la iniciativa o experiencia Debe ser una frase breve que explique a lo que se dedican, porque a veces un título no alcanza a dimensionar el trabajo que se hace.
                                                        </label>
                                                        <hr>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <input type="text" class="form-control" rows="3" name="nombreIncExp" id="nombreIncExp"></input>
                                                        <br>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>

                                        <!--//Pregunta 1-->

                                        <!--Pregunta 2-->
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                                <label>
                                                    Resumen sobre el trabajo de la iniciativa (el resumen debe contar de manera sintética el trabajo que realiza la iniciativa, tratando de incluir: la forma en qué surgió y el contexto en cómo lo hace; principales actores involucrados, propósitos más importantes, problemas a los que se han enfrentado y soluciones encontradas, retos futuros en los que trabajan).
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <textarea class="form-control" rows="3" name="resIni" id="resIni"></textarea>
                                                <br>

                                            </div>
                                            <hr>
                                        </div>
                                        <!--//Pregunta 2-->
                                        <br>
                                        <br>

                                        <!--Pregunta 3-->
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <label>
                                                    Año en que iniciaron actividades.
                                                </label>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <input type="date" name="fechaAtlas" id="fechaAtlas" >
                                            </div>
                                        </div>
                                        <!--//Pregunta 3-->

                                        <br>

                                        <!--Pregunta 3-->
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <label>
                                                    <br>
                                                    Ubique en el mapa su experiencia agroecológica, arrastrando el marcador <img src=" img/marcador.png" style="

                                                    max-width: 100%; 
                                                    width: 2.5%;
                                                    height: auto;
                                                    position: relative;
                                                    right: 3px;">
                                                    en el sitio que corresponda.
                                                    <br>
                                                </label>

                                                <label>

                                                </label>
                                            </div>

                                        </div>



                                        <!--pregunta 4-->
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" onload="initialize()">

                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tam" id="map_canvas" style="max-width: 100%;
                          position: relative;
                          overflow: hidden;
                          height: 250px;
                          width: 100%;">
                                            </div>
                                            <input id="txtLat" type="hidden" name="txtLat" style="color:red" value="" />
                                            <input id="txtLng" type="hidden" name="txtLng" style="color:red" value="" /><br />
                                            <br />
                                            <br />
                                        </div>
                                        <!--//pregunta4-->
                                    </div>
                                </div>
                                <br>

                                <input type="button" name="previous" class="previous-form btn btn-default" value="Regresar" />
                                <input type="button" name="next" class="next-form btn btn-info" value="Siguente" />

                            </fieldset>




                            <fieldset>
                                <h2> Hacia una nueva política pública</h2>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">


                                            <strong>
                                                Nos interesa mucho el poder compartir tu palabra sobre tus experiencias y retos, por ello te rogaríamos nos ayuden contestando las siguientes preguntas:

                                            </strong>
                                        </div>

                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                            <h3> Políticas públicas:</h3>
                                        </div>

                                        <!--Pregunta_1_politicas-->
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                                <label>
                                                    ¿Qué políticas públicas actuales consideras que favorecen el impulso de la agroecología en tu región y en el país?
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <textarea class="form-control" rows="3" name="politicas1" id="politicas1"></textarea>
                                                <br>
                                            </div>
                                            <hr>
                                        </div>
                                        <!--//Pregunta_1_politicas-->

                                        <!--Pregunta_2_politicas-->
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                                <label>
                                                    ¿Qué políticas públicas actuales consideras que pueden estar limitando el impulso de la agroecología en tu región y en el país?
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <textarea class="form-control" rows="3" name="politicas2" id="politicas2"></textarea>
                                                <br>
                                            </div>
                                            <hr>
                                        </div>
                                        <!--//Pregunta_2_politicas-->

                                        <!--Pregunta_3_politicas-->
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                                <label>
                                                    ¿Qué faltaría hacer para implementar una política pública para fortalecer la transición agroecológica en el país?
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <textarea class="form-control" rows="3" name="politicas3" id="politicas3"></textarea>
                                                <br>
                                            </div>
                                            <hr>
                                        </div>
                                        <!--//Pregunta_3_politicas-->

                                        <!--Pregunta_4_politicas-->
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                                <label>
                                                    ¿Cuáles son los obstáculos para lograr una transición agroecológica nacional?
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <textarea class="form-control" rows="3" name="politicas4" id="politicas4"></textarea>
                                                <br>
                                            </div>
                                            <hr>
                                        </div>
                                        <!--//Pregunta_4_politicas-->
                                    </div>
                                </div>
                                <input type="button" name="previous" class="previous-form btn btn-default" value="Regresar" />
                                <input type="button" name="next" class="next-form btn btn-info" value="Siguente" />
                            </fieldset>

                            <fieldset>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                            <h3>Tejiendo redes</h3>
                                        </div>

                                        <!--Pregunta_1_redes-->
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                                <label>
                                                    ¿Con quién se relacionan para realizar nuestro trabajo desde un enfoque agroecológico? (p. Ej. otros agricultores, organizaciones, escuelas, gobiernos)
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <textarea class="form-control" rows="3" name="redes1" id="redes1"></textarea>
                                                <br>
                                            </div>
                                            <hr>
                                        </div>
                                        <!--//Pregunta_1_redes-->

                                        <!--Pregunta_2_redes-->
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-justify">
                                                <label>
                                                    ¿Qué propondrían para fortalecer su trabajo en torno a la agroecología?
                                                </label>

                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <textarea class="form-control" rows="3" name="redes2" id="redes2"></textarea>
                                                <br>
                                            </div>

                                        </div>
                                        <!--//Pregunta_2_redes-->
                                        <hr>

                                        <!--Pregunta_3_redes-->
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                                <label>
                                                    ¿Qué se podría hacer para una transición agroecológica a escala de país/estado/municipio?
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <textarea class="form-control" rows="3" name="redes3" id="redes3"></textarea>
                                                <br>
                                            </div>
                                            <hr>
                                        </div>
                                        <!--//Pregunta_3_redes-->

                                        <!--Pregunta_4_redes-->
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                                <label>
                                                    ¿Cuáles son los obstáculos para lograr una transición agroecológica nacional?
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <textarea class="form-control" rows="3" name="redes4" id="redes4"></textarea>
                                                <br>
                                            </div>
                                            <hr>
                                        </div>
                                        <!--//Pregunta_4_redes-->
                                    </div>
                                </div>

                                <input type="button" name="previous" class="previous-form btn btn-default" value="Regresar" />
                                <input type="button" name="next" class="next-form btn btn-info" value="Siguente" />




                            </fieldset>

                            <fieldset>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                            <h3>Formación.</h3>
                                        </div>
                                        <!--Pregunta_1_Formacion-->
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                                <label>
                                                    ¿En qué temas consideras qué tu colectivo puede apoyar para formar otros grupos?
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <textarea class="form-control" rows="3" name="formacion1" id="formacion1"></textarea>
                                                <br>
                                            </div>
                                            <hr>
                                        </div>
                                        <!--//Pregunta_1_Formacion-->
                                        <!--Pregunta_2_Formacion-->
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12text-justify">
                                                <label>
                                                    ¿Qué temas de formación consideras que son importantes para fortalecer el trabajo de tú colectivo?
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <textarea class="form-control" rows="3" name="formacion2" id="formacion2"></textarea>
                                                <br>
                                            </div>
                                            <hr>
                                        </div>
                                        <!--//Pregunta_2_formacion-->
                                    </div>
                                </div>
                                <input type="button" name="previous" class="previous-form btn btn-default" value="Regresar" />
                                <input type="button" name="next" class="next-form btn btn-info" value="Siguente" />
                            </fieldset>

                            <fieldset>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                            <h3>Seguridad:</h3>
                                        </div>
                                        <!--Pregunta_1_seguridad-->
                                        <br>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                                <label>
                                                    ¿El contexto de seguridad en tu región permite se lleven a cabo intercambios y/o visitas de otras experiencias?
                                                </label>
                                                <hr>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 seguridad">
                                                <ul>
                                                    <li><input type="checkbox" name="seguridad1" id="seguridad1"> <label>Si es posible</label></li>
                                                    <li><input type="checkbox" name="seguridad2" id="seguridad2"> <label>En ocasiones es complicado</label></li>
                                                    <li><input type="checkbox" name="seguridad3" id="seguridad3"><label>No es posible por el momento</label></li>
                                                    <li><input type="checkbox" name="seguridad4" id="seguridad4"><label>otro</label></li>
                                                </ul>
                                            </div>
                                            <hr>
                                        </div>
                                        <!--//Pregunta_1_seguridad-->
                                    </div>
                                </div>
                                <input type="button" name="previous" class="previous-form btn btn-default" value="Regresar" />
                                <input type="button" name="next" class="next-form btn btn-info" value="Siguente" />
                            </fieldset>

                            <fieldset>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                            <h3>Nota:</h3>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-justify">
                                            <label>
                                                ¿Si conoces a alguien que pueda estar interesada/o en llenar esta ficha? ¿Nos puedes compartir su contacto?
                                            </label>
                                            <hr>
                                        </div>


                                        <!--nombre-nota-->
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName2">Nombre</label>
                                            <input type="text" class="form-control" name="nota1" id="nota1" placeholder="">
                                        </div>
                                        <!--/nombre-nota-->

                                        <!--correo-nota-->
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName2">Correo</label>
                                            <input type="text" class="form-control" name="nota2" id="nota2" placeholder="">
                                        </div>
                                        <!--/correo-nota-->

                                        <!--telefono-->
                                        <div class="form-group col-md-4">
                                            <label for="exampleInputName2">Telefono</label>
                                            <input type="text" class="form-control" name="nota3" id="nota3" placeholder="">
                                        </div>
                                        <!--/telefono-->
                                    </div>
                                </div>
                                <br>
                                <input type="button" name="previous" class="previous-form btn btn-default" value="Regresar" />

                                <input type="submit" name="submit" id="guardar" class="submit btn btn-success" value="Guardar" />




                    </div>
                </div>
                <br>



                <!--

                <a type="hidden" href="informe.php" class="btn btn-success">
                                    Guardar
                                </a>


                        <a href="https://www.uv.mx/apps/cuo/opc/recultivar_mexico/Red_cultural/index.php" class="btn btn-success">
                            Guardar
                        </a>


                -->

            </fieldset>





        </form>
    </div>
    </div>



    </div>


</section>