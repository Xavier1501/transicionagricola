<?php


$sql = "
select  
caec.AutoConsumo,
 caec.CertificaParticipativa,
 caec.CertificacionTerceraParte,
 caec.ComercioLocal,
 caec.ComercioRegional,
 caec.Consumo,
 caec.Semillas,
 caec.Insumos,
 caec.Credito,
 caec.Servicios,
 caec.EmpresaProducInsumo,
 caec.IniciativaIntercambio,
 caec.MercadoAlternativo,
 caec.SistemaGesta,
 caec.TianguisOrganico,
 caec.OtroEconomia
from co_agr_entrevista cae 
 LEFT JOIN co_agr_economia caec ON cae.idEntrevista = caec.idAreaTrabajo
where cae.idEntrevista ='$id'";

$query = $conexion->query($sql);
$entrevistaArray4 = $query->fetch_array(MYSQLI_ASSOC);



$cont4 =0;
$boolEc;

foreach ($entrevistaArray4 as $item){
    if ($item == 1 ){
        $cont4 ++;
    }

}

if ($entrevistaArray4['OtroEconomia'] === ''){
    $boolEc = 'vacio';

}else{
    $boolEc = false;

}



?>




<?php  if ($cont4 == '0' and $boolEc === 'vacio'): ?>


<?php else: ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Economia</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover">

                            <?php echo $AutoConsumo = (!empty($entrevistaArray4['AutoConsumo']) )? '<p>Autoconsumo</p>':'' ?>
                            <?php echo $CertificaParticipativa = (!empty($entrevistaArray4['CertificaParticipativa']) )? '<p>Certificación participativa (Sistema garantía)</p>':'' ?>
                            <?php echo $CertificacionTerceraParte = (!empty($entrevistaArray4['CertificacionTerceraParte']) )? '<p>Certificación de tercera parte </p>':'' ?>
                            <?php echo $ComercioLocal = (!empty($entrevistaArray4['ComercioLocal']) )? '<p>Comercialización local</p>':'' ?>
                            <?php echo $ComercioRegional = (!empty($entrevistaArray4['ComercioRegional']) )? '<p>Comercialización regional</p>':'' ?>
                            <?php echo $Consumo = (!empty($entrevistaArray4['Consumo']) )? '<p>Consumo</p>':'' ?>
                            <?php echo $Semillas = (!empty($entrevistaArray4['Semillas']) )? '<p>Semillas</p>':'' ?>
                            <?php echo $Insumos = (!empty($entrevistaArray4['Insumos']) )? '<p>Insumos</p>':'' ?>
                            <?php echo $Credito = (!empty($entrevistaArray4['Credito']) )? '<p>Crédito</p>':'' ?>
                            <?php echo $Servicios = (!empty($entrevistaArray4['Servicios']) )? '<p>Servicios</p>':'' ?>
                            <?php echo $EmpresaProducInsumo = (!empty($entrevistaArray4['EmpresaProducInsumo']) )? '<p>Empresa productora de insumos</p>':'' ?>
                            <?php echo $IniciativaIntercambio = (!empty($entrevistaArray4['IniciativaIntercambio']) )? '<p>Iniciativa de intercambio (trueque)</p>':'' ?>
                            <?php echo $MercadoAlternativo = (!empty($entrevistaArray4['MercadoAlternativo']) )? '<p>Mercado alternativo (agroecológico)</p>':'' ?>
                            <?php echo $SistemaGesta = (!empty($entrevistaArray4['SistemaGesta']) )? '<p>Sistema de cestas</p>':'' ?>
                            <?php echo $TianguisOrganico = (!empty($entrevistaArray4['TianguisOrganico']) )? '<p>Tianguis orgánico':'' ?>
                            <?php echo $OtroEconomia = (!empty($entrevistaArray4['OtroEconomia']) )? '<p>Otro (especificar): '.$entrevistaArray4['OtroEconomia'].'</p>':'' ?>










            </table>
        </div>
    </div>
    </div>
    </div>

<?php endif; ?>
