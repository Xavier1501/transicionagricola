<?php


$sql = "
select  
casaf.AreaDestinoConservaSilvo,
  casaf.SistemaAgroforestal,
  casaf.BosqueComestible,
  casaf.CercasVivas,
  casaf.BarreraRompeVientos,
  casaf.OtrosistemaAgro 
from co_agr_entrevista cae 
LEFT JOIN co_agr_sistemasagroforestales casaf ON cae.idEntrevista = casaf.idAreaTrabajo
where cae.idEntrevista ='$id'";

$query = $conexion->query($sql);
$entrevistaArray12 = $query->fetch_array(MYSQLI_ASSOC);



$cont12 =0;
$bool12;

foreach ($entrevistaArray12 as $item){
    if ($item == 1 ){
        $cont12 ++;
    }

}

if ($entrevistaArray12['OtrosistemaAgro'] === ''){
    $bool12 = 'vacio';

}else{
    $bool12 = false;

}



?>




<?php  if ($cont12 == '0' and $bool12 === 'vacio'): ?>
<?php else: ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"> Sistemas agroforestales</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <ul>
                            <?php echo $ProduccionSemilla = (!empty($entrevistaArray12['AreaDestinoConservaSilvo']) )? '<p> Montes y áreas comunes destinadas a la conservación de sistemas silvopastoriles</p>':'' ?>
                            <?php echo $SistemaAgroforestal = (!empty($entrevistaArray12['SistemaAgroforestal']) )? '<p>Sistemas Agroforestales Tradicionales</p>':'' ?>
                            <?php echo $BosqueComestible = (!empty($entrevistaArray12['BosqueComestible']) )? '<p>Bosques comestibles</p>':'' ?>
                            <?php echo $CercasVivas = (!empty($entrevistaArray12['CercasVivas']) )? '<p>Cercas vivas</p>':'' ?>
                            <?php echo $BarreraRompeVientos = (!empty($entrevistaArray12['BarreraRompeVientos']) )? '<p>Barreras rompevientos':'' ?>
                            <?php echo $OtrosistemaAgro= (!empty($entrevistaArray12['OtrosistemaAgro']) )? '<p>Otro (especificar): '.$entrevistaArray12['OtrosistemaAgro'].'</p>':'' ?>
                        </ul>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
