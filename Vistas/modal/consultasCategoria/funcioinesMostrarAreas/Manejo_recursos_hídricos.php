<?php


$sql = "
select  
 camch.AlmacenAguaLluvia,
 camch.EnfoqueMicrocuenca,
 camch.ImplementacionEcotecnologicas,
 camch.ManejoComuniAgua,
 camch.RiesgoDrenaje,
 camch.TratamientoAgua,
 camch.OtroManejoH 
from co_agr_entrevista cae 
 LEFT JOIN co_agr_manejorecursoshidricos camch ON cae.idEntrevista = camch.idAreaTrabajo
where cae.idEntrevista ='$id'";

$query = $conexion->query($sql);
$entrevistaArray10 = $query->fetch_array(MYSQLI_ASSOC);



$cont10 =0;
$boolMRHI;

foreach ($entrevistaArray10 as $item){
    if ($item == '1' ){
        $cont10 ++;
    }

}

if ($entrevistaArray10['OtroManejoH'] === ''){
    $boolMRHI = 'vacio';

}else{
    $boolMRHI = false;

}



?>




<?php  if ($cont10 == '0' and $boolMRHI === 'vacio'): ?>

<?php else: ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"> Manejo de recursos hídricos</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <ul>
                            <?php echo $AlmacenAguaLluvia = (!empty($entrevistaArray10['AlmacenAguaLluvia']) )? '<p>Captación y almacenamiento de agua de lluvias</p>':'' ?>
                            <?php echo $EnfoqueMicrocuenca = (!empty($entrevistaArray10['EnfoqueMicrocuenca']) )? '<p>Enfoque de microcuencas</p>':'' ?>
                            <?php echo $ImplementacionEcotecnologicas = (!empty($entrevistaArray10['ImplementacionEcotecnologicas']) )? '<p>Implementación de ecotecnologías</p>':'' ?>
                            <?php echo $ManejoComuniAgua = (!empty($entrevistaArray10['ManejoComuniAgua']) )? '<p>Manejo comunitario del agua</p>':'' ?>
                            <?php echo $RiesgoDrenaje = (!empty($entrevistaArray10['RiesgoDrenaje']) )? '<p>Riego y drenaje</p>':'' ?>
                            <?php echo $TratamientoAgua = (!empty($entrevistaArray10['TratamientoAgua']) )? '<p>Tratamiento de aguas</p>':'' ?>
                            <?php echo $OtroManejoH = (!empty($entrevistaArray10['OtroManejoH']) )? '<p> Otro (especificar): '.$entrevistaArray10['OtroManejoH'].'</p>':'' ?>
                        </ul>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
