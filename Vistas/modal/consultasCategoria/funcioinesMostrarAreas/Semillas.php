<?php


$sql = "
select  
cas.AlmacenConservacionSemillas,
  cas.BancosComuniSemilla,
  cas.IntercambioSemilla,
  cas.MejoraPlantaAnimal,
  cas.ProduccionSemilla,
  cas.OtroSemillas 
from co_agr_entrevista cae 
LEFT JOIN co_agr_semillas cas ON cae.idEntrevista = cas.idAreaTrabajo
where cae.idEntrevista ='$id'";

$query = $conexion->query($sql);
$entrevistaArray12 = $query->fetch_array(MYSQLI_ASSOC);



$cont12 =0;
$bool12;

foreach ($entrevistaArray12 as $item){
    if ($item == 1 ){
        $cont12 ++;
    }

}

if ($entrevistaArray12['OtroSemillas'] === ''){
    $bool12 = 'vacio';

}else{
    $bool12 = false;

}



?>




<?php  if ($cont12 == '0' and $bool12 === 'vacio'): ?>
<?php else: ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"> Semillas</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <ul>
                            <?php echo $AlmacenConservacionSemillas = (!empty($entrevistaArray12['AlmacenConservacionSemillas']) )? '<p>Almacenamiento y conservación de semillas</p>':'' ?>
                            <?php echo $BancosComuniSemilla = (!empty($entrevistaArray12['BancosComuniSemilla']) )? '<p>Bancos comunitarios de semillas</p>':'' ?>
                            <?php echo $IntercambioSemilla = (!empty($entrevistaArray12['IntercambioSemilla']) )? '<p>Intercambio de Semillas</p>':'' ?>
                            <?php echo $MejoraPlantaAnimal = (!empty($entrevistaArray12['MejoraPlantaAnimal']) )? '<p>Mejoramiento de plantas y animales </p>':'' ?>
                            <?php echo $ProduccionSemilla = (!empty($entrevistaArray12['ProduccionSemilla']) )? '<p>Producción de semillas nativas y criollas</p>':'' ?>
                            <?php echo $OtroSemillas= (!empty($entrevistaArray12['OtroSemillas']) )? '<p>Otro (especificar)'.$entrevistaArray12['OtroSemillas'].'</p>':'' ?>
                        </ul>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
