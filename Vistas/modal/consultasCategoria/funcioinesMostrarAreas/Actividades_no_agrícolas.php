<?php


$sql5 = "
select  
caa.Artesania,
 caa.CajaAhorro,
 caa.Turismo,
 caa.Turismo,
 caa.Culturales,
 caa.ComedorComuni,
 caa.ComedorSocial,
 caa.ComedorEscolar ,
 caa.Restaurant,
 caa.OtroActNoAgri
from co_agr_entrevista cae 
LEFT JOIN co_agr_actividadnoagricola caa ON cae.idEntrevista = caa.idAreaTrabajo
where cae.idEntrevista ='$id'";

$query = $conexion->query($sql5);
$entrevistaArray5 = $query->fetch_array(MYSQLI_ASSOC);



$cont5 =0;
$boolANAG;

foreach ($entrevistaArray5 as $item){
    if ($item == 1 ){
        $cont5 ++;
    }

}

if ($entrevistaArray5['OtroActNoAgri'] === ''){
    $boolANAG = 'vacio';

}else{
    $boolANAG = false;

}

//var_dump($entrevistaArray5);


?>


<?php  if ($cont5 == '0' and $boolANAG === 'vacio'): ?>

<?php else: ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Actividades no agrícolas</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <ul>
                            <?php echo $Artesania = (!empty($entrevistaArray5['Artesania']) )? '<p>Artesanías</p>':'' ?>
                            <?php echo $CajaAhorro = (!empty($entrevistaArray5['CajaAhorro']) )? '<p>Cajas de ahorro y crédito</p>':'' ?>
                            <?php echo $Turismo = (!empty($entrevistaArray5['Turismo']) )? '<p>Turismo</p>':'' ?>
                            <?php echo $Culturales = (!empty($entrevistaArray5['Culturales']) )? '<p>Culturales</p>':'' ?>
                            <?php echo $ComedorComuni= (!empty($entrevistaArray5['ComedorComuni']) )? '<p>Comedores comunitarios</p>':'' ?>
                            <?php echo $ComedorSocial = (!empty($entrevistaArray5['ComedorSocial']) )? '<p>Comedores sociales</p>':'' ?>
                            <?php echo $ComedorEscolar = (!empty($entrevistaArray5['ComedorEscolar']) )? '<p>Comedores escolares</p>':'' ?>
                            <?php echo $Restaurant = (!empty($entrevistaArray5['Restaurant']) )? '<p>Restaurantes, fondas y/o cocinas que ofrecen comida agroecológica</p>':'' ?>
                            <?php echo $OtroActNoAgri = (!empty($entrevistaArray5['OtroActNoAgri']) )? '<p>Otro (especificar): '.$entrevistaArray5['OtroActNoAgri'].'</p>':'' ?>
                        </ul>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
