<?php


$sql = "
select  
 caed.EduAmbiental,
 caed.EscuelaCampesina,
 caed.EscuelaIndigena,
 caed.FormacionTecnico,
 caed.ComuniAprende,
 caed.Basicas,
 caed.MediaSuperior,
 caed.Superior,
 caed.IniciativaCampesina,
 caed.OtroEcudacion
from co_agr_entrevista cae 
LEFT JOIN co_agr_educacion caed ON cae.idEntrevista = caed.idAreaTrabajo
where cae.idEntrevista ='$id'";

$query = $conexion->query($sql);
$entrevistaArray6 = $query->fetch_array(MYSQLI_ASSOC);




$cont6 =0;
$boolEdu;

foreach ($entrevistaArray6 as $item){
    if ($item === '1'){
        $cont6++;
    }
}



if ($entrevistaArray6['OtroEcudacion'] === ''){
    $boolEdu = 'vacio';

}else{
    $boolEdu = false;

}




?>
<?php  if ($cont6 == '0' and $boolEdu === 'vacio'): ?>
<?php else: ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Educación</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <ul>
                            <?php echo $EduAmbiental = (!empty($entrevistaArray6['EduAmbiental']) )? '<p>Educación ambiental</p>':'' ?>
                            <?php echo $EscuelaCampesina = (!empty($entrevistaArray6['EscuelaCampesina']) )? '<p>Escuela campesina</p>':'' ?>
                            <?php echo $EscuelaIndigena = (!empty($entrevistaArray6['EscuelaIndigena']) )? '<p>Escuela indígena </p>':'' ?>
                            <?php echo $FormacionTecnico = (!empty($entrevistaArray6['FormacionTecnico']) )? '<p>Formación de técnicos</p>':'' ?>
                            <?php echo $ComuniAprende = (!empty($entrevistaArray6['ComuniAprende']) )? '<p>Comunidad de aprendizaje</p>':'' ?>
                            <?php echo $Basicas = (!empty($entrevistaArray6['Basicas']) )? '<p>Básica</p>':'' ?>
                            <?php echo $MediaSuperior = (!empty($entrevistaArray6['MediaSuperior']) )? '<p>Media Superior</p>':'' ?>
                            <?php echo $Superior = (!empty($entrevistaArray6['Superior']) )? '<p>Superior</p>':'' ?>
                            <?php echo $IniciativaCampesina = (!empty($entrevistaArray6['IniciativaCampesina']) )? '<p>Iniciativa de campesino a campesino</p>':'' ?>
                            <?php echo $OtroEcudacion = (!empty($entrevistaArray6['OtroEcudacion']) )? '<p>Otro (especificar): '.$entrevistaArray6['OtroEcudacion'].'</p>':'' ?>
                        </ul>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
