<?php


$sql = "
select  
caiar.Comunicacion,
 caiar.DiagAgroecosistema,
 caiar.Experimentacion,
 caiar.FormacionCapacitacion,
 caiar.Investigacion,
 caiar.OrdenTerritoral,
 caiar.OtroInvestigacionRural 
from co_agr_entrevista cae 
 LEFT JOIN co_agr_investigacionagricolarural caiar ON cae.idEntrevista = caiar.idAreaTrabajo
where cae.idEntrevista ='$id'";

$query = $conexion->query($sql);
$entrevistaArray9 = $query->fetch_array(MYSQLI_ASSOC);



$cont9 =0;
$boolIAER;

foreach ($entrevistaArray9 as $item){
    if ($item == '1' ){
        $cont9 ++;
    }

}

if ($entrevistaArray9['OtroInvestigacionRural'] === ''){
    $boolIAER = 'vacio';

}else{
    $boolIAER = false;

}



?>




<?php  if ($cont9 == '0' and $boolIAER === 'vacio'): ?>

<?php else: ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">Investigación agrícola y Extensión rural</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <ul>
                            <?php echo $Comunicacion = (!empty($entrevistaArray9['Comunicacion']) )? '<p>Comunicación</p>':'' ?>
                            <?php echo $DiagAgroecosistema = (!empty($entrevistaArray9['DiagAgroecosistema']) )? '<p>Diagnóstico de agroecosistemas</p>':'' ?>
                            <?php echo $Experimentacion = (!empty($entrevistaArray9['Experimentacion']) )? '<p>Experimentación</p>':'' ?>
                            <?php echo $FormacionCapacitacion = (!empty($entrevistaArray9['FormacionCapacitacion']) )? '<p>Formación y capacitación</p>':'' ?>
                            <?php echo $Investigacion = (!empty($entrevistaArray9['Investigacion']) )? '<p>Investigación</p>':'' ?>
                            <?php echo $OrdenTerritoral = (!empty($entrevistaArray9['OrdenTerritoral']) )? '<p>Ordenamientos territoriales</p>':'' ?>
                            <?php echo $OtroTrabajoNiñoJoventud = (!empty($entrevistaArray9['OtroInvestigacionRural']) )? '<p>Otro (especificar)'.$entrevistaArray9['OtroInvestigacionRural'].'</p>':'' ?>
                        </ul>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
