<?php


$sql = "
select  
caspag.Permacultura,
  caspag.AgriculturaBiodinamica,
  caspag.AgriculturaNatural,
  caspag.AgriculturaOrganica,
  caspag.Agrohomeopatia,
  caspag.TransicionSustiInsumo,
  caspag.SustitucionAgroquimica,
  caspag.ManejoBioologico,
  caspag.ManejoIntegrado,
  caspag.AbonoVerde,
  caspag.BarbechoDescanso,
  caspag.HarinaRoca,
  caspag.Cobertura,
  caspag.Compostaje,
  caspag.MejoraFertilidad,
  caspag.ProduccionBiofertilizante,
  caspag.ProduccionAbonoSolido,
  caspag.PracticaConservacionSuelo,
  caspag.SiembraDirectaCultivo,
  caspag.SiembraCultivoAnimal,
  caspag.SiembraContivoConRotu,
  caspag.SiembraDirecta,
  caspag.Cereales,
  caspag.CultivoAsociado,
  caspag.Forraje,
  caspag.Frutales,
  caspag.Hortaliza,
  caspag.Leguminosa,
  caspag.Medicinales,
  caspag.Oleaginosa,
  caspag.Textil,
  caspag.OtroAgricola
from co_agr_entrevista cae 
 LEFT JOIN co_agr_sistemaproduccionagricola caspag ON cae.idEntrevista = caspag.idAreaTrabajo 
where cae.idEntrevista ='$id'";

$query = $conexion->query($sql);
$entrevistaArray15 = $query->fetch_array(MYSQLI_ASSOC);

//var_dump($entrevistaArray15);

$cont15 =0;
$bool15;

foreach ($entrevistaArray15 as $item){
    if ($item == '1' ){
        $cont15 ++;
    }

}

if ($entrevistaArray15['OtroAgricola'] === ''){
    $bool15 = 'vacio';

}else{
    $bool15 = false;

}

//var_dump($entrevistaArray15['OtroProductoForestales']);



?>




<?php  if ($cont15 == '0' and $bool15 === 'vacio'): ?>
<?php else: ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"> Sistemas de producción agrícola</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <ul><p></p>

                                    <?php echo $Permacultura = (!empty($entrevistaArray15['Permacultura']) )? '<p>Permacultura</p>':'' ?>
                                    <?php echo $AgriculturaBiodinamica = (!empty($entrevistaArray15['AgriculturaBiodinamica']) )? '<p>Agricultura biodinámica</p>':'' ?>
                                    <?php echo $AgriculturaNatural = (!empty($entrevistaArray15['AgriculturaNatural']) )? '<p>Agricultura natural</p>':'' ?>
                                    <?php echo $AgriculturaOrganica = (!empty($entrevistaArray15['AgriculturaOrganica']) )? '<p>Agricultura orgánica</p>':'' ?>
                                    <?php echo $Agrohomeopatia = (!empty($entrevistaArray15['Agrohomeopatia']) )? '<p>Agrohomeopatía</p>':'' ?>
                                    <?php echo $TransicionSustiInsumo = (!empty($entrevistaArray15['TransicionSustiInsumo']) )? '<p>Transición en sustitución de insumos</p>':'' ?>


                                    <?php echo $SustitucionAgroquimica = (!empty($entrevistaArray15['SustitucionAgroquimica']) )? '<p>Sustitución de agroquímicos</p>':'' ?>
                                    <?php echo $ManejoBioologico = (!empty($entrevistaArray15['ManejoBioologico']) )? '<p>Manejo biológico</p>':'' ?>
                                    <?php echo $ManejoIntegrado = (!empty($entrevistaArray15['ManejoIntegrado']) )? '<p>Manejo integrado</p>':'' ?>



                                    <?php echo $AbonoVerde = (!empty($entrevistaArray15['AbonoVerde']) )? '<p>Abonos verdes</p>':'' ?>
                                    <?php echo $BarbechoDescanso = (!empty($entrevistaArray15['BarbechoDescanso']) )? '<p>Barbecho o descanso</p>':'' ?>
                                    <?php echo $HarinaRoca = (!empty($entrevistaArray15['HarinaRoca']) )? '<p>Uso de harinas de roca</p>':'' ?>
                                    <?php echo $Cobertura = (!empty($entrevistaArray15['Cobertura']) )? '<p>Coberteras</p>':'' ?>
                                    <?php echo $Compostaje = (!empty($entrevistaArray15['Compostaje']) )? '<p>Compostaje</p>':'' ?>
                                    <?php echo $MejoraFertilidad = (!empty($entrevistaArray15['MejoraFertilidad']) )? '<p>Mejoramiento de la fertilidad</p>':'' ?>
                                    <?php echo $ProduccionBiofertilizante = (!empty($entrevistaArray15['ProduccionBiofertilizante']) )? '<p>Producción de biofertilizantes</p>':'' ?>
                                    <?php echo $ProduccionAbonoSolido = (!empty($entrevistaArray15['ProduccionAbonoSolido']) )? '<p>Producción de abonos sólidos</p>':'' ?>
                                    <?php echo $PracticaConservacionSuelo = (!empty($entrevistaArray15['PracticaConservacionSuelo']) )? '<p>Práticas de conservación de suelos</p>':'' ?>
                                    <?php echo $SiembraDirectaCultivo = (!empty($entrevistaArray15['SiembraDirectaCultivo']) )? '<p>Siembra directa y cultivo</p>':'' ?>
                                    <?php echo $SiembraCultivoAnimal = (!empty($entrevistaArray15['SiembraCultivoAnimal']) )? '<p>Siembra y cultivo con tracción animal</p>':'' ?>
                                    <?php echo $SiembraContivoConRotu = (!empty($entrevistaArray15['SiembraContivoConRotu']) )? '<p>Siembra y cultivo con (roturacion tractor, maquinaria)</p>':'' ?>
                                    <?php echo $SiembraDirecta = (!empty($entrevistaArray15['SiembraDirecta']) )? '<p>Siembra directa</p>':'' ?>




                                    <?php echo $Cereales = (!empty($entrevistaArray15['Cereales']) )? '<p<Cereales</p>':'' ?>
                                    <?php echo $CultivoAsociado = (!empty($entrevistaArray15['CultivoAsociado']) )? '<p<Cultivos asociados</p>':'' ?>
                                    <?php echo $Forraje = (!empty($entrevistaArray15['Forraje']) )? '<p<Forrajes</p>':'' ?>
                                    <?php echo $Frutales = (!empty($entrevistaArray15['Frutales']) )? '<p<Frutales</p>':'' ?>
                                    <?php echo $Hortaliza = (!empty($entrevistaArray15['Hortaliza']) )? '<p<Hortalizas</p>':'' ?>
                                    <?php echo $Leguminosa = (!empty($entrevistaArray15['Leguminosa']) )? '<p<Leguminosas</p>':'' ?>
                                    <?php echo $Medicinales = (!empty($entrevistaArray15['Medicinales']) )? '<p<Medicinales</p>':'' ?>
                                    <?php echo $Oleaginosa = (!empty($entrevistaArray15['Oleaginosa']) )? '<p<Oleaginosas</p>':'' ?>
                                    <?php echo $Textil = (!empty($entrevistaArray15['Textil']) )? '<p<Textiles</p>':'' ?>

                            <?php echo $OtroAgricola= (!empty($entrevistaArray15['OtroAgricola']) )? '<p>Otro (especificar): '.$entrevistaArray15['OtroAgricola'].'</p>':'' ?>
                        </ul>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
