<?php

include '../../../ConexionBD.php';

$id = $_GET['id'];

//var_dump($id);

$sqlHcomunitarios = "        
 
select
distinct cae.idEntrevista,
cae.NombreGrupo,
cai.NombreIniciativa,
cae.SitioWebPerfil,
cai.Longitud,
cai.Latitud,
cai.Resumen,
cai.AnoInicio,
cae.Correo,
caest.estado,
camun.municipio 
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idIniciativa
left join co_agr_agricultura_urbana caaur on  caaur.idEntrevistaFK = cae.idEntrevista
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where cae.idEntrevista = '$id'      
        ";

$resultHcomunitarios = $conexion->query($sqlHcomunitarios);
$datos = $resultHcomunitarios->fetch_array(MYSQLI_ASSOC);


?>
<div class="container-fluid">
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Información</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <th>Grupo:</th>
                                    <td><?php echo $datos['NombreGrupo'] ?></td>

                                </tr>
                                <tr>
                                    <th>Iniciativa:</th>
                                    <td><?php echo $datos['NombreIniciativa'] ?></td>
                                </tr>
                                <tr>
                                    <th>Sitio web:</th>
                                    <td><?php echo $datos['SitioWebPerfil'] ?></td>
                                </tr>
                                <tr>
                                    <th>Año:</th>
                                    <td><?php echo $datos['AnoInicio'] ?></td>
                                </tr>
                                <tr>
                                    <th>Correo:</th>
                                    <td><?php echo $datos['Correo'] ?></td>
                                </tr>
                                <tr>
                                    <th>Estado:</th>
                                    <td><?php echo $datos['estado'] ?></td>

                                </tr>
                                <tr>
                                    <th>Municipio:</th>
                                    <td><?php echo $datos['municipio'] ?></td>
                                </tr>
                                <tr>
                                    <th>Resumen:</th>
                                    <td><p class="text-justify"> <?php echo $datos['Resumen'] ?></p></td>
                                </tr>
                            </tbody>

                        </table>
                    </div>


                    <?php $datos['Resumen'] ?>
                </div>
            </div>




        </div>




    </div>
</div>