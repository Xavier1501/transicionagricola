<?php

include '../../../ConexionBD.php';

$id = $_GET['id'];

//var_dump($id);

$sqlHcomunitarios = "        
 
select 
distinct cae.idEntrevista,
cae.NombreGrupo,
cai.NombreIniciativa,
cae.SitioWebPerfil,
cae.SitioFacebook,
cae.instagram,
cai.Longitud,
cai.Latitud,
cai.Resumen,
cai.AnoInicio,
cae.Correo,
cai.estados,
cai.municipio 
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cae.idEntrevista = '$id'      
        ";

$resultHcomunitarios = $conexion->query($sqlHcomunitarios);
$datos = $resultHcomunitarios->fetch_array(MYSQLI_ASSOC);

//var_dump($datos);

$sqlVideo = "select 
                    distinct cae.idEntrevista,
                    cau.idUrl,
                    cau.Url
                    from co_agr_entrevista cae 
                    left join co_agr_url cau on cae.idEntrevista =cau.idAreaTrabajo
                    where cae.idEntrevista = '$id'";

$resultV = $conexion->query($sqlVideo);
$datosV = $resultV->fetch_array(MYSQLI_ASSOC);

//var_dump($datosV);


$youtube = substr($datosV['Url'],0,strlen('https://www.youtube.com/')+1);
$facebook = substr($datosV['Url'],0,strlen('https://www.facebook.com/'));






?>


<style>

    @media (min-width: 576px) {

        .img-redes-uv {
            max-width: 100%;
            width: 12%;
            height: auto;
            padding: 1rem;
        }

    }

.img-redes-uv {

    width: 10%;
    padding: 1rem;

}

.img-galery-uv {
    max-width: 100%;
    width: 100%;
    height: auto%;
    border-radius: 20px;
}

.img-galery-uv:hover{
    border: 5px solid #F7F7F7;
}

.p-g-uv{
    margin: 0 !important;
    padding: 1rem;
}


</style>





<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 col-md-offset-5">
        <?php if (($datos['SitioWebPerfil'] === "") && ($datos['SitioFacebook'] === "") && ($datos['instagram'] === "")): ?>
        <?php else: ?>
            <label for="">Visitanos en</label>
        <?php endif;?>

            <?php if ($datos['SitioWebPerfil'] === ""): ?>
            <?php else: ?>
                <a href="<?php echo $datos['SitioWebPerfil']; ?>" target="_blank" rel="noopener noreferrer"><img src="img/redes/e.png" class="img-redes-uv">
                </a>
            <?php endif;?>


            <?php if ($datos['SitioFacebook'] === ""): ?>
            <?php else: ?>
                <a href="<?php echo $datos['SitioFacebook']; ?>" target="_blank" rel="noopener noreferrer"><img src="img/redes/f.png" class="img-redes-uv">
                </a>
            <?php endif;?>


            <?php if ($datos['instagram'] === ""): ?>
            <?php else: ?>
                <a href="<?php echo $datos['instagram']; ?>" target="_blank" rel="noopener noreferrer"><img src="img/redes/i.png" class="img-redes-uv">
                </a>
            <?php endif;?>

            <?php if ($youtube === 'https://www.youtube.com/' || $facebook === 'https://www.facebook.com/'): ?>
                <a href="<?php echo $datosV['Url']; ?>" target="_blank" rel="noopener noreferrer"><img src="img/redes/en.png" class="img-redes-uv">
                </a>
            <?php else: ?>

            <?php endif;?>
        </div>



        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Información</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <th>Grupo:</th>
                                    <td><?php echo $datos[
                                        'NombreGrupo'
                                    ]; ?></td>
                                </tr>
                                <tr>
                                    <th>Iniciativa:</th>
                                    <td><?php echo $datos[
                                        'NombreIniciativa'
                                    ]; ?></td>
                                </tr>
                                <tr>
                                    <th>Año:</th>
                                    <td><?php echo $datos['AnoInicio']; ?></td>
                                </tr>
                                <tr>
                                    <th>Correo:</th>
                                    <td><?php echo $datos['Correo']; ?></td>
                                </tr>
                                <tr>
                                    <th>Estado:</th>
                                    <td><?php echo $datos['estados']; ?></td>
                                </tr>
                                <tr>
                                    <th>Municipio:</th>
                                    <td><?php echo $datos['municipio']; ?></td>
                                </tr>
                                <tr>
                                    <th>Resumen:</th>
                                    <td>
                                        <p class="text-justify"> <?php echo $datos[
                                            'Resumen'
                                        ]; ?></p>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                    </div>


                    <?php $datos['Resumen']; ?>
                </div>
            </div>




        </div>



        <?php

        $sqlimg = "        
 
select
img.url 
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_img img on cae.idEntrevista = img.idAreaTrabajo 
where cae.idEntrevista = '$id';      
        ";
        $resultImg = $conexion->query($sqlimg);
        while ($img = $resultImg->fetch_array(MYSQLI_ASSOC)){
            $imgArray[] = $img;
        }






        ?>






        <?php if ($imgArray[0]['url'] != ''): ?>


        <style>
            .galery-container {
                display: flex;
                flex-direction: row;
                margin: 0 auto;
                padding: 5px;
                width: 100%;
                justify-content: center;
            }
            .image {
                transition: all 0.5s;
                height: 300px;
                width: auto;
                margin: 5px;
                border-radius: 25px;
                /* box-shadow: 0px 0px 5px 5px rgba(184, 184, 184, 1); */
            }
            .img-galeria {
                height: 100%;
                margin: auto;
                max-width: 100%;
                object-fit: cover;
                border-radius: 25px;
            }



        </style>





                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">


                        <div class="galery-container">
                            <div class="image">
                                <img class="img-galeria"
                                        src="<?php echo $imgArray[2]['url'] ?>"
                                />
                            </div>
                            <div class="image">
                                <img class="img-galeria"
                                        ssrc="<?php echo $imgArray[1]['url'] ?>"
                                />
                            </div>
                            <div class="image">
                                <img class="img-galeria"
                                        src="<?php echo $imgArray[0]['url'] ?>"
                                />
                            </div>
                        </div>
                    </div>























        <?php else: ?>

        <?php endif; ?>









    </div>
</div>


