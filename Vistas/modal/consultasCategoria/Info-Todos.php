<?php
$id = $_GET['id'];
//var_dump($id);
include '../../../ConexionBD.php';
include_once 'funcioinesMostrarAreas/Areas.php';

?>




<style>
    .img-redes-uv {
        max-width: 100%;
        width: 9%;
        height: auto;
        padding: 1rem;
    }

    .img-galery-uv {
        max-width: 100%;
        width: 100%;
        height: auto;
    }
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 col-md-offset-6">
            <?php if (($datos['SitioWebPerfil'] === "") && ($datos['SitioFacebook'] === "") && ($datos['instagram'] === "")): ?>
            <?php else: ?>
                <label for="">Visitanos en</label>
            <?php endif;?>

            <?php if ($datos['SitioWebPerfil'] === ""): ?>
            <?php else: ?>
                <a href="<?php echo $datos['SitioWebPerfil']; ?>" target="_blank" rel="noopener noreferrer"><img src="img/redes/e.png" class="img-redes-uv">
                </a>
            <?php endif;?>


            <?php if ($datos['SitioFacebook'] === ""): ?>
            <?php else: ?>
                <a href="<?php echo $datos['SitioFacebook']; ?>" target="_blank" rel="noopener noreferrer"><img src="img/redes/f.png" class="img-redes-uv">
                </a>
            <?php endif;?>


            <?php if ($datos['instagram'] === ""): ?>
            <?php else: ?>
                <a href="<?php echo $datos['instagram']; ?>" target="_blank" rel="noopener noreferrer"><img src="img/redes/i.png" class="img-redes-uv">
                </a>
            <?php endif;?>

            <?php if ($youtube === 'https://www.youtube.com/' || $facebook === 'https://www.facebook.com/'): ?>
                <a href="<?php echo $datosV['Url']; ?>" target="_blank" rel="noopener noreferrer"><img src="img/redes/en.png" class="img-redes-uv">
                </a>
            <?php else: ?>

            <?php endif;?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Información</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>Grupo:</th>
                                <td><?php echo $datos[
                                    'NombreGrupo'
                                    ]; ?></td>
                            </tr>
                            <tr>
                                <th>Iniciativa:</th>
                                <td><?php echo $datos[
                                    'NombreIniciativa'
                                    ]; ?></td>
                            </tr>
                            <tr>
                                <th>Año:</th>
                                <td><?php echo $datos['AnoInicio']; ?></td>
                            </tr>
                            <tr>
                                <th>Correo:</th>
                                <td><?php echo $datos['Correo']; ?></td>
                            </tr>
                            <tr>
                                <th>Estado:</th>
                                <td><?php echo $datos['estados']; ?></td>
                            </tr>
                            <tr>
                                <th>Municipio:</th>
                                <td><?php echo $datos['municipio']; ?></td>
                            </tr>
                            <tr>
                                <th>Resumen:</th>
                                <td>
                                    <p class="text-justify"> <?php echo $datos[
                                        'Resumen'
                                        ]; ?></p>
                                </td>
                            </tr>
                            </tbody>

                        </table>
                    </div>


                    <?php $datos['Resumen']; ?>
                </div>
            </div>
        </div>



        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <strong>Área(s) en las que trabajan</strong>
        </div>

        <?php
        include_once 'funcioinesMostrarAreas/Urbana.php';
        include_once 'funcioinesMostrarAreas/Salud_plantas_medicinales.php';
        include_once 'funcioinesMostrarAreas/Desarrollo_Local.php';
        include_once 'funcioinesMostrarAreas/Economia.php';
        include_once 'funcioinesMostrarAreas/Actividades_no_agrícolas.php';
        include_once 'funcioinesMostrarAreas/Educacion.php';
        include_once 'funcioinesMostrarAreas/DrechosHumanos.php';
        include_once 'funcioinesMostrarAreas/Trabajos_con_NJ.php';
        include_once 'funcioinesMostrarAreas/Investigación_agrícola_Extensión_rural.php';
        include_once 'funcioinesMostrarAreas/Manejo_recursos_hídricos.php';
        include_once 'funcioinesMostrarAreas/Manejo_poscosecha.php';
        include_once 'funcioinesMostrarAreas/Semillas.php';
        include_once 'funcioinesMostrarAreas/Sistemas_agroforestales.php';
        include_once 'funcioinesMostrarAreas/Productos_forestales.php';
        include_once 'funcioinesMostrarAreas/Sistemas_producción_animal.php';
        include_once 'funcioinesMostrarAreas/Sistemas_producción_agrícola.php';


        ?>






























        <?php
        $sql = "
        select * from
        co_agr_entrevista cae
        LEFT JOIN co_agr_nucleoagrario can ON cae.idEntrevista = can.IdTipoOrganizacion
        LEFT JOIN co_agr_fundacion caf ON cae.idEntrevista = caf.IdTipoOrganizacion
        left join co_agr_gobierno cag on cae.idEntrevista = cag.IdTipoOrganizacion
        left join co_agr_institucioneducativa caie on cae.idEntrevista = caie.IdTipoOrganizacion
        left join co_agr_tipoorganizacion catp on cae.idEntrevista = catp.idTipoColectivo
        left join co_agr_cooperativa_colectivo cacc on cae.idEntrevista = cacc.idTipoColectivo
        left join co_agr_empresa_colectivo caec on cae.idEntrevista = caec.idTipoColectivo
        left join co_agr_grupos_organizados cagu on cae.idEntrevista = cagu.idTipoColectivo
        left join co_agr_tipocolectivoorganizacion catpo on cae.idEntrevista = catpo.idEntrevista
        where cae.idEntrevista = '$id'
        ";
        $query = $conexion->query($sql);
        $entrevistaArray2 = $query->fetch_array(MYSQLI_ASSOC);
        //var_dump($entrevistaArray);



        ?>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <strong>Tipo de colectivo y/o organización</strong>

        </div>

        <?php
        include_once 'funcioinesMostrarAreas/Núcleo_Agrario.php';
        include_once 'funcioinesMostrarAreas/Fundación.php';
        include_once 'funcioinesMostrarAreas/Gobierno.php';
        include_once 'funcioinesMostrarAreas/Instituciones_Educativas.php';
        include_once 'funcioinesMostrarAreas/Tipo_Organización.php';
        include_once 'funcioinesMostrarAreas/Cooperativa.php';
        include_once 'funcioinesMostrarAreas/Tipo_Empresa.php';
        include_once 'funcioinesMostrarAreas/Grupos_Organizados.php';
        include_once 'funcioinesMostrarAreas/Programa_Extensión.php';


        ?>
        <!--



















        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Otro</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <ul>
                                <li> <input type="text" name="otro_Colec1" id="otro_Colec1"  value="<?php echo $Otro= (!empty($entrevistaArray['Otro']) )? $entrevistaArray2['Otro']:'' ?>" > Otro (especificar) </li>
                            </ul>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        -->


    </div>
</div>




