

<section class="menu-vertical">
    <input type="checkbox" id="check" />
    <label for="check">
        <i class="fas fa-bars" id="btn"></i>
        <i class="fas fa-times" id="cancel"></i>
    </label>

    <div class="sidebar">
        <header>Capas</header>
        <ul>
            <li>
                <details>
                    <summary class="title-category-uv">
                        Agricultura Urbana
                    </summary>
                    <ul class="op-category-uv">
                        <li>

                            <input name="op" id="op1" value="1" type="checkbox" class="form-check-input"> Azotea
                        </li>
                        <li>
                            <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> Balcon
                        </li>
                        <li>
                            <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> Traspatio
                        </li>
                        <li>
                            <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> Parque o espacios publicos
                        </li>

                        <li>

                            <details>
                                <summary>
                                    Huertos
                                </summary>
                        <li>
                            <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> Huertos comunitarios
                        </li>
                        <li>
                            <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> Huertos sociales
                        </li>
                        <li>
                            <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> Huertos institucionales
                        </li>
                        <li>
                            <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> Huertos de empresas
                        </li>
                        <li>
                            <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> otros
                        </li>

                </details>

            </li>

        </ul>
        </details>
        </li>

        <li>
            <details>
                <summary class="title-category-uv">
                    Salud y plantas medicinales
                </summary>
                <ul class="op-category-uv">
                    <li>
                        <input name="op" id="op" value="1" type="checkbox" class="form-check-input"> Homeopatía
                    </li>
                    <li>
                        <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> Medicina tradicional
                    </li>
                    <li>
                        <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> Preparados botánicos
                    </li>
                    <li>
                        <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> Remedios caseros
                    </li>
                    <li>
                        <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> Nutrición agroecológica
                    </li>
                    <li>
                        <input name="op" id="op" value="1" type="checkbox" class="text-category-uv"> Otro
                    </li>
                </ul>
            </details>
        </li>
        </ul>
    </div>
</section>