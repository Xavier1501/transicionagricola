<?php

include_once '../ConexionBD.php';
include_once 'MetodosIncuesta.php';

$objMetodo = new MetodosIncuesta();



$nameGroup = $conexion->real_escape_string($_POST['name']);
$nameRepresentative = $conexion->real_escape_string($_POST['representante']);
$email = $conexion->real_escape_string($_POST['correo']);
$urlSocial = $conexion->real_escape_string($_POST['urlSocial']);
$state = $conexion->real_escape_string($_POST['estado']);
$municipality = $conexion->real_escape_string($_POST['municipio']);
$address = $conexion ->real_escape_string($_POST['direccion']);
$cp = $conexion ->real_escape_string($_POST['cp']);

/*Traemos los check */



/*Agricultura Urbana*/
$urbanFarming = $conexion->real_escape_string($_POST['AgriUr1']);

//var_dump('Agricultura Urbana:'. $urbanFarming);


/*Salud y plantas medicinales*/
$health_1 = $conexion->real_escape_string($_POST['salud1']);
$health_2 = $conexion->real_escape_string($_POST['salud2']);
$health_3 = $conexion->real_escape_string($_POST['salud3']);
$health_4 = $conexion->real_escape_string($_POST['salud4']);
$health_5 = $conexion->real_escape_string($_POST['salud5']);





/*Actividades no agrícolas*/
$non_agricultural_1 = $conexion->real_escape_string($_POST['ActividadNoAgro1']);
$non_agricultural_2 = $conexion->real_escape_string($_POST['ActividadNoAgro2']);
$non_agricultural_3 = $conexion->real_escape_string($_POST['ActividadNoAgro3']);
$non_agricultural_4 = $conexion->real_escape_string($_POST['ActividadNoAgro4']);
$non_agricultural_5 = $conexion->real_escape_string($_POST['ActividadNoAgro5']);

/* Derechos Humanos*/
$Human_Right_1 = $conexion->real_escape_string($_POST['DerechosH1']);

/*Desarrollo Local*/
$deve_local_1 = $conexion->real_escape_string($_POST['DesarrolloLocal1']);
$deve_local_2 = $conexion->real_escape_string($_POST['DesarrolloLocal2']);
$deve_local_3 = $conexion->real_escape_string($_POST['DesarrolloLocal3']);
$deve_local_4 = $conexion->real_escape_string($_POST['DesarrolloLocal4']);
$deve_local_5 = $conexion->real_escape_string($_POST['DesarrolloLocal5']);
$deve_local_6 = $conexion->real_escape_string($_POST['DesarrolloLocal6']);


/*economia*/
$economy_1=$conexion -> real_escape_string($_POST['Economia1']);
$economy_2 =$conexion -> real_escape_string($_POST['Economia2']);
$economy_3 =$conexion -> real_escape_string($_POST['Economia3']);
$economy_4 =$conexion -> real_escape_string($_POST['Economia4']);
$economy_5 =$conexion -> real_escape_string($_POST['Economia5']);
$economy_6 =$conexion -> real_escape_string($_POST['Economia6']);
$economy_7 =$conexion -> real_escape_string($_POST['Economia7']);
$economy_8 =$conexion -> real_escape_string($_POST['Economia8']);
$economy_9 =$conexion -> real_escape_string($_POST['Economia9']);
$economy_10 =$conexion -> real_escape_string($_POST['Economia10']);
$economy_11 =$conexion -> real_escape_string($_POST['Economia11']);




/*economia->cooperativas*/
$economy_cooperatives_1 = $conexion -> real_escape_string($_POST['Cooperativa1']);
$economy_cooperatives_2 = $conexion -> real_escape_string($_POST['Cooperativa2']);
$economy_cooperatives_3 = $conexion -> real_escape_string($_POST['Cooperativa3']);


/*economia -> cooperativas -> produccion*/
$economy_cooperatives_production_1 = $conexion -> real_escape_string($_POST['producion01']);
$economy_cooperatives_production_2 = $conexion -> real_escape_string($_POST['producion02']);


/*Educacion*/
$education_1 = $conexion -> real_escape_string($_POST['Educacion1']);
$education_2 = $conexion -> real_escape_string($_POST['Educacion2']);
$education_3 = $conexion -> real_escape_string($_POST['Educacion3']);
$education_4 = $conexion -> real_escape_string($_POST['Educacion4']);
$education_5 = $conexion -> real_escape_string($_POST['Educacion5']);
$education_6 = $conexion -> real_escape_string($_POST['Educacion6']);


/*Derechos Humanos*/
$human_rights_1 = $conexion -> real_escape_string($_POST['DerechosH1']);

/* Trabajo con niños / joventud */
$work_children_1 = $conexion -> real_escape_string($_POST['TrabajoNJ1']);
$work_children_2 = $conexion -> real_escape_string($_POST['TrabajoNJ2']);
$work_children_3 = $conexion -> real_escape_string($_POST['TrabajoNJ3']);
$work_children_4 = $conexion -> real_escape_string($_POST['TrabajoNJ4']);


/* Investigación agrícola y Extensión rural */
$investigation_1 = $conexion -> real_escape_string($_POST['Investigacion1']);
$investigation_2 = $conexion -> real_escape_string($_POST['Investigacion2']);
$investigation_3= $conexion -> real_escape_string($_POST['Investigacion3']);
$investigation_4 = $conexion -> real_escape_string($_POST['Investigacion4']);
$investigation_5 = $conexion -> real_escape_string($_POST['Investigacion5']);
$investigation_6 = $conexion -> real_escape_string($_POST['Investigacion6']);
$investigation_7 = $conexion -> real_escape_string($_POST['Investigacion7']);


/*  Manejo de recursos hídricos */

$driving_resources_1 = $conexion -> real_escape_string($_POST['ManejoRecursos1']);
$driving_resources_2 = $conexion -> real_escape_string($_POST['ManejoRecursos2']);
$driving_resources_3 = $conexion -> real_escape_string($_POST['ManejoRecursos3']);
$driving_resources_4 = $conexion -> real_escape_string($_POST['ManejoRecursos4']);
$driving_resources_5 = $conexion -> real_escape_string($_POST['ManejoRecursos5']);
$driving_resources_6 = $conexion -> real_escape_string($_POST['ManejoRecursos6']);
$driving_resources_7 = $conexion -> real_escape_string($_POST['ManejoRecursos7']);


/* Manejo poscosecha*/
$driving_harvest_1  = $conexion -> real_escape_string($_POST['ManejoPoscosecha1']);
$driving_harvest_2  = $conexion -> real_escape_string($_POST['ManejoPoscosecha2']);
$driving_harvest_3  = $conexion -> real_escape_string($_POST['ManejoPoscosecha3']);
$driving_harvest_4  = $conexion -> real_escape_string($_POST['ManejoPoscosecha4']);



/*Semillas*/
$seeds_1 = $conexion -> real_escape_string($_POST['Semillas1']);
$seeds_2 = $conexion -> real_escape_string($_POST['Semillas2']);
$seeds_3 = $conexion -> real_escape_string($_POST['Semillas3']);
$seeds_4 = $conexion -> real_escape_string($_POST['Semillas4']);
$seeds_5 = $conexion -> real_escape_string($_POST['Semillas5']);
$seeds_6 = $conexion -> real_escape_string($_POST['Semillas6']);


/*Genero*/
$gender  = $conexion ->real_escape_string($_POST['genero1']);

/*Sistemas agroforestales*/
$system_agroforestry_1 = $conexion -> real_escape_string($_POST['sistemaAgro1']);
$system_agroforestry_2 = $conexion -> real_escape_string($_POST['sistemaAgro2']);
$system_agroforestry_3 = $conexion -> real_escape_string($_POST['sistemaAgro3']);

/*Sistemas agroforestales -> Arborizacion*/
$system_agroforestry_tree_1 = $conexion -> real_escape_string($_POST['arbo1']);
$system_agroforestry_tree_2 = $conexion -> real_escape_string($_POST['arbo2']);
$system_agroforestry_tree_3 = $conexion -> real_escape_string($_POST['arbo3']);

/*Productos forestales*/
$proucts_forestry_1 = $conexion ->real_escape_string($_POST['prof1']);
$proucts_forestry_2 = $conexion ->real_escape_string($_POST['prof2']);
$proucts_forestry_3 = $conexion ->real_escape_string($_POST['prof3']);

/*  Sistemas de producción animal */
$system_production_animal_1 = $conexion ->real_escape_string($_POST['sisProAn1']);
$system_production_animal_2 = $conexion ->real_escape_string($_POST['sisProAn2']);
$system_production_animal_3 = $conexion ->real_escape_string($_POST['sisProAn3']);
$system_production_animal_4 = $conexion ->real_escape_string($_POST['sisProAn4']);
$system_production_animal_5 = $conexion ->real_escape_string($_POST['sisProAn5']);
$system_production_animal_6 = $conexion ->real_escape_string($_POST['sisProAn6']);
$system_production_animal_7 = $conexion ->real_escape_string($_POST['sisProAn7']);
$system_production_animal_8 = $conexion ->real_escape_string($_POST['sisProAn8']);
$system_production_animal_9 = $conexion ->real_escape_string($_POST['sisProAn9']);
$system_production_animal_10 = $conexion ->real_escape_string($_POST['sisProAn10']);
$system_production_animal_11 = $conexion ->real_escape_string($_POST['sisProAn11']);
$system_production_animal_12 = $conexion ->real_escape_string($_POST['sisProAn12']);
$system_production_animal_13 = $conexion ->real_escape_string($_POST['sisProAn13']);
$system_production_animal_14 = $conexion ->real_escape_string($_POST['sisProAn14']);
$system_production_animal_15 = $conexion ->real_escape_string($_POST['sisProAn15']);
//var_dump($system_production_animal_15);

/*  Sistemas de producción animal -> Apicultura */
$system_beekeeping_1 = $conexion ->real_escape_string($_POST['apicultura1']);
$system_beekeeping_2 = $conexion ->real_escape_string($_POST['apicultura2']);

/*Sistemas de producción agrícola -> Estrategias alternativas de producción:*/
$system_agricola_1 = $conexion ->real_escape_string($_POST['estAlPro1']);
$system_agricola_2 = $conexion ->real_escape_string($_POST['estAlPro2']);
$system_agricola_3 = $conexion ->real_escape_string($_POST['estAlPro3']);
$system_agricola_4 = $conexion ->real_escape_string($_POST['estAlPro4']);
$system_agricola_5 = $conexion ->real_escape_string($_POST['estAlPro5']);
$system_agricola_6 = $conexion ->real_escape_string($_POST['estAlPro6']);

/*Sistemas de producción agrícola -> Manejo de plagas y enfermedades::*/
$driving_pests_diseases_1 = $conexion -> real_escape_string($_POST['manPlEn1']);
$driving_pests_diseases_2 = $conexion -> real_escape_string($_POST['manPlEn2']);
$driving_pests_diseases_3 = $conexion -> real_escape_string($_POST['manPlEn3']);

/*Sistemas de producción agrícola -> Manejo de suelos:*/
$system_driving_soils_1 = $conexion -> real_escape_string($_POST['mandSue1']);
$system_driving_soils_2 = $conexion -> real_escape_string($_POST['mandSue2']);
$system_driving_soils_3 = $conexion -> real_escape_string($_POST['mandSue3']);
$system_driving_soils_4 = $conexion -> real_escape_string($_POST['mandSue4']);
$system_driving_soils_5 = $conexion -> real_escape_string($_POST['mandSue5']);
$system_driving_soils_6 = $conexion -> real_escape_string($_POST['mandSue6']);
$system_driving_soils_7 = $conexion -> real_escape_string($_POST['mandSue7']);
$system_driving_soils_8 = $conexion -> real_escape_string($_POST['mandSue8']);
$system_driving_soils_9 = $conexion -> real_escape_string($_POST['mandSue9']);
$system_driving_soils_10 = $conexion -> real_escape_string($_POST['mandSue10']);
$system_driving_soils_11 = $conexion -> real_escape_string($_POST['mandSue11']);
$system_driving_soils_12 = $conexion -> real_escape_string($_POST['mandSue12']);
$system_driving_soils_13 = $conexion -> real_escape_string($_POST['mandSue13']);

/*Sistemas de producción agrícola -> Sistemas de producción agrícola:*/

$system_production_agricola_1 = $conexion -> real_escape_string($_POST['sisdProdAgri1']);
$system_production_agricola_2 = $conexion -> real_escape_string($_POST['sisdProdAgri2']);
$system_production_agricola_3 = $conexion -> real_escape_string($_POST['sisdProdAgri3']);
$system_production_agricola_4 = $conexion -> real_escape_string($_POST['sisdProdAgri4']);
$system_production_agricola_5 = $conexion -> real_escape_string($_POST['sisdProdAgri5']);
$system_production_agricola_6 = $conexion -> real_escape_string($_POST['sisdProdAgri6']);
$system_production_agricola_7 = $conexion -> real_escape_string($_POST['sisdProdAgri7']);
$system_production_agricola_8 = $conexion -> real_escape_string($_POST['sisdProdAgri7']);
$system_production_agricola_9 = $conexion -> real_escape_string($_POST['sisdProdAgri8']);


/*Sistemas de producción agrícola -> Sistemas de producción agrícola:*/
$system_production_1 = $conexion -> real_escape_string($_POST['sisdProdAgri10']);






//var_dump($data);


//--------Colectivo & Organización---//

/* Nucelo Agrario*/
$Agriculture_Nucleo1 = $conexion -> real_escape_string($_POST['nucAgri1']);
$Agriculture_Nucleo2 = $conexion -> real_escape_string($_POST['nucAgri2']);

/*Fundación*/
$Fundation1 = $conexion -> real_escape_string($_POST['fundacion1']);
$Fundation2 = $conexion -> real_escape_string($_POST['fundacion2']);
//$Fundation3 = $conexion -> real_escape_string($_POST['fundacion3']);


/* Gobierno*/
$Goverment1 = $conexion -> real_escape_string($_POST['gobierno1']);
$Goverment2 = $conexion -> real_escape_string($_POST['gobierno2']);
$Goverment3 = $conexion -> real_escape_string($_POST['gobierno3']);

/* Institucuines Educativas*/
$Education_Institute1 = $conexion -> real_escape_string($_POST['intEduc1']);
$Education_Institute2 = $conexion -> real_escape_string($_POST['intEduc2']);
$Education_Institute3 = $conexion -> real_escape_string($_POST['intEduc3']);
$Education_Institute4 = $conexion -> real_escape_string($_POST['intEduc4']);
$Education_Institute5 = $conexion -> real_escape_string($_POST['intEduc5']);
$Education_Institute6 = $conexion -> real_escape_string($_POST['intEduc6']);
$Education_Institute7 = $conexion -> real_escape_string($_POST['intEduc7']);
$Education_Institute8 = $conexion -> real_escape_string($_POST['intEduc8']);

/* Organización Campesina*/
$Farm_Organization = $conexion -> real_escape_string($_POST['orgCamp1']);

/*Colectivo Mujeres*/
$Women_Colective = $conexion -> real_escape_string($_POST['colMuj1']);

/*Cooperativa*/
$Cooperative = $conexion -> real_escape_string($_POST['coop1']);

/*Cooperativa de Consumo*/
$Consume_Cooperative = $conexion -> real_escape_string($_POST['coopCon1']);

/*Cooperativa de Producción*/
$Production_Coperative = $conexion -> real_escape_string($_POST['cooProd1']);

/*Ejido*/
$Ejido = $conexion -> real_escape_string($_POST['eji1']);

/*Empresa Social*/
$Social_Company = $conexion -> real_escape_string($_POST['empSoc1']);

/*Empresa Privada*/
$Company_Private = $conexion -> real_escape_string($_POST['empPriv1']);

/*Grupo de Asesores Tecnicos*/
$Technical_Grup = $conexion -> real_escape_string($_POST['grupAse']);

/*Grupo de consumidores*/
$Consumer_Group = $conexion -> real_escape_string($_POST['grupCons1']);

/*Grupo de investigación*/
$Investigation_Group = $conexion -> real_escape_string($_POST['grupoInve1']);

/*Grupo Estudiantil*/
$Student_Group = $conexion -> real_escape_string($_POST['grupEst1']);

/*Grupo de la Iglesia*/
$Church_Group = $conexion -> real_escape_string($_POST['grupIgl1']);

/*Organización de la Sociedad Civil*/
$Organization_Civil = $conexion -> real_escape_string($_POST['orgSocCiv1']);

/*Progama de extension*/
$Extension_Group = $conexion -> real_escape_string($_POST['progExt1']);

/* Otro-Colectivo*/
$Other_Colectivo = $conexion -> real_escape_string($_POST['otro_Colec1']);




//--------Seguridad---//

$Security1 = $conexion -> real_escape_string($_POST['seguridad1']);
$Security2 = $conexion -> real_escape_string($_POST['seguridad2']);
$Security3 = $conexion -> real_escape_string($_POST['seguridad3']);
$Security4 = $conexion -> real_escape_string($_POST['seguridad4']);

/*
$data = array(
    'security' => array(
        '1' => $Security1 = ($Security1 == 'on')? '1': '0',
        '2' => $Security2 = ($Security2 == 'on')? '1': '0',
        '3' => $Security3 = ($Security3 == 'on')? '1': '0',
        '4' => $Security4 = ($Security4 == 'on')? '1': '0'

    ),

    );
*/
//var_dump($data);



/* Atlas de iniciativas*/

$atlas_iniciatives_1 = $conexion -> real_escape_string($_POST['nombreIncExp']);
$atlas_iniciatives_2 = $conexion -> real_escape_string($_POST['resIni']);
$atlas_iniciatives_3 = $conexion -> real_escape_string($_POST['fechaAtlas']);
$atlas_iniciatives_Lat_4 = $conexion -> real_escape_string($_POST['txtLat']);
$atlas_iniciatives_Lng_5 = $conexion -> real_escape_string($_POST['txtLng']);

/*Hacia una nueva política pública*/
$public_politics_1 = $conexion ->real_escape_string($_POST['politicas1']);
$public_politics_2 = $conexion ->real_escape_string($_POST['politicas2']);
$public_politics_3 = $conexion ->real_escape_string($_POST['politicas3']);
$public_politics_4 = $conexion ->real_escape_string($_POST['politicas4']);

/*Tejiendo redes*/

$weaving_nets_1 = $conexion -> real_escape_string($_POST['redes1']);
$weaving_nets_2 = $conexion -> real_escape_string($_POST['redes2']);
$weaving_nets_3 = $conexion -> real_escape_string($_POST['redes3']);
$weaving_nets_4 = $conexion -> real_escape_string($_POST['redes4']);

/*Formacion*/

$training_1 = $conexion -> real_escape_string($_POST['formacion1']);
$training_2 = $conexion -> real_escape_string($_POST['formacion2']);

/*Nota*/
$note_1 = $conexion -> real_escape_string($_POST['nota1']);
$note_2 = $conexion -> real_escape_string($_POST['nota2']);
$note_3 = $conexion -> real_escape_string($_POST['nota3']);



/*javier*/

/*carlos */

$data = array(
    'nameGroup' => $nameGroup,
    'nameRepresentative' => $nameRepresentative,
    'email' => $email,
    'urlSocial' => $urlSocial,
    'state' => $state,
    'municipality' => $municipality,
    'address' => $address,
    'cp' => $cp,
    'urbanFarming' => $urbanFarming = ($urbanFarming == 'on')? '1': '0',
    'systemProductionAgricola' =>$system_production_1,

    'health' => array(
        '1' => $health_1 = ($health_1 == 'on')? '1': '0',
        '2' => $health_2 = ($health_2 == 'on')? '1': '0',
        '3' => $health_3 = ($health_3 == 'on')? '1': '0',
        '4' => $health_4 = ($health_4 == 'on')? '1': '0',
        '5' => $health_5
    ),
    'non_agricultural' => array(
        '1' => $non_agricultural_1 = ($non_agricultural_1 == 'on')? '1': '0',
        '2' => $non_agricultural_2 = ($non_agricultural_2 == 'on')? '1': '0',
        '3' => $non_agricultural_3 = ($non_agricultural_3 == 'on')? '1': '0',
        '4' => $non_agricultural_4 = ($non_agricultural_4 == 'on')? '1': '0',
        '5' => $non_agricultural_5
    ),
    'localDevelopment' => array(
        '1' => $deve_local_1 = ($deve_local_1 == 'on')? '1': '0',
        '2' => $deve_local_2 = ($deve_local_2 == 'on')? '1': '0',
        '3' => $deve_local_3 = ($deve_local_3 == 'on')? '1': '0',
        '4' => $deve_local_4 = ($deve_local_4 == 'on')? '1': '0',
        '5' => $deve_local_5 = ($deve_local_5 == 'on')? '1': '0',
        '6' => $deve_local_6

    ),

    'Economy' => array(
        '1' => $economy_1 = ($economy_1 == 'on')? '1': '0',
        '2' => $economy_2 = ($economy_2 == 'on')? '1': '0',
        '3' => $economy_3 = ($economy_3 == 'on')? '1': '0',
        '4' => $economy_4 = ($economy_4 == 'on')? '1': '0',
        '5' => $economy_5 = ($economy_5 == 'on')? '1': '0',
        '6' => $economy_6 = ($economy_6 == 'on')? '1': '0',
        '7' => $economy_7 = ($economy_7 == 'on')? '1': '0',
        '8' => $economy_8 = ($economy_8 == 'on')? '1': '0',
        '9' => $economy_9 = ($economy_9 == 'on')? '1': '0',
        '10' => $economy_10 = ($economy_10 == 'on')? '1': '0',
        '11' => $economy_11

    ),

    'economyCooperatives' => array(

        '1' => $economy_cooperatives_1 = ($economy_cooperatives_1 == 'on')? '1': '0',
        '2' => $economy_cooperatives_2 = ($economy_cooperatives_2 == 'on')? '1': '0',
        '3' => $economy_cooperatives_3 = ($economy_cooperatives_3 == 'on')? '1': '0'
    ),



    'economyProduction' => array(
        '1' => $economy_cooperatives_production_1 = ($economy_cooperatives_production_1 == 'on')? '1': '0',
        '2' => $economy_cooperatives_production_2 = ($economy_cooperatives_production_2 == 'on')? '1': '0'

    ),



    'education' => array(
        '1' => $education_1 = ($education_1 == 'on')? '1': '0',
        '2' => $education_2 = ($education_2 == 'on')? '1': '0',
        '3' => $education_3 = ($education_3 == 'on')? '1': '0',
        '4' => $education_4 = ($education_4 == 'on')? '1': '0',
        '5' => $education_5 = ($education_5 == 'on')? '1': '0',
        '6' => $education_6

    ),



    'wordChildren' => array(
        '1' => $work_children_1 = ($work_children_1 == 'on')? '1': '0',
        '2' => $work_children_2 = ($work_children_2 == 'on')? '1': '0',
        '3' => $work_children_3 = ($work_children_3 == 'on')? '1': '0',
        '4' => $work_children_4
    ),

    'investigation' => array(
        '1' => $investigation_1 = ($investigation_1 == 'on')? '1': '0',
        '2' => $investigation_2 = ($investigation_2 == 'on')? '1': '0',
        '3' => $investigation_3 = ($investigation_3 == 'on')? '1': '0',
        '4' => $investigation_4 = ($investigation_4 == 'on')? '1': '0',
        '5' => $investigation_5 = ($investigation_5 == 'on')? '1': '0',
        '6' => $investigation_6 = ($investigation_6 == 'on')? '1': '0',
        '7' => $investigation_7
    ),

    'drivingResources' => array(
        '1' => $driving_resources_1 = ($driving_resources_1 == 'on')? '1': '0',
        '2' => $driving_resources_2 = ($driving_resources_2 == 'on')? '1': '0',
        '3' => $driving_resources_3 = ($driving_resources_3 == 'on')? '1': '0',
        '4' => $driving_resources_4 = ($driving_resources_4 == 'on')? '1': '0',
        '5' => $driving_resources_5 = ($driving_resources_5 == 'on')? '1': '0',
        '6' => $driving_resources_6 = ($driving_resources_6 == 'on')? '1': '0',
        '7' => $driving_resources_7
    ),

    'drivingHarvest' => array(
        '1' => $driving_harvest_1 = ($driving_harvest_1 == 'on')? '1': '0',
        '2' => $driving_harvest_2 = ($driving_harvest_2 == 'on')? '1': '0',
        '3' => $driving_harvest_3 = ($driving_harvest_3 == 'on')? '1': '0',
        '4' => $driving_harvest_4

    ),

    'seeds' =>array(
        '1' => $seeds_1 = ($seeds_1 == 'on')? '1': '0',
        '2' => $seeds_2 = ($seeds_2 == 'on')? '1': '0',
        '3' => $seeds_3 = ($seeds_3 == 'on')? '1': '0',
        '4' => $seeds_4 = ($seeds_4 == 'on')? '1': '0',
        '5' => $seeds_5 = ($seeds_5 == 'on')? '1': '0',
        '6' => $seeds_6

    ),
    'gender' => $gender = ($gender == 'on')? '1': '0',

    'systemAgroforestry' => array(
        '1' => $system_agroforestry_1 = ($system_agroforestry_1 == 'on')? '1': '0',
        '2' => $system_agroforestry_2 = ($system_agroforestry_2 == 'on')? '1': '0',
        '3' => $system_agroforestry_3
    ),

    'systemAgroforestryTree' => array(
        '1' => $system_agroforestry_tree_1 = ($system_agroforestry_tree_1 == 'on')? '1': '0',
        '2' => $system_agroforestry_tree_2 = ($system_agroforestry_tree_2 == 'on')? '1': '0',
        '3' => $system_agroforestry_tree_3 = ($system_agroforestry_tree_3 == 'on')? '1': '0'
    ),

    'productsForestry' => array(
        '1' => $proucts_forestry_1 = ($proucts_forestry_1 == 'on')? '1': '0',
        '2' => $proucts_forestry_2 = ($proucts_forestry_2 == 'on')? '1': '0',
        '3' => $proucts_forestry_3
    ),

    'systemProductionAnimal' =>array(
        '1' => $system_production_animal_1 = ($system_production_animal_1 == 'on')? '1': '0',
        '2' => $system_production_animal_2 = ($system_production_animal_2 == 'on')? '1': '0',
        '3' => $system_production_animal_3 = ($system_production_animal_3 == 'on')? '1': '0',
        '4' => $system_production_animal_4 = ($system_production_animal_4 == 'on')? '1': '0',
        '5' => $system_production_animal_5 = ($system_production_animal_5 == 'on')? '1': '0',
        '6' => $system_production_animal_6 = ($system_production_animal_6 == 'on')? '1': '0',
        '7' => $system_production_animal_7 = ($system_production_animal_7 == 'on')? '1': '0',
        '8' => $system_production_animal_8 = ($system_production_animal_8 == 'on')? '1': '0',
        '9' => $system_production_animal_9 = ($system_production_animal_9 == 'on')? '1': '0',
        '10' => $system_production_animal_10 = ($system_production_animal_10 == 'on')? '1': '0',
        '11' => $system_production_animal_11 = ($system_production_animal_11 == 'on')? '1': '0',
        '12' => $system_production_animal_12 = ($system_production_animal_12 == 'on')? '1': '0',
        '13' => $system_production_animal_13 = ($system_production_animal_13 == 'on')? '1': '0',
        '14' => $system_production_animal_14 = ($system_production_animal_14 == 'on')? '1': '0',
        '15' => $system_production_animal_15
    ),

    'systemBeekeeping' => array(
        '1' => $system_beekeeping_1 = ($system_beekeeping_1 == 'on')? '1': '0',
        '2' => $system_beekeeping_2 = ($system_beekeeping_2 == 'on')? '1': '0'
    ),

    'systemAgriculaProduction' => array(
        '1' => $system_agricola_1 = ($system_agricola_1 == 'on')? '1': '0',
        '2' => $system_agricola_2 = ($system_agricola_2 == 'on')? '1': '0',
        '3' => $system_agricola_3 = ($system_agricola_3 == 'on')? '1': '0',
        '4' => $system_agricola_4 = ($system_agricola_4 == 'on')? '1': '0',
        '5' => $system_agricola_5 = ($system_agricola_5 == 'on')? '1': '0',
        '6' => $system_agricola_6 = ($system_agricola_6 == 'on')? '1': '0'
    ),


    'drivingPestsDiseases' => array(
        '1' => $driving_pests_diseases_1 = ($driving_pests_diseases_1 == 'on')? '1': '0',
        '2' => $driving_pests_diseases_2 = ($driving_pests_diseases_2 == 'on')? '1': '0',
        '3' => $driving_pests_diseases_3 = ($driving_pests_diseases_3 == 'on')? '1': '0'

    ),

    'systemDrivingSoils' => array(
        '1' => $system_driving_soils_1 = ($system_driving_soils_1 == 'on')? '1': '0',
        '2' => $system_driving_soils_2 = ($system_driving_soils_2 == 'on')? '1': '0',
        '3' => $system_driving_soils_3 = ($system_driving_soils_3 == 'on')? '1': '0',
        '4' => $system_driving_soils_4 = ($system_driving_soils_4 == 'on')? '1': '0',
        '5' => $system_driving_soils_5 = ($system_driving_soils_5 == 'on')? '1': '0',
        '6' => $system_driving_soils_6 = ($system_driving_soils_6 == 'on')? '1': '0',
        '7' => $system_driving_soils_7 = ($system_driving_soils_7 == 'on')? '1': '0',
        '8' => $system_driving_soils_8 = ($system_driving_soils_8 == 'on')? '1': '0',
        '9' => $system_driving_soils_9 = ($system_driving_soils_9 == 'on')? '1': '0',
        '10' => $system_driving_soils_10 = ($system_driving_soils_10 == 'on')? '1': '0',
        '11' => $system_driving_soils_11 = ($system_driving_soils_11 == 'on')? '1': '0',
        '12' => $system_driving_soils_12 = ($system_driving_soils_12 == 'on')? '1': '0',
        '13' => $system_driving_soils_13 = ($system_driving_soils_13 == 'on')? '1': '0'

    ),

    'systemProductionAgricola_1' => array(
        '1' => $system_production_agricola_1 = ($system_production_agricola_1 == 'on')? '1': '0',
        '2' => $system_production_agricola_2 = ($system_production_agricola_2 == 'on')? '1': '0',
        '3' => $system_production_agricola_3 = ($system_production_agricola_3 == 'on')? '1': '0',
        '4' => $system_production_agricola_4 = ($system_production_agricola_4 == 'on')? '1': '0',
        '5' => $system_production_agricola_5 = ($system_production_agricola_5 == 'on')? '1': '0',
        '6' => $system_production_agricola_6 = ($system_production_agricola_6 == 'on')? '1': '0',
        '7' => $system_production_agricola_7 = ($system_production_agricola_7 == 'on')? '1': '0',
        '8' => $system_production_agricola_8 = ($system_production_agricola_8 == 'on')? '1': '0',
        '9' => $system_production_agricola_9 = ($system_production_agricola_9 == 'on')? '1': '0'
    ),

    'systemProductionAgricolaRaiz' => $system_production_agricola_9,

    'agroNucelo' => array(
        '1' => $Agriculture_Nucleo1 = ($Agriculture_Nucleo1 == 'on')? '1': '0',
        '2' => $Agriculture_Nucleo2 = ($Agriculture_Nucleo2== 'on')? '1': '0'
    ),

    'fundation' => array(
        '1' => $Fundation1 = ($Fundation1 == 'on')? '1':'0',
        '2' => $Fundation2 = ($Fundation2 == 'on')? '1':'0',
        //'3' => $Fundation3 = ($Fundation3 == 'on')? '1':'0'
    ),

    'goberment' => array(
        '1' => $Goverment1 = ($Goverment1 == 'on')? '1':'0',
        '2' => $Goverment2 = ($Goverment2 == 'on')? '1':'0',
        '3' => $Goverment3 = ($Goverment3 == 'on')? '1':'0'
    ),

    'educative' => array(
        '1' => $Education_Institute1 = ($Education_Institute1 == 'on')? '1':'0',
        '2' => $Education_Institute2 = ($Education_Institute2 == 'on')? '1':'0',
        '3' => $Education_Institute3 = ($Education_Institute3 == 'on')? '1':'0',
        '4' => $Education_Institute4 = ($Education_Institute4 == 'on')? '1':'0',
        '5' => $Education_Institute5 = ($Education_Institute5 == 'on')? '1':'0',
        '6' => $Education_Institute6 = ($Education_Institute6 == 'on')? '1':'0',
        '7' => $Education_Institute7 = ($Education_Institute7 == 'on')? '1':'0',
        '8' => $Education_Institute8 = ($Education_Institute8 == 'on')? '1':'0',

    ),

    'farmOrganization' => $Farm_Organization = ($Farm_Organization == 'on')? '1': '0',

    'womenColective' => $Women_Colective = ($Women_Colective == 'on')? '1': '0',

    'cooperative' => $Cooperative = ($Cooperative == 'on')? '1': '0',

    'consumeCooperative' => $Consume_Cooperative = ($Consume_Cooperative == 'on')? '1': '0',

    'production_Coperative' => $Production_Coperative = ($Production_Coperative == 'on')? '1': '0',

    'ejido' => $Ejido = ($Ejido == 'on')? '1': '0',

    'socialCompany' => $Social_Company = ($Social_Company == 'on')? '1': '0',

    'privateCompany' => $Company_Private = ($Company_Private == 'on')? '1': '0',

    'technicalGroup' => $Technical_Grup = ($Technical_Grup == 'on')? '1': '0',

    'consumerGroup' => $Consumer_Group = ($Consumer_Group == 'on')? '1': '0',

    'investigationGroup' => $Investigation_Group = ($Investigation_Group == 'on')? '1': '0',

    'studentGroup' => $Student_Group = ($Student_Group == 'on')? '1': '0',

    'ChurchGroup' => $Church_Group = ($Church_Group == 'on')? '1': '0',

    'organizationCivil' => $Organization_Civil = ($Organization_Civil == 'on')? '1': '0',

    'extensionGroup' => $Extension_Group = ($Extension_Group == 'on')? '1': '0',

    'otherColectivo' => $Other_Colectivo,
    'humanRight' => $human_rights_1 =  ($human_rights_1 == 'on')? '1': '0',


    'atlas' => array(
        '1' => $atlas_iniciatives_1,
        '2' => $atlas_iniciatives_2,
        '3' => $atlas_iniciatives_3,
        '4' => $atlas_iniciatives_Lat_4,
        '5' => $atlas_iniciatives_Lng_5
    ),

    'politics' => array(
        '1' => $public_politics_1,
        '2' => $public_politics_2,
        '3' => $public_politics_3,
        '4' => $public_politics_4
    ),

    'networks' => array(
        '1' => $weaving_nets_1,
        '2' => $weaving_nets_2,
        '3' => $weaving_nets_3,
        '4' => $weaving_nets_4
    ),
    'training' => array(
        '1'=> $training_1,
        '2'=> $training_2
    ),

    'security' => array(
        '1' => $Security1 = ($Security1 == 'on')? '1': '0',
        '2' => $Security2 = ($Security2 == 'on')? '1': '0',
        '3' => $Security3 = ($Security3 == 'on')? '1': '0',
        '4' => $Security4 = ($Security4 == 'on')? '1': '0'

    ),

    'note' => array(
        '1' => $note_1,
        '2' => $note_2,
        '3' => $note_3
    )


);














