

/*.-----------------------------------*/
function show_Edu_Amb() {
    for (i = 0; i < array_Edu_EduAmbiental.length; i++) {
        var position = new google.maps.LatLng(array_Edu_EduAmbiental[i]['lat'], array_Edu_EduAmbiental[i]['log']);
        var tipo = array_Edu_EduAmbiental[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_Edu_EduAmbiental[i]['nombre']
        });

        O_array_Edu_EduAmbiental.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_Edu_EduAmbiental[i]['id'],function(){
                    $("#id-eamb-educ").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Edu_Amb(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_Edu_EduAmbiental.length; i++) {
        O_array_Edu_EduAmbiental[i].setMap(map);

    }
}
function ocultar_Edu_Amb() {
    Edu_Amb(null);
}

/*.-----------------------------------*/
function show_Edu_Camp() {
    for (i = 0; i < array_Edu_EscuelaCampesina.length; i++) {
        var position = new google.maps.LatLng(array_Edu_EscuelaCampesina[i]['lat'], array_Edu_EscuelaCampesina[i]['log']);
        var tipo = array_Edu_EscuelaCampesina[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_Edu_EscuelaCampesina[i]['nombre']
        });

        O_array_Edu_EscuelaCampesina.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_Edu_EscuelaCampesina[i]['id'],function(){
                    $("#id-ecam-educ").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Edu_Camp(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_Edu_EscuelaCampesina.length; i++) {
        O_array_Edu_EscuelaCampesina[i].setMap(map);

    }
}
function ocultar_Edu_Camp() {
    Edu_Camp(null);
}

/*.-----------------------------------*/
function show_Edu_EscInd() {
    for (i = 0; i < array_Edu_EscuelaIndigena.length; i++) {
        var position = new google.maps.LatLng(array_Edu_EscuelaIndigena[i]['lat'], array_Edu_EscuelaIndigena[i]['log']);
        var tipo = array_Edu_EscuelaIndigena[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_Edu_EscuelaIndigena[i]['nombre']
        });

        O_array_Edu_EscuelaIndigena.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_Edu_EscuelaIndigena[i]['id'],function(){
                    $("#id-eind-educ").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Edu_EscInd(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_Edu_EscuelaIndigena.length; i++) {
        O_array_Edu_EscuelaIndigena[i].setMap(map);

    }
}
function ocultar_Edu_EscInd() {
    Edu_EscInd(null);
}


/*.-----------------------------------*/
function show_Edu_aprende() {
    for (i = 0; i < array_Edu_ComuniAprende.length; i++) {
        var position = new google.maps.LatLng(array_Edu_ComuniAprende[i]['lat'], array_Edu_ComuniAprende[i]['log']);
        var tipo = array_Edu_ComuniAprende[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(250, 227, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_Edu_ComuniAprende[i]['nombre']
        });

        O_array_Edu_ComuniAprende.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_Edu_ComuniAprende[i]['id'],function(){
                    $("#id-capre-educ").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);array_Edu_ComuniAprende
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Edu_aprende(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_Edu_ComuniAprende.length; i++) {
        O_array_Edu_ComuniAprende[i].setMap(map);

    }
}
function ocultar_Edu_aprende() {
    Edu_aprende(null);
}

/*.-----------------------------------*/
function show_Edu_inici() {
    for (i = 0; i < array_Edu_IniciativaCampesina.length; i++) {
        var position = new google.maps.LatLng(array_Edu_IniciativaCampesina[i]['lat'], array_Edu_IniciativaCampesina[i]['log']);
        var tipo = array_Edu_IniciativaCampesina[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(24, 194, 208, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_Edu_IniciativaCampesina[i]['nombre']
        });

        O_array_Edu_IniciativaCampesina.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_Edu_IniciativaCampesina[i]['id'],function(){
                    $("#id-icc-educ").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);array_Edu_ComuniAprende
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Edu_inici(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_Edu_IniciativaCampesina.length; i++) {
        O_array_Edu_IniciativaCampesina[i].setMap(map);

    }
}
function ocultar_Edu_inici() {
    Edu_inici(null);
}

/*.-----------------------------------*/
function show_Edu_basica() {
    for (i = 0; i < array_Edu_Basicas.length; i++) {
        var position = new google.maps.LatLng(array_Edu_Basicas[i]['lat'], array_Edu_Basicas[i]['log']);
        var tipo = array_Edu_Basicas[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(250, 80, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_Edu_Basicas[i]['nombre']
        });

        O_array_Edu_Basicas.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_Edu_Basicas[i]['id'],function(){
                    $("#id-cabas-educ-educ").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);array_Edu_ComuniAprende
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Edu_basica(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_Edu_Basicas.length; i++) {
        O_array_Edu_Basicas[i].setMap(map);

    }
}
function ocultar_Edu_basica() {
    Edu_basica(null);
}

/*.-----------------------------------*/
function show_Edu_ms() {
    for (i = 0; i < array_Edu_MediaSuperior.length; i++) {
        var position = new google.maps.LatLng(array_Edu_MediaSuperior[i]['lat'], array_Edu_MediaSuperior[i]['log']);
        var tipo = array_Edu_MediaSuperior[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(13, 185, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_Edu_MediaSuperior[i]['nombre']
        });

        O_array_Edu_MediaSuperior.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_Edu_MediaSuperior[i]['id'],function(){
                    $("#id-efms-educ").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Edu_ms(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_Edu_MediaSuperior.length; i++) {
        O_array_Edu_MediaSuperior[i].setMap(map);

    }
}
function ocultar_Edu_ms() {
    Edu_ms(null);
}

/*.-----------------------------------*/
function show_Edu_s() {
    for (i = 0; i < array_Edu_Superior.length; i++) {
        var position = new google.maps.LatLng(array_Edu_Superior[i]['lat'], array_Edu_Superior[i]['log']);
        var tipo = array_Edu_Superior[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_Edu_Superior[i]['nombre']
        });

        O_array_Edu_Superior.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_Edu_Superior[i]['id'],function(){
                    $("#id-efs-educ").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Edu_s(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_Edu_Superior.length; i++) {
        O_array_Edu_Superior[i].setMap(map);

    }
}
function ocultar_Edu_s() {
    Edu_s(null);
}

/*.-----------------------------------*/
function show_Edu_otro() {
    for (i = 0; i < array_Edu_OtroEcudacion.length; i++) {
        var position = new google.maps.LatLng(array_Edu_OtroEcudacion[i]['lat'], array_Edu_OtroEcudacion[i]['log']);
        var tipo = array_Edu_OtroEcudacion[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_Edu_OtroEcudacion[i]['nombre']
        });

        O_array_Edu_OtroEcudacion.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_Edu_OtroEcudacion[i]['id'],function(){
                    $("#id-otro-educ").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Edu_otro(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_Edu_OtroEcudacion.length; i++) {
        O_array_Edu_OtroEcudacion[i].setMap(map);

    }
}
function ocultar_Edu_otro() {
    Edu_otro(null);
}

/*.-----------------------------------*/

function categoriaEducacion() {
    $('#menu-educacion').show("swing");
}

function mostrarCapaEducacion() {


    if(array_Edu_EduAmbiental){
        show_Edu_Amb();
    }
    if(array_Edu_EscuelaCampesina){
        show_Edu_Camp();
    }
    if(array_Edu_EscuelaIndigena){
        show_Edu_EscInd();
    }

    if(array_Edu_ComuniAprende){
        show_Edu_aprende();
    }

     if(array_Edu_IniciativaCampesina){
        show_Edu_inici();
    }

    if(array_Edu_MediaSuperior){
        show_Edu_ms();
    }

    if(array_Edu_Basicas){
        show_Edu_basica();
    }

    if (array_Edu_Superior){
        show_Edu_s();
    }


    if (array_Edu_OtroEcudacion){
        show_Edu_otro();
    }






}

function ocultarCapaEducacion() {
    $('#menu-educacion').hide("linear");
    ocultar_Edu_Amb();
    ocultar_Edu_Camp();
    ocultar_Edu_EscInd();
    ocultar_Edu_aprende();
    ocultar_Edu_inici();
    ocultar_Edu_basica();
    ocultar_Edu_ms();
    ocultar_Edu_s();
    ocultar_Edu_otro();
}



function mostrarEducacion() {
    $("#educacion").on('change', function(){
        if ($(this).is(':checked') ) {


            ocultarCapaAgricultura();
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas();
            ocultarCapaDH();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaSistemaProduccionAgricola()
            ocultarCapaEconomia();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            mostrarCapaEducacion();
            categoriaEducacion();
        }else{
           ocultarCapaEducacion();



        }
    });

}