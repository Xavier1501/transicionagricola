



/*.-----------------------------------*/
function showSpan_ae() {
    for (i = 0; i < arrego_span_ae.length; i++) {
        var position = new google.maps.LatLng(arrego_span_ae[i]['lat'], arrego_span_ae[i]['log']);
        var tipo = arrego_span_ae[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_ae[i]['nombre']
        });

        O_arrego_span_ae.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_ae[i]['id'],function(){
                    $("#id-ae-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_ae(map) {
   /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_ae.length; i++) {
        O_arrego_span_ae[i].setMap(map);

    }
}
function ocultar_Span_ae() {
    Span_ae(null);
}

/*.-----------------------------------*/
function showSpan_an() {
    for (i = 0; i < arrego_span_an.length; i++) {
        var position = new google.maps.LatLng(arrego_span_an[i]['lat'], arrego_span_an[i]['log']);
        var tipo = arrego_span_an[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_an[i]['nombre']
        });

        O_arrego_span_an.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_an[i]['id'],function(){
                    $("#id-an-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_an(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_an.length; i++) {
        O_arrego_span_an[i].setMap(map);

    }
}
function ocultar_Span_an() {
    Span_an(null);
}


/*.-----------------------------------*/
function showSpan_acua() {
    for (i = 0; i < arrego_span_acua.length; i++) {
        var position = new google.maps.LatLng(arrego_span_acua[i]['lat'], arrego_span_acua[i]['log']);
        var tipo = arrego_span_acua[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_acua[i]['nombre']
        });

        O_arrego_span_acua.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_acua[i]['id'],function(){
                    $("#id-ac-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_acua(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_acua.length; i++) {
        O_arrego_span_acua[i].setMap(map);

    }
}
function ocultar_Span_acua() {
    Span_acua(null);
}

/*.-----------------------------------*/
function showSpan_aves() {
    for (i = 0; i < arrego_span_aves.length; i++) {
        var position = new google.maps.LatLng(arrego_span_aves[i]['lat'], arrego_span_aves[i]['log']);
        var tipo = arrego_span_aves[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_aves[i]['nombre']
        });

        O_arrego_span_aves.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_aves[i]['id'],function(){
                    $("#id-aves-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_aves(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_aves.length; i++) {
        O_arrego_span_aves[i].setMap(map);

    }
}
function ocultar_Span_aves() {
    Span_aves(null);
}

/*.-----------------------------------*/
function showSpan_bov() {
    for (i = 0; i < arrego_span_bov.length; i++) {
        var position = new google.maps.LatLng(arrego_span_bov[i]['lat'], arrego_span_bov[i]['log']);
        var tipo = arrego_span_bov[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(161, 35, 175, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_aves[i]['nombre']
        });

        O_arrego_span_bov.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_bov[i]['id'],function(){
                    $("#id-Bovinos-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_bov(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_bov.length; i++) {
        O_arrego_span_bov[i].setMap(map);

    }
}
function ocultar_Span_bov() {
    Span_bov(null);
}

/*.-----------------------------------*/
function showSpan_capr() {
    for (i = 0; i < arrego_span_capr.length; i++) {
        var position = new google.maps.LatLng(arrego_span_capr[i]['lat'], arrego_span_capr[i]['log']);
        var tipo = arrego_span_capr[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(250, 227, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_capr[i]['nombre']
        });

        O_arrego_span_capr.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_capr[i]['id'],function(){
                    $("#id-Caprinos-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_capr(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_capr.length; i++) {
        O_arrego_span_capr[i].setMap(map);

    }
}
function ocultar_Span_capr() {
    Span_capr(null);
}

/*.-----------------------------------*/
function showSpan_cuni() {
    for (i = 0; i < arrego_span_cuni.length; i++) {
        var position = new google.maps.LatLng(arrego_span_cuni[i]['lat'], arrego_span_cuni[i]['log']);
        var tipo = arrego_span_cuni[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(250, 80, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_cuni[i]['nombre']
        });

        O_arrego_span_cuni.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_cuni[i]['id'],function(){
                    $("#id-Conejo-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_coni(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_cuni.length; i++) {
        O_arrego_span_cuni[i].setMap(map);

    }
}
function ocultar_Span_cuni() {
    Span_cuni(null);
}

/*.-----------------------------------*/
function showSpan_equi() {
    for (i = 0; i < arrego_span_equi.length; i++) {
        var position = new google.maps.LatLng(arrego_span_equi[i]['lat'], arrego_span_equi[i]['log']);
        var tipo = arrego_span_equi[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(13, 185, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_equi[i]['nombre']
        });

        O_arrego_span_equi.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_equi[i]['id'],function(){
                    $("#id-cmi-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_equi(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_equi.length; i++) {
        O_arrego_span_equi[i].setMap(map);

    }
}
function ocultar_Span_equi() {
    Span_equi(null);
}


/*.-----------------------------------*/
function showSpan_lomb() {
    for (i = 0; i < arrego_span_lomb.length; i++) {
        var position = new google.maps.LatLng(arrego_span_lomb[i]['lat'], arrego_span_lomb[i]['log']);
        var tipo = arrego_span_lomb[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_lomb[i]['nombre']
        });

        O_arrego_span_lomb.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_lomb[i]['id'],function(){
                    $("#id-Lombricultura-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_lomb(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_lomb.length; i++) {
        O_arrego_span_lomb[i].setMap(map);

    }
}
function ocultar_Span_lomb() {
    Span_lomb(null);
}

/*.-----------------------------------*/
function showSpan_ovi() {
    for (i = 0; i < arrego_span_ovi.length; i++) {
        var position = new google.maps.LatLng(arrego_span_ovi[i]['lat'], arrego_span_ovi[i]['log']);
        var tipo = arrego_span_ovi[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(24, 194, 208, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_ovi[i]['nombre']
        });

        O_arrego_span_ovi.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_ovi[i]['id'],function(){
                    $("#id-Ovinos-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_ovi(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_ovi.length; i++) {
        O_arrego_span_ovi[i].setMap(map);

    }
}
function ocultar_Span_ovi() {
    Span_ovi(null);
}

/*.-----------------------------------*/
function showSpan_pes() {
    for (i = 0; i < arrego_span_pes.length; i++) {
        var position = new google.maps.LatLng(arrego_span_pes[i]['lat'], arrego_span_pes[i]['log']);
        var tipo = arrego_span_pes[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(137, 79, 53, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_pes[i]['nombre']
        });

        O_arrego_span_pes.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_pes[i]['id'],function(){
                    $("#id-Pesca-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_pes(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_pes.length; i++) {
        O_arrego_span_pes[i].setMap(map);

    }
}
function ocultar_Span_pes() {
    Span_pes(null);
}

/*.-----------------------------------*/
function showSpan_porc() {
    for (i = 0; i < arrego_span_porc.length; i++) {
        var position = new google.maps.LatLng(arrego_span_porc[i]['lat'], arrego_span_porc[i]['log']);
        var tipo = arrego_span_porc[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(247, 66, 162, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_porc[i]['nombre']
        });

        O_arrego_span_porc.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_porc[i]['id'],function(){
                    $("#id-Porcino-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_porc(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_porc.length; i++) {
        O_arrego_span_porc[i].setMap(map);

    }
}
function ocultar_Span_porc() {
    Span_porc(null);
}

/*.-----------------------------------*/
function showSpan_conejo() {
    for (i = 0; i < arrego_span_conejo.length; i++) {
        var position = new google.maps.LatLng(arrego_span_conejo[i]['lat'], arrego_span_conejo[i]['log']);
        var tipo = arrego_span_conejo[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(137, 79, 53, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_conejo[i]['nombre']
        });

        O_arrego_span_conejo.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_conejo[i]['id'],function(){
                    $("#id-Conejo-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_conejo(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_conejo.length; i++) {
        O_arrego_span_conejo[i].setMap(map);

    }
}
function ocultar_Span_conejo() {
    Span_conejo(null);
}

/*.-----------------------------------*/
function showSpan_mg() {
    for (i = 0; i < arrego_span_mg.length; i++) {
        var position = new google.maps.LatLng(arrego_span_mg[i]['lat'], arrego_span_mg[i]['log']);
        var tipo = arrego_span_mg[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 28, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_mg[i]['nombre']
        });

        O_arrego_span_mg.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_mg[i]['id'],function(){
                    $("#id-mg-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_mg(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_mg.length; i++) {
        O_arrego_span_mg[i].setMap(map);

    }
}
function ocultar_Span_mg() {
    Span_mg(null);
}

/*.-----------------------------------*/
function showSpan_nan() {
    for (i = 0; i < arrego_span_nan.length; i++) {
        var position = new google.maps.LatLng(arrego_span_nan[i]['lat'], arrego_span_nan[i]['log']);
        var tipo = arrego_span_nan[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(216, 252, 255, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_nan[i]['nombre']
        });

        O_arrego_span_nan.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_nan[i]['id'],function(){
                    $("#id-na-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_nan(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_nan.length; i++) {
        O_arrego_span_nan[i].setMap(map);

    }
}
function ocultar_Span_nan() {
    Span_nan(null);
}

/*.-----------------------------------*/
function showSpan_ps() {
    for (i = 0; i < arrego_span_ps.length; i++) {
        var position = new google.maps.LatLng(arrego_span_ps[i]['lat'], arrego_span_ps[i]['log']);
        var tipo = arrego_span_ps[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(216, 222, 255, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_ps[i]['nombre']
        });

        O_arrego_span_ps.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_ps[i]['id'],function(){
                    $("#id-na-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_ps(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_ps.length; i++) {
        O_arrego_span_ps[i].setMap(map);

    }
}
function ocultar_Span_ps() {
    Span_ps(null);
}

/*.-----------------------------------*/
function showSpan_vn() {
    for (i = 0; i < arrego_span_vn.length; i++) {
        var position = new google.maps.LatLng(arrego_span_vn[i]['lat'], arrego_span_vn[i]['log']);
        var tipo = arrego_span_vn[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 255, 97, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_vn[i]['nombre']
        });

        O_arrego_span_vn.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_vn[i]['id'],function(){
                    $("#id-vn-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_vn(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_vn.length; i++) {
        O_arrego_span_vn[i].setMap(map);

    }
}
function ocultar_Span_vn() {
    Span_vn(null);
}

/*.-----------------------------------*/
function showSpan_hom() {
    for (i = 0; i < arrego_span_hom.length; i++) {
        var position = new google.maps.LatLng(arrego_span_hom[i]['lat'], arrego_span_hom[i]['log']);
        var tipo = arrego_span_hom[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 255, 97, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_hom[i]['nombre']
        });

        O_arrego_span_hom.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_hom[i]['id'],function(){
                    $("#id-home-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_hom(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_hom.length; i++) {
        O_arrego_span_hom[i].setMap(map);

    }
}
function ocultar_Span_hom() {
    Span_vn(null);
}

/*.-----------------------------------*/
function showSpan_cons() {
    for (i = 0; i < arrego_span_cons.length; i++) {
        var position = new google.maps.LatLng(arrego_span_cons[i]['lat'], arrego_span_cons[i]['log']);
        var tipo = arrego_span_cons[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(36, 255, 255, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_cons[i]['nombre']
        });

        O_arrego_span_cons.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_cons[i]['id'],function(){
                    $("#id-cmi-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_cons(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_cons.length; i++) {
        O_arrego_span_cons[i].setMap(map);

    }
}
function ocultar_Span_cons() {
    Span_cons(null);
}

/*.-----------------------------------*/
function showSpan_Otro() {
    for (i = 0; i < arrego_span_otro.length; i++) {
        var position = new google.maps.LatLng(arrego_span_otro[i]['lat'], arrego_span_otro[i]['log']);
        var tipo = arrego_span_otro[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_span_otro[i]['nombre']
        });

        O_arrego_span_cons.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_span_otro[i]['id'],function(){
                    $("#id-otro-spani").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Span_Otro(map) {
    /// console.log(O_arrego_span_ae);
    for (let i = 0; i < O_arrego_span_otro.length; i++) {
        O_arrego_span_otro[i].setMap(map);

    }
}
function ocultar_Span_Otro() {
    Span_Otro(null);
}









/*--------------------------------------*/

function CategoriaSPAnimal() {
    $('#menu-SistemaPA').show("swing");
}

function mostrarCapaSistemaProduccionAnimal() {
    if (arrego_span_ae) {
        showSpan_ae();
    }

    if (arrego_span_an) {
        showSpan_an();
    }

    if (arrego_span_acua) {
        showSpan_acua();
    }

    if (arrego_span_aves) {
        showSpan_aves();
    }

    if (arrego_span_bov) {
        showSpan_bov();
    }

    if (arrego_span_capr) {
        showSpan_capr();
    }

    if (arrego_span_cuni) {
        showSpan_capr();
    }

    if (arrego_span_equi) {
        showSpan_equi();
    }

    if (arrego_span_lomb) {
        showSpan_lomb();
    }

    if(arrego_span_ovi){
        showSpan_ovi();
    }

    if(arrego_span_pes){
        showSpan_pes();
    }

    if(arrego_span_porc){
        showSpan_porc();
    }

    if(arrego_span_conejo){
        showSpan_conejo();
    }

    if(arrego_span_mg){
        showSpan_mg();
    }

    if(arrego_span_nan){
        showSpan_nan();
    }

    if(arrego_span_ps){
        showSpan_ps();
    }

    if(arrego_span_vn){
        showSpan_vn();
    }

    if(arrego_span_hom){
        showSpan_hom();
    }
    if(arrego_span_cons){
        showSpan_cons();
    }

    if(arrego_span_otro){
        showSpan_Otro();
    }




}


function ocultarCapaSistemaProduccionAnimal() {
    $('#menu-SistemaPA').hide("linear");
    ocultar_Span_ae();
    ocultar_Span_an();
    ocultar_Span_acua();
    ocultar_Span_aves();
    ocultar_Span_bov();
    ocultar_Span_capr();
    ocultar_Span_equi();
    ocultar_Span_lomb();
    ocultar_Span_lomb();
    ocultar_Span_ovi();
    ocultar_Span_pes();
    ocultar_Span_porc();
    ocultar_Span_conejo();
    ocultar_Span_mg();
    ocultar_Span_nan();
    ocultar_Span_ps();
    ocultar_Span_vn();
    ocultar_Span_hom();
    ocultar_Span_cons();
    ocultar_Span_Otro()





}

function mostrarSistemaProduccionAnimal() {
    $("#sistemaProduccionAnimal").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas();
            ocultarCapaAgricultura();
            ocultarCapaDLocal();
            ocultarCapaDH();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaEconomia();
            ocultarCapaEducacion();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();

            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAgricola();
            mostrarCapaSistemaProduccionAnimal();
            CategoriaSPAnimal();

        }else{

            ocultarCapaSistemaProduccionAnimal();


        }
    });

}