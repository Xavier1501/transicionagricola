

/*funcion del mapa*/
function initialize() {
    // Creamos el objeto
    var map = new google.maps.Map(document.getElementById('map_canvas'), {
        zoom: 5,
        center: new google.maps.LatLng(19.5214634, -96.9179886),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // creates a draggable marker to the given coords
    var vMarker = new google.maps.Marker({
        position: new google.maps.LatLng(19.5214634, -96.9179886),
        draggable: true
    });

    // adds a listener to the marker
    // gets the coords when drag event ends
    // then updates the input with the new coords
    google.maps.event.addListener(vMarker, 'dragend', function (evt) {
        $("#txtLat").val(evt.latLng.lat().toFixed(6));
        $("#txtLng").val(evt.latLng.lng().toFixed(6));
        map.panTo(evt.latLng);
    });

    // centers the map on markers coords
    map.setCenter(vMarker.position);

    // adds the marker on the map
    vMarker.setMap(map);
}



/*Fecha actual*/




window.onload = function () {
    let fecha = new Date();
    let mes = fecha.getMonth() + 1;
    let dia = fecha.getDate();
    let ano = fecha.getFullYear();

    if (dia < 10)
        dia = '0' + dia;
    if (mes < 10)
        mes = '0' + mes;
    document.getElementById('fechaAtlas').value = ano + "-" + mes + "-" + dia;
}

/*Ajax para registrar a un nuevo usuario*/
$(document).ready(function () {
    $('#registrar').on('click', function (event) {
        event.preventDefault();
        let data = $('#registroFormUser').serialize();
        let url = "Controller/registerUser.php";

        let validar = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        let pass = $('#password').val();
        let pass2 = $('#password2').val();

        let user = $("#user").val();

        if (pass.length < 8) {
            $('#Error.has-error').removeClass('has-error');
            $('#nameDiv2.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('.has-error').remove();
            $('#Error').addClass('has-error');
            $('#Error').append('<strong class="help-block">La contraseña debe de ser mayor a 8 caracteres. </strong>');
            return false;
        } else {
            $('#nameDiv.has-error').removeClass('has-error');
            $('.help-block').remove();
        }

        if (pass === pass2){
            $.ajax({
                type: "POST",
                data: data,
                url: url,
                success: function (data) {
                    console.log(data);
                    if (data == 1) {
                        $('#Error.has-error').removeClass('has-error');
                        $('#nameDiv2.has-error').removeClass('has-error');
                        $('.help-block').remove();
                        $('.has-error').remove();
                        $('#Error').addClass('has-error');
                        $('#Error').append('<strong class="help-block  text-center alert alert-success ">Registro realizado con éxito' +
                            '. </strong>');


                    }else{
                        $('#Error.has-error').removeClass('has-error');
                        $('#nameDiv2.has-error').removeClass('has-error');
                        $('.help-block').remove();
                        $('.has-error').remove();
                        $('#Error').addClass('has-error');
                        $('#Error').append('<strong class="help-block">El usuario  ya está registrado en la base de datos. </strong>');


                    }
                }
            });

        }else{
            $('#Error.has-error').removeClass('has-error');
            $('#nameDiv2.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('.has-error').remove();
            $('#Error').addClass('has-error');
            $('#Error').append('<strong class="help-block">Las contraseñas no son iguales verifíquelas por favor. </strong>');
            return false;
        }



    });




    /*inicio de secion login*/
    $('#ingresar').on('click', function (event) {
        event.preventDefault();

        let data = $('#user_session').serialize();
        let url = "Controller/user.php";

        //console.log(data);

        let user = $('#username').val();
        let pass = $('#userPassword').val();


        if (user.length <= 0) {
            $('#Error.has-error').removeClass('has-error');
            $('#nuserDiv.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('.has-error').remove();
            $('#Error').addClass('has-error');
            $('#Error').append('<strong class="help-block">Es necesario el usuario. </strong>');
            return false;
        } else {
            $('#Error.has-error').removeClass('has-error');
            $('.help-block').remove();
        }

        if (pass.length <= 0) {
            $('#Error.has-error').removeClass('has-error');
            $('#nuserDiv.has-error').removeClass('has-error');
            $('.help-block').remove();
            $('.has-error').remove();
            $('#Error').addClass('has-error');
            $('#Error').append('<strong class="help-block">Es necesaria la contraseña. </strong>');
            return false;
        } else {
            $('#Error.has-error').removeClass('has-error');
            $('.help-block').remove();
        }


        //console.log(data);
        $.ajax({
            type: "POST",
            data: data,
            url: url,
            success: function (data) {

                console.log(data);

                if (data === '0'){

                    $('#Error.has-error').removeClass('has-error');
                    $('#nuserDiv.has-error').removeClass('has-error');
                    $('.help-block').remove();
                    $('.has-error').remove();
                    $('#Error').addClass('has-error');
                    $('#Error').append('<strong class="help-block">Contraseña incorrecta. </strong>');
                }

                if (data === '1'){
                    //console.log('admin');
                    window.location.href = "Admin.php";
                }

                if (data === '2'){
                    window.location.href = "Formulario.php";
                    //alert(data);
                    //console.log('  no admin');
                }
            }
        });
    });
});



function toTop() {
    window.scrollTo(0, 0)
}

/*Duplicar Estados*/
$(document).ready(function(){
    $("#cbx_estado").change(function () {
        $('#cbx_localidad').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
        $("#cbx_estado option:selected").each(function () {
            id_estado = $(this).val();
            $.post("Controller/estados.php", { id_estado: id_estado }, function(data){
                $("#cbx_municipio").html(data);
            });
        });
    })
});


/* carga de imagen*/
$(document).ready(function () {

    function fileTam(){

    }

    $('#id-img').on('click', function () {
        let datos = new FormData(document.getElementById("register_form"));
        let url = "Controller/Cimg.php";


        let file1 = $('#file1').val();
        let file2 = $('#file2').val();
        let file3 = $('#file3').val();


        $('#error-img.has-error').removeClass('alert alert-danger');
        $('.help-block').remove();


        if (file1 === ''){
            //alert('d');
            $('#error-img.has-error').removeClass('alert alert-danger');
            $('.help-block').remove();
            $('#error-img').addClass('has-error');
            $('#error-img').append('<div class=" alert alert-danger help-block" > <strong> Es necesario cargar la imagen 1. </strong> </div>');
            return false;

        }


        if (file2 === ''){
            //alert('d');
            $('#error-img.has-error').removeClass('alert alert-danger');
            $('.help-block').remove();
            $('#error-img').addClass('has-error');
            $('#error-img').append('<div class=" alert alert-danger help-block" > <strong> Es necesario cargar la imagen 2. </strong> </div>');
            return false;

        }

        if (file3 === ''){
            //alert('d');
            $('#error-img.has-error').removeClass('alert alert-danger');
            $('.help-block').remove();
            $('#error-img').addClass('has-error');
            $('#error-img').append('<div class=" alert alert-danger help-block" > <strong> Es necesario cargar la imagen 3. </strong> </div>');
            return false;

        }

        var fileSize = $('#file1')[0].files[0].size;
        var siezekiloByte = parseInt(fileSize / 1024);


        var fileSize2 = $('#file2')[0].files[0].size;
        var siezekiloByte2 = parseInt(fileSize2 / 1024);

        var fileSize3 = $('#file3')[0].files[0].size;
        var siezekiloByte3 = parseInt(fileSize3 / 1024);
        if (siezekiloByte > 1000){
            //alert('d');
            $('#error-img.has-error').removeClass('alert alert-danger');
            $('.help-block').remove();
            $('#error-img').addClass('has-error');
            $('#error-img').append('<div class=" alert alert-danger help-block" > <strong> La imagen 1 no debe de sobre pasar 1 MB. </strong> </div>');
            return false;
        }

        if (siezekiloByte2 > 1000){
            //alert('d');
            $('#error-img.has-error').removeClass('alert alert-danger');
            $('.help-block').remove();
            $('#error-img').addClass('has-error');
            $('#error-img').append('<div class=" alert alert-danger help-block" > <strong> La imagen 2 no debe de sobre pasar de 1 MB. </strong> </div>');
            return false;
        }

        if (siezekiloByte3 > 1000){
            //alert('d');
            $('#error-img.has-error').removeClass('alert alert-danger');
            $('.help-block').remove();
            $('#error-img').addClass('has-error');
            $('#error-img').append('<div class=" alert alert-danger help-block" > <strong> La imagen 3 no debe de sobre pasar de 1 MB. </strong> </div>');
            return false;
        }

        $.ajax({
            type: "POST",
            data: datos,
            url: url,
            dataType: "HTML",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {

                console.log(data);

                if (data == '1truetruetrue'){
                    //alert('d');
                    $('#error-img.has-error').removeClass('alert alert-danger');
                    $('.help-block').remove();
                    $('#error-img').addClass('has-error');
                    $('#error-img').append('<div class=" alert alert-danger help-block" > <strong> Imagenes Cargadas con exito. </strong> </div>');
                    return false;
                }

            }
        });
    });



});


/*muestra de informes de  mapa*/





$(document).ready(function () {
    /*fase1*/


    $('#urlInter').on('change', function () {
        $('#InterDiv.has-warning').removeClass('has-warning');
        $('.help-block').remove();
        $('#InterDiv').addClass('has-warning');
        $('#InterDiv').append('<strong class="help-block">Nota: El sitio de internet debe de ir parecido a esta forma: https://www.mi_Pagina.com.mx/  </strong>')


    });

    $('#urlSocial').on('change', function () {
        $('#faceDiv.has-warning').removeClass('has-warning');
        $('.help-block').remove();
        $('#faceDiv').addClass('has-warning');
        $('#faceDiv').append('<strong class="help-block">Nota: El sitio de Facebook debe de ir parecido a esta forma: https://www.facebook.com/Mi_Pagina </strong>')


    });

    $('#ins').on('change', function () {
        $('#IntamDiv.has-warning').removeClass('has-warning');
        $('.help-block').remove();
        $('#IntamDiv').addClass('has-warning');
        $('#IntamDiv').append('<strong class="help-block">Nota: El sitio de  Instagram debe de ir parecido a esta forma: https://www.instagram.com/mi_instagram </strong>')


    });

    $('#estado').on('change', function () {
        $('#IntamDiv.has-warning').removeClass('has-warning');
        $('.help-block').remove();


    });

    $('#guardarFase1').on('click', function () {

        let intenet = navigator.onLine ? '1':'0';

        if (intenet === '1'){
            let datos = $('#register_form').serialize();
            let url = "Controller/Cfase1.php";

            let nombre = $("#name").val();
            let representante = $("#representante").val();
            let correo = $("#correo").val();
            let sitioInter = $("#urlInter").val();
            let sitioFace = $("#urlSocial").val();
            let estados = $("#estado").val();
            let municipio = $("#municipio").val();
            let urlInter = $("#urlInter").val();
            let urlSocial = $("#urlSocial").val();
            let ins = $("#ins").val();




            let direccion = $("#direccion").val();
            let cp = $("#cp").val();

            if (estados.length <= 0) {

                $('#estadoDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
                $('#estadoDiv').addClass('has-error');
                $('#estadoDiv').append('<strong class="help-block">Es necesario el estado.</strong>');
                return false;
            } else {
                $('#estadoDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
            }

            if (municipio.length <= 0) {
                $('#municipioDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
                $('#municipioDiv').addClass('has-error');
                $('#municipioDiv').append('<strong class="help-block">Es necesario que seleccione un Municipio.</strong>');
                return false;
            } else {
                $('#municipioDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
            }

            if (nombre.length <= 0) {
                $('#nameDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
                $('#nameDiv').addClass('has-error');
                $('#nameDiv').append('<strong class="help-block">Es necesario un nombre. </strong>');
                return false;
            } else {
                $('#nameDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
            }

            if (representante.length <= 0) {
                $('#repreDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
                $('#repreDiv').addClass('has-error');
                $('#repreDiv').append('<strong class="help-block">Es necesario el nombre del repesentante.</strong>');
                return false;
            } else {
                $('#repreDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
            }


            if (correo.length <= 0) {
                $('#correoDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
                $('#correoDiv').addClass('has-error');
                $('#correoDiv').append('<strong class="help-block">Es necesario el correo.</strong>');
                return false;
            } else {
                $('#correoDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
            }





            if (direccion.length <= 0) {
                $('#direccionDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
                $('#direccionDiv').addClass('has-error');
                $('#direccionDiv').append('<strong class="help-block">Es necesaria la dirección.</strong>');
                return false;
            } else {
                $('#direccionDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
            }

            if (cp.length <= 0) {
                $('#cpDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
                $('#cpDiv').addClass('has-error');
                $('#cpDiv').append('<strong class="help-block">Es necesario el código postal.</strong>');
                return false;
            } else {
                $('#cpDiv.has-error').removeClass('has-error');
                $('.help-block').remove();
            }


            $.ajax({
                type: "POST",
                data: datos,
                url: url,
                success: function (data) {
                    console.log(data);
                    if (data == '1'){
                        console.log(data);
                        console.log('registrado');
                        window.location.href = "Preguntas-fase2.php";
                    }
                }
            });
        }else{
           alert('Verifique su conexión de internet e intente darle nuevamente "Siguiente".');

        }


    });




    $('#guardarFase4').on('click', function () {
        //let datos = $('#register_form').serialize();

        let intenet = navigator.onLine ? '1':'0';

        if(intenet === '1'){
            let datos = new FormData(document.getElementById("register_form"));
            let url = "Controller/Cfase4.php";
            let lat = $('#txtLat').val();
            let lon = $('#txtLng').val();

            //console.log(datos);
            if (lat.length == 0) {
                //alert('d');
                $('#error.has-error').removeClass('alert alert-danger');
                $('.help-block').remove();
                $('#error').addClass('has-error');
                $('#error').append('<div class=" alert alert-danger help-block" > <strong>Es necesario seleccionar una ubicación en el Mapa. </strong> </div>');
                return false;
            } else {
                $('#error.has-error').removeClass('has-error');
                $('.help-block').remove();
            }
            $.ajax({
                type: "POST",
                data: datos,
                url: url,
                dataType: "HTML",
                data: datos,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);

                    if (data == 'grupo'){
                        //console.log(data);
                        //console.log('registrado');
                        //window.location.href = "Preguntas-fase4_1.php";
                    }

                    if (data == 'continuar'){
                        //console.log(data);
                        //console.log('registrado');
                        // window.location.href = "Preguntas-fase4_1.php";
                    }



                }
            });
        }else{
            alert('Verifique su conexión de internet e intente darle nuevamente "Siguiente".');

        }
    });

    $('#guardarFase5').on('click', function () {

        let intenet = navigator.onLine ? '1':'0';
        if(intenet === '1'){
            let datos = $('#register_form').serialize();
            let url = "Controller/Cfase5.php";
            //console.log(datos);
            $.ajax({
                type: "POST",
                data: datos,
                url: url,

                success: function (data) {

                    //console.log(data);

                    if (data == '1'){
                        //console.log(data);
                        //console.log('registrado');
                        window.location.href = "Preguntas-fase6.php";
                    }
                }
            });

        }else{
            alert('Verifique su conexión de internet e intente darle nuevamente "Siguiente".');

        }


    });

    $('#guardarFase6').on('click', function () {


        let intenet = navigator.onLine ? '1':'0';
        if(intenet === '1'){
            let datos = $('#register_form').serialize();
            let url = "Controller/Cfase6.php";
            //console.log(datos);
            $.ajax({
                type: "POST",
                data: datos,
                url: url,
                success: function (data) {

                    //console.log(data);

                    if (data == '1'){
                        //console.log(data);
                        console.log('registrado');
                        window.location.href = "Preguntas-fase7.php";
                    }
                }
            });
        }else{
            alert('Verifique su conexión de internet e intente darle nuevamente "Siguiente".');

        }
    });

    $('#guardarFase7').on('click', function () {


        let intenet = navigator.onLine ? '1':'0';
        if(intenet === '1'){
            let datos = $('#register_form').serialize();
            let url = "Controller/Cfase7.php";
            //console.log(datos);
            $.ajax({
                type: "POST",
                data: datos,
                url: url,
                success: function (data) {

                    console.log(data);

                    if (data == '1'){
                        //console.log(data);
                        //console.log('registrado');
                        window.location.href = "Preguntas-fase8.php";
                    }
                }
            });
        }else{
            alert('Verifique su conexión de internet e intente darle nuevamente "Siguiente".');

        }



    });

    $('#guardarFase8').on('click', function () {

        let intenet = navigator.onLine ? '1':'0';
        if(intenet === '1'){
            let datos = $('#register_form').serialize();
            let url = "Controller/Cfase8.php";
            //console.log(datos);
            $.ajax({
                type: "POST",
                data: datos,
                url: url,
                success: function (data) {
                    console.log(data);
                    if (data == '1'){
                        //console.log(data);
                        //console.log('registrado');
                        window.location.href = "Preguntas-fase9.php";
                    }
                }
            });
        }else{
            alert('Verifique su conexión de internet e intente darle nuevamente "Siguiente".');

        }



    });



    /*editCarrusel*/
    $('#guardarFase9').on('click',function () {


        let intenet = navigator.onLine ? '1':'0';
        if(intenet === '1'){
            let datos = new FormData(document.getElementById("register_form9"));
            //let datos = $('#frmEditCarrusel').serialize();
            let url = "Controller/Cfase9.php";
            console.log(datos);

            $.ajax({
                url: url,
                type: "POST",
                dataType: "HTML",
                data: datos,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);

                }
            });
        }else{
            alert('Verifique su conexión de internet e intente darle nuevamente "Siguiente".');

        }



    });


    $('#guardarFase2').on('click', function () {
        let intenet = navigator.onLine ? '1':'0';
        if(intenet === '1'){
            let datos = $('#register_form').serialize();
            let url = "Controller/Cfase2.php";
            //console.log(datos);
            $.ajax({
                type: "POST",
                data: datos,
                url: url,
                success: function (data) {
                    console.log(data);
                    if (data == 'grupo'){
                        //console.log(data);
                        //console.log('registrado');
                        window.location.href = "Preguntas-fase3.php";
                    }

                    if (data == 'individual'){
                        //console.log(data);
                        //console.log('registrado');
                        window.location.href = "Preguntas-fase4.php";
                    }
                }
            });
        }else{
            alert('Verifique su conexión de internet e intente darle nuevamente "Siguiente".');

        }




    });

    $('#guardarFase4_1').on('click', function () {
        let intenet = navigator.onLine ? '1':'0';
        if (intenet === '1') {
            let datos = $('#register_form').serialize();
            let url = "Controller/Cfase4_1.php";
            console.log(datos);
            $.ajax({
                type: "POST",
                data: datos,
                url: url,
                success: function (data) {
                    console.log(data);
                    if (data == '1') {
                        //console.log(data);
                        //console.log('registrado');
                        window.location.href = "Preguntas-fase5.php";
                    }


                }

            });
        }else{
            alert('Verifique su conexión de internet e intente darle nuevamente "Siguiente".');
        }
    });


    $('#guardarFase3').on('click', function () {
        let intenet = navigator.onLine ? '1':'0';
        if (intenet === '1') {
            let datos = $('#register_form').serialize();
            let url = "Controller/Cfase3.php";
            $.ajax({
                type: "POST",
                data: datos,
                url: url,
                success: function (data) {
                    console.log(data);
                    if (data == '1') {
                        //console.log(data);
                        //console.log('registrado');
                        window.location.href = "Preguntas-fase4.php";
                    }
                }
            });
        }else{
            alert('Verifique su conexión de internet e intente darle nuevamente "Siguiente".');

        }
    });



    /*Contador de palabraspor cada texarea*/

    $("#resumen").on('keypress', function() {
            var limit = 500;
            $("#resumen").attr('maxlength', limit);
            var init = $(this).val().length;

            if(init<limit){
                init++;
                $('#caracteres').text("Máximo 500 caracteres:" + init);
            }

        });

    $("#surgimiento").on('keypress', function() {
        var limit = 500;
        $("#surgimiento").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-sur').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#historico").on('keypress', function() {
        var limit = 500;
        $("#historico").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-his').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#historico").on('keypress', function() {
        var limit = 500;
        $("#historico").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-his').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#actor").on('keypress', function() {
        var limit = 500;
        $("#actor").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-ac').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#proposito").on('keypress', function() {
        var limit = 500;
        $("#proposito").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-p').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#problemas").on('keypress', function() {
        var limit = 500;
        $("#problemas").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-pr').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#soluciones").on('keypress', function() {
        var limit = 500;
        $("#soluciones").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-sol').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#retos").on('keypress', function() {
        var limit = 500;
        $("#retos").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-retos').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#financiamiento").on('keypress', function() {
        var limit = 500;
        $("#financiamiento").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-fin').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#IgualdaPorque").on('keypress', function() {
        var limit = 500;
        $("#IgualdaPorque").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-igpq').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#DerechoMaternalPorQue").on('keypress', function() {
        var limit = 500;
        $("#DerechoMaternalPorQue").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-dmpq').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#AcosoSexual").on('keypress', function() {
        var limit = 500;
        $("#AcosoSexual").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-fin').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#DiscriminacionPorQue").on('keypress', function() {
        var limit = 500;
        $("#DiscriminacionPorQue").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-dispq').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#MujeresParticipanteProyectoporQue").on('keypress', function() {
        var limit = 500;
        $("#MujeresParticipanteProyectoporQue").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-mpppq').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#EnfermosCuidadosEspeciapesPorQue").on('keypress', function() {
        var limit = 500;
        $("#EnfermosCuidadosEspeciapesPorQue").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-ecepq').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#PoliticaEquidad").on('keypress', function() {
        var limit = 500;
        $("#PoliticaEquidad").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-peq').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#politicas1").on('keypress', function() {
        var limit = 500;
        $("#politicas1").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-pol1').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#politicas2").on('keypress', function() {
        var limit = 500;
        $("#politicas2").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-pol2').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#politicas3").on('keypress', function() {
        var limit = 500;
        $("#politicas3").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-pol3').text("Máximo 500 caracteres:" + init);
        }

    });

    /*Fase 6*/

    $("#redes1").on('keypress', function() {
        var limit = 500;
        $("#redes1").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-rd1').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#redes2").on('keypress', function() {
        var limit = 500;
        $("#redes2").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-rd2').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#redes3").on('keypress', function() {
        var limit = 500;
        $("#redes3").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-rd3').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#redes4").on('keypress', function() {
        var limit = 500;
        $("#redes4").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-rd4').text("Máximo 500 caracteres:" + init);
        }

    });

    /* fase 7*/

    $("#formacion1").on('keypress', function() {
        var limit = 500;
        $("#formacion1").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-for1').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#formacion2").on('keypress', function() {
        var limit = 500;
        $("#formacion2").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-for2').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#formacion5").on('keypress', function() {
        var limit = 500;
        $("#formacion5").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-for5').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#formacion8").on('keypress', function() {
        var limit = 500;
        $("#formacion8").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-for8').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#formacion9").on('keypress', function() {
        var limit = 500;
        $("#formacion9").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-for9').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#formacion10").on('keypress', function() {
        var limit = 500;
        $("#formacion10").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-for10').text("Máximo 500 caracteres:" + init);
        }

    });

    $("#formacion11").on('keypress', function() {
        var limit = 500;
        $("#formacion11").attr('maxlength', limit);
        var init = $(this).val().length;

        if(init<limit){
            init++;
            $('#car-for11').text("Máximo 500 caracteres:" + init);
        }

    });
});


/* Validar  Contraseña*/



/*Genero*/


$(document).ready(function() {





    $('input:radio[name=IgualdaSalario]').click(function (event) {
        var valor = $(event.target).val();
        let v1 = $('input:radio[name=IgualdaSalario]:checked').val();

        console.log(v1);
        if (v1 == "No") {
            $("#vista").show();

        } else if (v1 == 'Si'){
            $("#vista").hide();

        }

    });

    $('input:radio[name=DerechoMaternal]').click(function (event) {
        var valor = $(event.target).val();
        let v2 = $('input:radio[name=DerechoMaternal]:checked').val();

        if (v2 == "No") {
            $("#vista2").show();

        } else if (v2 == 'Si'){
            $("#vista2").hide();

        }

    });

    $('input:radio[name=Discriminacion]').click(function (event) {
        var valor = $(event.target).val();
        let v3 = $('input:radio[name=Discriminacion]:checked').val();

        if (v3 == "No") {
            $("#vista3").show();

        } else if (v3 == 'Si'){
            $("#vista3").hide();

        }

    });

    $('input:radio[name=MujeresParticipanteProyecto]').click(function (event) {
        var valor = $(event.target).val();
        let v4 = $('input:radio[name=MujeresParticipanteProyecto]:checked').val();

        if (v4 == "Si") {
            $("#vista4").show();
        } else if (v4 == 'No'){
            $("#vista4").hide();

        }

    });

    $('input:radio[name=MujeresParticipanteProyecto]').click(function (event) {
        var valor = $(event.target).val();
        let v4 = $('input:radio[name=MujeresParticipanteProyecto]:checked').val();

        if (v4 == "No") {
            $("#vista4").show();

        } else if (v4 == 'Si'){
            $("#vista4").hide();

        }

    });

    $('input:radio[name=EnfermosCuidadosEspeciales]').click(function (event) {
        var valor = $(event.target).val();
        let v5 = $('input:radio[name=EnfermosCuidadosEspeciales]:checked').val();

        if (v5 == "Si") {
            $("#vista5").show();

        } else if (v5 == 'No'){
            $("#vista5").hide();

        }

    });



    /*fase 7*/

    $('input:radio[name=formacion3]').click(function (event) {

        //alert('sa');
        var valor = $(event.target).val();
        let f7v5 = $('input:radio[name=formacion3]:checked').val();
        //console.log(f7v5);
        if (f7v5 == "SI") {
            $("#vista5").show();
        } else if (f7v5 == 'NO'){
            $("#vista5").hide();

        }

    });

    $('input:radio[name=formacion6]').click(function (event) {

        //alert('sa');
        var valor = $(event.target).val();
        let f7v6 = $('input:radio[name=formacion6]:checked').val();
        //console.log(f7v5);
        if (f7v6 == "SI") {
            $("#vista6").show();
        } else if (f7v6 == 'NO'){
            $("#vista6").hide();

        }

    });

});

















