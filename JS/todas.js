
/*.-----------------------------------*/
function showTodasLasIniciativas(){
    for (i = 0; i < iniciativa.length; i++) {
        var position = new google.maps.LatLng(iniciativa[i]['lat'], iniciativa[i]['log']);
        var tipo = iniciativa[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 0, 99, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: iniciativa[i]['nombre']
        });

        O_iniciativa.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/Info-Todos.php?id='+iniciativa[i]['id'],function(){
                    $("#id-todos").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function TodasLasIniciativas(map) {

    for (let i = 0; i < O_iniciativa.length; i++) {
        O_iniciativa[i].setMap(map);

    }
}
function ocultar_TodasIniciativas() {
    TodasLasIniciativas(null);
}








function mostraTodo() {
    showTodasLasIniciativas();
}
function ocultarTodasLasIniciativas() {
    ocultar_TodasIniciativas();

}


function ocultarTodo() {
    ocultarCapaAgricultura();
    ocultarCapaSaludPlantasMedicinales();
    ocultarCapaActividadesNoAgricolas();
    ocultarCapaDH();
    ocultarCapaTNJ();
    ocultarCapaIAR();
    ocultarCapaMRH();
    ocultarCapaMP();
    ocultarCapaDH();
    ocultarCapaSem();
    ocultarCapaSistemaProduccionAgricola()
    ocultarCapaEconomia();
    ocultarCapaDLocal();
    ocultarCapaSistemaAgroforestales();
    ocultarCapaProductosForestales();
    ocultarTodasLasIniciativas()
}


function mostrarTodasIniciativas() {

    mostraTodo();
    $("#todas").on('change', function(){
        if ($(this).is(':checked') ) {
            //alert('muestra')
            ocultarTodo();
            mostraTodo();

        }else{

            ocultarTodasLasIniciativas()
        }
    });

}