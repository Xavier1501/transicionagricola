
/*-----------------------------Proyectos----------------------*/

function showoDL_Proyectos() {
    for (i = 0; i < arr_DL_AP.length; i++) {
        var position = new google.maps.LatLng(arr_DL_AP[i]['lat'], arr_DL_AP[i]['log']);
        var tipo = arr_DL_AP[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arr_DL_AP[i]['nombre']
        });

        O_arr_DL_AP.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arr_DL_AP[i]['id'],function(){
                    $("#id-prot-dl").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function DLProyectos(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_DL_AP.length; i++) {
        O_arr_DL_AP[i].setMap(map);

    }
}
function ocultarDLProyectos() {
    DLProyectos(null);
}

/*.-----------------------------------*/
function mostrarCapaDLocal() {
    $('#menu-desarrollo-humano').show("swing");
    showoDL_Proyectos();
}

function ocultarCapaDLocal() {
    $('#menu-desarrollo-humano').hide("linear");
    ocultarDLProyectos();
}


/*-----------------------------Desarrollo----------------------*/

function showoDL_Desarrollo() {
    for (i = 0; i < arr_dlDesarrollo.length; i++) {
        var position = new google.maps.LatLng(arr_dlDesarrollo[i]['lat'], arr_dlDesarrollo[i]['log']);
        var tipo = arr_dlDesarrollo[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arr_DL_AP[i]['nombre']
        });

        O_arr_DL_Des.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arr_dlDesarrollo[i]['id'],function(){
                    $("#id-desorrlo-dl").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function DLDesarrollo(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_DL_Des.length; i++) {
        O_arr_DL_Des[i].setMap(map);

    }
}
function ocultarDLDesarrollo() {
    DLDesarrollo(null);
}

/*.-----------------------------------*/
function mostrarCapaDLocal() {
    $('#menu-desarrollo-humano').show("swing");
    showoDL_Proyectos();
    showoDL_Desarrollo();
}

function ocultarCapaDLocal() {
    $('#menu-desarrollo-humano').hide("linear");
    ocultarDLProyectos();

}

/*--------------------------Organizacion Agricola-------------------------*/

function showoDL_Org() {
    for (i = 0; i < arr_dl_Org.length; i++) {
        var position = new google.maps.LatLng(arr_dl_Org[i]['lat'], arr_dl_Org[i]['log']);
        var tipo = arr_dl_Org[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arr_DL_AP[i]['nombre']
        });

        O_arr_DL_AP.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arr_dl_Org[i]['id'],function(){
                    $("#id-orga-dl").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function DLOrg(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_DL_AP.length; i++) {
        O_arr_DL_AP[i].setMap(map);

    }
}
function ocultarDLOrg() {
    DLOrg(null);
}


/*--------------------------Programas gubernamentales-------------------------*/

function showoDL_Gob() {
    for (i = 0; i < arr_dl_Gob.length; i++) {
        var position = new google.maps.LatLng(arr_dl_Gob[i]['lat'], arr_dl_Gob[i]['log']);
        var tipo = arr_dl_Gob[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arr_dl_Gob[i]['nombre']
        });

        O_arr_dl_Gob.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arr_dl_Gob[i]['id'],function(){
                    $("#id-programa-dl").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function DLGob(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_dl_Gob.length; i++) {
        O_arr_dl_Gob[i].setMap(map);

    }
}
function ocultarDLGob() {
    DLGob(null);
}

/*--------------------------Programas nucleo-------------------------*/

function showoDL_Nucleo() {
    for (i = 0; i < arr_dl_Nuc.length; i++) {
        var position = new google.maps.LatLng(arr_dl_Nuc[i]['lat'], arr_dl_Nuc[i]['log']);
        var tipo = arr_dl_Nuc[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arr_dl_Nuc[i]['nombre']
        });

        O_arr_dl_Nuc.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arr_dl_Nuc[i]['id'],function(){
                    $("#id-nucleo-dl").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function DLNucleo(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_dl_Nuc.length; i++) {
        O_arr_dl_Nuc[i].setMap(map);

    }
}
function ocultarDLNucleo() {
    DLNucleo(null);
}

/*--------------------------Otros-------------------------*/

function showoDL_Otro() {
    for (i = 0; i < arr_dl_Otro.length; i++) {
        var position = new google.maps.LatLng(arr_dl_Otro[i]['lat'], arr_dl_Otro[i]['log']);
        var tipo = arr_dl_Otro[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arr_dl_Otro[i]['nombre']
        });

        O_arr_dl_Otro.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arr_dl_Otro[i]['id'],function(){
                    $("#id-otro-dl").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function DLOtro(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_dl_Otro.length; i++) {
        O_arr_dl_Otro[i].setMap(map);

    }
}
function ocultarDLOtro() {
    DLOtro(null);
}





/*.-----------------------------------*/

function categoriaDlocal() {
    $('#menu-desarrollo-humano').show("swing");
}

function mostrarCapaDLocal() {



    if(arr_DL_AP){
        showoDL_Proyectos();
    }

    if(arr_dlDesarrollo){
        showoDL_Desarrollo();
    }

    if(arr_dl_Org){
        showoDL_Org();
    }

    if(arr_dl_Gob){
        showoDL_Gob();
    }

    if(arr_dl_Nuc){
        showoDL_Nucleo();
    }

    if(arr_dl_Otro){
        showoDL_Otro();
    }

}

function ocultarCapaDLocal() {
    $('#menu-desarrollo-humano').hide("linear");
    ocultarDLProyectos();
    ocultarDLDesarrollo();
    ocultarDLOrg();
    ocultarDLGob();
    ocultarDLNucleo();
    ocultarDLOtro();

}





function mostrarDLocal() {
    $("#desarrolloLocal").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaAgricultura();
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas();
            ocultarCapaDH();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaEconomia();
            ocultarCapaSistemaProduccionAgricola();
            ocultarCapaEducacion();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            categoriaDlocal();
            mostrarCapaDLocal();


        }else{
            ocultarCapaDLocal();
            ocultarDLDesarrollo();


        }
    });

}