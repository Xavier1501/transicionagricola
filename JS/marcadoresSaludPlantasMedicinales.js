
function showoSaludOtro() {
    for (i = 0; i < arregloSaludOtrosaludPlantas.length; i++) {
        var position = new google.maps.LatLng(arregloSaludOtrosaludPlantas[i]['lat'], arregloSaludOtrosaludPlantas[i]['log']);
        var tipo = arregloSaludOtrosaludPlantas[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloSaludOtrosaludPlantas[i]['nombre']
        });

        ocultarSaludPlantasMedicnalesOtro.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloSaludOtrosaludPlantas[i]['id'],function(){
                    $("#id-salud-p-otro").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SaludOtro(map) {
    console.log(ocultarSaludOtro);
    for (let i = 0; i < ocultarSaludPlantasMedicnalesOtro.length; i++) {
        ocultarSaludPlantasMedicnalesOtro[i].setMap(map);
    }
}
function ocultarSaludOtro() {
    SaludOtro(null);
}
/*.-----------------------------------*/


function showoSaludNutricion() {
    for (i = 0; i < arregloSaludNutricionAgri.length; i++) {
        var position = new google.maps.LatLng(arregloSaludNutricionAgri[i]['lat'], arregloSaludNutricionAgri[i]['log']);
        var tipo = arregloSaludNutricionAgri[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(161, 35, 175, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloSaludNutricionAgri[i]['nombre']
        });

        ocultarSaludNutricionAgri.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloSaludNutricionAgri[i]['id'],function(){
                    $("#id-salud-p-nutricion").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SaludNutricion(map) {
    console.log(ocultarSaludNutricionAgri);
    for (let i = 0; i < ocultarSaludNutricionAgri.length; i++) {
        ocultarSaludNutricionAgri[i].setMap(map);

    }
}
function ocultarSaludNutricion() {
    SaludNutricion(null);
}


/*-------------------------------------*/


function showoSaludPlantasMedicnalesRemediosCaseros() {
    for (i = 0; i < arregloSaludPlantasMedicnalesRemedioCasero.length; i++) {
        var position = new google.maps.LatLng(arregloSaludPlantasMedicnalesRemedioCasero[i]['lat'], arregloSaludPlantasMedicnalesRemedioCasero[i]['log']);
        var tipo = arregloSaludPlantasMedicnalesRemedioCasero[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloSaludPlantasMedicnalesRemedioCasero[i]['nombre']
        });

        ocultarSaludPlantasMedicnalesPreparadosBotanicos.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloSaludPlantasMedicnalesRemedioCasero[i]['id'],function(){
                    $("#id-salud-p-remedio").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SaludPlantasMedicnalesRemediosCaseros(map) {
    console.log(ocultarSaludPlantasMedicnalesRemedioCasero);
    for (let i = 0; i < ocultarSaludPlantasMedicnalesRemedioCasero.length; i++) {
        ocultarSaludPlantasMedicnalesRemedioCasero[i].setMap(map);

    }
}
function ocultarSaludRemediosCaseros() {
    SaludPlantasMedicnalesRemediosCaseros(null);
}


/*-------------------------------------*/

function showoSaludPlantasMedicnalesPreparadosBotanicos() {
    for (i = 0; i < arregloSaludPlantasMedicnalesPreparadosBotanicos.length; i++) {
        var position = new google.maps.LatLng(arregloSaludPlantasMedicnalesPreparadosBotanicos[i]['lat'], arregloSaludPlantasMedicnalesPreparadosBotanicos[i]['log']);
        var tipo = arregloSaludPlantasMedicnalesPreparadosBotanicos[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloSaludPlantasMedicnalesPreparadosBotanicos[i]['nombre']
        });

        ocultarSaludPlantasMedicnalesPreparadosBotanicos.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloSaludPlantasMedicnalesHomepatia[i]['id'],function(){
                    $("#id-salud-p-preparativos").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SaludPlantasMedicnalesPreparadosBotanicos(map) {
    console.log(ocultarSaludPlantasMedicnalesPreparadosBotanicos);
    for (let i = 0; i < ocultarSaludPlantasMedicnalesPreparadosBotanicos.length; i++) {
        ocultarSaludPlantasMedicnalesPreparadosBotanicos[i].setMap(map);

    }
}
function ocultarSaludPreparativosBotanicos() {
    SaludPlantasMedicnalesPreparadosBotanicos(null);
}


/*--------------------------------------------------------------*/

function showoSaludPlantasMedicnalesHomepatia() {
    for (i = 0; i < arregloSaludPlantasMedicnalesHomepatia.length; i++) {
        var position = new google.maps.LatLng(arregloSaludPlantasMedicnalesHomepatia[i]['lat'], arregloSaludPlantasMedicnalesHomepatia[i]['log']);
        var tipo = arregloSaludPlantasMedicnalesHomepatia[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 194, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloSaludPlantasMedicnalesHomepatia[i]['nombre']
        });

        ocultarSaludPlantasMedicnalesHomepatia.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloSaludPlantasMedicnalesHomepatia[i]['id'],function(){
                    $("#id-salud-p-homepatia").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa
function SaludPlantasMedicnalesHomepatia(map) {
    console.log(ocultarSaludPlantasMedicnalesHomepatia);
    for (let i = 0; i < ocultarSaludPlantasMedicnalesHomepatia.length; i++) {
        ocultarSaludPlantasMedicnalesHomepatia[i].setMap(map);

    }
}
function ocultarSaludHomeopatia() {
    SaludPlantasMedicnalesHomepatia(null);
}

/*---------------------------------------------------*/

function showoSaludPlantasMedicnalesMedicina() {
    for (i = 0; i < arregloSaludPlantasMedicnalesMedicina.length; i++) {
        var position = new google.maps.LatLng(arregloSaludPlantasMedicnalesMedicina[i]['lat'], arregloSaludPlantasMedicnalesMedicina[i]['log']);
        var tipo = arregloSaludPlantasMedicnalesMedicina[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloSaludPlantasMedicnalesMedicina[i]['nombre']
        });

        ocultarSaludPlantasMedicnalesMedicina.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloSaludPlantasMedicnalesMedicina[i]['id'],function(){
                    $("#id-salud-p-medicina").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function SaludPlantasMedicnalesMedicina(map) {
    console.log(ocultarSaludPlantasMedicnalesMedicina);
    for (let i = 0; i < ocultarSaludPlantasMedicnalesMedicina.length; i++) {
        ocultarSaludPlantasMedicnalesMedicina[i].setMap(map);

    }
}
function ocultarSaludMedicina() {
    SaludPlantasMedicnalesMedicina(null);
}


function categoriaSPM() {
    $('#menu-salud-plantas-medicinales').show("swing");
}


function mostrarCapaSaludPlantasMedicinales() {



    if (arregloSaludPlantasMedicnalesHomepatia){
        showoSaludPlantasMedicnalesHomepatia();
    }

    if (arregloSaludPlantasMedicnalesMedicina){
        showoSaludPlantasMedicnalesMedicina();
    }
    if (arregloSaludPlantasMedicnalesPreparadosBotanicos){
        showoSaludPlantasMedicnalesPreparadosBotanicos();
    }

    if (arregloSaludPlantasMedicnalesRemedioCasero){
        showoSaludPlantasMedicnalesRemediosCaseros();
    }
    if (arregloSaludNutricionAgri){
        showoSaludNutricion();
    }


    if (arregloSaludOtrosaludPlantas){
        showoSaludOtro();
    }
}


function ocultarCapaSaludPlantasMedicinales() {
    $('#menu-salud-plantas-medicinales').hide("linear");
    ocultarSaludHomeopatia();
    ocultarSaludMedicina();
    ocultarSaludPreparativosBotanicos();
    ocultarSaludRemediosCaseros();
    ocultarSaludNutricion();
    ocultarSaludNutricion();
    ocultarSaludOtro();
}





function mostrarSaludPlantasMedicinales() {
    $("#saludPlantasMedicinales").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaAgricultura();
            ocultarCapaActividadesNoAgricolas();
            ocultarCapaDLocal();
            ocultarCapaDH();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaEconomia();
            ocultarCapaSistemaProduccionAgricola()
            ocultarCapaEducacion();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            mostrarCapaSaludPlantasMedicinales();
            categoriaSPM();

        }else{
            $('#menu-salud-plantas-medicinales').hide("linear");
            ocultarCapaSaludPlantasMedicinales()


        }
    });

}