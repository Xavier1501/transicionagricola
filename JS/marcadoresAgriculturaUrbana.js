
function showAgriculturaUrbanaHsociales() {
    for (i = 0; i < AgUrHempresas.length; i++) {
        var position = new google.maps.LatLng(AgUrHempresas[i]['lat'], AgUrHempresas[i]['log']);
        var tipo = AgUrHempresas[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 194, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: AgUrHempresas[i]['nombre']
        });

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+AgUrHempresas[i]['id'],function(){
                    $("#modelId2").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
function showAgriculturaUrbanaHempresas() {
    for (i = 0; i < AgUrHempresas.length; i++) {
        var position = new google.maps.LatLng(AgUrHempresas[i]['lat'], AgUrHempresas[i]['log']);
        var tipo = AgUrHempresas[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 194, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: AgUrHempresas[i]['nombre']
        });
        ocultarArregloHempresas.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+AgUrHempresas[i]['id'],function(){
                    $("#modelId2").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
function showAgriculturaUrbanaHcomunitarios() {
    /*
     * se aplica un para reccorrero el json
     *
     * */
    console.log(AgUrHcomunitarios);
    for (i = 0; i < AgUrHcomunitarios.length; i++) {
        var position = new google.maps.LatLng(AgUrHcomunitarios[i]['lat'], AgUrHcomunitarios[i]['log']);
        var tipo = AgUrHcomunitarios[i]['id'];






        marker = new google.maps.Marker({

            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(161, 35, 175, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: AgUrHcomunitarios[i]['id']
        });


        ocultarArregloHcomunitarios.push(marker);
        (function(i, marker) {

            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+AgUrHcomunitarios[i]['id'],function(){
                    $("#modelId").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }



}
function showAgriculturaUrbanaTranspatio(){

    /*
     * se aplica un para reccorrero el json
     *
     * */

    //console.log(AgUrTraspatio);
    for (i = 0; i < AgUrTraspatio.length; i++) {
        var position = new google.maps.LatLng(AgUrTraspatio[i]['lat'], AgUrTraspatio[i]['log']);
        var tipo = AgUrTraspatio[i]['id'];
        //alert(tipo)
        marker = new google.maps.Marker({
            position: position,
            icon: 'img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: AgUrTraspatio[i]['nombre']
        });

        ocultarArregloTraspatio.push(marker);
        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+AgUrTraspatio[i]['id'],function(){
                    $("#modelId3").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);


    }

}
function showAgriculturaUrbanaParques() {

    /*
     * se aplica un para reccorrero el json
     *
     * */

    console.log(AgUrParques);
    for (i = 0; i < AgUrParques.length; i++) {
        var position = new google.maps.LatLng(AgUrParques[i]['lat'], AgUrParques[i]['log']);
        var tipo = AgUrParques[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: 'img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: AgUrParques[i]['nombre']
        });

        ocultarArregloParque.push(marker);
        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+AgUrParques[i]['id'],function(){
                    $("#modelId4").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
function showAgriculturaUrbanaBalcon() {

    /*
     * se aplica un para reccorrero el json
     *
     * */


    console.log(AgUrBalcon);
    for (i = 0; i < AgUrBalcon.length; i++) {
        var variableModal = AgUrBalcon[i]['id'];
        //alert(variableModal);
        var position = new google.maps.LatLng(AgUrBalcon[i]['lat'], AgUrBalcon[i]['log']);
        marker = new google.maps.Marker({
            position: position,
            icon: 'img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: AgUrBalcon[i]['nombre']
        });

        ocultarArregloBalcon.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+AgUrBalcon[i]['id'],function(){
                    $("#modelId5").modal("show");
                });

                let contentMap = infowindow.setContent(
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }


}
function showMarkets() {
    console.log(datosMarkets);
    for (i = 0; i < datosMarkets.length; i++) {
        var position = new google.maps.LatLng(datosMarkets[i]['lat'], datosMarkets[i]['log']);
        var tipo = datosMarkets[i]['iniciativa'];

        marker = new google.maps.Marker({
            position: position,
            icon: 'img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: datosMarkets[i]['nombre']
        });

        ocultarArregloAzotea.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow();
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+datosMarkets[i]['id'],function(){
                    $("#modelId6").modal("show");
                });

                let contentMap = infowindow.setContent(""+id +"");

                //error imprementado aproposito para que bloquie  en info.
                contentMap.hide();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }



}
function showAgriculturaUrbanaHsociales() {
    console.log(arregloAgrulturaHsociales);
    for (i = 0; i < arregloAgrulturaHsociales.length; i++) {
        var position = new google.maps.LatLng(arregloAgrulturaHsociales[i]['lat'], arregloAgrulturaHsociales[i]['log']);
        var tipo = arregloAgrulturaHsociales[i]['iniciativa'];

        marker = new google.maps.Marker({
            position: position,
            icon: 'img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(25, 17, 233, 0.79)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloAgrulturaHsociales[i]['nombre']
        });

        ocultarArregloHsociales.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow();
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloAgrulturaHsociales[i]['id'],function(){
                    $("#modelId7").modal("show");
                });

                let contentMap = infowindow.setContent(""+id +"");

                //error imprementado aproposito para que bloquie  en info.
                contentMap.hide();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }



}
function showAgriculturaUrbanaHistitucionales() {
    console.log(arregloAgrulturaHsociales);
    for (i = 0; i < arregloAgrulturaHsociales.length; i++) {
        var position = new google.maps.LatLng(arregloAgrulturaHsociales[i]['lat'], arregloAgrulturaHsociales[i]['log']);
        var tipo = arregloAgrulturaHsociales[i]['iniciativa'];

        marker = new google.maps.Marker({
            position: position,
            icon: 'img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(192, 66, 233, 0.79)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloAgrulturaHsociales[i]['nombre']
        });
        ocultarArregloHisntitucional.push(marker);



        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow();
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloAgrulturaHsociales[i]['id'],function(){
                    $("#modelId8").modal("show");
                });
                let contentMap = infowindow.setContent(""+id +"");
                //error imprementado aproposito para que bloquie  en info.
                contentMap.hide();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }



}

function showAgriculturaUrbanaHotros() {
    console.log(arregloAgrulturaHOtros);
    for (i = 0; i < arregloAgrulturaHOtros.length; i++) {
        var position = new google.maps.LatLng(arregloAgrulturaHOtros[i]['lat'], arregloAgrulturaHOtros[i]['log']);
        var tipo = arregloAgrulturaHOtros[i]['iniciativa'];

        marker = new google.maps.Marker({
            position: position,
            icon: 'img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloAgrulturaHOtros[i]['nombre']
        });
        ocultarArregloHotros.push(marker);



        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow();
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloAgrulturaHOtros[i]['id'],function(){
                    $("#modelId9").modal("show");
                });

                let contentMap = infowindow.setContent(""+id +"");

                //error imprementado aproposito para que bloquie  en info.
                contentMap.hide();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }



}



// funcion para apagar la capa hComunitarios
function Hcomunitarios(map) {

    //console.log(ocultarArregloHcomunitarios);
    for (let i = 0; i < ocultarArregloHcomunitarios.length; i++) {
        ocultarArregloHcomunitarios[i].setMap(map);

    }




}
function ocultarHcomunitarios() {
    Hcomunitarios(null);
}



// funcion para apagar la capa Hempresa
function Hempresas (map) {

    for (let i = 0; i < ocultarArregloHempresas.length; i++) {
        ocultarArregloHempresas[i].setMap(map);

    }




}
function ocultarHempresa() {
    Hempresas(null);
}

// funcion para apagar la capa transpatio
function Traspatio (map) {

    for (let i = 0; i < ocultarArregloTraspatio.length; i++) {
        ocultarArregloTraspatio[i].setMap(map);

    }
}
function ocultarTraspatio() {
    Traspatio(null);
}

// funcion para apagar la capa parque
function Parque(map) {

    for (let i = 0; i < ocultarArregloParque.length; i++) {
        ocultarArregloParque[i].setMap(map);

    }
}
function ocultarParque() {
    Parque(null);
}

// funcion para apagar la capa balcon
function balcon(map) {

    for (let i = 0; i < ocultarArregloBalcon.length; i++) {
        ocultarArregloBalcon[i].setMap(map);

    }
}
function ocultarBalcon() {
    balcon(null);
}

// funcion para apagar la capa azotea
function Azotea(map) {

    for (let i = 0; i < ocultarArregloAzotea.length; i++) {
        ocultarArregloAzotea[i].setMap(map);

    }
}
function ocultarAzotea() {
    Azotea(null);
}
// funcion para apagar la capa Hsociales
function Hsociales(map) {

    for (let i = 0; i < ocultarArregloHsociales.length; i++) {
        ocultarArregloHsociales[i].setMap(map);

    }
}


function ocultarHsociales() {
    Hsociales(null);
}

// funcion para apagar la capa Hisntitucional
function Hisntitucional(map) {

    for (let i = 0; i < ocultarArregloHisntitucional.length; i++) {
        ocultarArregloHisntitucional[i].setMap(map);

    }
}
function ocultarHisntitucional() {
    Hisntitucional(null);
}

// funcion para apagar la capa Hotro
function Hotro(map) {

    console.log(ocultarArregloHotros);

    for (let i = 0; i < ocultarArregloHotros.length; i++) {
        ocultarArregloHotros[i].setMap(map);

    }
}
function ocultarHotro() {
    Hotro(null);
}



function categoriaAUrbana () {
    $('#menu-Agricultura').show("swing");
}

function mostrarCapaAgricultura() {
    if (AgUrHcomunitarios){
        showAgriculturaUrbanaHcomunitarios();

    }


    if(AgUrHempresas){
        showAgriculturaUrbanaHempresas();
    }

    if(datosMarkets){
        showMarkets();
    }



    if(AgUrTraspatio){
        showAgriculturaUrbanaTranspatio();
    }
    if(AgUrParques){
        showAgriculturaUrbanaParques()
    }
    if(AgUrBalcon){
        showAgriculturaUrbanaBalcon();
    }



    if(arregloAgrulturaHsociales){
        showAgriculturaUrbanaHsociales();
    }



    if (arregloAgrulturaHistitucional){
        showAgriculturaUrbanaHistitucionales();
    }


    //console.log(arregloAgrulturaHOtros)
    if (arregloAgrulturaHOtros){
        showAgriculturaUrbanaHotros();
    }




}

function ocultarCapaAgricultura() {
    $('#menu-Agricultura').hide("linear");
    ocultarHcomunitarios();
    ocultarHempresa();
    ocultarTraspatio();
    ocultarParque();
    ocultarBalcon();
    ocultarAzotea();
    ocultarHsociales();
    ocultarHisntitucional();
    ocultarHotro();

}




function mostrarAgricultura() {
    $("#agriculturaUrbana").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas();
            ocultarCapaDLocal();
            ocultarCapaDH();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaEconomia();
            ocultarCapaSistemaProduccionAgricola();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            mostrarCapaAgricultura();
            categoriaAUrbana();
        }else{
            ocultarCapaAgricultura();
        }
    });

}







