
function showoNoAgricolaTurismo() {
    for (i = 0; i < arregloNoAgricolasTurismo.length; i++) {
        var position = new google.maps.LatLng(arregloNoAgricolasTurismo[i]['lat'], arregloNoAgricolasTurismo[i]['log']);
        var tipo = arregloNoAgricolasTurismo[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloNoAgricolasTurismo[i]['nombre']
        });

        OcultarNoAgricolasTurismo.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloNoAgricolasTurismo[i]['id'],function(){
                    $("#id-no_agricola-turismo").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);arregloNoAgricolasTurismo
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function noAgricolaTurismo(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < OcultarNoAgricolasTurismo.length; i++) {
        OcultarNoAgricolasTurismo[i].setMap(map);
    }
}

function ocultarNoagricolaTurismo() {
    noAgricolaTurismo(null);
}

/*.-----------------------------------*/

function showoNoAgricolaArtesanal() {
    for (i = 0; i < arregloNoAgricolasArtesanales.length; i++) {
        var position = new google.maps.LatLng(arregloNoAgricolasArtesanales[i]['lat'], arregloNoAgricolasArtesanales[i]['log']);
        var tipo = arregloNoAgricolasArtesanales[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloNoAgricolasArtesanales[i]['nombre']
        });

        OcultarNoAgricolasArtesanales.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloNoAgricolasArtesanales[i]['id'],function(){
                    $("#id-Agricolas-artesanales").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function noAgricolaArtesanal(map) {
    console.log(ocultarSaludOtro);
    for (let i = 0; i < OcultarNoAgricolasArtesanales.length; i++) {
        OcultarNoAgricolasArtesanales[i].setMap(map);
    }
}

function ocultarNoagricolaArtesal() {
    noAgricolaArtesanal(null);
}
/*.-----------------------------------*/
function showoNoAgricolaCaja() {
    for (i = 0; i < arregloNoAgricolasCaja.length; i++) {
        var position = new google.maps.LatLng(arregloNoAgricolasCaja[i]['lat'], arregloNoAgricolasCaja[i]['log']);
        var tipo = arregloNoAgricolasCaja[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloNoAgricolasArtesanales[i]['nombre']
        });

        OcultarNoAgricolasCaja.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloNoAgricolasCaja[i]['id'],function(){
                    $("#id-Agricolas-caja").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function noAgricolaCaja(map) {
    console.log(ocultarSaludOtro);
    for (let i = 0; i < OcultarNoAgricolasCaja.length; i++) {
        OcultarNoAgricolasCaja[i].setMap(map);
    }
}

function ocultarNoagricolaCaja() {
    noAgricolaCaja(null);
}



/*.-----------------------------------*/

function showoNoAgricolaCulturales() {
    for (i = 0; i < arregloNoAgricolasCulturales.length; i++) {
        var position = new google.maps.LatLng(arregloNoAgricolasCulturales[i]['lat'], arregloNoAgricolasCulturales[i]['log']);
        var tipo = arregloNoAgricolasTurismo[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloNoAgricolasCulturales[i]['nombre']
        });

        OcultarNoAgricolasCulturales.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloNoAgricolasCulturales[i]['id'],function(){
                    $("#id-no-agricola-culturales").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa
function noAgricolaCulturales(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < OcultarNoAgricolasCulturales.length; i++) {
        OcultarNoAgricolasCulturales[i].setMap(map);
    }
}

function ocultarNoagricolaCultura() {
    noAgricolaCulturales(null);
}


/*.-----------------------------------*/

function showoNoAgricolaCulturales() {
    for (i = 0; i < arregloNoAgricolasCulturales.length; i++) {
        var position = new google.maps.LatLng(arregloNoAgricolasCulturales[i]['lat'], arregloNoAgricolasCulturales[i]['log']);
        var tipo = arregloNoAgricolasTurismo[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloNoAgricolasCulturales[i]['nombre']
        });

        OcultarNoAgricolasCulturales.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloNoAgricolasCulturales[i]['id'],function(){
                    $("#id-no-agricola-culturales").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa
function noAgricolaCulturales(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < OcultarNoAgricolasCulturales.length; i++) {
        OcultarNoAgricolasCulturales[i].setMap(map);
    }
}

function ocultarNoagricolaCultura() {
    noAgricolaCulturales(null);
}


/*.-----------------------------------*/

function showoNoAgricolaComedor() {
    for (i = 0; i < arregloNoAgricolasComedor.length; i++) {
        var position = new google.maps.LatLng(arregloNoAgricolasComedor[i]['lat'], arregloNoAgricolasComedor[i]['log']);
        var tipo = arregloNoAgricolasComedor[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(161, 35, 175, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloNoAgricolasComedorEscolar[i]['nombre']
        });

        OcultarNoAgricolasComedor.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloNoAgricolasComedor[i]['id'],function(){
                    $("#id-no-agricolas-comedor").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function noAgricolaComedor(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < OcultarNoAgricolasComedor.length; i++) {
        OcultarNoAgricolasComedor[i].setMap(map);
    }
}

function ocultarNoagricolaComedor() {
    noAgricolaComedor(null);
}

/*.-----------------------------------*/

function showoNoAgricolaComedorEscolar() {
    for (i = 0; i < arregloNoAgricolasComedorEscolar.length; i++) {
        var position = new google.maps.LatLng(arregloNoAgricolasComedorEscolar[i]['lat'], arregloNoAgricolasComedorEscolar[i]['log']);
        var tipo = arregloNoAgricolasComedorEscolar[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(250, 80, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloNoAgricolasComedorEscolar[i]['nombre']
        });

        OcultarNoAgricolasComedorEscolar.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloNoAgricolasComedorEscolar[i]['id'],function(){
                    $("#id-no-agricolas-comedor-escolar").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function noAgricolaComedorEscolar(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < OcultarNoAgricolasComedorEscolar.length; i++) {
        OcultarNoAgricolasComedorEscolar[i].setMap(map);
    }
}

function ocultarNoagricolaComedorEscolar() {
    noAgricolaComedorEscolar(null);
}

/*.-----------------------------------*/

function showoNoAgricolaRestaurante() {
    for (i = 0; i < arregloNoAgricolasRestautan.length; i++) {
        var position = new google.maps.LatLng(arregloNoAgricolasRestautan[i]['lat'], arregloNoAgricolasRestautan[i]['log']);
        var tipo = arregloNoAgricolasRestautan[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(13, 185, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloNoAgricolasRestautan[i]['nombre']
        });

        OcultarNoAgricolasRestaunte.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloNoAgricolasRestautan[i]['id'],function(){
                    $("#id-no-agricola-restaurante").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function noAgricolaRestaurante(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < OcultarNoAgricolasRestaunte.length; i++) {
        OcultarNoAgricolasRestaunte[i].setMap(map);
    }
}

function ocultarNoagricolaRestaurante() {
    noAgricolaRestaurante(null);
}

/*.-----------------------------------*/

function showoNoAgricolaOtro() {
    for (i = 0; i < arregloNoAgricolasOtro.length; i++) {
        var position = new google.maps.LatLng(arregloNoAgricolasOtro[i]['lat'], arregloNoAgricolasOtro[i]['log']);
        var tipo = arregloNoAgricolasOtro[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloNoAgricolasRestautan[i]['nombre']
        });

        OcultarNoAgricolasOtro.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloNoAgricolasOtro[i]['id'],function(){
                    $("#id-Agricolas-no-otro").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function noAgricolaOtro(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < OcultarNoAgricolasOtro.length; i++) {
        OcultarNoAgricolasOtro[i].setMap(map);
    }
}

function ocultarNoagricolaOtro() {
    noAgricolaOtro(null);
}
/*.-----------------------------------*/


function categoriaANAgr() {
    $('#menu-actividades-no-agricolas').show("swing");
}
function mostrarCapaActividadesNoAgricolas() {


    if (arregloNoAgricolasArtesanales){
        showoNoAgricolaArtesanal();
    }

    if (arregloNoAgricolasCaja){
        showoNoAgricolaCaja();
    }

    if (arregloNoAgricolasTurismo){
        showoNoAgricolaTurismo();
    }


    if (arregloNoAgricolasCulturales){
        showoNoAgricolaCulturales();
    }

    if(arregloNoAgricolasComedorEscolar){
        showoNoAgricolaComedorEscolar();
    }

    if (arregloNoAgricolasComedor){
        showoNoAgricolaComedor();
    }


    if (arregloNoAgricolasRestautan){
        showoNoAgricolaRestaurante();
    }

    if (arregloNoAgricolasRestautan){
        showoNoAgricolaRestaurante();
    }


    if(arregloNoAgricolasOtro){
        showoNoAgricolaOtro();
    }

}

function ocultarCapaActividadesNoAgricolas() {
    $('#menu-actividades-no-agricolas').hide("linear");
    ocultarNoagricolaArtesal();
    ocultarNoagricolaCaja();
    ocultarNoagricolaTurismo();
    ocultarNoagricolaCultura();
    ocultarNoagricolaComedor();
    ocultarNoagricolaComedorEscolar();
    ocultarNoagricolaRestaurante();
    ocultarNoagricolaOtro();

}



function mostrarActividadesNoAgricolas() {
    $("#actividadeNoAgricolas").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaAgricultura();
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaDLocal();
            ocultarCapaDH();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaEconomia();
            ocultarCapaSistemaProduccionAgricola();
            ocultarCapaEducacion();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            mostrarCapaActividadesNoAgricolas();

            categoriaANAgr();


        }else{
            ocultarCapaActividadesNoAgricolas();


        }
    });

}