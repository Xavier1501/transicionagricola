



/*.-----------------------------------*/
function show_SA_adcs() {
    for (i = 0; i < arrego_sa_adcs.length; i++) {
        var position = new google.maps.LatLng(arrego_sa_adcs[i]['lat'], arrego_sa_adcs[i]['log']);
        var tipo = arrego_sa_adcs[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_sa_adcs[i]['nombre']
        });

        O_arrego_sa_adcs.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_sa_adcs[i]['id'],function(){
                    $("#id-macdcss-sagro").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SA_adcs(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_arrego_sa_adcs.length; i++) {
        O_arrego_sa_adcs[i].setMap(map);

    }
}
function ocultar_SA_adcs() {
    SA_adcs(null);
}

/*.-----------------------------------*/
function show_SA_sa() {
    for (i = 0; i < arrego_sa_sa.length; i++) {
        var position = new google.maps.LatLng(arrego_sa_sa[i]['lat'], arrego_sa_sa[i]['log']);
        var tipo = arrego_sa_sa[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_sa_sa[i]['nombre']
        });

        O_arrego_sa_sa.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_sa_sa[i]['id'],function(){
                    $("#id-ast-sagro").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SA_sa(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_arrego_sa_sa.length; i++) {
        O_arrego_sa_sa[i].setMap(map);

    }
}
function ocultar_SA_sa() {
    SA_sa(null);
}


/*.-----------------------------------*/
function show_SA_bc() {
    for (i = 0; i < arrego_sa_bc.length; i++) {
        var position = new google.maps.LatLng(arrego_sa_bc[i]['lat'], arrego_sa_bc[i]['log']);
        var tipo = arrego_sa_bc[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_sa_bc[i]['nombre']
        });

        O_arrego_sa_bc.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_sa_bc[i]['id'],function(){
                    $("#id-bc-sagro").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SA_bc(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_arrego_sa_bc.length; i++) {
        O_arrego_sa_bc[i].setMap(map);
    }
}
function ocultar_SA_bc() {
    SA_bc(null);
}

/*.-----------------------------------*/
function show_SA_cv() {
    for (i = 0; i < arrego_sa_cv.length; i++) {
        var position = new google.maps.LatLng(arrego_sa_cv[i]['lat'], arrego_sa_cv[i]['log']);
        var tipo = arrego_sa_bc[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_sa_cv[i]['nombre']
        });

        O_arrego_sa_bc.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_sa_cv[i]['id'],function(){
                    $("#id-cv-sagro").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SA_cv(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_arrego_sa_bc.length; i++) {
        O_arrego_sa_bc[i].setMap(map);
    }
}
function ocultar_SA_cv() {
    SA_cv(null);
}


/*.-----------------------------------*/
function show_SA_brv() {
    for (i = 0; i < arrego_sa_brv.length; i++) {
        var position = new google.maps.LatLng(arrego_sa_brv[i]['lat'], arrego_sa_brv[i]['log']);
        var tipo = arrego_sa_brv[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(161, 35, 175, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_sa_brv[i]['nombre']
        });

        O_arrego_sa_brv.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_sa_brv[i]['id'],function(){
                    $("#id--sagro").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
function SA_brv(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_arrego_sa_brv.length; i++) {
        O_arrego_sa_brv[i].setMap(map);
    }
}
function ocultar_SA_brv() {
    SA_brv(null);
}

/*.-----------------------------------*/
function show_SA_otro() {
    for (i = 0; i < arrego_sa_otro.length; i++) {
        var position = new google.maps.LatLng(arrego_sa_otro[i]['lat'], arrego_sa_otro[i]['log']);
        var tipo = arrego_sa_otro[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(161, 35, 175, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_sa_otro[i]['nombre']
        });

        O_arrego_sa_otro.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_sa_otro[i]['id'],function(){
                    $("#id-otro-sagro").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
function SA_otro(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_arrego_sa_otro.length; i++) {
        O_arrego_sa_otro[i].setMap(map);
    }
}
function ocultar_SA_otro() {
    SA_otro(null);
}


/*.-----------------------------------*/

function categoriaSA() {
    $('#menu-SistemaAgroforestal').show("swing");
}

function mostrarCapaSistemaAgroforestales() {
    if (arrego_sa_adcs){
        show_SA_adcs();
    }

    if (arrego_sa_sa){
        show_SA_sa();
    }

    if (arrego_sa_bc){
        show_SA_bc();
    }

    if (arrego_sa_cv){
        show_SA_cv();
    }

    if (arrego_sa_brv){
        show_SA_brv();
    }

    if (arrego_sa_otro){
        show_SA_otro();
    }

}

function ocultarCapaSistemaAgroforestales() {
    $('#menu-SistemaAgroforestal').hide("linear");
    ocultar_SA_adcs();
    ocultar_SA_sa();
    ocultar_SA_bc();
    ocultar_SA_cv();
    ocultar_SA_brv();
    ocultar_SA_otro();

}



function mostrarsistemaAgroforestales() {
    $("#sistemasAgroforestales").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaAgricultura();
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas();
            ocultarCapaDH();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaSistemaProduccionAgricola()
            ocultarCapaEconomia();
            ocultarCapaDLocal();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            mostrarCapaSistemaAgroforestales();
            ocultarCapaSistemaProduccionAnimal();
            categoriaSA();

        }else{
            ocultarCapaSistemaAgroforestales();
        }
    });

}