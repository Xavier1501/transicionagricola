

/*.-----------------------------------*/

function showMRH_lluvias() {
    for (i = 0; i < arrego_MRH_lluvias.length; i++) {
        var position = new google.maps.LatLng(arrego_MRH_lluvias[i]['lat'], arrego_MRH_lluvias[i]['log']);
        var tipo = arrego_MRH_lluvias[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MRH_lluvias[i]['nombre']
        });

        O_arr_MRH_lluvias.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MRH_lluvias[i]['id'],function(){
                    $("#id-ca-MRH").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function MRH_lluvias(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < O_arr_MRH_lluvias.length; i++) {
        O_arr_MRH_lluvias[i].setMap(map);
    }
}

function ocultarMRH_lluvias() {
    MRH_lluvias(null);
}


/*.-----------------------------------*/

function showMRH_micro() {
    for (i = 0; i < arrego_MRH_microcuenta.length; i++) {
        var position = new google.maps.LatLng(arrego_MRH_microcuenta[i]['lat'], arrego_MRH_microcuenta[i]['log']);
        var tipo = arrego_MRH_microcuenta[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MRH_microcuenta[i]['nombre']
        });

        O_arr_MRH_microcuenta.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MRH_microcuenta[i]['id'],function(){
                    $("#id-en-MRH").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function MRH_micro(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < O_arr_MRH_microcuenta.length; i++) {
        O_arr_MRH_microcuenta[i].setMap(map);
    }
}

function ocultarMRH_micro() {
    MRH_micro(null);
}



/*.-----------------------------------*/

function showMRH_tecno() {
    for (i = 0; i < arrego_MRH_tecnologias.length; i++) {
        var position = new google.maps.LatLng(arrego_MRH_tecnologias[i]['lat'], arrego_MRH_tecnologias[i]['log']);
        var tipo = arrego_MRH_tecnologias[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MRH_tecnologias[i]['nombre']
        });

        O_arr_MRH_tecnologias.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MRH_tecnologias[i]['id'],function(){
                    $("#id-m-MRH").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function MRH_tecno(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < O_arr_MRH_tecnologias.length; i++) {
        O_arr_MRH_tecnologias[i].setMap(map);
    }
}

function ocultarMRH_tecno() {
    MRH_tecno(null);
}


/*.-----------------------------------*/

function showMRH_agua() {
    for (i = 0; i < arrego_MRH_agua.length; i++) {
        var position = new google.maps.LatLng(arrego_MRH_agua[i]['lat'], arrego_MRH_agua[i]['log']);
        var tipo = arrego_MRH_agua[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MRH_agua[i]['nombre']
        });

        O_arr_MRH_agua.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MRH_agua[i]['id'],function(){
                    $("#id-ta-MRH").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function MRH_agua(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < O_arr_MRH_agua.length; i++) {
        O_arr_MRH_agua[i].setMap(map);
    }
}

function ocultarMRH_agua() {
    MRH_agua(null);
}

/*.-----------------------------------*/

function showMRH_agua() {
    for (i = 0; i < arrego_MRH_agua.length; i++) {
        var position = new google.maps.LatLng(arrego_MRH_agua[i]['lat'], arrego_MRH_agua[i]['log']);
        var tipo = arrego_MRH_agua[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MRH_agua[i]['nombre']
        });

        O_arr_MRH_agua.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MRH_agua[i]['id'],function(){
                    $("#id-ta-MRH").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function MRH_agua(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < O_arr_MRH_agua.length; i++) {
        O_arr_MRH_agua[i].setMap(map);
    }
}

function ocultarMRH_agua() {
    MRH_agua(null);
}


/*.-----------------------------------*/

function showMRH_riesgo() {
    for (i = 0; i < arrego_MRH_riesgo.length; i++) {
        var position = new google.maps.LatLng(arrego_MRH_riesgo[i]['lat'], arrego_MRH_riesgo[i]['log']);
        var tipo = arrego_MRH_riesgo[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(161, 35, 175, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MRH_riesgo[i]['nombre']
        });

        O_arr_MRH_riesgo.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MRH_riesgo[i]['id'],function(){
                    $("#id-rd-MRH").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function MRH_riesgo(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < O_arr_MRH_riesgo.length; i++) {
        O_arr_MRH_riesgo[i].setMap(map);
    }
}

function ocultarMRH_riesgo() {
    MRH_riesgo(null);
}



/*.-----------------------------------*/

function showMRH_otro() {
    for (i = 0; i < arrego_MRH_otro.length; i++) {
        var position = new google.maps.LatLng(arrego_MRH_otro[i]['lat'], arrego_MRH_otro[i]['log']);
        var tipo = arrego_MRH_otro[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MRH_otro[i]['nombre']
        });

        O_arr_MRH_tratamiento.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MRH_otro[i]['id'],function(){
                    $("#id-ot-MRH").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function MRH_otro(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < O_arr_MRH_otro.length; i++) {
        O_arr_MRH_otro[i].setMap(map);
    }
}

function ocultarMRH_otro() {
    MRH_otro(null);
}

/*.-----------------------------------*/

function showMRH_tratamiento() {
    for (i = 0; i < arrego_MRH_tratamiento.length; i++) {
        var position = new google.maps.LatLng(arrego_MRH_tratamiento[i]['lat'], arrego_MRH_tratamiento[i]['log']);
        var tipo = arrego_MRH_tratamiento[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(250, 227, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MRH_tratamiento[i]['nombre']
        });

        O_arr_MRH_tratamiento.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MRH_tratamiento[i]['id'],function(){
                    $("#id-ta-MRH").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function MRH_tratamiento(map) {
    //console.log(ocultarSaludOtro);
    for (let i = 0; i < O_arr_MRH_tratamiento.length; i++) {
        O_arr_MRH_tratamiento[i].setMap(map);
    }
}

function ocultarMRH_tratamiento() {
    MRH_tratamiento(null);
}

/*.-----------------------------------*/


function categoriaMRH() {
    $('#menu-MRH').show("swing");
}

function mostrarCapaMRH() {
    showMRH_lluvias();
    showMRH_micro();
    showMRH_tecno();
    showMRH_agua();
    showMRH_riesgo();
    showMRH_tratamiento();
    showMRH_otro();
}


function ocultarCapaMRH() {
    $('#menu-MRH').hide("linear");
    ocultarMRH_lluvias();
    ocultarMRH_micro();
    ocultarMRH_tecno();
    ocultarMRH_agua();
    ocultarMRH_riesgo();
    ocultarMRH_tratamiento();
    ocultarMRH_otro();


}





function mostrarMRH() {
    $("#manejoRecursosHibridos").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaAgricultura();
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas()
            ocultarCapaDLocal();
            ocultarDHdh();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaEconomia();
            ocultarCapaSistemaProduccionAgricola();
            ocultarCapaEducacion();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            mostrarCapaMRH();
            categoriaMRH();
        }else{
            ocultarCapaMRH();
        }
    });

}