/*--------------------------Derechos humanos-------------------------*/

function showoDH_dh() {
    for (i = 0; i < arrego_DH_dh.length; i++) {
        var position = new google.maps.LatLng(arrego_DH_dh[i]['lat'], arrego_DH_dh[i]['log']);
        var tipo = arrego_DH_dh[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_DH_dh[i]['nombre']
        });

        O_arr_dh_dh.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_DH_dh[i]['id'],function(){
                    $("#id-dh-dh").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function DHdh(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_dh_dh.length; i++) {
        O_arr_dh_dh[i].setMap(map);

    }
}
function ocultarDHdh() {
    DHdh(null);
}


/*--------------------------Derechos ambientales-------------------------*/

function showoDH_da() {
    for (i = 0; i < arrego_DH_da.length; i++) {
        var position = new google.maps.LatLng(arrego_DH_da[i]['lat'], arrego_DH_da[i]['log']);
        var tipo = arrego_DH_da[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_DH_da[i]['nombre']
        });

        O_arr_dh_da.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_DH_da[i]['id'],function(){
                    $("#id-da-dh").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function DHda(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_dh_da.length; i++) {
        O_arr_dh_da[i].setMap(map);

    }
}
function ocultarDHda() {
    DHda(null);
}




/*--------------------------Derechos economicos-------------------------*/

function showoDH_de() {
    for (i = 0; i < arrego_DH_de.length; i++) {
        var position = new google.maps.LatLng(arrego_DH_de[i]['lat'], arrego_DH_de[i]['log']);
        var tipo = arrego_DH_de[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_DH_de[i]['nombre']
        });

        O_arr_dh_de.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_DH_de[i]['id'],function(){
                    $("#id-de-dh").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function DHde(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_dh_de.length; i++) {
        O_arr_dh_de[i].setMap(map);

    }
}
function ocultarDHde() {
    DHde(null);
}





/*.-----------------------------------*/
function categoriaDH() {
    $('#menu-DH').show("swing");
}


function mostrarCapaDH() {
    if (arrego_DH_dh){
        showoDH_dh();
    }

    if (arrego_DH_da){
        showoDH_da();
    }

    if (arrego_DH_de){
        showoDH_de();
    }
}



function ocultarCapaDH() {
    $('#menu-DH').hide("linear");
    ocultarDHdh()
    ocultarDHda();
    ocultarDHde();


}





function mostrarDH() {
    $("#derechosHumanos").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaAgricultura();
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas()
            ocultarCapaDLocal();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaSem();
            ocultarCapaEconomia();
            ocultarCapaSistemaProduccionAgricola();
            ocultarCapaEducacion();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            mostrarCapaDH();
            categoriaDH();
        }else{
            ocultarCapaDH();
        }
    });

}