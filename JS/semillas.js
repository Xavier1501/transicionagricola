

/*.-----------------------------------*/
function showoSemAlmCon() {
    for (i = 0; i < arrego_sem.length; i++) {
        var position = new google.maps.LatLng(arrego_sem[i]['lat'], arrego_sem[i]['log']);
        var tipo = arrego_sem[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_sem[i]['nombre']
        });

        O_arrego_sem.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_sem[i]['id'],function(){
                    $("#id-ac-s").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SemillasSemAlmCon(map) {
    console.log(O_arrego_sem);
    for (let i = 0; i < O_arrego_sem.length; i++) {
        O_arrego_sem[i].setMap(map);

    }
}
function ocultarSemAlmCon() {
    SemillasSemAlmCon(null);
}

/*.-----------------------------------*/
function showoSemBan() {
    for (i = 0; i < arrego_ban.length; i++) {
        var position = new google.maps.LatLng(arrego_ban[i]['lat'], arrego_ban[i]['log']);
        var tipo = arrego_ban[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_ban[i]['nombre']
        });

        O_arrego_ban.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_ban[i]['id'],function(){
                    $("#id-bc-s").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SemillasSemBam(map) {
    console.log(O_arrego_ban);
    for (let i = 0; i < O_arrego_ban.length; i++) {
        O_arrego_ban[i].setMap(map);

    }
}
function ocultarSemBam() {
    SemillasSemBam(null);
}


/*.-----------------------------------*/
function showoSemInte() {
    for (i = 0; i < arrego_inte.length; i++) {
        var position = new google.maps.LatLng(arrego_inte[i]['lat'], arrego_inte[i]['log']);
        var tipo = arrego_inte[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_inte[i]['nombre']
        });

        O_arrego_inte.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_inte[i]['id'],function(){
                    $("#id-int-s").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SemillasSemInte(map) {
    console.log(O_arrego_inte);
    for (let i = 0; i < O_arrego_inte.length; i++) {
        O_arrego_inte[i].setMap(map);

    }
}
function ocultarSemInte() {
    SemillasSemInte(null);
}


/*.-----------------------------------*/
function showoSemAnimal() {
    for (i = 0; i < arrego_mpAnimal.length; i++) {
        var position = new google.maps.LatLng(arrego_mpAnimal[i]['lat'], arrego_mpAnimal[i]['log']);
        var tipo = arrego_mpAnimal[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_mpAnimal[i]['nombre']
        });

        O_arrego_mpAnimal.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_mpAnimal[i]['id'],function(){
                    $("#id-mpa-s").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SemillasSemAnimal(map) {
    console.log(O_arrego_mpAnimal);
    for (let i = 0; i < O_arrego_mpAnimal.length; i++) {
        O_arrego_mpAnimal[i].setMap(map);

    }
}
function ocultarSemAnimal() {
    SemillasSemAnimal(null);
}


/*.-----------------------------------*/
function showoSemAnimal() {
    for (i = 0; i < arrego_mpAnimal.length; i++) {
        var position = new google.maps.LatLng(arrego_mpAnimal[i]['lat'], arrego_mpAnimal[i]['log']);
        var tipo = arrego_mpAnimal[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_mpAnimal[i]['nombre']
        });

        O_arrego_mpAnimal.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_mpAnimal[i]['id'],function(){
                    $("#id-mpa-s").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SemillasSemAnimal(map) {
    console.log(O_arrego_mpAnimal);
    for (let i = 0; i < O_arrego_mpAnimal.length; i++) {
        O_arrego_mpAnimal[i].setMap(map);

    }
}
function ocultarSemAnimal() {
    SemillasSemAnimal(null);
}

/*.-----------------------------------*/
function showoSemPro() {
    for (i = 0; i < arrego_proSem.length; i++) {
        var position = new google.maps.LatLng(arrego_proSem[i]['lat'], arrego_proSem[i]['log']);
        var tipo = arrego_proSem[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(161, 35, 175, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_proSem[i]['nombre']
        });

        O_arrego_proSem.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_proSem[i]['id'],function(){
                    $("#id-psnc-s").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SemillasSemPro(map) {
    console.log(O_arrego_proSem);
    for (let i = 0; i < O_arrego_proSem.length; i++) {
        O_arrego_proSem[i].setMap(map);

    }
}
function ocultarSemPro() {
    SemillasSemPro(null);
}

/*.-----------------------------------*/
function showoSemO() {
    for (i = 0; i < arrego_otroSem.length; i++) {
        var position = new google.maps.LatLng(arrego_otroSem[i]['lat'], arrego_otroSem[i]['log']);
        var tipo = arrego_otroSem[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_otroSem[i]['nombre']
        });

        O_arrego_otroSem.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_otroSem[i]['id'],function(){
                    $("#id-otro-s").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SemillasSemO(map) {
    console.log(O_arrego_otroSem);
    for (let i = 0; i < O_arrego_otroSem.length; i++) {
        O_arrego_otroSem[i].setMap(map);

    }
}
function ocultarSemO() {
    SemillasSemO(null);
}








/*.-----------------Funciones------------------*/

function CategoriaSemillas() {
    $('#menu-semillas').show("swing");
}

function mostrarCapaSem() {


    if (arrego_sem){
        showoSemAlmCon();
    }

    if (arrego_ban){
        showoSemBan();
    }

    if (arrego_inte){
        showoSemInte();
    }

    if (arrego_mpAnimal){
        showoSemAnimal();
    }

    if (arrego_proSem){
        showoSemPro();
    }

    if (arrego_otroSem){
        showoSemO();
    }


}

function ocultarCapaSem() {
    $('#menu-semillas').hide("linear");
    ocultarSemAlmCon()
    ocultarSemBam();
    ocultarSemInte();
    ocultarSemAnimal();
    ocultarSemPro();
    ocultarSemO();

}
function mostrarSemillas() {
    $("#semillas").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaAgricultura();
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas()
            ocultarCapaDLocal();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaEconomia();
            ocultarCapaSistemaProduccionAgricola();
            ocultarCapaEducacion();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            mostrarCapaSem();
            CategoriaSemillas();
        }else{
            ocultarCapaSem();
        }
    });

}