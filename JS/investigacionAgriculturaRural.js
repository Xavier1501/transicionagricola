
/*--------------------------comunicacion-------------------------*/

function showoIAR_c() {
    for (i = 0; i < arrego_IAER_c.length; i++) {
        var position = new google.maps.LatLng(arrego_IAER_c[i]['lat'], arrego_IAER_c[i]['log']);
        var tipo = arrego_IAER_c[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_IAER_c[i]['nombre']
        });

        O_arr_IAER_c.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_IAER_c[i]['id'],function(){
                    $("#id-c-IAR").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function IAR_c(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_IAER_c.length; i++) {
        O_arr_IAER_c[i].setMap(map);

    }
}
function ocultarIAR_C() {
    IAR_c(null);
}

/*--------------------------Diagnóstico de agroecosistemas-------------------------*/

function showoIAR_Da() {
    for (i = 0; i < arrego_IAER_d.length; i++) {
        var position = new google.maps.LatLng(arrego_IAER_d[i]['lat'], arrego_IAER_d[i]['log']);
        var tipo = arrego_IAER_d[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_IAER_d[i]['nombre']
        });

        O_arr_IAER_d.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_IAER_d[i]['id'],function(){
                    $("#id-d-IAR").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa medicina
function IAR_d(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_IAER_d.length; i++) {
        O_arr_IAER_d[i].setMap(map);

    }
}
function ocultarIAR_Da() {
    IAR_d(null);
}



/*--------------------------experiemntacion-------------------------*/

function showoIAR_Exp() {
    for (i = 0; i < arrego_IAER_e.length; i++) {
        var position = new google.maps.LatLng(arrego_IAER_e[i]['lat'], arrego_IAER_e[i]['log']);
        var tipo = arrego_IAER_e[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_IAER_e[i]['nombre']
        });

        O_arr_IAER_e.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_IAER_e[i]['id'],function(){
                    $("#id-e-IAR").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa medicina
function IAR_exp(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_IAER_e.length; i++) {
        O_arr_IAER_e[i].setMap(map);

    }
}
function ocultarIAR_Exp() {
    IAR_exp(null);
}

/*--------------------------Formacion y capacitacion-------------------------*/

function showoIAR_fc() {
    for (i = 0; i < arrego_IAER_fc.length; i++) {
        var position = new google.maps.LatLng(arrego_IAER_fc[i]['lat'], arrego_IAER_fc[i]['log']);
        var tipo = arrego_IAER_fc[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_IAER_fc[i]['nombre']
        });

        O_arr_IAER_fc.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_IAER_fc[i]['id'],function(){
                    $("#id-f-IAR").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa medicina
function IAR_fc(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_IAER_fc.length; i++) {
        O_arr_IAER_fc[i].setMap(map);

    }
}
function ocultarIAR_fc() {
    IAR_fc(null);
}


/*--------------------------Investigacion-------------------------*/

function showoIAR_Inv() {
    for (i = 0; i < arrego_IAER_in.length; i++) {
        var position = new google.maps.LatLng(arrego_IAER_in[i]['lat'], arrego_IAER_in[i]['log']);
        var tipo = arrego_IAER_in[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(161, 35, 175, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_IAER_in[i]['nombre']
        });

        O_arr_IAER_in.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_IAER_in[i]['id'],function(){
                    $("#id-in-IAR").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa medicina
function IAR_Inv(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_IAER_in.length; i++) {
        O_arr_IAER_in[i].setMap(map);

    }
}
function ocultarIAR_Inv() {
    IAR_Inv(null);
}


/*--------------------------Ordenamientos territoriales-------------------------*/
function showoIAR_Ot() {
    for (i = 0; i < arrego_IAER_orn.length; i++) {
        var position = new google.maps.LatLng(arrego_IAER_orn[i]['lat'], arrego_IAER_orn[i]['log']);
        var tipo = arrego_IAER_orn[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(250, 227, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_IAER_orn[i]['nombre']
        });

        O_arr_IAER_orn.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_IAER_orn[i]['id'],function(){
                    $("#id-or-IAR").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa medicina
function IAR_Ot(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_IAER_orn.length; i++) {
        O_arr_IAER_orn[i].setMap(map);

    }
}
function ocultarIAR_Ot() {
    IAR_Ot(null);
}


/*--------------------------Otro-------------------------*/

function showoIAR_Otro() {
    for (i = 0; i < arrego_IAER_otro.length; i++) {
        var position = new google.maps.LatLng(arrego_IAER_otro[i]['lat'], arrego_IAER_otro[i]['log']);
        var tipo = arrego_IAER_otro[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_IAER_otro[i]['nombre']
        });

        O_arr_IAER_otro.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_IAER_otro[i]['id'],function(){
                    $("#id-o-IAR").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function IAR_Otro(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_IAER_otro.length; i++) {
        O_arr_IAER_otro[i].setMap(map);

    }
}
function ocultarIAR_Otro() {
    IAR_Otro(null);
}

/*.-----------------------------------*/
function categoriaIAR() {
    $('#menu-IAR').show("swing");
}

function mostrarCapaIAR() {
    if (arrego_IAER_c){
        showoIAR_c();
    }
    if (arrego_IAER_d){
        showoIAR_Da();
    }

    if (arrego_IAER_e){
        showoIAR_Exp();
    }
    if (arrego_IAER_fc){
        showoIAR_fc();
    }
    if (arrego_IAER_in){
        showoIAR_Inv();
    }

    if (arrego_IAER_orn){
        showoIAR_Ot();
    }

    if (arrego_IAER_otro){
        showoIAR_Otro();
    }
}

function ocultarCapaIAR() {
    $('#menu-IAR').hide("linear");
    ocultarIAR_C();
    ocultarIAR_Da();
    ocultarIAR_Exp();
    ocultarIAR_fc();
    ocultarIAR_Inv();
    ocultarIAR_Ot();
    ocultarIAR_Otro();

}

function mostrarIAR() {
    $("#invAgricolaExtension").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaAgricultura();
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas()
            ocultarCapaDLocal();
            ocultarDHdh();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaEconomia();
            ocultarCapaSistemaProduccionAgricola();
            ocultarCapaEducacion();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            mostrarCapaIAR();
            categoriaIAR();
        }else{
            ocultarCapaIAR();
        }
    });

}