/*.-----------------------------------*/
function showSPA_Per() {
    for (i = 0; i < arregloSistemasProdPermacultura.length; i++) {
        var position = new google.maps.LatLng(arregloSistemasProdPermacultura[i]['lat'], arregloSistemasProdPermacultura[i]['log']);
        var tipo = arregloSistemasProdPermacultura[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arregloSistemasProdPermacultura[i]['nombre']
        });

        O_arregloSistemasProdPermacultura.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arregloSistemasProdPermacultura[i]['id'],function(){
                    $("#id-p-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_P(map) {
    console.log(O_arregloSistemasProdPermacultura);
    for (let i = 0; i < O_arregloSistemasProdPermacultura.length; i++) {
        O_arregloSistemasProdPermacultura[i].setMap(map);

    }
}
function ocultarSPA_p() {
    SPA_P(null);
}


/*.-----------------------------------*/
function showSPA_Ab() {
    for (i = 0; i < arreglo_spa_Abio.length; i++) {
        var position = new google.maps.LatLng(arreglo_spa_Abio[i]['lat'], arreglo_spa_Abio[i]['log']);
        var tipo = arreglo_spa_Abio[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_spa_Abio[i]['nombre']
        });

        O_arreglo_spa_Abio.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_spa_Abio[i]['id'],function(){
                    $("#id-ab-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);arreglo_spa_Abio
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Ab(map) {
    console.log(O_arreglo_spa_Abio);
    for (let i = 0; i < O_arreglo_spa_Abio.length; i++) {
        O_arreglo_spa_Abio[i].setMap(map);

    }
}
function ocultarSPA_Ab() {
    SPA_Ab(null);
}

/*.-----------------------------------*/
function showSPA_AN() {
    for (i = 0; i < arreglo_spa_AN.length; i++) {
        var position = new google.maps.LatLng(arreglo_spa_AN[i]['lat'], arreglo_spa_AN[i]['log']);
        var tipo = arreglo_spa_AN[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(145, 150, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_spa_AN[i]['nombre']
        });

        O_arreglo_spa_AN.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_spa_AN[i]['id'],function(){
                    $("#id-nat-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);arreglo_spa_Abio
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_AN(map) {
    console.log(O_arreglo_spa_AN);
    for (let i = 0; i < O_arreglo_spa_AN.length; i++) {
        O_arreglo_spa_AN[i].setMap(map);

    }
}
function ocultarSPA_AN() {
    SPA_AN(null);
}

/*.-----------------------------------*/
function showSPA_ao() {
    for (i = 0; i < arreglo_spa_AO.length; i++) {
        var position = new google.maps.LatLng(arreglo_spa_AO[i]['lat'], arreglo_spa_AO[i]['log']);
        var tipo = arreglo_spa_AO[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_spa_AO[i]['nombre']
        });

        O_arreglo_spa_AO.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_spa_AO[i]['id'],function(){
                    $("#id-nat-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);arreglo_spa_Abio
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_ao(map) {
    console.log(O_arreglo_spa_AO);
    for (let i = 0; i < O_arreglo_spa_AO.length; i++) {
        O_arreglo_spa_AO[i].setMap(map);

    }
}
function ocultarSPA_ao() {
    SPA_ao(null);
}

/*.-----------------------------------*/
function showSPA_Agro() {
    for (i = 0; i < arreglo_spa_Ag.length; i++) {
        var position = new google.maps.LatLng(arreglo_spa_Ag[i]['lat'], arreglo_spa_Ag[i]['log']);
        var tipo = arreglo_spa_Ag[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_spa_Ag[i]['nombre']
        });

        O_arreglo_spa_Ag.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_spa_Ag[i]['id'],function(){
                    $("#id-agro-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);arreglo_spa_Abio
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Agro(map) {
    console.log(O_arreglo_spa_Ag);
    for (let i = 0; i < O_arreglo_spa_Ag.length; i++) {
        O_arreglo_spa_Ag[i].setMap(map);

    }
}
function ocultarSPA_Agro() {
    SPA_Agro(null);
}

/*.-----------------------------------*/
function showSPA_Agro() {
    for (i = 0; i < arreglo_spa_Ag.length; i++) {
        var position = new google.maps.LatLng(arreglo_spa_Ag[i]['lat'], arreglo_spa_Ag[i]['log']);
        var tipo = arreglo_spa_Ag[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_spa_Ag[i]['nombre']
        });

        O_arreglo_spa_Ag.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_spa_Ag[i]['id'],function(){
                    $("#id-agro-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);arreglo_spa_Abio
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Agro(map) {
    console.log(O_arreglo_spa_Ag);
    for (let i = 0; i < O_arreglo_spa_Ag.length; i++) {
        O_arreglo_spa_Ag[i].setMap(map);

    }
}
function ocultarSPA_Agro() {
    SPA_Agro(null);
}


/*.-----------------------------------*/
function showSPA_tsi() {
    for (i = 0; i < arreglo_tsi.length; i++) {
        var position = new google.maps.LatLng(arreglo_tsi[i]['lat'], arreglo_tsi[i]['log']);
        var tipo = arreglo_tsi[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(161, 35, 175, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_tsi[i]['nombre']
        });

        O_arreglo_tsi.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_tsi[i]['id'],function(){
                    $("#id-tsi-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);arreglo_spa_Abio
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_tsi(map) {
    console.log(O_arreglo_tsi);
    for (let i = 0; i < O_arreglo_tsi.length; i++) {
        O_arreglo_tsi[i].setMap(map);

    }
}
function ocultarSPA_tsi() {
    SPA_tsi(null);
}

/*.-----------------------------------*/
function showSPA_stq() {
    for (i = 0; i < arreglo_stq.length; i++) {
        var position = new google.maps.LatLng(arreglo_stq[i]['lat'], arreglo_stq[i]['log']);
        var tipo = arreglo_stq[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(161, 35, 175, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_stq[i]['nombre']
        });

        O_arreglo_stq.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_stq[i]['id'],function(){
                    $("#id-sa-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);arreglo_spa_Abio
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_tsq(map) {
    console.log(O_arreglo_stq);
    for (let i = 0; i < O_arreglo_stq.length; i++) {
        O_arreglo_stq[i].setMap(map);

    }
}
function ocultarSPA_tsq() {
    SPA_tsq(null);
}


/*.-----------------------------------*/
function showSPA_mbio() {
    for (i = 0; i < arreglo_mbio.length; i++) {
        var position = new google.maps.LatLng(arreglo_mbio[i]['lat'], arreglo_mbio[i]['log']);
        var tipo = arreglo_mbio[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(250, 227, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_mbio[i]['nombre']
        });

        O_arreglo_mbio.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_mbio[i]['id'],function(){
                    $("#id-Mb-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);arreglo_spa_Abio
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_mbio(map) {
    console.log(O_arreglo_mbio);
    for (let i = 0; i < O_arreglo_mbio.length; i++) {
        O_arreglo_mbio[i].setMap(map);

    }
}
function ocultarSPA_mbio() {
    SPA_mbio(null);
}


/*.-----------------------------------*/
function showSPA_mint() {
    for (i = 0; i < arreglo_mint.length; i++) {
        var position = new google.maps.LatLng(arreglo_mint[i]['lat'], arreglo_mint[i]['log']);
        var tipo = arreglo_mint[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(13, 185, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_mint[i]['nombre']
        });

        O_arreglo_mint.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_mint[i]['id'],function(){
                    $("#id-Mi-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);arreglo_spa_Abio
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_mint(map) {
    console.log(O_arreglo_mint);
    for (let i = 0; i < O_arreglo_mint.length; i++) {
        O_arreglo_mint[i].setMap(map);

    }
}
function ocultarSPA_mint() {
    SPA_mint(null);
}


/*.-----------------------------------*/
function showSPA_ave() {
    for (i = 0; i < arreglo_ave.length; i++) {
        var position = new google.maps.LatLng(arreglo_ave[i]['lat'], arreglo_ave[i]['log']);
        var tipo = arreglo_ave[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(13, 185, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_ave[i]['nombre']
        });

        O_arreglo_ave.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_ave[i]['id'],function(){
                    $("#id-abver-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);arreglo_spa_Abio
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_ave(map) {
    console.log(O_arreglo_ave);
    for (let i = 0; i < O_arreglo_ave.length; i++) {
        O_arreglo_ave[i].setMap(map);

    }
}
function ocultarSPA_ave() {
    SPA_ave(null);
}

/*.-----------------------------------*/
function showSPA_bd() {
    for (i = 0; i < arreglo_bDes.length; i++) {
        var position = new google.maps.LatLng(arreglo_bDes[i]['lat'], arreglo_bDes[i]['log']);
        var tipo = arreglo_bDes[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(24, 194, 208, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_bDes[i]['nombre']
        });

        O_arreglo_bDes.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_bDes[i]['id'],function(){
                    $("#id-bade-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);arreglo_spa_Abio
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_db(map) {
    console.log(O_arreglo_bDes);
    for (let i = 0; i < O_arreglo_bDes.length; i++) {
        O_arreglo_bDes[i].setMap(map);

    }
}
function ocultarSPA_db() {
    SPA_db(null);
}


/*.-----------------------------------*/
function showSPA_roca() {
    for (i = 0; i < arreglo_hroca.length; i++) {
        var position = new google.maps.LatLng(arreglo_hroca[i]['lat'], arreglo_hroca[i]['log']);
        var tipo = arreglo_hroca[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(137, 79, 53, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_hroca[i]['nombre']
        });

        O_arreglo_hroca.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_hroca[i]['id'],function(){
                    $("#id-uhr-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_roca(map) {
    console.log(O_arreglo_hroca);
    for (let i = 0; i < O_arreglo_hroca.length; i++) {
        O_arreglo_hroca[i].setMap(map);

    }
}
function ocultarSPA_roca() {
    SPA_roca(null);
}

/*.-----------------------------------*/
function showSPA_cober() {
    for (i = 0; i < arreglo_comp.length; i++) {
        var position = new google.maps.LatLng(arreglo_comp[i]['lat'], arreglo_comp[i]['log']);
        var tipo = arreglo_comp[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 119, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_comp[i]['nombre']
        });

        O_arreglo_comp.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_comp[i]['id'],function(){
                    $("#id-cober-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_cober(map) {
    console.log(O_arreglo_cob);
    for (let i = 0; i < O_arreglo_cob.length; i++) {
        O_arreglo_cob[i].setMap(map);

    }
}
function ocultarSPA_cober() {
    SPA_cober(null);
}


/*.-----------------------------------*/
function showSPA_compo() {
    for (i = 0; i < arreglo_comp.length; i++) {
        var position = new google.maps.LatLng(arreglo_comp[i]['lat'], arreglo_comp[i]['log']);
        var tipo = arreglo_comp[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 119, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_comp[i]['nombre']
        });

        O_arreglo_comp.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_comp[i]['id'],function(){
                    $("#id-comp-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_compo(map) {
    console.log(O_arreglo_comp);
    for (let i = 0; i < O_arreglo_comp.length; i++) {
        O_arreglo_comp[i].setMap(map);

    }
}
function ocultarSPA_compo() {
    SPA_compo(null);
}

/*.-----------------------------------*/
function showSPA_meFer() {
    for (i = 0; i < arreglo_MFer.length; i++) {
        var position = new google.maps.LatLng(arreglo_MFer[i]['lat'], arreglo_MFer[i]['log']);
        var tipo = arreglo_MFer[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 28, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_MFer[i]['nombre']
        });

        O_arreglo_MFer.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_MFer[i]['id'],function(){
                    $("#id-mejFer-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_meFer(map) {
    console.log(O_arreglo_MFer);
    for (let i = 0; i < O_arreglo_MFer.length; i++) {
        O_arreglo_MFer[i].setMap(map);

    }
}
function ocultar_meFer() {
    SPA_meFer(null);
}

/*.-----------------------------------*/
function showSPA_PrBio() {
    for (i = 0; i < arreglo_PrBio.length; i++) {
        var position = new google.maps.LatLng(arreglo_PrBio[i]['lat'], arreglo_PrBio[i]['log']);
        var tipo = arreglo_PrBio[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 28, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_PrBio[i]['nombre']
        });

        O_arreglo_PrBio.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_PrBio[i]['id'],function(){
                    $("#id-prodBio-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_prBio(map) {
    console.log(O_arreglo_PrBio);
    for (let i = 0; i < O_arreglo_PrBio.length; i++) {
        O_arreglo_PrBio[i].setMap(map);

    }
}
function ocultar_prBio() {
    SPA_prBio(null);
}

/*.-----------------------------------*/
function showSPA_PAS() {
    for (i = 0; i < arreglo_PAS.length; i++) {
        var position = new google.maps.LatLng(arreglo_PAS[i]['lat'], arreglo_PAS[i]['log']);
        var tipo = arreglo_PAS[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(216, 252, 255, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_PAS[i]['nombre']
        });

        O_arreglo_PAS.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_PAS[i]['id'],function(){
                    $("#id-pasol-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_PAS(map) {
    console.log(O_arreglo_PAS);
    for (let i = 0; i < O_arreglo_PAS.length; i++) {
        O_arreglo_PAS[i].setMap(map);

    }
}
function ocultar_PAs() {
    SPA_PAS(null);
}

/*.-----------------------------------*/
function showSPA_Pcsuelos() {
    for (i = 0; i < arreglo_PcSue.length; i++) {
        var position = new google.maps.LatLng(arreglo_PcSue[i]['lat'], arreglo_PcSue[i]['log']);
        var tipo = arreglo_PcSue[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(216, 222, 255, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_PcSue[i]['nombre']
        });

        O_arreglo_PcSue.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_PcSue[i]['id'],function(){
                    $("#id-pcsuelos-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Pcsuelos(map) {
    console.log(O_arreglo_PcSue);
    for (let i = 0; i < O_arreglo_PcSue.length; i++) {
        O_arreglo_PcSue[i].setMap(map);

    }
}
function ocultar_Pcsuelos() {
    SPA_Pcsuelos(null);
}


/*.-----------------------------------*/
function showSPA_sDCul() {
    for (i = 0; i < arreglo_sDCul.length; i++) {
        var position = new google.maps.LatLng(arreglo_sDCul[i]['lat'], arreglo_sDCul[i]['log']);
        var tipo = arreglo_sDCul[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 255, 97, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_sDCul[i]['nombre']
        });

        O_arreglo_sDCul.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_sDCul[i]['id'],function(){
                    $("#id-sdcul-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_sDCul(map) {
    console.log(O_arreglo_sDCul);
    for (let i = 0; i < O_arreglo_sDCul.length; i++) {
        O_arreglo_sDCul[i].setMap(map);

    }
}
function ocultar_sDCul() {
    SPA_sDCul(null);
}

/*.-----------------------------------*/
function showSPA_sCulAni() {
    for (i = 0; i < arreglo_sCulAni.length; i++) {
        var position = new google.maps.LatLng(arreglo_sCulAni[i]['lat'], arreglo_sCulAni[i]['log']);
        var tipo = arreglo_sCulAni[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 255, 205, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_sCulAni[i]['nombre']
        });

        O_arreglo_sCulAni.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_sCulAni[i]['id'],function(){
                    $("#id-sctanimal-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_sCulAni(map) {
    console.log(O_arreglo_sCulAni);
    for (let i = 0; i < O_arreglo_sCulAni.length; i++) {
        O_arreglo_sCulAni[i].setMap(map);

    }
}
function ocultar_sCulAni() {
    SPA_sCulAni(null);
}

/*.-----------------------------------*/
function showSPA_sDir() {
    for (i = 0; i < arreglo_sDir.length; i++) {
        var position = new google.maps.LatLng(arreglo_sDir[i]['lat'], arreglo_sDir[i]['log']);
        var tipo = arreglo_sDir[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 156, 205, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_sDir[i]['nombre']
        });

        O_arreglo_sDir.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_sDir[i]['id'],function(){
                    $("#id-sccr-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_sDir(map) {
    console.log(O_arreglo_sDir);
    for (let i = 0; i < O_arreglo_sDir.length; i++) {
        O_arreglo_sDir[i].setMap(map);

    }
}
function ocultar_sDir() {
    SPA_sDir(null);
}

/*.-----------------------------------*/
function showSPA_Cereales() {
    for (i = 0; i < arreglo_cereales.length; i++) {
        var position = new google.maps.LatLng(arreglo_cereales[i]['lat'], arreglo_cereales[i]['log']);
        var tipo = arreglo_cereales[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(196, 16, 99, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_cereales[i]['nombre']
        });

        O_arreglo_cereales.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_cereales[i]['id'],function(){
                    $("#id-cereales-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Cereales(map) {
    console.log(O_arreglo_cereales);
    for (let i = 0; i < O_arreglo_cereales.length; i++) {
        O_arreglo_cereales[i].setMap(map);

    }
}
function ocultar_Cereales() {
    SPA_Cereales(null);
}

/*.-----------------------------------*/
function showSPA_cultiAso() {
    for (i = 0; i < arreglo_cutAso.length; i++) {
        var position = new google.maps.LatLng(arreglo_cutAso[i]['lat'], arreglo_cutAso[i]['log']);
        var tipo = arreglo_cutAso[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 205, 252, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_cutAso[i]['nombre']
        });

        O_arreglo_cutAso.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_cutAso[i]['id'],function(){
                    $("#id-caso-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_cultiAso(map) {
    console.log(O_arreglo_cutAso);
    for (let i = 0; i < O_arreglo_cutAso.length; i++) {
        O_arreglo_cutAso[i].setMap(map);

    }
}
function ocultar_cultiAso() {
    SPA_cultiAso(null);
}

/*.-----------------------------------*/
function showSPA_Forraje() {
    for (i = 0; i < arreglo_Forraje.length; i++) {
        var position = new google.maps.LatLng(arreglo_Forraje[i]['lat'], arreglo_Forraje[i]['log']);
        var tipo = arreglo_Forraje[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(177, 205, 252, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_Forraje[i]['nombre']
        });

        O_arreglo_Forraje.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_Forraje[i]['id'],function(){
                    $("#id-forrajes-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Forraje(map) {
    console.log(O_arreglo_Forraje);
    for (let i = 0; i < O_arreglo_Forraje.length; i++) {
        O_arreglo_Forraje[i].setMap(map);

    }
}
function ocultar_Forraje() {
    SPA_Forraje(null);
}

/*.-----------------------------------*/
function showSPA_Frutales() {
    for (i = 0; i < arreglo_Frutales.length; i++) {
        var position = new google.maps.LatLng(arreglo_Frutales[i]['lat'], arreglo_Frutales[i]['log']);
        var tipo = arreglo_Frutales[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(177, 205, 104, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_Frutales[i]['nombre']
        });

        O_arreglo_Frutales.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_Frutales[i]['id'],function(){
                    $("#id-Frutales-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Frutales(map) {
    console.log(O_arreglo_Frutales);
    for (let i = 0; i < O_arreglo_Frutales.length; i++) {
        O_arreglo_Frutales[i].setMap(map);

    }
}
function ocultar_Frutales() {
    SPA_Frutales(null);
}

/*.-----------------------------------*/
function showSPA_Hortaliza() {
    for (i = 0; i < arreglo_Hortaliza.length; i++) {
        var position = new google.maps.LatLng(arreglo_Hortaliza[i]['lat'], arreglo_Hortaliza[i]['log']);
        var tipo = arreglo_Hortaliza[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(251, 218, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_Hortaliza[i]['nombre']
        });

        O_arreglo_Hortaliza.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_Hortaliza[i]['id'],function(){
                    $("#id-Hortalizas-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Hortaliza(map) {
    console.log(O_arreglo_Hortaliza);
    for (let i = 0; i < O_arreglo_Hortaliza.length; i++) {
        O_arreglo_Hortaliza[i].setMap(map);

    }
}
function ocultar_Hortaliza() {
    SPA_Hortaliza(null);
}

/*.-----------------------------------*/
function showSPA_Leguminosa() {
    for (i = 0; i < arreglo_Leguminosa.length; i++) {
        var position = new google.maps.LatLng(arreglo_Leguminosa[i]['lat'], arreglo_Leguminosa[i]['log']);
        var tipo = arreglo_Leguminosa[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(0, 15, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_Leguminosa[i]['nombre']
        });

        O_arreglo_Leguminosa.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_Leguminosa[i]['id'],function(){
                    $("#id-Leguminosas-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Leguminosa(map) {
    console.log(O_arreglo_Leguminosa);
    for (let i = 0; i < O_arreglo_Leguminosa.length; i++) {
        O_arreglo_Leguminosa[i].setMap(map);

    }
}
function ocultar_Leguminosa() {
    SPA_Leguminosa(null);
}

/*.-----------------------------------*/
function showSPA_Medicinales() {
    for (i = 0; i < arreglo_Medicinales.length; i++) {
        var position = new google.maps.LatLng(arreglo_Medicinales[i]['lat'], arreglo_Medicinales[i]['log']);
        var tipo = arreglo_Medicinales[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(0, 157, 255, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_Medicinales[i]['nombre']
        });

        O_arreglo_Medicinales.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_Medicinales[i]['id'],function(){
                    $("#id-Medicinales-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Medicinales(map) {
    console.log(O_arreglo_Medicinales);
    for (let i = 0; i < O_arreglo_Medicinales.length; i++) {
        O_arreglo_Medicinales[i].setMap(map);

    }
}
function ocultar_Medicinales() {
    SPA_Medicinales(null);
}

/*.-----------------------------------*/
function showSPA_Oleaginosa() {
    for (i = 0; i < arreglo_Oleaginosa.length; i++) {
        var position = new google.maps.LatLng(arreglo_Oleaginosa[i]['lat'], arreglo_Oleaginosa[i]['log']);
        var tipo = arreglo_Oleaginosa[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 255, 255, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_Oleaginosa[i]['nombre']
        });

        O_arreglo_Oleaginosa.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_Oleaginosa[i]['id'],function(){
                    $("#id-Oleaginosas-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Oleaginosa(map) {
    console.log(O_arreglo_Oleaginosa);
    for (let i = 0; i < O_arreglo_Oleaginosa.length; i++) {
        O_arreglo_Oleaginosa[i].setMap(map);

    }
}
function ocultar_Oleaginosa() {
    SPA_Oleaginosa(null);
}

/*.-----------------------------------*/
function showSPA_Textil() {
    for (i = 0; i < arreglo_Textil.length; i++) {
        var position = new google.maps.LatLng(arreglo_Textil[i]['lat'], arreglo_Textil[i]['log']);
        var tipo = arreglo_Textil[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(36, 255, 255, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_Textil[i]['nombre']
        });

        O_arreglo_Textil.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_Textil[i]['id'],function(){
                    $("#id-Textiles-spa").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Textil(map) {
    console.log(O_arreglo_Textil);
    for (let i = 0; i < O_arreglo_Textil.length; i++) {
        O_arreglo_Textil[i].setMap(map);

    }
}
function ocultar_Textil() {
    SPA_Textil(null);
}

/*.-----------------------------------*/
function showSPA_Otro() {
    for (i = 0; i < arreglo_OtroAgricola.length; i++) {
        var position = new google.maps.LatLng(arreglo_OtroAgricola[i]['lat'], arreglo_OtroAgricola[i]['log']);
        var tipo = arreglo_OtroAgricola[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arreglo_OtroAgricola[i]['nombre']
        });

        O_arreglo_OtroAgricola.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arreglo_OtroAgricola[i]['id'],function(){
                    $("#id-ad").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function SPA_Otro(map) {
    console.log(O_arreglo_OtroAgricola);
    for (let i = 0; i < O_arreglo_OtroAgricola.length; i++) {
        O_arreglo_OtroAgricola[i].setMap(map);

    }
}
function ocultar_Otro() {
    SPA_Otro(null);
}

/*--------------------------------------*/

function CategoriaSPAgri() {
    $('#menu-sistema-produccion-agricola').show("swing");
}

function mostrarCapaSistemaProduccionAgricola() {

   if(arregloSistemasProdPermacultura){
       showSPA_Per();
    }

    if(arreglo_spa_Abio){
       showSPA_Ab();
    }

    if(arreglo_spa_AN){
        showSPA_AN();
    }
    if(arreglo_spa_AO){
        showSPA_ao();
    }

    if(arreglo_spa_Ag){
        showSPA_Agro();
    }

    if(arreglo_tsi){
        showSPA_tsi();
    }

    if(arreglo_stq){
        showSPA_stq();
    }

    if(arreglo_mbio){
       showSPA_mbio();
    }
    if(arreglo_ave){
        showSPA_ave();
    }

    if(arreglo_bDes){
        showSPA_bd();
    }

     if(arreglo_hroca){
        showSPA_roca();
    }

    if(arreglo_hroca){
        showSPA_roca();
    }

    if(arreglo_cob){
        showSPA_cober();
    }

    if(arreglo_comp){
        showSPA_compo();
    }

    if (arreglo_MFer){
        showSPA_meFer();
    }

    if (arreglo_PrBio){
        showSPA_PrBio();
    }

    if(arreglo_PAS){
        showSPA_PAS();
    }



    if(arreglo_PcSue){
        showSPA_Pcsuelos();
    }



    if(arreglo_sDCul){
        showSPA_sCulAni();
    }

    if(arreglo_sDCul){
        showSPA_sDCul();
    }
    if(arreglo_sCulAni){
        showSPA_sCulAni();
    }

    if(arreglo_sDir){
        showSPA_sDir();
    }

    if(arreglo_cereales){
        showSPA_Cereales();
    }

    if(arreglo_cutAso){
        showSPA_cultiAso();
    }

    if(arreglo_Forraje){
        showSPA_Forraje();
    }

     if(arreglo_Frutales){
        showSPA_Frutales();
    }

    if(arreglo_Hortaliza){
        showSPA_Hortaliza();
    }

    if(arreglo_Leguminosa){
        showSPA_Leguminosa();
    }
    if(arreglo_Medicinales){
        showSPA_Medicinales();
    }
    if(arreglo_Oleaginosa){
        showSPA_Oleaginosa();
    }
    if(arreglo_Textil){
        showSPA_Textil();
    }
    if(arreglo_OtroAgricola){
        showSPA_Otro();
    }


}


function ocultarCapaSistemaProduccionAgricola() {
    $('#menu-sistema-produccion-agricola').hide("linear");
    ocultarSPA_p();
    ocultarSPA_Ab();
    ocultarSPA_AN();
    ocultarSPA_ao();
    ocultarSPA_Agro();
    ocultarSPA_tsi()
    ocultarSPA_tsq()
    ocultarSPA_mbio();
    ocultarSPA_ave();
    ocultarSPA_db();
    ocultarSPA_roca();
    ocultarSPA_compo();
    ocultar_meFer();
    ocultar_prBio();
    ocultar_PAs();
    ocultar_Pcsuelos();
    ocultar_sDCul();
    ocultar_sCulAni();
    ocultar_sDir();
    ocultar_Cereales();
    ocultar_cultiAso();
    ocultar_Forraje();
    ocultar_Frutales();
    ocultar_Hortaliza();
    ocultar_Leguminosa();
    ocultar_Medicinales();
    ocultar_Oleaginosa();
    ocultar_Textil();
    ocultar_Otro();


}

function mostrarSistemaProduccionAgricola() {
    $("#sistemaProduccionAgricola").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas();
            ocultarCapaAgricultura();
            ocultarCapaDLocal();
            ocultarCapaDH();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaEconomia();
            ocultarCapaEducacion();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            mostrarCapaSistemaProduccionAgricola();
            ocultarCapaSistemaProduccionAnimal();
            ocultarTodasLasIniciativas();
            CategoriaSPAgri();

        }else{

            ocultarCapaSistemaProduccionAgricola();


        }
    });

}