







/*.-----------------------------------*/
function show_pf_pm() {
    for (i = 0; i < arrego_pf_pm.length; i++) {
        var position = new google.maps.LatLng(arrego_pf_pm[i]['lat'], arrego_pf_pm[i]['log']);
        var tipo = arrego_pf_pm[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_pf_pm[i]['nombre']
        });

        O_arrego_pf_pm.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_pf_pm[i]['id'],function(){
                    $("#id-pfm-pfos").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
function pf_pm(map) {
    for (let i = 0; i < O_arrego_pf_pm.length; i++) {
        O_arrego_pf_pm[i].setMap(map);
    }
}
function ocultar_pf_pm() {
    pf_pm(null);
}

/*.-----------------------------------*/
function show_pf_pnm() {
    for (i = 0; i < arrego_pf_pnm.length; i++) {
        var position = new google.maps.LatLng(arrego_pf_pnm[i]['lat'], arrego_pf_pnm[i]['log']);
        var tipo = arrego_pf_pnm[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_pf_pnm[i]['nombre']
        });

        O_arrego_pf_pnm.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_pf_pnm[i]['id'],function(){
                    $("#id-pfnm-pfos").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
function pf_pnm(map) {
    for (let i = 0; i < O_arrego_pf_pnm.length; i++) {
        O_arrego_pf_pnm[i].setMap(map);
    }
}
function ocultar_pf_pnm() {
    pf_pnm(null);
}

/*.-----------------------------------*/
function show_pf_otro() {
    for (i = 0; i < arrego_pf_otro.length; i++) {
        var position = new google.maps.LatLng(arrego_pf_otro[i]['lat'], arrego_pf_otro[i]['log']);
        var tipo = arrego_pf_otro[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_pf_otro[i]['nombre']
        });

        O_arrego_pf_pnm.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_pf_otro[i]['id'],function(){
                    $("#id-otro-pfos").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
function pf_otro(map) {
    for (let i = 0; i < O_arrego_pf_pnm.length; i++) {
        O_arrego_pf_pnm[i].setMap(map);
    }
}
function ocultar_pf_otro() {
    pf_otro(null);
}


/*.-----------------------------------*/

function CategoriaPF() {
    $('#menu-productosForestales').show("swing");
}

function mostrarCapaProductosForestales() {

    if (arrego_pf_pm){
        show_pf_pm();
    }

    if (arrego_pf_pnm){
        show_pf_pnm();
    }
    if (arrego_pf_otro){
        show_pf_otro();
    }



}

function ocultarCapaProductosForestales() {
    $('#menu-productosForestales').hide("linear");
    ocultar_pf_pm();
    ocultar_pf_pnm();
    ocultar_pf_otro();


}



function mostrarProductosForestales() {
    $("#productosForestales").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaAgricultura();
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas();
            ocultarCapaDH();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaSistemaProduccionAgricola()
            ocultarCapaEconomia();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            mostrarCapaProductosForestales();
            CategoriaPF();
        }else{
            ocultarCapaProductosForestales();
        }
    });

}