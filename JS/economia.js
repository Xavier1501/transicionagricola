/*.-----------------------------------*/
function show_Eco_AutoConsumo() {
    for (i = 0; i < array_E_AutoConsumo.length; i++) {
        var position = new google.maps.LatLng(array_E_AutoConsumo[i]['lat'], array_E_AutoConsumo[i]['log']);
        var tipo = array_E_AutoConsumo[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_AutoConsumo[i]['nombre']
        });

        O_array_E_AutoConsumo.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_AutoConsumo[i]['id'],function(){
                    $("#id-auto-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_AutoConsumo(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_AutoConsumo.length; i++) {
        O_array_E_AutoConsumo[i].setMap(map);

    }
}
function ocultar_Eco_AutoConsumo() {
    Eco_AutoConsumo(null);
}


/*.-----------------------------------*/
function show_Eco_CertificaParticipativa() {
    for (i = 0; i < array_E_CertificaParticipativa.length; i++) {
        var position = new google.maps.LatLng(array_E_CertificaParticipativa[i]['lat'], array_E_CertificaParticipativa[i]['log']);
        var tipo = array_E_CertificaParticipativa[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_CertificaParticipativa[i]['nombre']
        });

        O_array_E_CertificaParticipativa.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_CertificaParticipativa[i]['id'],function(){
                    $("#id-cpspg-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_CertificaParticipativa(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_CertificaParticipativa.length; i++) {
        O_array_E_CertificaParticipativa[i].setMap(map);

    }
}
function ocultar_Eco_CertificaParticipativa() {
    Eco_CertificaParticipativa(null);
}





/*.-----------------------------------*/
function show_Eco_CTP() {
    for (i = 0; i < array_E_CTP.length; i++) {
        var position = new google.maps.LatLng(array_E_CTP[i]['lat'], array_E_CTP[i]['log']);
        var tipo = array_E_CTP[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_CTP[i]['nombre']
        });

        O_array_E_CTP.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_CTP[i]['id'],function(){
                    $("#id-ctp-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_CTP(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_CTP.length; i++) {
        O_array_E_CTP[i].setMap(map);

    }
}
function ocultar_Eco_CTP() {
    Eco_CTP(null);
}

/*.-----------------------------------*/
function show_Eco_CL() {
    for (i = 0; i < array_E_CL.length; i++) {
        var position = new google.maps.LatLng(array_E_CL[i]['lat'], array_E_CL[i]['log']);
        var tipo = array_E_CL[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(193, 96, 36, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_CL[i]['nombre']
        });

        O_array_E_CL.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_CL[i]['id'],function(){
                    $("#id-cl-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_CL(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_CL.length; i++) {
        O_array_E_CL[i].setMap(map);

    }
}
function ocultar_Eco_CL() {
    Eco_CL(null);
}


/*.-----------------------------------*/
function show_Eco_CReg() {
    for (i = 0; i < array_E_CReg.length; i++) {
        var position = new google.maps.LatLng(array_E_CReg[i]['lat'], array_E_CReg[i]['log']);
        var tipo = array_E_CReg[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(161, 35, 175, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_CReg[i]['nombre']
        });

        O_array_E_CReg.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_CReg[i]['id'],function(){
                    $("#id-cr-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_CReg(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_CReg.length; i++) {
        O_array_E_CReg[i].setMap(map);

    }
}
function ocultar_Eco_CReg() {
    Eco_CReg(null);
}

/*.-----------------------------------*/
function show_Eco_EPIns() {
    for (i = 0; i < array_E_EPIns.length; i++) {
        var position = new google.maps.LatLng(array_E_EPIns[i]['lat'], array_E_EPIns[i]['log']);
        var tipo = array_E_EPIns[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(137, 79, 53, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_EPIns[i]['nombre']
        });

        O_array_E_EPIns.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_EPIns[i]['id'],function(){
                    $("#id-epins-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_EPIns(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_EPIns.length; i++) {
        O_array_E_EPIns[i].setMap(map);

    }
}
function ocultar_Eco_EPIns() {
    Eco_EPIns(null);
}

/*.-----------------------------------*/
function show_Eco_II() {
    for (i = 0; i < array_E_II.length; i++) {
        var position = new google.maps.LatLng(array_E_II[i]['lat'], array_E_II[i]['log']);
        var tipo = array_E_II[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(247, 66, 162, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_II[i]['nombre']
        });

        O_array_E_II.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_II[i]['id'],function(){
                    $("#id-iit-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_II(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_II.length; i++) {
        O_array_E_II[i].setMap(map);

    }
}
function ocultar_Eco_II() {
    Eco_II(null);
}

/*.-----------------------------------*/
function show_Eco_Mal() {
    for (i = 0; i < array_E_MAl.length; i++) {
        var position = new google.maps.LatLng(array_E_MAl[i]['lat'], array_E_MAl[i]['log']);
        var tipo = array_E_MAl[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 119, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_MAl[i]['nombre']
        });

        O_array_E_MAl.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_MAl[i]['id'],function(){
                    $("#id-maa-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_Mal(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_MAl.length; i++) {
        O_array_E_MAl[i].setMap(map);

    }
}
function ocultar_Eco_Mal() {
    Eco_Mal(null);
}


/*.-----------------------------------*/
function show_Eco_SGe() {
    for (i = 0; i < array_E_SGe.length; i++) {
        var position = new google.maps.LatLng(array_E_SGe[i]['lat'], array_E_SGe[i]['log']);
        var tipo = array_E_SGe[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(255, 28, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_SGe[i]['nombre']
        });

        O_array_E_SGe.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_SGe[i]['id'],function(){
                    $("#id-sc-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_SGe(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_SGe.length; i++) {
        O_array_E_SGe[i].setMap(map);

    }
}
function ocultar_Eco_SGe() {
    Eco_SGe(null);
}

/*.-----------------------------------*/
function show_Eco_To() {
    for (i = 0; i < array_E_TOrg.length; i++) {
        var position = new google.maps.LatLng(array_E_TOrg[i]['lat'], array_E_TOrg[i]['log']);
        var tipo = array_E_TOrg[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 28, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_TOrg[i]['nombre']
        });

        O_array_E_TOrg.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_TOrg[i]['id'],function(){
                    $("#id-to-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_To(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_TOrg.length; i++) {
        O_array_E_TOrg[i].setMap(map);

    }
}
function ocultar_Eco_To() {
    Eco_To(null);
}

/*.-----------------------------------*/
function show_Eco_Con() {
    for (i = 0; i < array_E_Con.length; i++) {
        var position = new google.maps.LatLng(array_E_Con[i]['lat'], array_E_Con[i]['log']);
        var tipo = array_E_Con[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(250, 227, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_Con[i]['nombre']
        });

        O_array_E_Con.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_Con[i]['id'],function(){
                    $("#id-consumo-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_Con(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_Con.length; i++) {
        O_array_E_Con[i].setMap(map);

    }
}
function ocultar_Eco_Con() {
    Eco_Con(null);
}

/*.-----------------------------------*/
function show_Eco_Credito() {
    for (i = 0; i < array_E_Credito.length; i++) {
        var position = new google.maps.LatLng(array_E_Credito[i]['lat'], array_E_Credito[i]['log']);
        var tipo = array_E_Credito[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_Credito[i]['nombre']
        });

        O_array_E_Credito.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_Credito[i]['id'],function(){
                    $("#id-credito-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_Credito(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_Credito.length; i++) {
        O_array_E_Credito[i].setMap(map);

    }
}
function ocultar_Eco_Credito() {
    Eco_Credito(null);
}

/*.-----------------------------------*/
function show_Eco_Servicios() {
    for (i = 0; i < array_E_Servicios.length; i++) {
        var position = new google.maps.LatLng(array_E_Servicios[i]['lat'], array_E_Servicios[i]['log']);
        var tipo = array_E_Servicios[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(24, 194, 208, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_Servicios[i]['nombre']
        });

        O_array_E_Servicios.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_Servicios[i]['id'],function(){
                    $("#id-ser-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_Servicios(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_Servicios.length; i++) {
        O_array_E_Servicios[i].setMap(map);

    }
}
function ocultar_Eco_Servicios() {
    Eco_Servicios(null);
}

/*.-----------------------------------*/
function show_Eco_Semillas() {
    for (i = 0; i < array_E_Semillas.length; i++) {
        var position = new google.maps.LatLng(array_E_Semillas[i]['lat'], array_E_Semillas[i]['log']);
        var tipo = array_E_Semillas[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(250, 80, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_Semillas[i]['nombre']
        });

        O_array_E_Semillas.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_Semillas[i]['id'],function(){
                    $("#id-sem-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_Semillas(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_Semillas.length; i++) {
        O_array_E_Semillas[i].setMap(map);

    }
}
function ocultar_Eco_Semillas() {
    Eco_Semillas(null);
}

/*.-----------------------------------*/
function show_Eco_Otro() {
    for (i = 0; i < array_E_OtroEconomia.length; i++) {
        var position = new google.maps.LatLng(array_E_OtroEconomia[i]['lat'], array_E_OtroEconomia[i]['log']);
        var tipo = array_E_OtroEconomia[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(250, 80, 0, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: array_E_OtroEconomia[i]['nombre']
        });

        O_array_E_OtroEconomia.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+array_E_OtroEconomia[i]['id'],function(){
                    $("#id-otro-eco").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}
// funcion para apagar la capa
function Eco_Otro(map) {
    //console.log(O_array_E_AutoConsumo);
    for (let i = 0; i < O_array_E_OtroEconomia.length; i++) {
        O_array_E_OtroEconomia[i].setMap(map);

    }
}
function ocultar_Eco_Otro() {
    Eco_Otro(null);
}

/*.-----------------------------------*/

function categoriaEconomia() {
    $('#menu-economia').show("swing");
}

function mostrarCapaEconomia() {


    if(array_E_AutoConsumo){
        show_Eco_AutoConsumo();
    }
    if(array_E_CertificaParticipativa){
        show_Eco_CertificaParticipativa();
    }
    if(array_E_CTP){
        show_Eco_CTP();
    }
    if(array_E_CReg){
        show_Eco_CReg();
    }

    if(array_E_EPIns){
        show_Eco_EPIns()
    }

    if(array_E_II){
        show_Eco_II();
    }

    if(array_E_MAl){
        show_Eco_Mal();
    }

    if(array_E_TOrg){
       show_Eco_To();
    }

    if(array_E_Con){
        show_Eco_Con();
    }
    if(array_E_Credito){
        show_Eco_Credito();
    }
    if(array_E_Servicios){
        show_Eco_Servicios();
    }
    if(array_E_Semillas){
        show_Eco_Semillas();
    }
    if(array_E_OtroEconomia){
        show_Eco_Otro();
    }
}

function ocultarCapaEconomia() {
    $('#menu-economia').hide("linear");
    ocultar_Eco_AutoConsumo();
    ocultar_Eco_CertificaParticipativa();
    ocultar_Eco_CTP();
    ocultar_Eco_CL();
    ocultar_Eco_CReg();
    ocultar_Eco_EPIns();
    ocultar_Eco_II();
    ocultar_Eco_Mal();
    ocultar_Eco_SGe();
    ocultar_Eco_To();
    ocultar_Eco_Con();
    ocultar_Eco_Credito();
    ocultar_Eco_Servicios();
    ocultar_Eco_Semillas();
    ocultar_Eco_Otro();


}



function mostrarEconomia() {
    $("#economia").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaAgricultura();
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas();
            ocultarCapaDH();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaSistemaProduccionAgricola();
            ocultarCapaEducacion();
            ocultarDLDesarrollo();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            mostrarCapaEconomia();
            categoriaEconomia();
        }else{
           ocultarCapaEconomia();



        }
    });

}