

/*--------------------------Huertos-parcelas escolaresDerechos humanos-------------------------*/

function showoTNJ_He() {
    for (i = 0; i < arrego_tnj_he.length; i++) {
        var position = new google.maps.LatLng(arrego_tnj_he[i]['lat'], arrego_tnj_he[i]['log']);
        var tipo = arrego_tnj_he[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_tnj_he[i]['nombre']
        });

        O_arr_tnj_he.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_tnj_he[i]['id'],function(){
                    $("#id-hpe-tnj").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function TNJ_He(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_tnj_he.length; i++) {
        O_arr_tnj_he[i].setMap(map);

    }
}
function ocultarTNJ_He() {
    TNJ_He(null);
}

/*--------------------------Educación Alimentaria y nutricional-------------------------*/

function showoTNJ_Ea() {
    for (i = 0; i < arrego_tnj_ea.length; i++) {
        var position = new google.maps.LatLng(arrego_tnj_ea[i]['lat'], arrego_tnj_ea[i]['log']);
        var tipo = arrego_tnj_ea[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_tnj_ea[i]['nombre']
        });

        O_arr_tnj_ea.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_tnj_ea[i]['id'],function(){
                    $("#id-ean-tnj").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function TNJ_Ea(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_tnj_ea.length; i++) {
        O_arr_tnj_ea[i].setMap(map);

    }
}
function ocultarTNJ_Ea() {
    TNJ_Ea(null);
}

/*--------------------------otro-------------------------*/

function showoTNJ_otro() {
    for (i = 0; i < arrego_tnj_otra.length; i++) {
        var position = new google.maps.LatLng(arrego_tnj_otra[i]['lat'], arrego_tnj_otra[i]['log']);
        var tipo = arrego_tnj_otra[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_tnj_otra[i]['nombre']
        });

        O_arr_tnj_otra.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_tnj_otra[i]['id'],function(){
                    $("#id-o-tnj").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function TNJ_otra(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_tnj_otra.length; i++) {
        O_arr_tnj_otra[i].setMap(map);

    }
}
function ocultarTNJ_otra() {
    TNJ_otra(null);
}



/*.-----------------------------------*/

function categoriaTNJ() {
    $('#menu-trabajoNiños').show("swing");
}
function mostrarCapaTNJ() {


    if (arrego_tnj_he){
        showoTNJ_He();
    }
    if (arrego_tnj_ea){
        showoTNJ_Ea();
    }

    if (arrego_tnj_otra){
        showoTNJ_otro();
    }

}

function ocultarCapaTNJ() {
    $('#menu-trabajoNiños').hide("linear");
    ocultarTNJ_He();
    ocultarTNJ_Ea();
    ocultarTNJ_otra();
}



function mostrarTNJ() {
    $("#trabajoNiñosJoventud").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaAgricultura();
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas()
            ocultarCapaDLocal();
            ocultarDHdh();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaEconomia();
            ocultarCapaSistemaProduccionAgricola();
            ocultarCapaEducacion();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            mostrarCapaTNJ();
            categoriaTNJ();

        }else{
            ocultarCapaTNJ();
        }
    });

}