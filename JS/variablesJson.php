
let infowindow;


let AgUrHcomunitarios = <?php echo json_encode($AgUrHcomunitarios);?>;
let AgUrHempresas = <?php echo json_encode($AgUrHempresas);?>;
let AgUrTraspatio = <?php echo json_encode($arregloTraspatio);?>;
let AgUrParques = <?php echo json_encode($AgUrParques);?>;
let AgUrBalcon = <?php echo json_encode($AgUrBalcon);?>;
let datosMarkets = <?php echo json_encode($datosMarkets);?>;
let arregloAgrulturaHsociales = <?php echo json_encode($arregloHsociales);?>;
let arregloAgrulturaHistitucional = <?php echo json_encode($arregloHistituciones);?>;
let arregloAgrulturaHOtros= <?php echo json_encode($arregloHotros);?>;

/*salud plantas medicionales*/

let arregloSaludPlantasMedicnalesHomepatia = <?php echo json_encode($arregloSaludHomeopatia);?>;
let arregloSaludPlantasMedicnalesMedicina = <?php echo json_encode($arregloSaludMedicina);?>;
let arregloSaludPlantasMedicnalesPreparadosBotanicos = <?php echo json_encode($arregloSaludPreparadosBotanicos);?>;
let arregloSaludPlantasMedicnalesRemedioCasero = <?php echo json_encode($arregloSaludRemedioCasero);?>;
let arregloSaludNutricionAgri = <?php echo json_encode($arregloSaludNutricionAgri);?>;
let arregloSaludOtrosaludPlantas = <?php echo json_encode($arregloSaludOtrosaludPlantas);?>;


/*Actividades no agricolas*/
let arregloNoAgricolasArtesanales= <?php echo json_encode($arregloActividadesNoAgricolasArtesania);?>;
let arregloNoAgricolasCaja= <?php echo json_encode($arregloActividadesNoAgricolasCaja);?>;
let arregloNoAgricolasTurismo= <?php echo json_encode($arregloActividadesNoAgricolasTurismo);?>;
let arregloNoAgricolasCulturales= <?php echo json_encode($arregloActividadesNoAgricolasCulturales);?>;
let arregloNoAgricolasComedor= <?php echo json_encode($arregloActividadesNoAgricolasComedorComuni);?>;
let arregloNoAgricolasComedorEscolar= <?php echo json_encode($arregloActividadesNoAgricolasComedorEscolar);?>;
let arregloNoAgricolasRestautan= <?php echo json_encode($arregloActividadesNoAgricolasRestaurant);?>;
let arregloNoAgricolasOtro= <?php echo json_encode($arregloActividadesNoAgricolasOtro);?>;

/*desarrollo local*/
let arr_DL_AP= <?php echo json_encode($arr_DL_AP);?>;
let arr_dlDesarrollo= <?php echo json_encode($arr_dlDesarrollo);?>;
let arr_dl_Org= <?php echo json_encode($arr_dl_Org);?>;
let arr_dl_Gob= <?php echo json_encode($arr_dl_Gob);?>;
let arr_dl_Nuc= <?php echo json_encode($arr_dl_Nuc);?>;
let arr_dl_Otro= <?php echo json_encode($arr_dl_Otro);?>;


/*Desarrollo Humano*/
let arrego_DH_dh= <?php echo json_encode($arrego_DH_dh);?>;
let arrego_DH_de= <?php echo json_encode($arrego_DH_de);?>;
let arrego_DH_da= <?php echo json_encode($arrego_DH_da);?>;


/*Trabajo con niños/juventud*/
let arrego_tnj_he= <?php echo json_encode($arrego_tnj_he);?>;
let arrego_tnj_ea= <?php echo json_encode($arrego_tnj_ea);?>;
let arrego_tnj_otra= <?php echo json_encode($arrego_tnj_otra);?>;


/*Investigacion Agricola Rural*/
let arrego_IAER_c= <?php echo json_encode($arrego_IAER_c);?>;
let arrego_IAER_d= <?php echo json_encode($arrego_IAER_d);?>;
let arrego_IAER_e= <?php echo json_encode($arrego_IAER_e);?>;
let arrego_IAER_fc= <?php echo json_encode($arrego_IAER_fc);?>;
let arrego_IAER_in= <?php echo json_encode($arrego_IAER_in);?>;
let arrego_IAER_orn= <?php echo json_encode($arrego_IAER_orn);?>;
let arrego_IAER_otro= <?php echo json_encode($arrego_IAER_otro);?>;

/*]Manejo recursos hibridos*/
let arrego_MRH_lluvias= <?php echo json_encode($arrego_MRH_lluvias);?>;
let arrego_MRH_microcuenta= <?php echo json_encode($arrego_MRH_microcuenta);?>;
let arrego_MRH_tecnologias= <?php echo json_encode($arrego_MRH_tecnologias);?>;
let arrego_MRH_agua= <?php echo json_encode($arrego_MRH_agua);?>;
let arrego_MRH_riesgo= <?php echo json_encode($arrego_MRH_riesgo);?>;
let arrego_MRH_tratamiento= <?php echo json_encode($arrego_MRH_tratamiento);?>;
let arrego_MRH_otro= <?php echo json_encode($arrego_MRH_otro);?>;

/*]Manejo Poscosecha*/
let arrego_MP_almacenamiento= <?php echo json_encode($arrego_MP_almacenamiento);?>;
let arrego_MP_comerc= <?php echo json_encode($arrego_MP_comerc);?>;
let arrego_MP_trnsfor= <?php echo json_encode($arrego_MP_trnsfor);?>;
let arrego_MP_otro= <?php echo json_encode($arrego_MP_otro);?>;

/*Semillas*/
let arrego_sem = <?php echo json_encode($arrego_sem);?>;
let arrego_ban = <?php echo json_encode($arrego_ban);?>;
let arrego_inte = <?php echo json_encode($arrego_inte);?>;
let arrego_mpAnimal = <?php echo json_encode($arrego_mpAnimal);?>;
let arrego_proSem = <?php echo json_encode($arrego_proSem);?>;
let arrego_otroSem = <?php echo json_encode($arrego_otroSem);?>;


/*Sistema Produccion Agricola*/

let arregloSistemasProdPermacultura = <?php echo json_encode($arregloSistemasProdPermacultura);?>;
let arreglo_spa_Abio = <?php echo json_encode($arreglo_spa_Abio);?>;
let arreglo_spa_AN = <?php echo json_encode($arreglo_spa_AN);?>;
let arreglo_spa_AO = <?php echo json_encode($arreglo_spa_AO);?>;
let arreglo_spa_Ag = <?php echo json_encode($arreglo_spa_Ag);?>;
let arreglo_tsi = <?php echo json_encode($arreglo_tsi);?>;
let arreglo_stq = <?php echo json_encode($arreglo_stq);?>;
let arreglo_mbio = <?php echo json_encode($arreglo_mbio);?>;
let arreglo_mint = <?php echo json_encode($arreglo_mint);?>;
let arreglo_ave = <?php echo json_encode($arreglo_ave);?>;
let arreglo_bDes = <?php echo json_encode($arreglo_bDes);?>;
let arreglo_hroca = <?php echo json_encode($arreglo_hroca);?>;
let arreglo_cob = <?php echo json_encode($arreglo_cob);?>;
let arreglo_comp = <?php echo json_encode($arreglo_comp);?>;
let arreglo_MFer = <?php echo json_encode($arreglo_MFer);?>;
let arreglo_PrBio = <?php echo json_encode($arreglo_PrBio);?>;
let arreglo_PAS = <?php echo json_encode($arreglo_PAS);?>;
let arreglo_PcSue = <?php echo json_encode($arreglo_PcSue);?>;
let arreglo_sDCul = <?php echo json_encode($arreglo_sDCul);?>;
let arreglo_sCulAni = <?php echo json_encode($arreglo_sCulAni);?>;
let arreglo_sDir = <?php echo json_encode($arreglo_sDir);?>;
let arreglo_cereales = <?php echo json_encode($arreglo_cereales);?>;
let arreglo_cutAso = <?php echo json_encode($arreglo_cutAso);?>;
let arreglo_Forraje = <?php echo json_encode($arreglo_Forraje);?>;
let arreglo_Frutales = <?php echo json_encode($arreglo_Frutales);?>;
let arreglo_Hortaliza = <?php echo json_encode($arreglo_Hortaliza);?>;
let arreglo_Leguminosa = <?php echo json_encode($arreglo_Leguminosa);?>;
let arreglo_Medicinales = <?php echo json_encode($arreglo_Medicinales);?>;
let arreglo_Oleaginosa = <?php echo json_encode($arreglo_Oleaginosa);?>;
let arreglo_Textil = <?php echo json_encode($arreglo_Textil);?>;
let arreglo_OtroAgricola = <?php echo json_encode($arreglo_OtroAgricola);?>;

/*Economia*/
let array_E_AutoConsumo = <?php echo json_encode($array_E_AutoConsumo);?>;
let array_E_CertificaParticipativa = <?php echo json_encode($array_E_CertificaParticipativa);?>;
let array_E_CTP = <?php echo json_encode($array_E_CTP);?>;
let array_E_CL = <?php echo json_encode($array_E_CL);?>;
let array_E_CReg = <?php echo json_encode($array_E_CReg);?>;
let array_E_EPIns = <?php echo json_encode($array_E_EPIns);?>;
let array_E_II = <?php echo json_encode($array_E_II);?>;
let array_E_MAl = <?php echo json_encode($array_E_MAl);?>;
let array_E_SGe = <?php echo json_encode($array_E_SGe);?>;
let array_E_TOrg = <?php echo json_encode($array_E_TOrg);?>;
let array_E_Con = <?php echo json_encode($array_E_Con);?>;
let array_E_Credito = <?php echo json_encode($array_E_Credito);?>;
let array_E_Servicios = <?php echo json_encode($array_E_Servicios);?>;
let array_E_Semillas = <?php echo json_encode($array_E_Semillas);?>;
let array_E_OtroEconomia = <?php echo json_encode($array_E_OtroEconomia);?>;

/*Educacion*/
let array_Edu_EduAmbiental = <?php echo json_encode($array_Edu_EduAmbiental);?>;
let array_Edu_EscuelaCampesina = <?php echo json_encode($array_Edu_EscuelaCampesina);?>;
let array_Edu_EscuelaIndigena = <?php echo json_encode($array_Edu_EscuelaIndigena);?>;
let array_Edu_FormacionTecnico = <?php echo json_encode($array_Edu_FormacionTecnico);?>;
let array_Edu_ComuniAprende = <?php echo json_encode($array_Edu_ComuniAprende);?>;
let array_Edu_IniciativaCampesina = <?php echo json_encode($array_Edu_IniciativaCampesina);?>;
let array_Edu_Basicas = <?php echo json_encode($array_Edu_Basicas);?>;
let array_Edu_MediaSuperior = <?php echo json_encode($array_Edu_MediaSuperior);?>;
let array_Edu_Superior = <?php echo json_encode($array_Edu_Superior);?>;
let array_Edu_OtroEcudacion = <?php echo json_encode($array_Edu_OtroEcudacion);?>;


/*Sistemas agroforestales*/
let arrego_sa_adcs = <?php echo json_encode($arrego_sa_adcs);?>;
let arrego_sa_sa = <?php echo json_encode($arrego_sa_sa);?>;
let arrego_sa_bc = <?php echo json_encode($arrego_sa_bc);?>;
let arrego_sa_cv = <?php echo json_encode($arrego_sa_cv);?>;
let arrego_sa_brv = <?php echo json_encode($arrego_sa_brv);?>;
let arrego_sa_otro = <?php echo json_encode($arrego_sa_otro);?>;



/*Productos forestales*/
let arrego_pf_pm = <?php echo json_encode($arrego_pf_pm);?>;
let arrego_pf_pnm = <?php echo json_encode($arrego_pf_pnm);?>;
let arrego_pf_otro = <?php echo json_encode($arrego_pf_otro);?>;


/*iniciativa*/
let iniciativa = <?php echo json_encode($arrego_iniciativas);?>;


/*Sistemas de producción animal*/
let arrego_span_ae = <?php echo json_encode($arrego_span_ae);?>;
let arrego_span_an = <?php echo json_encode($arrego_span_an);?>;
let arrego_span_acua = <?php echo json_encode($arrego_span_acua);?>;
let arrego_span_aves = <?php echo json_encode($arrego_span_aves);?>;
let arrego_span_bov = <?php echo json_encode($arrego_span_bov);?>;
let arrego_span_capr = <?php echo json_encode($arrego_span_capr);?>;
let arrego_span_cuni = <?php echo json_encode($arrego_span_cuni);?>;
let arrego_span_equi = <?php echo json_encode($arrego_span_equi);?>;
let arrego_span_lomb = <?php echo json_encode($arrego_span_lomb);?>;
let arrego_span_ovi = <?php echo json_encode($arrego_span_ovi);?>;
let arrego_span_pes = <?php echo json_encode($arrego_span_pes);?>;
let arrego_span_porc = <?php echo json_encode($arrego_span_porc);?>;
let arrego_span_conejo = <?php echo json_encode($arrego_span_conejo);?>;
let arrego_span_mg = <?php echo json_encode($arrego_span_mg);?>;
let arrego_span_nan = <?php echo json_encode($arrego_span_nan);?>;
let arrego_span_ps = <?php echo json_encode($arrego_span_ps);?>;
let arrego_span_vn = <?php echo json_encode($arrego_span_vn);?>;
let arrego_span_hom = <?php echo json_encode($arrego_span_hom);?>;
let arrego_span_cons = <?php echo json_encode($arrego_span_cons);?>;
let arrego_span_otro = <?php echo json_encode($arrego_span_otro);?>;

/*Sistemas de producción animal*/
let O_arrego_span_ae = [];
let O_arrego_span_an = [];
let O_arrego_span_acua = [];
let O_arrego_span_aves = [];
let O_arrego_span_bov = [];
let O_arrego_span_capr = [];
let O_arrego_span_cuni = [];
let O_arrego_span_equi = [];
let O_arrego_span_lomb = [];
let O_arrego_span_ovi = [];
let O_arrego_span_pes = [];
let O_arrego_span_porc = [];
let O_arrego_span_conejo = [];
let O_arrego_span_mg = [];
let O_arrego_span_nan = [];
let O_arrego_span_ps = [];
let O_arrego_span_vn =[];
let O_arrego_span_hom = [];
let O_arrego_span_cons = [];
let O_arrego_span_otro = [];

/*iniciativa*/
let O_iniciativa = [];



/*Productos forestales*/
let O_arrego_pf_pm = [];
let O_arrego_pf_pnm = [];
let O_arrego_pf_otro = [];

/*Sistemas agroforestales*/
let O_arrego_sa_adcs = [];
let O_arrego_sa_sa = [];
let O_arrego_sa_bc = [];
let O_arrego_sa_cv = [];
let O_arrego_sa_brv =[];
let O_arrego_sa_otro = [];





/*Educacion*/
let O_array_Edu_EduAmbiental = [];
let O_array_Edu_EscuelaCampesina = [];
let O_array_Edu_EscuelaIndigena = [];
let O_array_Edu_FormacionTecnico = [];
let O_array_Edu_ComuniAprende = [];
let O_array_Edu_IniciativaCampesina = [];
let O_array_Edu_Basicas = [];
let O_array_Edu_MediaSuperior = [];
let O_array_Edu_Superior = [];
let O_array_Edu_OtroEcudacion = [];


/*Economia*/
let O_array_E_AutoConsumo = [];
let O_array_E_CertificaParticipativa = [];
let O_array_E_CTP = [];
let O_array_E_CL = [];
let O_array_E_CReg = [];
let O_array_E_EPIns = [];
let O_array_E_II = [];
let O_array_E_MAl = [];
let O_array_E_SGe = [];
let O_array_E_TOrg = [];
let O_array_E_Con = [];
let O_array_E_Credito = [];
let O_array_E_Servicios = [];
let O_array_E_Semillas = [];
let O_array_E_OtroEconomia = [];

/*Sistema Produccion Agricola*/

let O_arregloSistemasProdPermacultura = [];
let O_arreglo_spa_Abio = [];
let O_arreglo_spa_AN = [];
let O_arreglo_spa_AO = [];
let O_arreglo_spa_Ag = [];
let O_arreglo_tsi = [];
let O_arreglo_stq = [];
let O_arreglo_mbio = [];
let O_arreglo_mint = [];
let O_arreglo_ave = [];
let O_arreglo_bDes = [];
let O_arreglo_hroca = [];
let O_arreglo_cob = [];
let O_arreglo_comp = [];
let O_arreglo_MFer = [];
let O_arreglo_PrBio = [];
let O_arreglo_PAS = [];
let O_arreglo_PcSue = [];
let O_arreglo_sDCul = [];
let O_arreglo_sCulAni = [];
let O_arreglo_sDir = [];
let O_arreglo_cereales = [];
let O_arreglo_cutAso = [];
let O_arreglo_Forraje = [];
let O_arreglo_Frutales = [];
let O_arreglo_Hortaliza = [];
let O_arreglo_Leguminosa = [];
let O_arreglo_Medicinales = [];
let O_arreglo_Oleaginosa = [];
let O_arreglo_Textil = [];
let O_arreglo_OtroAgricola = [];



/*Semillas*/

let O_arrego_sem = [];
let O_arrego_ban = [];
let O_arrego_inte = [];
let O_arrego_mpAnimal = [];
let O_arrego_proSem = [];
let O_arrego_otroSem = [];




/*]Manejo Poscosecha*/
let O_arr_MP_almacenamiento= [];
let O_arr_MP_comerc= [];
let O_arr_MP_trnsfor= [];
let O_arr_MP_otro= [];

/*]Manejo recursos hibridos*/
let O_arr_MRH_lluvias= [];
let O_arr_MRH_microcuenta=[];
let O_arr_MRH_tecnologias= [];
let O_arr_MRH_agua= [];
let O_arr_MRH_riesgo= [];
let O_arr_MRH_tratamiento= [];
let O_arr_MRH_otro = [];


let map;
let ocultarArregloHcomunitarios = [];
let ocultarArregloHempresas = [];
let ocultarArregloTraspatio = [];
let ocultarArregloParque = [];
let ocultarArregloBalcon = [];
let ocultarArregloHsociales = [];
let ocultarArregloHisntitucional = [];
let ocultarArregloAzotea =[];
let ocultarArregloHotros = [];

/*salud plantas medicionales*/
let ocultarSaludPlantasMedicnalesHomepatia = [];
let ocultarSaludPlantasMedicnalesMedicina = [];
let ocultarSaludPlantasMedicnalesPreparadosBotanicos = [];
let ocultarSaludPlantasMedicnalesRemedioCasero = [];
let ocultarSaludNutricionAgri = [];
let ocultarSaludPlantasMedicnalesOtro= [];


/*Actividades no agricolas*/
let OcultarNoAgricolasArtesanales = [];
let OcultarNoAgricolasCaja = [];
let OcultarNoAgricolasTurismo = [];
let OcultarNoAgricolasCulturales = [];
let OcultarNoAgricolasComedor = [];
let OcultarNoAgricolasComedorEscolar = [];
let OcultarNoAgricolasRestaunte = [];
let OcultarNoAgricolasOtro = [];


/*desarrollo local*/
let O_arr_DL_AP = [];
let O_arr_DL_Des = [];
let O_arr_dl_Org = [];
let O_arr_dl_Gob = [];
let O_arr_dl_Nuc = [];
let O_arr_dl_Otro = [];


/*Desarrollo humano*/
let O_arr_dh_dh = [];
let O_arr_dh_da = [];
let O_arr_dh_de = [];

/*Trabajo con niños/juventud*/
let O_arr_tnj_he= [];
let O_arr_tnj_ea= [];
let O_arr_tnj_otra= [];

/*Investigacion Agricola Rural*/
let O_arr_IAER_c= [];
let O_arr_IAER_d= [];
let O_arr_IAER_e= [];
let O_arr_IAER_fc= [];
let O_arr_IAER_in= [];
let O_arr_IAER_orn= [];
let O_arr_IAER_otro= [];

