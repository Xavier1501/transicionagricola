/*--------------------------Almacenamiento-------------------------*/

function showoMP_Alm() {
    for (i = 0; i < arrego_MP_almacenamiento.length; i++) {
        var position = new google.maps.LatLng(arrego_MP_almacenamiento[i]['lat'], arrego_MP_almacenamiento[i]['log']);
        var tipo = arrego_MP_almacenamiento[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(209, 0, 73, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MP_almacenamiento[i]['nombre']
        });

        O_arr_MP_almacenamiento.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MP_almacenamiento[i]['id'],function(){
                    $("#id-Al-MP").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function MP_alm(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_MP_almacenamiento.length; i++) {
        O_arr_MP_almacenamiento[i].setMap(map);

    }
}
function ocultarMP_alm() {
    MP_alm(null);
}


/*--------------------------Comerci-------------------------*/

function showoMP_com() {
    for (i = 0; i < arrego_MP_comerc.length; i++) {
        var position = new google.maps.LatLng(arrego_MP_comerc[i]['lat'], arrego_MP_comerc[i]['log']);
        var tipo = arrego_MP_comerc[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(84, 0, 209, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MP_comerc[i]['nombre']
        });

        O_arr_MP_comerc.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MP_comerc[i]['id'],function(){
                    $("#id-Com-MP").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function MP_comec(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_MP_comerc.length; i++) {
        O_arr_MP_comerc[i].setMap(map);
    }
}
function ocultarMP_comec() {
    MP_comec(null);
}


/*--------------------------trnsformacion-------------------------*/

function showoMP_trans() {
    for (i = 0; i < arrego_MP_trnsfor.length; i++) {
        var position = new google.maps.LatLng(arrego_MP_trnsfor[i]['lat'], arrego_MP_trnsfor[i]['log']);
        var tipo = arrego_MP_trnsfor[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MP_trnsfor[i]['nombre']
        });

        O_arr_MP_trnsfor.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MP_trnsfor[i]['id'],function(){
                    $("#id-pt-MP").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function MP_trasnf(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_MP_trnsfor.length; i++) {
        O_arr_MP_trnsfor[i].setMap(map);
    }
}
function ocultarMP_transf() {
    MP_trasnf(null);
}

/*--------------------------trnsformacion-------------------------*/

function showoMP_trans() {
    for (i = 0; i < arrego_MP_trnsfor.length; i++) {
        var position = new google.maps.LatLng(arrego_MP_trnsfor[i]['lat'], arrego_MP_trnsfor[i]['log']);
        var tipo = arrego_MP_trnsfor[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(187, 126, 71, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MP_trnsfor[i]['nombre']
        });

        O_arr_MP_trnsfor.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MP_trnsfor[i]['id'],function(){
                    $("#id-pt-MP").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function MP_trasnf(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_MP_trnsfor.length; i++) {
        O_arr_MP_trnsfor[i].setMap(map);
    }
}
function ocultarMP_transf() {
    MP_trasnf(null);
}

/*--------------------------otro-------------------------*/

function showoMP_otro() {
    for (i = 0; i < arrego_MP_otro.length; i++) {
        var position = new google.maps.LatLng(arrego_MP_otro[i]['lat'], arrego_MP_otro[i]['log']);
        var tipo = arrego_MP_otro[i]['id'];

        marker = new google.maps.Marker({
            position: position,
            icon: '/img/m1.png',
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                scale: 5, //tamaño
                strokeColor: 'rgba(255, 255, 255, 1)', //color del borde
                strokeWeight: .5, //grosor del borde
                fillColor: 'rgba(186, 161, 148, 1)', //color de relleno
                fillOpacity: 1 // opacidad del relleno
            },
            map: map,
            title: arrego_MP_otro[i]['nombre']
        });

        O_arr_MP_otro.push(marker);

        (function(i, marker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (!infowindow) {
                    infowindow = new google.maps.InfoWindow({
                        maxWidth:0
                    });
                }
                let id = $('.modal-body').load('Vistas/modal/consultasCategoria/info-Category-modal-DB.php?id='+arrego_MP_otro[i]['id'],function(){
                    $("#id-otro-MP").modal("show");
                });

                let contentMap = infowindow.setContent(
                    "Agricultura/Azotea"+
                    ""+id +""
                );
                contentMap.hidden();
                infowindow.open(map, marker);
                map.setZoom(10);
                map.setCenter(marker.getPosition());
            });
        })(i, marker);

    }

}

// funcion para apagar la capa medicina
function MP_otro(map) {
    //console.log(O_arr_DL_AP);
    for (let i = 0; i < O_arr_MP_otro.length; i++) {
        O_arr_MP_otro[i].setMap(map);
    }
}
function ocultarMP_otro() {
    MP_otro(null);
}

/*.-----------------------------------*/

function categoriaMP() {
    $('#menu-MP-m').show("swing");
    alert(4);
}


function mostrarCapaMP() {
    showoMP_Alm();
    showoMP_com();
    showoMP_trans();
    showoMP_otro();
}

function ocultarCapaMP() {
    $('#menu-MP-m').hide("linear");
    ocultarMP_alm();
    ocultarMP_comec();
    ocultarMP_transf();
    ocultarMP_otro();
}



function mostrarMP() {
    $("#manejoPoscosecha").on('change', function(){
        if ($(this).is(':checked') ) {
            ocultarCapaAgricultura();
            ocultarCapaSaludPlantasMedicinales();
            ocultarCapaActividadesNoAgricolas()
            ocultarCapaDLocal();
            ocultarCapaTNJ();
            ocultarCapaIAR();
            ocultarCapaMRH();
            ocultarCapaMP();
            ocultarCapaDH();
            ocultarCapaSem();
            ocultarCapaEconomia();
            ocultarCapaSistemaProduccionAgricola();
            ocultarCapaEducacion();
            ocultarCapaDLocal();
            ocultarCapaSistemaAgroforestales();
            ocultarCapaProductosForestales();
            ocultarTodasLasIniciativas();
            ocultarCapaSistemaProduccionAnimal();
            mostrarCapaMP();
            categoriaMP();
        }else{
            ocultarCapaMP();
        }
    });

}