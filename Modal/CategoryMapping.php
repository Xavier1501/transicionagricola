<?php


class CategoryMapping extends MetodosIncuesta
{
    public function AgriculturaUrbana(){
        $sql ="
        
        select * from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idIniciativa
left join co_agr_agricultura_urbana caaur on  caaur.idEntrevistaFK = cae.idEntrevista 
inner join (
select * from co_agr_ubicacion cau 
inner join co_agr_estado caes on  caes.id_estado = cau.idEstado 
inner join co_agr_municipio cam on cam.id_municipio = cau.idMunicipio 
) co_agr_ubicacion on co_agr_ubicacion.idEntrevista = cae.idEntrevista
group by cae.idEntrevista 
        
        ";

        $result = $conexion->query($sql);
        $data = array();



        while ($row = $result ->fetch_array(MYSQLI_ASSOC)){
            $datosMarkets[]  = array(
                'id' => $row['id'],
                'iniciativa'=> $row['iniciativa'],
                'grupo' => $row['grupo'],
                'sitio' => $row['sitio'],
                'lat' => $row['lat'],
                'log' => $row['log'],
                'resumen' => $row['resumen'],
                'fecha' => $row['fecha'],
                'estado' => $row['estado'],

            );
        }
    }

}