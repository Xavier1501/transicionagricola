<?php


/*creamos consultas*/

$sqlAT = "
select 
distinct cae.idEntrevista, 
cai.idIniciativa,
cai.NombreIniciativa,
cau.usuario,
cau.Contrasena,
cau.fecha_creacion,
cau.tipo, 
cae.NombreGrupo,
cae.NombreRepresentante,
cae.Correo,
cae.SitioFacebook,
cae.SitioWebPerfil,
cae.instagram,
cae.Direccion,
cae.CodigoPostal,
caes.estado,
camun.municipio
from co_agr_entrevista cae 
left join co_agr_usuario cau on cae.idEntrevista = cau.idUsuario 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista 
left join co_agr_estado caes on caub.idEstado = caes.id_estado 
LEFT JOIN co_agr_municipio camun ON caub.idMunicipio =  camun.id_municipio
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc
";

$queryF = $conexion->query($sqlF);


$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setTitle('Ficha-tecnica');


$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1','Numero')
    ->setCellValue('B1','Usuario')
    ->setCellValue('C1','Contraseña')
    ->setCellValue('D1','Fecha de  craeacion')
    ->setCellValue('E1','Tipo de grupo')
    ->setCellValue('F1','Nombre de ficha')
    ->setCellValue('G1','Nombre representante')
    ->setCellValue('H1','Correo')
    ->setCellValue('I1','Facebook')
    ->setCellValue('J1','Pagina internet')
    ->setCellValue('K1','Instagram')
    ->setCellValue('L1','Direccion')
    ->setCellValue('M1','Codigo Postal')
    ->setCellValue('N1','Estado')
    ->setCellValue('O1','Municipio')
    ->setCellValue('P1','iniciativa');


$i =2;

while($row = $queryF ->fetch_array(MYSQLI_ASSOC)){
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i,$row['idEntrevista'] )
        ->setCellValue('B'.$i,$row['usuario'])
        ->setCellValue('C'.$i,$row['Contrasena'])
        ->setCellValue('D'.$i,$row['fecha_creacion'])
        ->setCellValue('E'.$i,$row['tipo'])
        ->setCellValue('F'.$i,$row['NombreGrupo'])
        ->setCellValue('G'.$i,$row['NombreRepresentante'])
        ->setCellValue('H'.$i,$row['Correo'])
        ->setCellValue('I'.$i,$row['SitioFacebook'])
        ->setCellValue('J'.$i,$row['SitioWebPerfil'])
        ->setCellValue('k'.$i,$row['instagram'])
        ->setCellValue('L'.$i,$row['Direccion'])
        ->setCellValue('M'.$i,$row['CodigoPostal'])
        ->setCellValue('N'.$i,$row['estado'])
        ->setCellValue('O'.$i,$row['municipio'])
        ->setCellValue('P'.$i,$row['NombreIniciativa']);

    $i++;
}
