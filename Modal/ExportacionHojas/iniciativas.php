<?php


/*creamos consultas*/

$sqliniciativa = "
select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
cai.idEntrevista, 
cai.NombreIniciativa,
cai.AnoInicio,
cai.Resumen,
cai.Surgimiento,
cai.historico,
cai.principalActor,
cai.problemas,
cai.proposito,
cai.soluciiones,
cai.retos,
cai.Latitud,
cai.Longitud,
cai.financiamiento,
cai.Fechacreacion,
cai.activo,
cai.estados,
cai.municipio,
cai.Continuarfases 
from co_agr_entrevista cae 
 left join co_agr_usuario cau on cae.idEntrevista = cau.idUsuario 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc
";

$queryIniciativa = $conexion->query($sqliniciativa);

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(3);
$objPHPExcel->getActiveSheet()->setTitle('Atlas-Iniciativas');


$objPHPExcel->setActiveSheetIndex(3)
    ->setCellValue('A1', 'Numero')
    ->setCellValue('B1', 'Nombre de la iniciativa o experiencia')
    ->setCellValue('C1', 'Resumen sobre el trabajo que realiza en la iniciativa o experiencia agroecológica')
    ->setCellValue('D1', '¿Cómo surgió su experiencia o iniciativa?')
    ->setCellValue('E1', 'Contexto histórico en que surge')
    ->setCellValue('F1', 'Principales actores involucrados')
    ->setCellValue('G1', 'Propósitos más importantes')
    ->setCellValue('H1', 'Problemas a los que se ha enfretado su inciativa o experiencia agroecológica')
    ->setCellValue('I1', 'Soluciones encontradas')
    ->setCellValue('J1', 'Retos futuros')
    ->setCellValue('K1', 'Si se ha recibido financiamiento, indicar de que fuente')
    ->setCellValue('L1', 'Año en que iniciaron actividades')
    ->setCellValue('M1', 'Estado')
    ->setCellValue('N1', 'Municipio');



$iin = 2;

while ($rowIniciativa = $queryIniciativa->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(3)
        ->setCellValue('A' . $iin, $rowIniciativa['idEntrevista'])
        ->setCellValue('B' . $iin, $rowIniciativa['NombreIniciativa'])
        ->setCellValue('C' . $iin, $rowIniciativa['Resumen'])
        ->setCellValue('D' . $iin, $rowIniciativa['Surgimiento'])
        ->setCellValue('E' . $iin, $rowIniciativa['historico'])
        ->setCellValue('F' . $iin, $rowIniciativa['principalActor'])
        ->setCellValue('G' . $iin, $rowIniciativa['proposito'])
        ->setCellValue('H' . $iin, $rowIniciativa['problemas'])
        ->setCellValue('I' . $iin, $rowIniciativa['soluciiones'])
        ->setCellValue('J' . $iin, $rowIniciativa['retos'])
        ->setCellValue('k' . $iin, $rowIniciativa['financiamiento'])
        ->setCellValue('L' . $iin, $rowIniciativa['AnoInicio'])
        ->setCellValue('M' . $iin, $rowIniciativa['estados'])
        ->setCellValue('N' . $iin, $rowIniciativa['municipio']);

    $iin++;
}