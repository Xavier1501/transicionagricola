<?php


/*creamos consultas*/

$sqlRedes = "
select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
 care.FortalezaTrabajo,
 care.ObstaculosTransicion,
 care.RelacionTrabajo,
 care.TransicionEscala 
from co_agr_entrevista cae 
 left join co_agr_redes care on cae.idEntrevista = care.idEntrevista 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$queryRedes = $conexion->query($sqlRedes);

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(6);
$objPHPExcel->getActiveSheet()->setTitle('Tejiendo Redes');


$objPHPExcel->setActiveSheetIndex(6)
    ->setCellValue('A1', 'Numero')
    ->setCellValue('B1', '¿Con quién se relaciona para realizar su trabajo, desde un enfoque agroecológico? (por ejemplo: otros agricultores/as, otras organizaciones, escuelas o instancias de gobierno)')
    ->setCellValue('C1', '¿Qué propondrían para fortalecer su trabajo en torno a la agroecología?')
    ->setCellValue('D1', 'Qué se podría hacer para una transición agroecológica a escala de país/estado/municipio?')
    ->setCellValue('E1', '¿Cuáles son los obstáculos para lograr una transición agroecológica nacional?');


$r = 2;
while ($rowRedes = $queryRedes->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(6)
        ->setCellValue('A' . $r, $rowRedes['idEntrevista'])
        ->setCellValue('B' . $r, $rowRedes['RelacionTrabajo'])
        ->setCellValue('C' . $r, $rowRedes['FortalezaTrabajo'])
        ->setCellValue('D' . $r, $rowRedes['TransicionEscala'])
        ->setCellValue('E' . $r, $rowRedes['ObstaculosTransicion']);
    $r++;
}