<?php


/*creamos consultas*/

$sqlRedes = "
select  
 distinct cae.idEntrevista ,
 cai.idIniciativa,
 cafo.ApoyoColectivo,
 cai.NombreIniciativa,
 cafo.Fortaleza,
 cafo.Capacitacion,
 cafo.si_Programas,
 cafo.cuales_programas,
 cafo.si_taller,
 cafo.Cuales_taller,
 cafo.PropuestaEductiva,
 cafo.Capacitacion,
 cafo.TemasApoyo 
from co_agr_entrevista cae 
left join co_agr_formacion cafo on cae.idEntrevista = cafo.idEntrevista 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$queryRedes = $conexion->query($sqlRedes);

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(7);
$objPHPExcel->getActiveSheet()->setTitle('Formación');


$objPHPExcel->setActiveSheetIndex(7)
    ->setCellValue('A1', 'Numero')
    ->setCellValue('B1', '¿En qué temas considera que su colectivo puede apoyar para formar otros grupos?')
    ->setCellValue('C1', '¿Qué temas de formación considera que son importantes para fortalecer el trabajo de su colectivo?')
    ->setCellValue('D1', '¿Conoce los programas educativos que se implementan en las universidades públicas, los bachilleratos y otras organizaciones educativas vinculadas con la agroecología?')
    ->setCellValue('E1', 'Cuáles')
    ->setCellValue('F1', '¿Ha tomado un curso o taller sobre agroecología?')
    ->setCellValue('G1', '¿En dónde y quién lo impartió?')
    ->setCellValue('H1', '¿Qué propuestas educativas propondría para el fomento de la agroecología?')
    ->setCellValue('I1', '¿Qué temas considera que su organización o colectivo puede apoyar en la capacitación de otros grupos?')
    ->setCellValue('J1', '¿Qué temas de capacitación considera que son importantes para fortalecer el trabajo de su organización o colectivo?');



$r = 2;
while ($rowRedes = $queryRedes->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(7)
        ->setCellValue('A' . $r, $rowRedes['idEntrevista'])
        ->setCellValue('B' . $r, $rowRedes['ApoyoColectivo'])
        ->setCellValue('C' . $r, $rowRedes['Fortaleza'])
        ->setCellValue('D' . $r, $rowRedes['si_Programas'])
        ->setCellValue('E' . $r, $rowRedes['cuales_programas'])
        ->setCellValue('F' . $r, $rowRedes['si_taller'])
        ->setCellValue('G' . $r, $rowRedes['Cuales_taller'])
        ->setCellValue('H' . $r, $rowRedes['PropuestaEductiva'])
        ->setCellValue('I' . $r, $rowRedes['TemasApoyo'])
        ->setCellValue('J' . $r, $rowRedes['Capacitacion']);
    $r++;
}
