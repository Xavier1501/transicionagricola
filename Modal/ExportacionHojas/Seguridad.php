<?php


/*creamos consultas*/

$sqlSeguridad= "
select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
 casg.Opciones 
from co_agr_entrevista cae 
 left join co_agr_seguridad casg on cae.idEntrevista = casg.idEntrevista 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$querySeguridad = $conexion->query($sqlSeguridad);

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(8);
$objPHPExcel->getActiveSheet()->setTitle('Seguridad');

$objPHPExcel->setActiveSheetIndex(8)
    ->setCellValue('A1', 'Numero')
    ->setCellValue('B1', '¿El contexto de seguridad en su región permite que se lleven a cabo intercambios y/o visitas de otras experiencias?');

$Seg = 2;
while ($rowSegurida = $querySeguridad->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(8)
        ->setCellValue('A' . $Seg, $rowSegurida['idEntrevista'])
        ->setCellValue('B' . $Seg, $rowSegurida['Opciones']);
    $Seg++;
}
