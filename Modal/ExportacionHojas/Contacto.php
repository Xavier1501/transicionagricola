<?php
$sqlContacto= "
select  
 distinct cae.idEntrevista,
 cae.NombreRepresentante,
 cai.idIniciativa,
 cacnt.Nombre,
 cacnt.Correo,
 cacnt.Telefono 
from co_agr_entrevista cae 
left join co_agr_contacto cacnt on cae.idEntrevista = cacnt.idEntrevista 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc
";

$queryContacto = $conexion->query($sqlContacto);

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(9);
$objPHPExcel->getActiveSheet()->setTitle('Contacto');

$objPHPExcel->setActiveSheetIndex(9)
    ->setCellValue('A1', 'Numero')
    ->setCellValue('B1', 'Nombre Representante')
    ->setCellValue('C1', 'Nombre Contacto')
    ->setCellValue('D1', 'Correo Contacto')
    ->setCellValue('F1', 'Telefono Contacto');


$contacto = 2;
while ($rowcontacto = $queryContacto->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(9)
        ->setCellValue('A' . $contacto, $rowcontacto['idEntrevista'])
        ->setCellValue('B' . $contacto, $rowcontacto['NombreRepresentante'])
        ->setCellValue('C' . $contacto, $rowcontacto['Nombre'])
        ->setCellValue('D' . $contacto, $rowcontacto['Correo'])
        ->setCellValue('F' . $contacto, $rowcontacto['Telefono']);
    $contacto++;
}
