<?php
/*creamos consultas*/

$sql4= "
select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
 cad.AcomProyectos,
 cad.Desarollo, 
 cad.OrganizacionAgriculturales,
 cad.ProgramaGobierno,
 cad.NucleoAgrario,
 cad.OtroDesaLocal
from co_agr_entrevista cae 
 LEFT JOIN co_agr_agricultura_urbana c ON cae.idEntrevista = c.idEntrevistaFK 
 LEFT JOIN co_agr_desarollolocal cad ON cae.idEntrevista = cad.idAreaTrabajo
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";
$query4 = $conexion->query($sql4);
$cuatro= 3;
while ($row4= $query4->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('Z' . $cuatro, ($row4['AcomProyectos'] === '0')?'NO':'SI')
        ->setCellValue('AA' . $cuatro, ($row4['Desarollo'] === '0')?'NO':'SI')
        ->setCellValue('AB' . $cuatro, ($row4['OrganizacionAgriculturales'] === '0')?'NO':'SI')
        ->setCellValue('AC' . $cuatro, ($row4['ProgramaGobierno'] === '0')?'NO':'SI')
        ->setCellValue('AD' . $cuatro, ($row4['NucleoAgrario'] === '0')?'NO':'SI')
        ->setCellValue('AE' . $cuatro, $row4['OtroDesaLocal'] );
    $cuatro++;
}

