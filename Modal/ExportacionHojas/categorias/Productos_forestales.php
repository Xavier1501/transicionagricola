<?php
$sql13 = "
 select  
 distinct cae.idEntrevista,
  cai.idIniciativa,
  capf.ProductoMaderable,
  capf.ProductoNoMaderable,
  capf.OtroProductoForestales 
 from co_agr_entrevista cae
 LEFT JOIN co_agr_productosforestales capf ON cae.idEntrevista = capf.idAreaTrabajo 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";
$query13 = $conexion->query($sql13);
$trece = 3;
while ($row13 = $query13->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('CP' . $trece, ($row13['ProductoMaderable'] === '0')? 'NO':'SI')
        ->setCellValue('CQ' . $trece, ($row13['ProductoNoMaderable'] === '0')? 'NO':'SI')
       ->setCellValue('CR' . $trece, $row13['OtroProductoForestales']);
    $trece++;
}
