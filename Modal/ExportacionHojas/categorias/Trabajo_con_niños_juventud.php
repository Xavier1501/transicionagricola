<?php
/*creamos consultas*/
$sql8= "
  select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
catnj.HuertoEscolar,
 catnj.EducacionAlimenticia,
 catnj.OtroTNJ
from co_agr_entrevista cae  
 LEFT JOIN co_agr_trabajoninojuventud catnj ON cae.idEntrevista = catnj.idAreaTrabajo
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$query8 = $conexion->query($sql8);
$ocho= 3;
while ($row8= $query8->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('BI' . $ocho, ($row8['HuertoEscolar'] === '0')?'NO': 'SI')
        ->setCellValue('BJ' . $ocho, ($row8['EducacionAlimenticia'] === '0')?'NO': 'SI')
        ->setCellValue('BK' . $ocho, $row8['OtroTNJ'] );
    $ocho++;
}