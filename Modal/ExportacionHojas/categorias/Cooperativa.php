<?php
/*creamos consultas*/
$sql21= "
 select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
 cacc.Consumo,
 cacc.Produccion,
 cacc.OtroCooperativaColectivo 
  from co_agr_entrevista cae 
 LEFT join co_agr_cooperativa_colectivo cacc on cae.idEntrevista = cacc.idTipoColectivo
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

 
";

$query21 = $conexion->query($sql21);
$veinteuno= 3;
while ($row21= $query21->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(2)
        ->setCellValue('T' . $veinteuno, ($row21['Consumo'] === '0')?'NO':'SI')
        ->setCellValue('u' . $veinteuno, ($row21['Produccion'] === '0')?'NO':'SI')
        ->setCellValue('v' . $veinteuno, $row21['OtroCooperativaColectivo']);
    $veinteuno++;
}
