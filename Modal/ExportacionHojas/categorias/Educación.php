<?php
/*creamos consultas*/
$sql6= "
  select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
 caed.EduAmbiental,
 caed.EscuelaCampesina,
 caed.EscuelaIndigena,
 caed.FormacionTecnico,
 caed.ComuniAprende,
 caed.Basicas,
 caed.MediaSuperior,
 caed.Superior,
 caed.IniciativaCampesina,
 caed.OtroEcudacion
from co_agr_entrevista cae  
 LEFT JOIN co_agr_educacion caed ON cae.idEntrevista = caed.idAreaTrabajo
  left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$query6 = $conexion->query($sql6);
$seis= 3;
while ($row6= $query6->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('AV' . $seis, ($row6['EduAmbiental'] === '0')?'NO':'SI')
        ->setCellValue('AW' . $seis, ($row6['EscuelaCampesina'] === '0')?'NO':'SI')
        ->setCellValue('AX' . $seis, ($row6['EscuelaIndigena'] === '0')?'NO':'SI')
        ->setCellValue('AY' . $seis, ($row6['FormacionTecnico'] === '0')?'NO':'SI')
        ->setCellValue('AZ' . $seis, ($row6['ComuniAprende'] === '0')?'NO':'SI')
        ->setCellValue('BA' . $seis, ($row6['Basicas'] === '0')?'NO':'SI')
        ->setCellValue('BB' . $seis, ($row6['MediaSuperior'] === '0')?'NO':'SI')
        ->setCellValue('BC' . $seis, ($row6['Superior'] === '0')?'NO':'SI')
        ->setCellValue('BD' . $seis, ($row6['IniciativaCampesina'] === '0')?'NO':'SI')
        ->setCellValue('BE' . $seis, $row6['OtroEcudacion']);
    $seis++;
}