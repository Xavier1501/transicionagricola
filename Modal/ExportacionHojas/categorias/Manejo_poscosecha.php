<?php
/*creamos consultas*/
$sql10= "
   select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
  caampc.Almacenamiento,
 caampc.Comercializacion,
 caampc.ProcesamientoTransfo,
 caampc.OtroPosCos
 from co_agr_entrevista cae
 LEFT JOIN co_agr_manejoposcosecha caampc ON cae.idEntrevista = caampc.idAreaTrabajo
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$query10 = $conexion->query($sql10);
$diez= 3;
while ($row10= $query10->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('BZ' . $diez, ($row10['Almacenamiento'] === '0')? 'NO':'SI')
        ->setCellValue('CA' . $diez, ($row10['Comercializacion'] === '0')? 'NO':'SI')
        ->setCellValue('CB' . $diez, ($row10['ProcesamientoTransfo'] === '0')? 'NO':'SI')
        ->setCellValue('CC' . $diez, $row10['OtroPosCos']);
    $diez++;
}

