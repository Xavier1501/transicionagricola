<?php
/*creamos consultas*/

$sql2= "

select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
 ca.Homepatia,
 ca.Medicina,
 ca.PreparadosBotanicos,
 ca.RemedioCasero,
 ca.NutricionAgri,
 ca.OtrosaludPlantas
from co_agr_entrevista cae 
 LEFT JOIN co_agr_saludplantasmedicinales ca ON cae.idEntrevista = ca.idAreaTrabajo
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc
";
$query2= $conexion->query($sql2);



$dos = 3;

while ($row2 = $query2->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('K' . $dos, ($row2['Homepatia'] === '0')?'NO':'SI')
        ->setCellValue('L' . $dos, ($row2['Medicina'] === '0')?'NO':'SI')
        ->setCellValue('M' . $dos, ($row2['PreparadosBotanicos'] === '0')?'NO':'SI')
        ->setCellValue('N' . $dos, ($row2['RemedioCasero'] === '0')?'NO':'SI')
        ->setCellValue('O' . $dos, ($row2['NutricionAgri'] === '0')?'NO':'SI')
        ->setCellValue('P' . $dos, $row2['OtrosaludPlantas'] );
    $dos++;
}
