<?php
/*creamos consultas*/
$sql16= "
  select  
 distinct cae.idEntrevista, 
 cai.idIniciativa,
 can.bienesComunales,
 can.Ejido 
  from co_agr_entrevista cae 
 LEFT join co_agr_nucleoagrario can ON cae.idEntrevista = can.IdTipoOrganizacion 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$query16 = $conexion->query($sql16);
$diezseis= 3;
while ($row16= $query16->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(2)
        ->setCellValue('A' . $diezseis, $row16['idEntrevista'])
        ->setCellValue('B' . $diezseis, ($row16['bienesComunales'] === '0')?'NO':'SI')
        ->setCellValue('C' . $diezseis, ($row16['Ejido'] === '0')?'NO':'SI');
        
    $diezseis++;
}
