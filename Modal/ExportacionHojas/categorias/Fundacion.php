<?php
/*creamos consultas*/
$sql17= "
  select  
 distinct cae.idEntrevista, 
 cai.idIniciativa,
 caf.Nacional,
 caf.Internacional 
  from co_agr_entrevista cae 
 LEFT join co_agr_fundacion caf ON cae.idEntrevista = caf.IdTipoOrganizacion 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$query17 = $conexion->query($sql17);
$diezsiete= 3;
while ($row17= $query17->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(2)
        ->setCellValue('D' . $diezsiete, ($row17['Nacional'] === '0')?'NO':'SI')
        ->setCellValue('E' . $diezsiete, ($row17['Internacional'] === '0')?'NO':'SI');

    $diezsiete++;
}
