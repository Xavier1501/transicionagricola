<?php
/*creamos consultas*/
$sql24= "
select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
  cat.PorgramaExtensionUniversitaria,
  cat.Otro 
  from co_agr_entrevista cae 
 LEFT join co_agr_tipocolectivoorganizacion cat  ON cae.idEntrevista = cat.idEntrevista 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

 
";

$query24 = $conexion->query($sql24);
$veintacuatro= 3;
while ($row24= $query24->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(2)
        ->setCellValue('AE' . $veintacuatro, ($row24['PorgramaExtensionUniversitaria'] === '0')?'NO':'SI')
        ->setCellValue('AF' . $veintacuatro, ($row24['Otro'] === '0')?'NO':'SI');
    $veintacuatro++;
}