<?php
/*creamos consultas*/
$sql9= "
   select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
 camch.AlmacenAguaLluvia,
 camch.EnfoqueMicrocuenca,
 camch.ImplementacionEcotecnologicas,
 camch.ManejoComuniAgua,
 camch.RiesgoDrenaje,
 camch.TratamientoAgua,
 camch.OtroManejoH 
 from co_agr_entrevista cae
 LEFT JOIN co_agr_manejorecursoshidricos camch ON cae.idEntrevista = camch.idAreaTrabajo
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$query9 = $conexion->query($sql9);
$nueve= 3;
while ($row9= $query9->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('BS' . $nueve, ($row9['AlmacenAguaLluvia'] === '0')?'NO': 'SI')
        ->setCellValue('BT' . $nueve, ($row9['EnfoqueMicrocuenca'] === '0')?'NO': 'SI')
        ->setCellValue('BU' . $nueve, ($row9['ImplementacionEcotecnologicas'] === '0')?'NO': 'SI')
        ->setCellValue('BV' . $nueve, ($row9['ManejoComuniAgua'] === '0')?'NO': 'SI')
        ->setCellValue('BW' . $nueve, ($row9['RiesgoDrenaje'] === '0')?'NO': 'SI')
        ->setCellValue('BX' . $nueve, ($row9['TratamientoAgua'] === '0')?'NO': 'SI')
        ->setCellValue('BY' . $nueve, $row9['OtroManejoH'] );


    $nueve++;
}
