<?php
/*creamos consultas*/

$sqlurbana= "

select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
 c.Azotea,
 c.Balcon,
 c.Parques,
 c.Traspatio,
 c.Hcomunitarios,
 c.Hempresas,
 c.HInstitucionales,
 c.HSociales,
 c.HOtros
from co_agr_entrevista cae 
LEFT JOIN co_agr_agricultura_urbana c ON cae.idEntrevista = c.idEntrevistaFK 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";
$queryurbana = $conexion->query($sqlurbana);



$urbana = 3;

while ($rowUrbana = $queryurbana->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('A' . $urbana, $rowUrbana['idEntrevista'])
        ->setCellValue('B' . $urbana, ($rowUrbana['Azotea'] === '0')?'NO': 'SI')
        ->setCellValue('C' . $urbana, ($rowUrbana['Balcon'] === '0')? 'NO':'SI')
        ->setCellValue('D' . $urbana, ($rowUrbana['Traspatio'] === '0')?'NO':'SI')
        ->setCellValue('E' . $urbana, ($rowUrbana['Parques'] === '0')?'NO':'SI')
        ->setCellValue('F' . $urbana, ($rowUrbana['Hcomunitarios'] === '0')?'NO':'SI')
        ->setCellValue('G' . $urbana, ($rowUrbana['HSociales'] === '0')?'NO':'SI')
        ->setCellValue('H' . $urbana, ($rowUrbana['HInstitucionales'] === '0')?'NO':'SI')
        ->setCellValue('I' . $urbana, ($rowUrbana['Hempresas'] === '0')?'NO':'SI')
        ->setCellValue('J' . $urbana, $rowUrbana['HOtros']);
    $urbana++;
}
