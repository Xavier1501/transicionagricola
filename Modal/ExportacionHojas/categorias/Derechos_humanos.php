<?php
/*creamos consultas*/
$sql7= "
   select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
cadh.DerechosHumanos,
 cadh.DerechosAmbientales,
 cadh.DerechosEconomicos
from co_agr_entrevista cae  
 LEFT JOIN co_agr_derechos_humanos cadh ON cae.idEntrevista = cadh.idAreaTrabajo 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$query7 = $conexion->query($sql7);
$siete= 3;
while ($row7= $query7->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('BF' . $siete, ($row7['DerechosHumanos'] === '0')?'NO': 'SI')
        ->setCellValue('BG' . $siete, ($row7['DerechosAmbientales'] === '0')?'NO': 'SI')
        ->setCellValue('BH' . $siete, ($row7['DerechosEconomicos'] === '0')?'NO': 'SI');
    $siete++;
}
