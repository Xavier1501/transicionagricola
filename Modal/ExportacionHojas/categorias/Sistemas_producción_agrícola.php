<?php
$sql15 = "
 select  
 distinct cae.idEntrevista,
  cai.idIniciativa,
  caspag.Permacultura,
  caspag.AgriculturaBiodinamica,
  caspag.AgriculturaNatural,
  caspag.AgriculturaOrganica,
  caspag.Agrohomeopatia,
  caspag.TransicionSustiInsumo,
  caspag.SustitucionAgroquimica,
  caspag.ManejoBioologico,
  caspag.ManejoIntegrado,
  caspag.AbonoVerde,
  caspag.BarbechoDescanso,
  caspag.HarinaRoca,
  caspag.Cobertura,
  caspag.Compostaje,
  caspag.MejoraFertilidad,
  caspag.ProduccionBiofertilizante,
  caspag.ProduccionAbonoSolido,
  caspag.PracticaConservacionSuelo,
  caspag.SiembraDirectaCultivo,
  caspag.SiembraCultivoAnimal,
  caspag.SiembraContivoConRotu,
  caspag.SiembraDirecta,
  caspag.Cereales,
  caspag.CultivoAsociado,
  caspag.Forraje,
  caspag.Frutales,
  caspag.Hortaliza,
  caspag.Leguminosa,
  caspag.Medicinales,
  caspag.Oleaginosa,
  caspag.Textil,
  caspag.OtroAgricola
  from co_agr_entrevista cae 
 LEFT JOIN co_agr_sistemaproduccionagricola caspag ON cae.idEntrevista = caspag.idAreaTrabajo
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";
$query15 = $conexion->query($sql15);
$quince = 3;
while ($row15 = $query15->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('DN' . $quince, ($row15['Permacultura'] === '0') ? 'NO' : 'SI')
        ->setCellValue('DO' . $quince, ($row15['AgriculturaBiodinamica'] === '0') ? 'NO' : 'SI')
        ->setCellValue('DP' . $quince, ($row15['AgriculturaNatural'] === '0') ? 'NO' : 'SI')
        ->setCellValue('DR' . $quince, ($row15['AgriculturaOrganica'] === '0') ? 'NO' : 'SI')
        ->setCellValue('DS' . $quince, ($row15['Agrohomeopatia'] === '0') ? 'NO' : 'SI')
        ->setCellValue('DT' . $quince, ($row15['TransicionSustiInsumo'] === '0') ? 'NO' : 'SI')
        ->setCellValue('DU' . $quince, ($row15['SustitucionAgroquimica'] === '0') ? 'NO' : 'SI')
        ->setCellValue('DV' . $quince, ($row15['ManejoBioologico'] === '0') ? 'NO' : 'SI')
        ->setCellValue('DW' . $quince, ($row15['ManejoIntegrado'] === '0') ? 'NO' : 'SI')
        ->setCellValue('DX' . $quince, ($row15['AbonoVerde'] === '0') ? 'NO' : 'SI')
        ->setCellValue('DY' . $quince, ($row15['BarbechoDescanso'] === '0') ? 'NO' : 'SI')
        ->setCellValue('DZ' . $quince, ($row15['HarinaRoca'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EA' . $quince, ($row15['Cobertura'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EB' . $quince, ($row15['Compostaje'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EC' . $quince, ($row15['MejoraFertilidad'] === '0') ? 'NO' : 'SI')
        ->setCellValue('ED' . $quince, ($row15['ProduccionBiofertilizante'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EE' . $quince, ($row15['ProduccionAbonoSolido'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EF' . $quince, ($row15['PracticaConservacionSuelo'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EG' . $quince, ($row15['SiembraDirectaCultivo'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EH' . $quince, ($row15['SiembraCultivoAnimal'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EI' . $quince, ($row15['SiembraContivoConRotu'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EK' . $quince, ($row15['SiembraDirecta'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EL' . $quince, ($row15['Cereales'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EM' . $quince, ($row15['CultivoAsociado'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EN' . $quince, ($row15['Forraje'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EO' . $quince, ($row15['Frutales'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EP' . $quince, ($row15['Hortaliza'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EQ' . $quince, ($row15['Leguminosa'] === '0') ? 'NO' : 'SI')
        ->setCellValue('ER' . $quince, ($row15['Medicinales'] === '0') ? 'NO' : 'SI')
        ->setCellValue('ES' . $quince, ($row15['Oleaginosa'] === '0') ? 'NO' : 'SI')
        ->setCellValue('ET' . $quince, ($row15['Textil'] === '0') ? 'NO' : 'SI')
        ->setCellValue('EU' . $quince, $row15['OtroAgricola']);

    $quince++;
}