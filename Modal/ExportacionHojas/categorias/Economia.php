<?php
/*creamos consultas*/
$sql5= "
 select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
 caec.AutoConsumo,
 caec.CertificaParticipativa,
 caec.CertificacionTerceraParte,
 caec.ComercioLocal,
 caec.ComercioRegional,
 caec.Consumo,
 caec.Semillas,
 caec.Insumos,
 caec.Credito,
 caec.Servicios,
 caec.EmpresaProducInsumo,
 caec.IniciativaIntercambio,
 caec.MercadoAlternativo,
 caec.SistemaGesta,
 caec.TianguisOrganico,
 caec.OtroEconomia
from co_agr_entrevista cae  
 LEFT JOIN co_agr_economia caec ON cae.idEntrevista = caec.idAreaTrabajo
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$query5 = $conexion->query($sql5);
$cinco= 3;
while ($row5= $query5->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('AF' . $cinco, ($row5['AutoConsumo'] === '0')?'NO':'SI')
        ->setCellValue('AG' . $cinco, ($row5['CertificaParticipativa'] === '0')?'NO':'SI')
        ->setCellValue('AH' . $cinco, ($row5['CertificacionTerceraParte'] === '0')?'NO':'SI')
        ->setCellValue('AI' . $cinco, ($row5['ComercioLocal'] === '0')?'NO':'SI')
        ->setCellValue('AJ' . $cinco, ($row5['ComercioRegional'] === '0')?'NO':'SI')
        ->setCellValue('AK' . $cinco, ($row5['Consumo'] === '0')?'NO':'SI')
        ->setCellValue('AL' . $cinco, ($row5['Semillas'] === '0')?'NO':'SI')
        ->setCellValue('AM' . $cinco, ($row5['Insumos'] === '0')?'NO':'SI')
        ->setCellValue('AN' . $cinco, ($row5['Credito'] === '0')?'NO':'SI')
        ->setCellValue('AO' . $cinco, ($row5['Servicios'] === '0')?'NO':'SI')
        ->setCellValue('AP' . $cinco, ($row5['EmpresaProducInsumo'] === '0')?'NO':'SI')
        ->setCellValue('AQ' . $cinco, ($row5['IniciativaIntercambio'] === '0')?'NO':'SI')
        ->setCellValue('AR' . $cinco, ($row5['MercadoAlternativo'] === '0')?'NO':'SI')
        ->setCellValue('AS' . $cinco, ($row5['SistemaGesta'] === '0')?'NO':'SI')
        ->setCellValue('AT' . $cinco, ($row5['TianguisOrganico'] === '0')?'NO':'SI')
        ->setCellValue('AU' . $cinco, $row5['OtroEconomia'] );
    $cinco++;
}
