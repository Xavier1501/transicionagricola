<?php
/*creamos consultas*/
$sql23= "
select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
  cago.AsesorTecnico,
  cago.Consumidor,
  cago.investigacion,
  cago.Estudiantil,
  cago.Eclesiastico,
  cago.OtroGrupoOrganizacion
  from co_agr_entrevista cae 
 LEFT join co_agr_grupos_organizados cago  ON cae.idEntrevista = cago.idTipoColectivo 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

 
";

$query23 = $conexion->query($sql23);
$veintitres= 3;
while ($row23= $query23->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(2)
        ->setCellValue('Y' . $veintitres, ($row23['AsesorTecnico'] === '0')?'NO':'SI')
        ->setCellValue('Z' . $veintitres, ($row23['Consumidor'] === '0')?'NO':'SI')
        ->setCellValue('AA' . $veintitres, ($row23['investigacion'] === '0')?'NO':'SI')
        ->setCellValue('AB' . $veintitres, ($row23['Estudiantil'] === '0')?'NO':'SI')
        ->setCellValue('AC' . $veintitres, ($row23['Eclesiastico'] === '0')?'NO':'SI')
        ->setCellValue('AD' . $veintitres, $row23['OtroGrupoOrganizacion'] );
    $veintitres++;
}