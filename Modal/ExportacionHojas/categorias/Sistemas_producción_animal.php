<?php
$sql14 = "
 select  
 distinct cae.idEntrevista,
  cai.idIniciativa,
  caspa.AbejaEuropeas,
  caspa.AbejasNativas,
  caspa.Acuacultura,
  caspa.Aves,
  caspa.Bovinos,
  caspa.Conejo,
  caspa.Caprinos,
  caspa.Cuniculturas,
  caspa.Equinos,
  caspa.Lombricultura,
  caspa.Ovinos,
  caspa.Pesca,
  caspa.Porcinos,
  caspa.MejoramientoGenetico,
  caspa.NutricionAnimal,
  caspa.PracticasSalud,
  caspa.VeterinariaNatural,
  caspa.Homeopatia,
  caspa.OtroProduccion,
  caspa.Construccio 
  from co_agr_entrevista cae 
 LEFT JOIN co_agr_sistemaproduccionanimal caspa ON cae.idEntrevista = caspa.idAreaTrabajo 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";
$query14 = $conexion->query($sql14);
$catorce = 3;
while ($row14 = $query14->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('CS' . $catorce, ($row14['AbejaEuropeas'] === '0')? 'NO':'SI')
        ->setCellValue('CT' . $catorce, ($row14['AbejasNativas'] === '0')? 'NO':'SI')
        ->setCellValue('CU' . $catorce, ($row14['Acuacultura'] === '0')? 'NO':'SI')
        ->setCellValue('CV' . $catorce, ($row14['Aves'] === '0')? 'NO':'SI')
        ->setCellValue('CW' . $catorce, ($row14['Bovinos'] === '0')? 'NO':'SI')
        ->setCellValue('CX' . $catorce, ($row14['Conejo'] === '0')? 'NO':'SI')
        ->setCellValue('CY' . $catorce, ($row14['Caprinos'] === '0')? 'NO':'SI')
        ->setCellValue('CZ' . $catorce, ($row14['Cuniculturas'] === '0')? 'NO':'SI')
        ->setCellValue('DA' . $catorce, ($row14['Equinos'] === '0')? 'NO':'SI')
        ->setCellValue('DB' . $catorce, ($row14['Lombricultura'] === '0')? 'NO':'SI')
        ->setCellValue('DC' . $catorce, ($row14['Ovinos'] === '0')? 'NO':'SI')
        ->setCellValue('DD' . $catorce, ($row14['Pesca'] === '0')? 'NO':'SI')
        ->setCellValue('DE' . $catorce, ($row14['Porcinos'] === '0')? 'NO':'SI')
        ->setCellValue('DF' . $catorce, ($row14['MejoramientoGenetico'] === '0')? 'NO':'SI')
        ->setCellValue('DG' . $catorce, ($row14['NutricionAnimal'] === '0')? 'NO':'SI')
        ->setCellValue('DH' . $catorce, ($row14['PracticasSalud'] === '0')? 'NO':'SI')
        ->setCellValue('DI' . $catorce, ($row14['VeterinariaNatural'] === '0')? 'NO':'SI')
        ->setCellValue('DK' . $catorce, ($row14['Homeopatia'] === '0')? 'NO':'SI')
        ->setCellValue('DL' . $catorce, $row14['OtroProduccion'] )
        ->setCellValue('DM' . $catorce, ($row14['Construccio'] === '0')? 'NO':'SI');
    $catorce++;
}