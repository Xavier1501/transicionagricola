<?php
$sql12 = "
 select  
 distinct cae.idEntrevista,
  cai.idIniciativa,
  casaf.AreaDestinoConservaSilvo,
  casaf.SistemaAgroforestal,
  casaf.BosqueComestible,
  casaf.CercasVivas,
  casaf.BarreraRompeVientos,
  casaf.OtrosistemaAgro 
 from co_agr_entrevista cae
 LEFT JOIN co_agr_sistemasagroforestales casaf ON cae.idEntrevista = casaf.idAreaTrabajo
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
 where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc
";
$query12 = $conexion->query($sql12);
$doce = 3;
while ($row12 = $query12->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('CJ' . $doce, ($row12['AreaDestinoConservaSilvo'] === '0')? 'NO':'SI')
        ->setCellValue('CK' . $doce, ($row12['SistemaAgroforestal'] === '0')? 'NO':'SI')
        ->setCellValue('CL' . $doce, ($row12['BosqueComestible'] === '0')? 'NO':'SI')
        ->setCellValue('CM' . $doce, ($row12['CercasVivas'] === '0')? 'NO':'SI')
        ->setCellValue('CN' . $doce, ($row12['BarreraRompeVientos'] === '0')? 'NO':'SI')
        ->setCellValue('CO' . $doce, $row12['OtrosistemaAgro']);
    $doce++;
}