<?php
/*creamos consultas*/
$sql20= "
  select  
 distinct cae.idEntrevista, 
 cai.idIniciativa,
 catp.OrganizacionCampesina,
 catp.SociedadCivil,
 catp.OtroTipoOrganizacion
  from co_agr_entrevista cae 
 LEFT join co_agr_tipoorganizacion catp ON cae.idEntrevista = catp.IdTipoOrganizacion 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

 
";

$query20 = $conexion->query($sql20);
$veinte= 3;
while ($row20= $query20->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(2)
        ->setCellValue('Q' . $veinte, ($row20['OrganizacionCampesina'] === '0')?'NO':'SI')
        ->setCellValue('R' . $veinte, ($row20['SociedadCivil'] === '0')?'NO':'SI')
        ->setCellValue('S' . $veinte, $row20['OtroTipoOrganizacion']);
    $veinte++;
}
