<?php
/*creamos consultas*/
$sql19= "
  select  
 distinct cae.idEntrevista, 
 cai.idIniciativa,
 caid.EscuelaCampesina,
 caid.EscuelaPrimaria,
 caid.EscuelaSecundaria,
 caid.Bachillerato,
 caid.Universidad,
 caid.UniversidadIntercultural,
 caid.UniversidadAlternativa,
 caid.UniversidadAlternativa 
  from co_agr_entrevista cae 
 LEFT join co_agr_institucioneducativa caid ON cae.idEntrevista = caid.IdTipoOrganizacion 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

 
";

$query19 = $conexion->query($sql19);
$dieznueve= 3;
while ($row19= $query19->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(2)
        ->setCellValue('I' . $dieznueve, ($row19['EscuelaCampesina'] === '0')?'NO':'SI')
        ->setCellValue('J' . $dieznueve, ($row19['EscuelaPrimaria'] === '0')?'NO':'SI')
        ->setCellValue('K' . $dieznueve, ($row19['EscuelaSecundaria'] === '0')?'NO':'SI')
        ->setCellValue('L' . $dieznueve, ($row19['Bachillerato'] === '0')?'NO':'SI')
        ->setCellValue('M' . $dieznueve, ($row19['Universidad'] === '0')?'NO':'SI')
        ->setCellValue('N' . $dieznueve, ($row19['UniversidadIntercultural'] === '0')?'NO':'SI')
        ->setCellValue('O' . $dieznueve, ($row19['UniversidadAlternativa'] === '0')?'NO':'SI')
        ->setCellValue('P' . $dieznueve, ($row19['UniversidadAlternativa'] === '0')?'NO':'SI');

    $dieznueve++;
}
