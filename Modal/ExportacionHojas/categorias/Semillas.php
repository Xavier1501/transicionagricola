<?php
$sql11 = "
   select  
 distinct cae.idEntrevista,
  cai.idIniciativa,
  cas.AlmacenConservacionSemillas,
  cas.BancosComuniSemilla,
  cas.IntercambioSemilla,
  cas.MejoraPlantaAnimal,
  cas.ProduccionSemilla,
  cas.OtroSemillas 
 from co_agr_entrevista cae
 LEFT JOIN co_agr_semillas cas ON cae.idEntrevista = cas.idAreaTrabajo
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";
$query11 = $conexion->query($sql11);
$once = 3;
while ($row11 = $query11->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('CD' . $once, ($row11['AlmacenConservacionSemillas'] === '0')? 'NO':'SI')
        ->setCellValue('CE' . $once, ($row11['BancosComuniSemilla'] === '0')? 'NO':'SI')
        ->setCellValue('CF' . $once, ($row11['IntercambioSemilla'] === '0')? 'NO':'SI')
        ->setCellValue('CG' . $once, ($row11['MejoraPlantaAnimal'] === '0')? 'NO':'SI')
        ->setCellValue('CH' . $once, ($row11['ProduccionSemilla'] === '0')? 'NO':'SI')
        ->setCellValue('CI' . $once, $row11['OtroSemillas']);
    $once++;
}