<?php
/*creamos consultas*/
$sql9= "
   select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
caiar.Comunicacion,
 caiar.DiagAgroecosistema,
 caiar.Experimentacion,
 caiar.FormacionCapacitacion,
 caiar.Investigacion,
 caiar.OrdenTerritoral,
 caiar.OtroInvestigacionRural 
 from co_agr_entrevista cae
 LEFT JOIN co_agr_investigacionagricolarural caiar ON cae.idEntrevista = caiar.idAreaTrabajo
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$query9 = $conexion->query($sql9);
$nueve= 3;
while ($row9= $query9->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('BL' . $nueve, ($row9['Comunicacion'] === '0')?'NO': 'SI')
        ->setCellValue('BM' . $nueve, ($row9['DiagAgroecosistema'] === '0')?'NO': 'SI')
        ->setCellValue('BN' . $nueve, ($row9['Experimentacion'] === '0')?'NO': 'SI')
        ->setCellValue('BO' . $nueve, ($row9['FormacionCapacitacion'] === '0')?'NO': 'SI')
        ->setCellValue('BP' . $nueve, ($row9['Investigacion'] === '0')?'NO': 'SI')
        ->setCellValue('BQ' . $nueve, ($row9['OrdenTerritoral'] === '0')?'NO': 'SI')
        ->setCellValue('BR' . $nueve, $row9['OtroInvestigacionRural'] );

    $nueve++;
}
