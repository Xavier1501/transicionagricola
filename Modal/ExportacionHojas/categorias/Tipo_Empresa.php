<?php
/*creamos consultas*/
$sql22= "
 select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
 caec.Social,
 caec.Privada 
  from co_agr_entrevista cae 
 LEFT join co_agr_empresa_colectivo caec  ON cae.idEntrevista = caec.idTipoColectivo 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

 
";

$query22 = $conexion->query($sql22);
$veintedos= 3;
while ($row22= $query22->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(2)
        ->setCellValue('w' . $veintedos, ($row22['Social'] === '0')?'NO':'SI')
        ->setCellValue('x' . $veintedos, ($row22['Privada'] === '0')?'NO':'SI');
    $veintedos++;
}
