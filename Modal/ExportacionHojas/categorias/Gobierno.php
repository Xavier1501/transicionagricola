<?php
/*creamos consultas*/
$sql18= "
  select  
 distinct cae.idEntrevista, 
 cai.idIniciativa,
 cag.Municipio,
 cag.Estatal,
 cag.Federal
  from co_agr_entrevista cae 
 LEFT join co_agr_gobierno CAG ON cae.idEntrevista = CAG.IdTipoOrganizacion 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$query18 = $conexion->query($sql18);
$diezocho= 3;
while ($row18= $query18->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(2)
        ->setCellValue('F' . $diezocho, ($row18['Municipio'] === '0')?'NO':'SI')
        ->setCellValue('G' . $diezocho, ($row18['Estatal'] === '0')?'NO':'SI')
        ->setCellValue('H' . $diezocho, ($row18['Federal'] === '0')?'NO':'SI');

    $diezocho++;
}