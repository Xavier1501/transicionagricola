<?php
/*creamos consultas*/

$sql3= "

select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
  caa.Artesania,
 caa.CajaAhorro,
 caa.Turismo,
 caa.Turismo,
 caa.Culturales,
 caa.ComedorComuni,
 caa.ComedorSocial,
 caa.ComedorEscolar ,
 caa.Restaurant,
 caa.OtroActNoAgri
from co_agr_entrevista cae 
 LEFT JOIN co_agr_agricultura_urbana c ON cae.idEntrevista = c.idEntrevistaFK 
 LEFT JOIN co_agr_actividadnoagricola caa ON cae.idEntrevista = caa.idAreaTrabajo
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc


";
$query3 = $conexion->query($sql3);



$tres = 3;

while ($row3 = $query3->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(1)
        ->setCellValue('Q' . $tres, ($row3['Artesania'] === '0')?'NO':'SI')
        ->setCellValue('R' . $tres, ($row3['CajaAhorro'] === '0')?'NO':'SI')
        ->setCellValue('S' . $tres, ($row3['Turismo'] === '0')?'NO':'SI')
        ->setCellValue('T' . $tres, ($row3['Culturales'] === '0')?'NO':'SI')
        ->setCellValue('U' . $tres, ($row3['ComedorComuni'] === '0')?'NO':'SI')
        ->setCellValue('V' . $tres, ($row3['ComedorSocial'] === '0')?'NO':'SI')
        ->setCellValue('W' . $tres, ($row3['ComedorEscolar'] === '0')?'NO':'SI')
        ->setCellValue('X' . $tres, ($row3['Restaurant'] === '0')?'NO':'SI')
        ->setCellValue('Y' . $tres, $row3['OtroActNoAgri']);
    $tres++;
}
