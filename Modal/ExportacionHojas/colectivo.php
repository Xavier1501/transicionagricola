<?php

/*creamos consultas*/

$sqliniciativa = "

";

$queryIniciativa = $conexion->query($sqliniciativa);

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(2);
$objPHPExcel->getActiveSheet()->setTitle('Tipo de colectivo');



$objPHPExcel->getActiveSheet()->mergeCells('B1:C1','Agricultura urbana');
$objPHPExcel->getActiveSheet()->mergeCells('D1:E1','Fundación');
$objPHPExcel->getActiveSheet()->mergeCells('F1:H1','Gobierno');
$objPHPExcel->getActiveSheet()->mergeCells('I1:P1','Instituciones Educativas');
$objPHPExcel->getActiveSheet()->mergeCells('Q1:S1','Tipo de Organización');
$objPHPExcel->getActiveSheet()->mergeCells('T1:V1','Cooperativa');
$objPHPExcel->getActiveSheet()->mergeCells('W1:X1','Tipo de Empresa');
$objPHPExcel->getActiveSheet()->mergeCells('Y1:AD1','Grupos Organizados');

$objPHPExcel->setActiveSheetIndex(2)
    ->setCellValue('B1', 'Núcleo Agrario')
    ->setCellValue('D1', 'Fundación')
    ->setCellValue('F1', 'Gobierno')
    ->setCellValue('I1', 'Instituciones Educativas')
    ->setCellValue('Q1', 'Tipo de Organización')
    ->setCellValue('T1', 'Cooperativa')
    ->setCellValue('W1', 'Tipo de Empresa')
    ->setCellValue('Y1', 'Grupos Organizados')
    ->setCellValue('AE1', 'Programa de Extensión')
    ->setCellValue('AF1', 'otro')

    ->setCellValue('A2', 'Numero')

    ->setCellValue('B2', 'Bienes Comunales')
    ->setCellValue('C2', 'Ejido')

    ->setCellValue('D2', 'Nacional')
    ->setCellValue('E2', 'Internacional')

    ->setCellValue('F2', 'Municipal')
    ->setCellValue('G2', 'Estatal')
    ->setCellValue('H2', 'Federal')

    ->setCellValue('I2', 'Escuela Campesina')
    ->setCellValue('J2', 'Escuela Primaria')
    ->setCellValue('K2', 'Escuela Secundaria')
    ->setCellValue('L2', 'Bachillerato')
    ->setCellValue('M2', 'Universidad')
    ->setCellValue('N2', 'Universidad Intercultural')
    ->setCellValue('O2', 'Universidad Alternativa')
    ->setCellValue('P2', 'Centro de Formación privado')


    ->setCellValue('Q2', 'Organización Campesina')
    ->setCellValue('R2', 'De la Sociedad Civil Organizada')
    ->setCellValue('S2', 'Otro (especificar)')


    ->setCellValue('T2', 'De Consumo')
    ->setCellValue('U2', 'De Producción')
    ->setCellValue('V2', 'Otro (especificar)')


    ->setCellValue('W2', 'Empresa Social')
    ->setCellValue('X2', 'Empresa Privada')

    ->setCellValue('Y2', 'Grupo de Asesores Tecnicos')
    ->setCellValue('Z2', 'Grupo de Consumidores')
    ->setCellValue('AA2', 'Grupo de Investigación')
    ->setCellValue('AB2', 'Grupo Estudiantil')
    ->setCellValue('AC2', 'Grupo Eclesiástico')
    ->setCellValue('AD2', 'Otro (especificar)')


    ->setCellValue('AE2', 'Programa de Extensión')

    ->setCellValue('AF2', 'Otro (especificar)');




include_once 'categorias/Nucleo_Agrario.php';
include_once 'categorias/Fundacion.php';
include_once 'categorias/Gobierno.php';
include_once 'categorias/Instituciones_Educativas.php';
include_once 'categorias/Tipo_Organización.php';
include_once 'categorias/Cooperativa.php';
include_once 'categorias/Tipo_Empresa.php';
include_once 'categorias/Tipo_Empresa.php';
include_once 'categorias/Grupos_Organizados.php';
include_once 'categorias/Programa_Extensión.php';






