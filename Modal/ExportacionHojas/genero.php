<?php


/*creamos consultas*/


$sqlGenero = "
select  
 distinct cae.idEntrevista,
 cai.idIniciativa,  
  cage.NMujeres,
  cage.NHombres,
  cage.NOtrosGeneros,
  cage.IntegranteNinos,
  cage.IntegranteAdolecentes,
  cage.IntegranteAdultos,
  cage.IntegrantesAdultosMayores,
  cage.PorcentajeHombresDirectivos,
  cage.PorcentajeMujeresDirectivos, 
  cage.PorcentajeHombresCoodinados,
  cage.PorcentajeMujeresCoordinados,
  cage.IgualdaSalario,
  cage.IgualdaPorque,
  cage.PoliticaEquidad,
  cage.DerechoMaternal,
  cage.DerechoMaternalPorQue,
  cage.AcosoSexual,
  cage.Discriminacion,
  cage.DiscriminacionPorQue,
  cage.MadreSoltera,
  cage.MujeresParticipanteProyecto,
  cage.MujeresParticipanteProyectoporQue,
  cage.DependenciasEconomicos,
  cage.EnfermosCuidadosEspeciales,
  cage.EnfermosCuidadosEspeciapesPorQue
  from co_agr_entrevista cae 
 LEFT join co_agr_genero cage ON cae.idEntrevista = cage.idAreaTrabajo 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc
";

$queryG = $conexion->query($sqlGenero);


$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(4);
$objPHPExcel->getActiveSheet()->setTitle('Género y agroecología');


$objPHPExcel->getActiveSheet()->mergeCells('B1:D1','¿Número de integrantes de la iniciativa o experiencia agroecológica?');
$objPHPExcel->getActiveSheet()->mergeCells('E1:H1','Integrantes por rango de edades');
$objPHPExcel->getActiveSheet()->mergeCells('I1:L1','Integrantes por rango de edades');
$objPHPExcel->getActiveSheet()->mergeCells('I1:L1','Integrantes por rango de edades');



$objPHPExcel->setActiveSheetIndex(4)
    ->setCellValue('A2', 'Numero')
    ->setCellValue('B1', '¿Número de integrantes de la iniciativa o experiencia agroecológica?')
    ->setCellValue('E1', 'Integrantes por rango de edades')
    ->setCellValue('I1', 'Porcentaje en puestos directivos, de representación, coordinación 
    (que permita evidenciar, aunque sea numéricamente la brecha de género)')
    ->setCellValue('B2', 'Número de mujeres?')
    ->setCellValue('C2', 'Número de hombres')
    ->setCellValue('D2', 'Número en otros géneros (LGBTTTIQ)')
    ->setCellValue('E2', 'Niños')
    ->setCellValue('F2', 'Adolescentes')
    ->setCellValue('G2', 'Adultos')
    ->setCellValue('H2', 'Adultos mayores')
    ->setCellValue('I2', 'Hombres en puesto directivos')
    ->setCellValue('J2', 'Mujeres en puestos directivos')
    ->setCellValue('K2', 'Hombres en puestos de coordinación')
    ->setCellValue('L2', 'Mujeres en puestos de coordinación')
    ->setCellValue('M2', '¿Hay igualdad de salarios para puestos equivalentes?')
    ->setCellValue('N2', 'Por qué')
    ->setCellValue('O2', '¿Tiene una política de equidad de género en su colectivo u organización? Favor de describirla brevemente')
    ->setCellValue('P2', '¿Se respetan los derechos de la maternidad?')
    ->setCellValue('Q2', 'Por qué')
    ->setCellValue('R2', '¿Como se manejan los casos de acoso sexual?')
    ->setCellValue('S2', '¿Se promueve la no discriminación?')
    ->setCellValue('T2', '¿Por que?')
    ->setCellValue('U2', '¿Existe apoyo para madres solteras?')
    ->setCellValue('V2', '¿Sabe acerca de las necesidades de las mujeres que participan en el proyecto?')
    ->setCellValue('W2', '¿Por que?')
    ->setCellValue('X2', '¿Tiene dependientes económicos?')
    ->setCellValue('Y2', '¿Tiene enfermos que requieren cuidados especiales?')
    ->setCellValue('Z2', '¿Cuáles?');





$g = 3;
while ($rowG = $queryG->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(4)
        ->setCellValue('A'.$g, $rowG['idEntrevista'] )
        ->setCellValue('B'.$g, $rowG['NMujeres'] )
        ->setCellValue('C'.$g, $rowG['NHombres'] )
        ->setCellValue('D'.$g, $rowG['NOtrosGeneros'] )
        ->setCellValue('E'.$g, $rowG['IntegranteNinos'] )
        ->setCellValue('F'.$g, $rowG['IntegranteAdolecentes'] )
        ->setCellValue('G'.$g, $rowG['IntegranteAdultos'] )
        ->setCellValue('H'.$g, $rowG['IntegrantesAdultosMayores'] )
        ->setCellValue('I'.$g, $rowG['PorcentajeHombresDirectivos'] )
        ->setCellValue('J'.$g, $rowG['PorcentajeMujeresDirectivos'] )
        ->setCellValue('K'.$g, $rowG['PorcentajeHombresCoodinados'] )
        ->setCellValue('L'.$g, $rowG['PorcentajeMujeresCoordinados'] )
        ->setCellValue('M'.$g, $rowG['IgualdaSalario'] )
        ->setCellValue('N'.$g, $rowG['IgualdaPorque'] )
        ->setCellValue('O'.$g, $rowG['PoliticaEquidad'] )
        ->setCellValue('P'.$g, $rowG['DerechoMaternal'] )
        ->setCellValue('Q'.$g, $rowG['DerechoMaternalPorQue'] )
        ->setCellValue('R'.$g, $rowG['AcosoSexual'] )
        ->setCellValue('S'.$g, $rowG['Discriminacion'] )
        ->setCellValue('T'.$g, $rowG['DiscriminacionPorQue'] )
        ->setCellValue('U'.$g, $rowG['MadreSoltera'] )
        ->setCellValue('V'.$g, $rowG['MujeresParticipanteProyecto']  )
        ->setCellValue('W'.$g, $rowG['MujeresParticipanteProyectoporQue'] )
        ->setCellValue('X'.$g, $rowG['DependenciasEconomicos'] )
        ->setCellValue('Y'.$g, $rowG['EnfermosCuidadosEspeciales'] )
        ->setCellValue('Z'.$g, $rowG['EnfermosCuidadosEspeciapesPorQue']);


    $g++;
}

