<?php


/*creamos consultas*/

$sqlpolitica = "
select  
 distinct cae.idEntrevista,
 cai.idIniciativa,
 capol.PoliticaActualFavor,
 capol.PoliticaActualLimitada,
 capol.PoliticaFaltante 
from co_agr_entrevista cae 
 left join co_agr_politica capol on cae.idEntrevista = capol.idEntrevista 
 left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
where cai.idIniciativa > 0 and cae.bajaLogica = 0 order by cai.idIniciativa asc

";

$queryPolitica = $conexion->query($sqlpolitica);

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(5);
$objPHPExcel->getActiveSheet()->setTitle('Politica');


$objPHPExcel->setActiveSheetIndex(5)
    ->setCellValue('A1', 'Numero')
    ->setCellValue('B1', '¿Qué políticas públicas actuales considera que favorecen el impulso de la agroecología en su región y en el país?')
    ->setCellValue('C1', '¿Qué políticas públicas actuales considera que pueden estar limitando el impulso de la agroecología en su región y en el país?')
    ->setCellValue('D1', '¿Qué faltaría hacer para implementar una política pública para fortalecer la transición agroecológica en el país?');




$ipol = 2;

while ($rowPolitica = $queryPolitica->fetch_array(MYSQLI_ASSOC)) {
    $objPHPExcel->setActiveSheetIndex(5)
        ->setCellValue('A' . $ipol, $rowPolitica['idEntrevista'])
        ->setCellValue('B' . $ipol, $rowPolitica['PoliticaActualFavor'])
        ->setCellValue('C' . $ipol, $rowPolitica['PoliticaActualLimitada'])
        ->setCellValue('D' . $ipol, $rowPolitica['PoliticaFaltante']);
    $ipol++;
}
