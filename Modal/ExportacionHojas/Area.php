<?php

$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setTitle('Área(s) en las que trabajan');



$objPHPExcel->getActiveSheet()->mergeCells('B1:J1','Agricultura urbana');
$objPHPExcel->getActiveSheet()->mergeCells('k1:P1','Salud y plantas medicinales');
$objPHPExcel->getActiveSheet()->mergeCells('Q1:Y1','Actividades no agrícolas');
$objPHPExcel->getActiveSheet()->mergeCells('Z1:AE1','Desarrollo Local');
$objPHPExcel->getActiveSheet()->mergeCells('AF1:AS1','Economia');
$objPHPExcel->getActiveSheet()->mergeCells('AV1:BE1','Educacion');
$objPHPExcel->getActiveSheet()->mergeCells('BF1:BH1','Derechos humanos');
$objPHPExcel->getActiveSheet()->mergeCells('BI1:BK1','Trabajo con niños/juventud');
$objPHPExcel->getActiveSheet()->mergeCells('Bl1:BR1','Investigación agrícola y Extensión rural');
$objPHPExcel->getActiveSheet()->mergeCells('BS1:BY1','Investigación agrícola y Extensión rural');
$objPHPExcel->getActiveSheet()->mergeCells('BZ1:CC1','Manejo poscosecha');
$objPHPExcel->getActiveSheet()->mergeCells('CD1:CI1','Semillas');
$objPHPExcel->getActiveSheet()->mergeCells('CJ1:CO1','Sistemas agroforestales');
$objPHPExcel->getActiveSheet()->mergeCells('CP1:CR1','Productos forestales');
//$objPHPExcel->getActiveSheet()->mergeCells('CS1:CM1','Sistemas de producción animal');

$objPHPExcel->setActiveSheetIndex(1)
    ->setCellValue('B1', 'Agricultura urbana')
    ->setCellValue('k1', 'Salud y plantas medicinales')
    ->setCellValue('Q1', 'Actividades no agrícolas')
    ->setCellValue('Z1', 'Desarrollo Local')
    ->setCellValue('AF1', 'Economia')
    ->setCellValue('AV1', 'Educación')
    ->setCellValue('BF1', 'Derechos humanos')
    ->setCellValue('BI1', 'Trabajo con niños/juventud')
    ->setCellValue('BL1', 'Investigación agrícola y Extensión rural')
    ->setCellValue('BS1', 'Manejo de recursos hídricos')
    ->setCellValue('BZ1', 'Manejo poscosecha')
    ->setCellValue('CD1', 'Semillas')
    ->setCellValue('CJ1', 'Sistemas agroforestales')
    ->setCellValue('CP1', 'Productos forestales')
    ->setCellValue('CS1', 'Sistemas de producción animal')
    ->setCellValue('DN1', 'Sistemas de producción agrícola')
    ->setCellValue('A2', 'Numero')
    ->setCellValue('B2', 'Azotea')
    ->setCellValue('C2', 'Balcón')
    ->setCellValue('D2', 'Traspatio')
    ->setCellValue('E2', 'Parques o espacios públicos')
    ->setCellValue('F2', 'Huertos comunitarios')
    ->setCellValue('G2', 'Huertos sociales')
    ->setCellValue('H2', 'Huertos institucionales')
    ->setCellValue('I2', 'Huertos de empresas')
    ->setCellValue('J2', 'Otros')
    ->setCellValue('K2', 'Homeopatía')
    ->setCellValue('L2', 'Medicina tradicional')
    ->setCellValue('M2', 'Preparados botánicos')
    ->setCellValue('N2', 'Remedios caseros')
    ->setCellValue('O2', 'Nutrición agroecológica')
    ->setCellValue('P2', 'Otro (especificar)')
    ->setCellValue('Q2', 'Artesanías')
    ->setCellValue('R2', 'Cajas de ahorro y crédito')
    ->setCellValue('S2', 'Turismo')
    ->setCellValue('T2', 'Culturales')
    ->setCellValue('U2', 'Comedores comunitarios')
    ->setCellValue('V2', 'Comedores sociales')
    ->setCellValue('W2', 'Comedores escolares')
    ->setCellValue('X2', 'Restaurantes, fondas y/o cocinas que ofrecen comida agroecológica')
    ->setCellValue('Y2', 'Otro (especificar)')
    ->setCellValue('Z2', 'Acompañamiento de proyectos')
    ->setCellValue('AA2', 'Desarrollo de proyectos productivos')
    ->setCellValue('AB2', 'Organización de agricultores/as')
    ->setCellValue('AC2', 'Programas gubernamentales')
    ->setCellValue('AD2', 'Planes de núcleos agrarios (ejidos, bienes comunales)')
    ->setCellValue('AE2', 'Otro (especificar)')
    ->setCellValue('AF2', 'Autoconsumo')
    ->setCellValue('AG2', 'Certificación participativa (Sistema participativo de garantía)')
    ->setCellValue('AH2', 'Certificación de tercera parte')
    ->setCellValue('AI2', 'Comercialización local')
    ->setCellValue('AJ2', 'Comercialización regional')
    ->setCellValue('AK2', 'Consumo')
    ->setCellValue('AL2', 'Semillas')
    ->setCellValue('AM2', 'Insumos')
    ->setCellValue('AN2', 'Crédito')
    ->setCellValue('AO2', 'Servicios')
    ->setCellValue('AP2', 'Empresa productora de insumos')
    ->setCellValue('AQ2', 'Iniciativa de intercambio (trueque)')
    ->setCellValue('AR2', 'Mercado alternativo (agroecológico)')
    ->setCellValue('AS2', 'Sistema de cestas')
    ->setCellValue('AT2', 'Tianguis orgánico')
    ->setCellValue('AU2', 'Otro (especificar)')
    ->setCellValue('AV2', 'Educación ambiental')
    ->setCellValue('AW2', 'Escuela campesina')
    ->setCellValue('AX2', 'Escuela indígena')
    ->setCellValue('AY2', 'Formación de técnicos')
    ->setCellValue('AZ2', 'Comunidad de aprendizaje')
    ->setCellValue('BA2', 'Básica')
    ->setCellValue('BB2', 'Media Superior')
    ->setCellValue('BC2', 'Superior')
    ->setCellValue('BD2', 'Iniciativa de campesino a campesino')
    ->setCellValue('BE2', 'Otro (especificar)')
    ->setCellValue('BG2', 'Derechos ambientales')
    ->setCellValue('BH2', 'Derechos económicos y de género')
    ->setCellValue('BF2', 'Derechos humanos')
    ->setCellValue('BI2', 'Huertos-parcelas escolares')
    ->setCellValue('BJ2', 'Educación Alimentaria y nutricional')
    ->setCellValue('BK2', 'Otro (especificar)')
    ->setCellValue('BL2', 'Comunicación')
    ->setCellValue('BM2', 'Diagnóstico de agroecosistemas')
    ->setCellValue('BN2', 'Experimentación')
    ->setCellValue('BO2', 'Formación y capacitación')
    ->setCellValue('BP2', 'Investigación')
    ->setCellValue('BQ2', 'Ordenamientos territoriales')
    ->setCellValue('BR2', 'Otro (especificar)')
    ->setCellValue('BS2', 'Captación y almacenamiento de agua de lluvias')
    ->setCellValue('BT2', 'Enfoque de microcuencas')
    ->setCellValue('BU2', 'Implementación de ecotecnologías')
    ->setCellValue('BV2', 'Manejo comunitario del agua')
    ->setCellValue('BW2', 'Riego y drenaje')
    ->setCellValue('BX2', 'Tratamiento de aguas')
    ->setCellValue('BY2', 'Otro (especificar)')
    ->setCellValue('BZ2', 'Almacenamientos')
    ->setCellValue('CA2', 'Comercialización')
    ->setCellValue('CB2', ' Procesamiento o transformación')
    ->setCellValue('CC2', 'Otro (especificar)')
    ->setCellValue('CD2', 'Almacenamiento y conservación de semillas')
    ->setCellValue('CE2', 'Bancos comunitarios de semillas')
    ->setCellValue('CF2', 'Intercambio de Semillas')
    ->setCellValue('CG2', 'Mejoramiento de plantas y animales')
    ->setCellValue('CH2', 'Producción de semillas nativas y criollas')
    ->setCellValue('CI2', 'Otro (especificar)')
    ->setCellValue('CJ2', 'Montes y áreas comunes destinadas a la conservación de sistemas silvopastoriles')
    ->setCellValue('CK2', 'Sistemas Agroforestales Tradicionales')
    ->setCellValue('CL2', 'Bosques comestibles')
    ->setCellValue('CM2', 'Cercas vivas')
    ->setCellValue('CN2', 'Barreras rompevientos')
    ->setCellValue('CO2', 'Otro (especificar)')
    ->setCellValue('CP2', 'Productos forestables maderables')
    ->setCellValue('CQ2', 'Productos forestales no maderables')
    ->setCellValue('CR2', 'Otro (especificar)')
    ->setCellValue('CS2', 'Abejas europeas')
    ->setCellValue('CT2', 'Abejas nativas')
    ->setCellValue('CU2', 'Acuacultura')
    ->setCellValue('CV2', 'Aves')
    ->setCellValue('CW2', 'Bovinos')
    ->setCellValue('CX2', 'Conejo')
    ->setCellValue('CY2', 'Caprinos')
    ->setCellValue('CZ2', 'Cunicultura')
    ->setCellValue('DA2', 'Equinos')
    ->setCellValue('DB2', 'Lombricultura')
    ->setCellValue('DC2', 'Ovinos')
    ->setCellValue('DD2', 'Pesca')
    ->setCellValue('DE2', 'Porcino')
    ->setCellValue('DF2', 'Mejoramiento genético')
    ->setCellValue('DG2', 'Nutrición Animal')
    ->setCellValue('DH2', 'Prácticas de Salud Animal')
    ->setCellValue('DI2', 'Veterinaria Natural')
    ->setCellValue('DK2', 'Homeopatia')
    ->setCellValue('DL2', 'Otro (especificar)')
    ->setCellValue('DM2', 'Construcción y mantenimiento de instalaciones')
    ->setCellValue('DN2', 'Permacultura')
    ->setCellValue('DO2', 'Agricultura biodinámica')
    ->setCellValue('DP2', 'Agricultura natural')
    ->setCellValue('DR2', 'Agricultura orgánica')
    ->setCellValue('DS2', 'Agrohomeopatía')
    ->setCellValue('DT2', 'Transición en sustitución de insumos')
    ->setCellValue('DU2', 'Sustitución de agroquímicos')
    ->setCellValue('DV2', 'Manejo biológico')
    ->setCellValue('DW2', 'Manejo integrado')
    ->setCellValue('DX2', 'Abonos verdes')
    ->setCellValue('DY2', 'Barbecho o descanso')
    ->setCellValue('DZ2', ' Uso de harinas de roca')
    ->setCellValue('EA2', 'Coberteras')
    ->setCellValue('EB2', 'Compostaje')
    ->setCellValue('EC2', 'Mejoramiento de la fertilidad')
    ->setCellValue('ED2', 'Producción de biofertilizantes')
    ->setCellValue('EE2', 'Producción de abonos sólidos')
    ->setCellValue('EF2', 'Práticas de conservación de suelos')
    ->setCellValue('EG2', 'Siembra directa y cultivo')
    ->setCellValue('EH2', 'Siembra y cultivo con tracción animal')
    ->setCellValue('EI2', 'Siembra y cultivo con (roturacion tractor, maquinaria)')
    ->setCellValue('EK2', 'Siembra directa')
    ->setCellValue('EL2', 'Cereales')
    ->setCellValue('EM2', 'Cultivos asociados')
    ->setCellValue('EN2', 'Forrajes')
    ->setCellValue('EO2', 'Frutales')
    ->setCellValue('EP2', 'Hortalizas')
    ->setCellValue('EQ2', 'Leguminosas')
    ->setCellValue('ER2', 'Medicinales')
    ->setCellValue('ES2', 'Oleaginosas')
    ->setCellValue('ET2', 'Textiles')
    ->setCellValue('EU2', 'Otro (especificar)');

include_once 'categorias/urbana.php';
include_once 'categorias/Salud_plantas_medicinales.php';
include_once 'categorias/Actividades_no_agrícolas.php';
include_once 'categorias/Desarrollo_Local.php';
include_once 'categorias/Economia.php';
include_once 'categorias/Educación.php';
include_once 'categorias/Derechos_humanos.php';
include_once 'categorias/Trabajo_con_niños_juventud.php';
include_once 'categorias/Investigación_agrícola_Extensión_rural.php';
include_once 'categorias/Manejo_recursos_hídricos.php';
include_once 'categorias/Manejo_poscosecha.php';
include_once 'categorias/Semillas.php';
include_once 'categorias/Sistemas_agroforestales.php';
include_once 'categorias/Productos_forestales.php';
include_once 'categorias/Sistemas_producción_animal.php';
include_once 'categorias/Sistemas_producción_agrícola.php';


