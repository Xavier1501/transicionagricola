<?php




/*--------------------------------------------------- actividades no agricolas artesania */
$sqlActividadesNoAgricolasArtesania ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_actividadnoagricola caana on cae.idEntrevista = caana.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caana.Artesania = 1 and cai.activo = 1;      
        ";

$resultActividadesNoAgricolasArtesania = $conexion->query($sqlActividadesNoAgricolasArtesania);
while ($rowActividadesNoAgricolasArtesania = $resultActividadesNoAgricolasArtesania ->fetch_array(MYSQLI_ASSOC)){
    $arregloActividadesNoAgricolasArtesania[]  = array(
        'id' => $rowActividadesNoAgricolasArtesania['idEntrevista'],
        'lat' => $rowActividadesNoAgricolasArtesania['Latitud'],
        'log' => $rowActividadesNoAgricolasArtesania['Longitud']

    );
}
if (isset($arregloActividadesNoAgricolasArtesania)){
    $arregloActividadesNoAgricolasArtesania ;
}else{
    $arregloActividadesNoAgricolasArtesania = 0;
}
//var_dump($arregloActividadesNoAgricolasArtesania);


/*--------------------------------------------------- actividades no agricolas caja */
$sqlActividadesNoAgricolasCaja="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_actividadnoagricola caana on cae.idEntrevista = caana.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caana.CajaAhorro = 1 and cai.activo = 1;      
        ";

$resultActividadesNoAgricolasCaja= $conexion->query($sqlActividadesNoAgricolasCaja);
while ($rowActividadesNoAgricolasCaja = $resultActividadesNoAgricolasCaja ->fetch_array(MYSQLI_ASSOC)){
    $arregloActividadesNoAgricolasCaja[]  = array(
        'id' => $rowActividadesNoAgricolasCaja['idEntrevista'],
        'lat' => $rowActividadesNoAgricolasCaja['Latitud'],
        'log' => $rowActividadesNoAgricolasCaja['Longitud']

    );
}
if (isset($arregloActividadesNoAgricolasCaja)){
    $arregloActividadesNoAgricolasCaja ;
}else{
    $arregloActividadesNoAgricolasCaja = 0;
}
//var_dump($arregloActividadesNoAgricolasCaja);

/*--------------------------------------------------- actividades no agricolas Turismo*/
$sqlActividadesNoAgricolasTurismo="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_actividadnoagricola caana on cae.idEntrevista = caana.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caana.Turismo = 1 and cai.activo = 1;      
        ";

$resultActividadesNoAgricolasTurismo= $conexion->query($sqlActividadesNoAgricolasTurismo);
while ($rowActividadesNoAgricolasTurismo = $resultActividadesNoAgricolasTurismo ->fetch_array(MYSQLI_ASSOC)){
    $arregloActividadesNoAgricolasTurismo[]  = array(
        'id' => $rowActividadesNoAgricolasTurismo['idEntrevista'],
        'lat' => $rowActividadesNoAgricolasTurismo['Latitud'],
        'log' => $rowActividadesNoAgricolasTurismo['Longitud']

    );
}
if (isset($arregloActividadesNoAgricolasTurismo)){
    $arregloActividadesNoAgricolasTurismo ;
}else{
    $arregloActividadesNoAgricolasTurismo = 0;
}
//var_dump($arregloActividadesNoAgricolasTurismo);

/*--------------------------------------------------- actividades no agricolas Culturales*/
$sqlActividadesNoAgricolasCulturales="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_actividadnoagricola caana on cae.idEntrevista = caana.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caana.Culturales = 1 and cai.activo = 1;      
        ";

$resultActividadesNoAgricolasTurismo= $conexion->query($sqlActividadesNoAgricolasCulturales);

while ($rowActividadesNoAgricolasCulturales = $resultActividadesNoAgricolasTurismo ->fetch_array(MYSQLI_ASSOC)){
    $arregloActividadesNoAgricolasCulturales[]  = array(
        'id' => $rowActividadesNoAgricolasCulturales['idEntrevista'],
        'lat' => $rowActividadesNoAgricolasCulturales['Latitud'],
        'log' => $rowActividadesNoAgricolasCulturales['Longitud']

    );
}
if (isset($arregloActividadesNoAgricolasCulturales)){
    $arregloActividadesNoAgricolasCulturales ;
}else{
    $arregloActividadesNoAgricolasCulturales = 0;
}
//var_dump($arregloActividadesNoAgricolasCulturales);

/*--------------------------------------------------- actividades no agricolas ComedorComuni*/
$sqlActividadesNoAgricolasComedorComuni="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_actividadnoagricola caana on cae.idEntrevista = caana.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caana.ComedorComuni = 1 and cai.activo = 1;      
        ";

$resultActividadesNoAgricoComedorComuni= $conexion->query($sqlActividadesNoAgricolasComedorComuni);

while ($rowActividadesNoAgricolaComedorComunis = $resultActividadesNoAgricoComedorComuni ->fetch_array(MYSQLI_ASSOC)){
    $arregloActividadesNoAgricolasComedorComuni[]  = array(
        'id' => $rowActividadesNoAgricolaComedorComunis['idEntrevista'],
        'lat' => $rowActividadesNoAgricolaComedorComunis['Latitud'],
        'log' => $rowActividadesNoAgricolaComedorComunis['Longitud']


    );
}
if (isset($arregloActividadesNoAgricolasComedorComuni)){
    $arregloActividadesNoAgricolasComedorComuni ;
}else{
    $arregloActividadesNoAgricolasComedorComuni = 0;
}
//var_dump($arregloActividadesNoAgricolasComedorComuni);

/*--------------------------------------------------- actividades no agricolas ComedorSocial*/
$sqlActividadesNoAgricolasComedorSocial="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_actividadnoagricola caana on cae.idEntrevista = caana.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caana.ComedorSocial = 1 and cai.activo = 1;      
        ";

$resultActividadesNoAgricoComedorSocial= $conexion->query($sqlActividadesNoAgricolasComedorSocial);

while ($rowActividadesNoAgricolaComedorSocial = $resultActividadesNoAgricoComedorSocial ->fetch_array(MYSQLI_ASSOC)){
    $arregloActividadesNoAgricolasComedorSocial[]  = array(
        'id' => $rowActividadesNoAgricolaComedorSocial['idEntrevista'],
        'lat' => $rowActividadesNoAgricolaComedorSocial['Latitud'],
        'log' => $rowActividadesNoAgricolaComedorSocial['Longitud']


    );
}
if (isset($arregloActividadesNoAgricolasComedorSocial)){
    $arregloActividadesNoAgricolasComedorSocial ;
}else{
    $arregloActividadesNoAgricolasComedorSocial = 0;
}
//var_dump($arregloActividadesNoAgricolasComedorSocial);

/*--------------------------------------------------- actividades no agricolas ComedorEscolar*/
$sqlActividadesNoAgricolasComedorEscolar="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_actividadnoagricola caana on cae.idEntrevista = caana.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caana.ComedorEscolar = 1 and cai.activo = 1;      
        ";

$resultActividadesNoAgricoComedorEscolar= $conexion->query($sqlActividadesNoAgricolasComedorEscolar);

while ($rowActividadesNoAgricolaComedorEscolar = $resultActividadesNoAgricoComedorEscolar ->fetch_array(MYSQLI_ASSOC)){
    $arregloActividadesNoAgricolasComedorEscolar[]  = array(
        'id' => $rowActividadesNoAgricolaComedorEscolar['idEntrevista'],
        'lat' => $rowActividadesNoAgricolaComedorEscolar['Latitud'],
        'log' => $rowActividadesNoAgricolaComedorEscolar['Longitud']
    );
}
if (isset($arregloActividadesNoAgricolasComedorEscolar)){
    $arregloActividadesNoAgricolasComedorEscolar ;
}else{
    $arregloActividadesNoAgricolasComedorEscolar = 0;
}
//var_dump($arregloActividadesNoAgricolasComedorEscolar);

/*--------------------------------------------------- actividades no agricolas Restaurant*/
$sqlActividadesNoAgricolasRestaurant="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_actividadnoagricola caana on cae.idEntrevista = caana.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caana.Restaurant = 1 and cai.activo = 1;      
        ";

$resultActividadesNoAgricoRestaurant= $conexion->query($sqlActividadesNoAgricolasRestaurant);

while ($rowActividadesNoAgricolaRestaurant = $resultActividadesNoAgricoRestaurant ->fetch_array(MYSQLI_ASSOC)){
    $arregloActividadesNoAgricolasRestaurant []  = array(
        'id' => $rowActividadesNoAgricolaRestaurant['idEntrevista'],
        'lat' => $rowActividadesNoAgricolaRestaurant['Latitud'],
        'log' => $rowActividadesNoAgricolaRestaurant['Longitud']
    );
}
if (isset($arregloActividadesNoAgricolasRestaurant)){
    $arregloActividadesNoAgricolasRestaurant ;
}else{
    $arregloActividadesNoAgricolasRestaurant = 0;
}
//var_dump($arregloActividadesNoAgricolasRestaurant);

/*--------------------------------------------------- actividades no agricolas Restaurant*/
$sqlActividadesNoAgricolasOtro="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_actividadnoagricola caana on cae.idEntrevista = caana.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caana.OtroActNoAgri <> '' and cai.activo = 1;      
        ";

$resultActividadesNoAgricoOtro= $conexion->query($sqlActividadesNoAgricolasOtro);

while ($rowActividadesNoAgricolaOtro = $resultActividadesNoAgricoOtro ->fetch_array(MYSQLI_ASSOC)){
    $arregloActividadesNoAgricolasOtro []  = array(
        'id' => $rowActividadesNoAgricolaOtro['idEntrevista'],
        'lat' => $rowActividadesNoAgricolaOtro['Latitud'],
        'log' => $rowActividadesNoAgricolaOtro['Longitud']
    );
}
if (isset($arregloActividadesNoAgricolasOtro)){
    $arregloActividadesNoAgricolasRestaurant ;
}else{
    $arregloActividadesNoAgricolasOtro = 0;
}
//var_dump($arregloActividadesNoAgricolasOtro);

