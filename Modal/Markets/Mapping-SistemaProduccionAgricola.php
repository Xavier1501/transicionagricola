<?php




/*--------------------------------------------------- sistemas de porduccion agricola Permacultura */


$sqlSistemasProdPermacultura ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.Permacultura =1 and cai.activo = 1 ;      
        ";

$resultSistemasProdPermacultura = $conexion->query($sqlSistemasProdPermacultura);

while ($rowSistemasProdPermacultura = $resultSistemasProdPermacultura ->fetch_array(MYSQLI_ASSOC)){
    $arregloSistemasProdPermacultura[]  = array(
        'id' => $rowSistemasProdPermacultura['idEntrevista'],
        'lat' => $rowSistemasProdPermacultura['Latitud'],
        'log' => $rowSistemasProdPermacultura['Longitud']

    );
}

if (isset($arregloSistemasProdPermacultura)){
    $arregloSistemasProdPermacultura ;
}else{
    $arregloSistemasProdPermacultura = 0;
}

//var_dump($arregloSistemasProdPermacultura);

/*--------------------------------------------------- AgriculturaBiodinamica*/


$sql_spa_Abio ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.AgriculturaBiodinamica =1 and cai.activo = 1 ;      
        ";

$result_spa_Abio = $conexion->query($sql_spa_Abio);

while ($row_spa_Abio = $result_spa_Abio ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_spa_Abio[]  = array(
        'id' => $row_spa_Abio['idEntrevista'],
        'lat' => $row_spa_Abio['Latitud'],
        'log' => $row_spa_Abio['Longitud']

    );
}

if (isset($arreglo_spa_Abio)){
    $arreglo_spa_Abio ;
}else{
    $arreglo_spa_Abio = 0;
}

//var_dump($arreglo_spa_Abio);

/*--------------------------------------------------- AgriculturaNatural*/


$sql_spa_AN ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.AgriculturaNatural =1 and cai.activo = 1 ;      
        ";

$result_spa_AN = $conexion->query($sql_spa_AN);

while ($row_spa_AN = $result_spa_AN ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_spa_AN[]  = array(
        'id' => $row_spa_AN['idEntrevista'],
        'lat' => $row_spa_AN['Latitud'],
        'log' => $row_spa_AN['Longitud']

    );
}

if (isset($arreglo_spa_AN)){
    $arreglo_spa_AN ;
}else{
    $arreglo_spa_AN = 0;
}

//var_dump($arreglo_spa_AN);

/*--------------------------------------------------- AgriculturaNatural*/


$sql_spa_AO ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.AgriculturaOrganica =1 and cai.activo = 1 ;      
        ";

$result_spa_AO = $conexion->query($sql_spa_AO);

while ($row_spa_AO = $result_spa_AO ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_spa_AO[]  = array(
        'id' => $row_spa_AO['idEntrevista'],
        'lat' => $row_spa_AO['Latitud'],
        'log' => $row_spa_AO['Longitud']

    );
}

if (isset($arreglo_spa_AO)){
    $arreglo_spa_AO ;
}else{
    $arreglo_spa_AO = 0;
}

//var_dump($arreglo_spa_AO);

/*--------------------------------------------------- Agrohomeopatia*/


$sql_spa_Ag ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.Agrohomeopatia =1 and cai.activo = 1 ;      
        ";

$result_spa_Ag = $conexion->query($sql_spa_Ag);

while ($row_spa_Ag = $result_spa_Ag ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_spa_Ag[]  = array(
        'id' => $row_spa_Ag['idEntrevista'],
        'lat' => $row_spa_Ag['Latitud'],
        'log' => $row_spa_Ag['Longitud']
    );
}

if (isset($arreglo_spa_Ag)){
    $arreglo_spa_Ag ;
}else{
    $arreglo_spa_Ag = 0;
}

//var_dump($arreglo_spa_Ag);

/*--------------------------------------------------- TransicionSustiInsumo*/
$sql_tsi ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.TransicionSustiInsumo =1 and cai.activo = 1 ;      
        ";
$result_tsi = $conexion->query($sql_tsi);
while ($row_tsi = $result_tsi ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_tsi[]  = array(
        'id' => $row_tsi['idEntrevista'],
        'lat' => $row_tsi['Latitud'],
        'log' => $row_tsi['Longitud']
    );
}
if (isset($arreglo_tsi)){
    $arreglo_tsi ;
}else{
    $arreglo_tsi = 0;
}
//var_dump($arreglo_tsi);

/*--------------------------------------------------- SustitucionAgroquimica*/
$sql_stq ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.SustitucionAgroquimica =1 and cai.activo = 1 ;      
        ";
$result_stq = $conexion->query($sql_stq);
while ($row_stq = $result_stq ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_stq[]  = array(
        'id' => $row_stq['idEntrevista'],
        'lat' => $row_stq['Latitud'],
        'log' => $row_stq['Longitud']
    );
}
if (isset($arreglo_stq)){
    $arreglo_stq ;
}else{
    $arreglo_stq = 0;
}
//var_dump($arreglo_stq);

/*--------------------------------------------------- ManejoBioologico*/
$sql_mbio ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.ManejoBioologico =1 and cai.activo = 1 ;      
        ";
$result_mbio = $conexion->query($sql_mbio);
while ($row_mbio = $result_mbio ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_mbio[]  = array(
        'id' => $row_mbio['idEntrevista'],
        'lat' => $row_mbio['Latitud'],
        'log' => $row_mbio['Longitud']
    );
}
if (isset($arreglo_mbio)){
    $arreglo_mbio ;
}else{
    $arreglo_mbio = 0;
}
//var_dump($arreglo_mbio);

/*--------------------------------------------------- ManejoIntegrado*/
$sql_mint ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.ManejoIntegrado =1 and cai.activo = 1 ;      
        ";
$result_mint = $conexion->query($sql_mint);
while ($row_mint = $result_mint ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_mint[]  = array(
        'id' => $row_mint['idEntrevista'],
        'lat' => $row_mint['Latitud'],
        'log' => $row_mint['Longitud']
    );
}
if (isset($arreglo_mint)){
    $arreglo_mint ;
}else{
    $arreglo_mint = 0;
}
//var_dump($arreglo_mint);
/*--------------------------------------------------- AbonoVerde*/
$sql_ave ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.AbonoVerde =1 and cai.activo = 1 ;      
        ";
$result_ave = $conexion->query($sql_ave);
while ($row_ave = $result_ave ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_ave[]  = array(
        'id' => $row_ave['idEntrevista'],
        'lat' => $row_ave['Latitud'],
        'log' => $row_ave['Longitud']
    );
}
if (isset($arreglo_ave)){
    $arreglo_ave ;
}else{
    $arreglo_ave = 0;
}
//var_dump($arreglo_ave);

/*--------------------------------------------------- BarbechoDescanso*/
$sql_bDes ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.BarbechoDescanso =1 and cai.activo = 1 ;      
        ";
$result_bDes = $conexion->query($sql_bDes);
while ($row_bDes = $result_bDes ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_bDes[]  = array(
        'id' => $row_bDes['idEntrevista'],
        'lat' => $row_bDes['Latitud'],
        'log' => $row_bDes['Longitud']
    );
}
if (isset($arreglo_bDes)){
    $arreglo_bDes ;
}else{
    $arreglo_bDes = 0;
}
//var_dump($arreglo_bDes);

/*--------------------------------------------------- HarinaRoca*/
$sql_hroca ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.HarinaRoca =1 and cai.activo = 1 ;      
        ";
$resul_hroca = $conexion->query($sql_hroca);
while ($row_hroca = $resul_hroca ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_hroca[]  = array(
        'id' => $row_hroca['idEntrevista'],
        'lat' => $row_hroca['Latitud'],
        'log' => $row_hroca['Longitud']
    );
}
if (isset($arreglo_hroca)){
    $arreglo_hroca ;
}else{
    $arreglo_hroca = 0;
}
//var_dump($arreglo_hroca);


/*--------------------------------------------------- Cobertura*/
$sql_cob ="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.Cobertura =1 and cai.activo = 1 ;      
        ";
$resul_cob = $conexion->query($sql_cob);
while ($row_cob = $resul_cob ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_cob[]  = array(
        'id' => $row_cob['idEntrevista'],
        'lat' => $row_cob['Latitud'],
        'log' => $row_cob['Longitud']
    );
}
if (isset($arreglo_cob)){
    $arreglo_cob ;
}else{
    $arreglo_cob = 0;
}
//var_dump($arreglo_cob);

/*--------------------------------------------------- Compostaje*/
$sql_comp="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.Compostaje =1 and cai.activo = 1 ;      
        ";
$resul_comp = $conexion->query($sql_comp);
while ($row_comp = $resul_comp ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_comp[]  = array(
        'id' => $row_comp['idEntrevista'],
        'lat' => $row_comp['Latitud'],
        'log' => $row_comp['Longitud']
    );
}
if (isset($arreglo_comp)){
    $arreglo_comp ;
}else{
    $arreglo_comp = 0;
}
//var_dump($arreglo_comp);

/*--------------------------------------------------- MejoraFertilidad*/
$sql_MFer="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.MejoraFertilidad =1 and cai.activo = 1 ;      
        ";
$resul_MFer = $conexion->query($sql_MFer);
while ($row_MFer = $resul_MFer ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_MFer[]  = array(
        'id' => $row_MFer['idEntrevista'],
        'lat' => $row_MFer['Latitud'],
        'log' => $row_MFer['Longitud']
    );
}
if (isset($arreglo_MFer)){
    $arreglo_MFer ;
}else{
    $arreglo_MFer = 0;
}
//var_dump($arreglo_MFer);

/*--------------------------------------------------- ProduccionBiofertilizante*/
$sql_PrBio="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.ProduccionBiofertilizante =1 and cai.activo = 1 ;      
        ";
$resul_PrBio = $conexion->query($sql_PrBio);
while ($row_PrBio = $resul_PrBio ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_PrBio[]  = array(
        'id' => $row_PrBio['idEntrevista'],
        'lat' => $row_PrBio['Latitud'],
        'log' => $row_PrBio['Longitud']
    );
}
if (isset($arreglo_PrBio)){
    $arreglo_PrBio ;
}else{
    $arreglo_PrBio = 0;
}
//var_dump($arreglo_PrBio);

/*--------------------------------------------------- ProduccionAbonoSolido*/
$sql_PAS="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.ProduccionAbonoSolido =1 and cai.activo = 1 ;      
        ";
$resul_PAS = $conexion->query($sql_PAS);
while ($row_PAS = $resul_PAS ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_PAS[]  = array(
        'id' => $row_PAS['idEntrevista'],
        'lat' => $row_PAS['Latitud'],
        'log' => $row_PAS['Longitud']
    );
}
if (isset($arreglo_PAS)){
    $arreglo_PAS ;
}else{
    $arreglo_PAS = 0;
}
//var_dump($arreglo_PAS);


/*--------------------------------------------------- PracticaConservacionSuelo*/
$sql_PcSue="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.PracticaConservacionSuelo =1 and cai.activo = 1 ;      
        ";
$resul_PcSue = $conexion->query($sql_PcSue);
while ($row_PcSue = $resul_PcSue ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_PcSue[]  = array(
        'id' => $row_PcSue['idEntrevista'],
        'lat' => $row_PcSue['Latitud'],
        'log' => $row_PcSue['Longitud']
    );
}
if (isset($arreglo_PcSue)){
    $arreglo_PcSue ;
}else{
    $arreglo_PcSue = 0;
}
//var_dump($arreglo_PcSue);

/*--------------------------------------------------- SiembraDirectaCultivo*/
$sql_sDCul="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.SiembraDirectaCultivo =1 and cai.activo = 1 ;      
        ";
$resul_sDCul = $conexion->query($sql_sDCul);
while ($row_sDCul = $resul_sDCul ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_sDCul[]  = array(
        'id' => $row_sDCul['idEntrevista'],
        'lat' => $row_sDCul['Latitud'],
        'log' => $row_sDCul['Longitud']
    );
}
if (isset($arreglo_sDCul)){
    $arreglo_sDCul ;
}else{
    $arreglo_sDCul = 0;
}
//var_dump($arreglo_sDCul);

/*--------------------------------------------------- SiembraCultivoAnimal*/
$sql_sCulAni="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.SiembraCultivoAnimal =1 and cai.activo = 1 ;      
        ";
$resul_sCulAni = $conexion->query($sql_sCulAni);
while ($row_sCulAni = $resul_sCulAni ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_sCulAni[]  = array(
        'id' => $row_sCulAni['idEntrevista'],
        'lat' => $row_sCulAni['Latitud'],
        'log' => $row_sCulAni['Longitud']
    );
}
if (isset($arreglo_sCulAni)){
    $arreglo_sCulAni ;
}else{
    $arreglo_sCulAni = 0;
}
//var_dump($arreglo_sCulAni);

/*--------------------------------------------------- SiembraDirecta*/
$sql_sDir="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.SiembraDirecta =1 and cai.activo = 1 ;      
        ";
$resul_sDir = $conexion->query($sql_sDir);
while ($row_sDir = $resul_sDir ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_sDir[]  = array(
        'id' => $row_sDir['idEntrevista'],
        'lat' => $row_sDir['Latitud'],
        'log' => $row_sDir['Longitud']
    );
}
if (isset($arreglo_sDir)){
    $arreglo_sDir ;
}else{
    $arreglo_sDir = 0;
}
//var_dump($arreglo_sDir);

/*--------------------------------------------------- Cereales*/
$sql_cereales="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.Cereales =1 and cai.activo = 1 ;      
        ";
$resul_cereales = $conexion->query($sql_cereales);
while ($row_cereales = $resul_cereales ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_cereales[]  = array(
        'id' => $row_cereales['idEntrevista'],
        'lat' => $row_cereales['Latitud'],
        'log' => $row_cereales['Longitud']
    );
}
if (isset($arreglo_cereales)){
    $arreglo_cereales ;
}else{
    $arreglo_cereales = 0;
}
//var_dump($arreglo_cereales);

/*--------------------------------------------------- CultivoAsociado*/
$sql_cutAso="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.CultivoAsociado =1 and cai.activo = 1 ;      
        ";
$resul_cutAso = $conexion->query($sql_cutAso);
while ($row_cutAso = $resul_cutAso ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_cutAso[]  = array(
        'id' => $row_cutAso['idEntrevista'],
        'lat' => $row_cutAso['Latitud'],
        'log' => $row_cutAso['Longitud']
    );
}
if (isset($arreglo_cutAso)){
    $arreglo_cutAso ;
}else{
    $arreglo_cutAso = 0;
}
//var_dump($arreglo_cutAso);



/*--------------------------------------------------- Forraje*/
$sql_Forraje="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.Forraje =1 and cai.activo = 1 ;      
        ";
$resul_Forraje = $conexion->query($sql_Forraje);
while ($row_Forraje = $resul_Forraje ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_Forraje[]  = array(
        'id' => $row_Forraje['idEntrevista'],
        'lat' => $row_Forraje['Latitud'],
        'log' => $row_Forraje['Longitud']
    );
}
if (isset($arreglo_Forraje)){
    $arreglo_Forraje ;
}else{
    $arreglo_Forraje = 0;
}
//var_dump($arreglo_Forraje);

/*--------------------------------------------------- Frutales*/
$sql_Frutales="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.Frutales =1 and cai.activo = 1 ;      
        ";
$resul_Frutales = $conexion->query($sql_Frutales);
while ($row_Frutales = $resul_Frutales ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_Frutales[]  = array(
        'id' => $row_Frutales['idEntrevista'],
        'lat' => $row_Frutales['Latitud'],
        'log' => $row_Frutales['Longitud']
    );
}
if (isset($arreglo_Frutales)){
    $arreglo_Frutales ;
}else{
    $arreglo_Frutales = 0;
}
//var_dump($arreglo_Frutales);

/*--------------------------------------------------- Hortaliza*/
$sql_Hortaliza="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.Hortaliza =1 and cai.activo = 1 ;      
        ";
$resul_Hortaliza = $conexion->query($sql_Hortaliza);
while ($row_Hortaliza = $resul_Hortaliza ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_Hortaliza[]  = array(
        'id' => $row_Hortaliza['idEntrevista'],
        'lat' => $row_Hortaliza['Latitud'],
        'log' => $row_Hortaliza['Longitud']
    );
}
if (isset($arreglo_Hortaliza)){
    $arreglo_Hortaliza ;
}else{
    $arreglo_Hortaliza = 0;
}
//var_dump($arreglo_Hortaliza);

/*--------------------------------------------------- Leguminosa*/
$sql_Leguminosa="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.Leguminosa =1 and cai.activo = 1 ;      
        ";
$resul_Leguminosa = $conexion->query($sql_Leguminosa);
while ($row_Leguminosa = $resul_Leguminosa ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_Leguminosa[]  = array(
        'id' => $row_Leguminosa['idEntrevista'],
        'lat' => $row_Leguminosa['Latitud'],
        'log' => $row_Leguminosa['Longitud']
    );
}
if (isset($arreglo_Leguminosa)){
    $arreglo_Leguminosa ;
}else{
    $arreglo_Leguminosa = 0;
}
//var_dump($arreglo_Leguminosa);


/*--------------------------------------------------- Medicinales*/
$sql_Medicinales="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.Medicinales =1 and cai.activo = 1 ;      
        ";
$resul_Medicinales = $conexion->query($sql_Medicinales);
while ($row_Medicinales = $resul_Medicinales ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_Medicinales[]  = array(
        'id' => $row_Medicinales['idEntrevista'],
        'lat' => $row_Medicinales['Latitud'],
        'log' => $row_Medicinales['Longitud']
    );
}
if (isset($arreglo_Medicinales)){
    $arreglo_Medicinales ;
}else{
    $arreglo_Medicinales = 0;
}
//var_dump($arreglo_Medicinales);

/*--------------------------------------------------- Oleaginosa*/
$sql_Oleaginosa="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.Oleaginosa =1 and cai.activo = 1 ;      
        ";
$resul_Oleaginosa = $conexion->query($sql_Oleaginosa);
while ($row_Oleaginosa = $resul_Oleaginosa ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_Oleaginosa[]  = array(
        'id' => $row_Oleaginosa['idEntrevista'],
        'lat' => $row_Oleaginosa['Latitud'],
        'log' => $row_Oleaginosa['Longitud']
    );
}
if (isset($arreglo_Oleaginosa)){
    $arreglo_Oleaginosa ;
}else{
    $arreglo_Oleaginosa = 0;
}
//var_dump($arreglo_Oleaginosa);

/*--------------------------------------------------- Textil*/
$sql_Textil="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.Textil =1 and cai.activo = 1 ;      
        ";
$resul_Textil = $conexion->query($sql_Textil);
while ($row_Textil = $resul_Textil ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_Textil[]  = array(
        'id' => $row_Textil['idEntrevista'],
        'lat' => $row_Textil['Latitud'],
        'log' => $row_Textil['Longitud']
    );
}
if (isset($arreglo_Textil)){
    $arreglo_Textil ;
}else{
    $arreglo_Textil = 0;
}
//var_dump($arreglo_Textil);

/*--------------------------------------------------- OtroAgricola*/
$sql_OtroAgricola="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemaproduccionagricola caspag on cae.idEntrevista = caspag.idAreaTrabajo 
where caspag.OtroAgricola <> '' and cai.activo = 1 ;      
        ";
$resul_OtroAgricola = $conexion->query($sql_OtroAgricola);
while ($row_OtroAgricola = $resul_OtroAgricola ->fetch_array(MYSQLI_ASSOC)){
    $arreglo_OtroAgricola[]  = array(
        'id' => $row_OtroAgricola['idEntrevista'],
        'lat' => $row_OtroAgricola['Latitud'],
        'log' => $row_OtroAgricola['Longitud']
    );
}
if (isset($arreglo_OtroAgricola)){
    $arreglo_OtroAgricola ;
}else{
    $arreglo_OtroAgricola = 0;
}
//var_dump($arreglo_OtroAgricola);
