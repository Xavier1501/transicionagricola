<?php


/*---------------------------------------------------ProductoMaderable */
$sql_pf_pm ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_productosforestales capfores  on cae.idEntrevista = capfores.idAreaTrabajo 
where capfores.ProductoMaderable =1 and cai.activo = 1    
        ";
$result_pf_pm = $conexion->query($sql_pf_pm);
while ($row_pf_pm = $result_pf_pm ->fetch_array(MYSQLI_ASSOC)){
    $arrego_pf_pm[]  = array(
        'id' => $row_pf_pm['idEntrevista'],
        'lat' => $row_pf_pm['Latitud'],
        'log' => $row_pf_pm['Longitud']

    );
}
if (isset($arrego_pf_pm)){
    $arrego_pf_pm ;
}else{
    $arrego_pf_pm = 0;
}
//var_dump($arrego_pf_pm);

/*---------------------------------------------------ProductoNoMaderable */
$sql_pf_pnm ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_productosforestales capfores  on cae.idEntrevista = capfores.idAreaTrabajo 
where capfores.ProductoNoMaderable =1 and cai.activo = 1    
        ";
$result_pf_pnm = $conexion->query($sql_pf_pnm);
while ($row_pf_pnm = $result_pf_pnm ->fetch_array(MYSQLI_ASSOC)){
    $arrego_pf_pnm[]  = array(
        'id' => $row_pf_pnm['idEntrevista'],
        'lat' => $row_pf_pnm['Latitud'],
        'log' => $row_pf_pnm['Longitud']

    );
}
if (isset($arrego_pf_pnm)){
    $arrego_pf_pnm ;
}else{
    $arrego_pf_pnm = 0;
}
//var_dump($arrego_pf_pnm);

/*---------------------------------------------------OtroProductoForestales */
$sql_pf_otro ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_productosforestales capfores  on cae.idEntrevista = capfores.idAreaTrabajo 
where capfores.OtroProductoForestales =1 and cai.activo = 1    
        ";
$result_pf_otro = $conexion->query($sql_pf_otro);
while ($row_pf_otro = $result_pf_otro ->fetch_array(MYSQLI_ASSOC)){
    $arrego_pf_otro[]  = array(
        'id' => $row_pf_otro['idEntrevista'],
        'lat' => $row_pf_otro['Latitud'],
        'log' => $row_pf_otro['Longitud']

    );
}
if (isset($arrego_pf_otro)){
    $arrego_pf_otro ;
}else{
    $arrego_pf_otro = 0;
}
//var_dump($arrego_pf_otro);



