<?php





/*---------------------------------------------------Manejo poscosecha lluvias  */


$sql_MP_almacenamiento ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_manejoposcosecha camc on cae.idEntrevista = camc.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where camc.Almacenamiento =1 and cai.activo = 1        
        ";

$result_MP_almacenamiento = $conexion->query($sql_MP_almacenamiento);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($row_MP_almacenamiento = $result_MP_almacenamiento ->fetch_array(MYSQLI_ASSOC)){
    $arrego_MP_almacenamiento[]  = array(
        'id' => $row_MP_almacenamiento['idEntrevista'],
        'lat' => $row_MP_almacenamiento['Latitud'],
        'log' => $row_MP_almacenamiento['Longitud']

    );
}
if (isset($arrego_MP_almacenamiento)){
    //echo'tiene valores';
    $arrego_MP_almacenamiento ;
}else{
    //echo 'no';
    $arrego_MP_almacenamiento = 0;
}

//var_dump($arrego_MP_almacenamiento);
/*---------------------------------------------------Manejo poscosecha comercializacion  */

$sql_MP_comerc ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_manejoposcosecha camc on cae.idEntrevista = camc.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where camc.Comercializacion =1 and cai.activo = 1      
        ";

$result_MP_comerc = $conexion->query($sql_MP_comerc);
//$datos = $result->fetch_array(MYSQLI_ASSOC);
//var_dump($datos);
while ($row_MP_comerc = $result_MP_comerc ->fetch_array(MYSQLI_ASSOC)){
    $arrego_MP_comerc[]  = array(
        'id' => $row_MP_comerc['idEntrevista'],
        'lat' => $row_MP_comerc['Latitud'],
        'log' => $row_MP_comerc['Longitud']

    );
}
if (isset($arrego_MP_comerc)){
    //echo'tiene valores';
    $arrego_MP_comerc ;
}else{
    //echo 'no';
    $arrego_MP_comerc = 0;
}

//var_dump($arrego_MP_comerc);

/*---------------------------------------------------Manejo poscosecha transformacion  */

$sql_MP_trnsfor ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_manejoposcosecha camc on cae.idEntrevista = camc.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where camc.ProcesamientoTransfo =1 and cai.activo = 1        
        ";

$result_MP_trnsfor = $conexion->query($sql_MP_trnsfor);
//$datos = $result->fetch_array(MYSQLI_ASSOC);
//var_dump($datos);
while ($row_MP_trnsfor = $result_MP_trnsfor ->fetch_array(MYSQLI_ASSOC)){
    $arrego_MP_trnsfor[]  = array(
        'id' => $row_MP_trnsfor['idEntrevista'],
        'lat' => $row_MP_trnsfor['Latitud'],
        'log' => $row_MP_trnsfor['Longitud']

    );
}
if (isset($arrego_MP_trnsfor)){
    //echo'tiene valores';
    $arrego_MP_trnsfor ;
}else{
    //echo 'no';
    $arrego_MP_trnsfor = 0;
}

//var_dump($arrego_MP_trnsfor);

/*---------------------------------------------------Manejo poscosecha otro  */

$sql_MP_otro ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_manejoposcosecha camc on cae.idEntrevista = camc.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where camc.OtroPosCos <> '' and cai.activo = 1       
        ";

$result_MP_otro = $conexion->query($sql_MP_otro);
//$datos = $result->fetch_array(MYSQLI_ASSOC);
//var_dump($datos);
while ($row_MP_otro = $result_MP_otro ->fetch_array(MYSQLI_ASSOC)){
    $arrego_MP_otro[]  = array(
        'id' => $row_MP_otro ['idEntrevista'],
        'lat' => $row_MP_otro ['Latitud'],
        'log' => $row_MP_otro ['Longitud']

    );
}
if (isset($arrego_MP_otro)){
    $arrego_MP_otro ;
}else{
    $arrego_MP_otro = 0;
}
//var_dump($arrego_MP_otro);

