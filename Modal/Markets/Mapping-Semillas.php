<?php
/*---------------------------------------------------AlmacenConservacionSemillas dh  */
$sql_sem ="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_semillas cas on cae.idEntrevista = cas.idAreaTrabajo 
where cas.AlmacenConservacionSemillas =1 and cai.activo = 1;        
        ";

$result_sem = $conexion->query($sql_sem);

while ($row_sem = $result_sem ->fetch_array(MYSQLI_ASSOC)){
    $arrego_sem[]  = array(
        'id' => $row_sem['idEntrevista'],
        'lat' => $row_sem['Latitud'],
        'log' => $row_sem['Longitud']

    );
}
if (isset($arrego_sem)){
    $arrego_sem ;
}else{
    $arrego_sem = 0;
}
//var_dump($arrego_sem);

/*---------------------------------------------------BancosComuniSemilla  */


$sql_ban ="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_semillas cas on cae.idEntrevista = cas.idAreaTrabajo 
where cas.BancosComuniSemilla =1 and cai.activo = 1;        
        ";

$result_ban = $conexion->query($sql_ban);

while ($row_ban = $result_ban ->fetch_array(MYSQLI_ASSOC)){
    $arrego_ban[]  = array(
        'id' => $row_ban['idEntrevista'],
        'lat' => $row_ban['Latitud'],
        'log' => $row_ban['Longitud']

    );
}
if (isset($arrego_ban)){
    $arrego_ban ;
}else{
    $arrego_ban = 0;
}
//var_dump($arrego_ban);

/*---------------------------------------------------IntercambioSemilla  */
$sql_inter ="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_semillas cas on cae.idEntrevista = cas.idAreaTrabajo 
where cas.IntercambioSemilla =1 and cai.activo = 1;        
        ";

$result_inter = $conexion->query($sql_inter);

while ($row_inte = $result_inter ->fetch_array(MYSQLI_ASSOC)){
    $arrego_inte[]  = array(
        'id' => $row_inte['idEntrevista'],
        'lat' => $row_inte['Latitud'],
        'log' => $row_inte['Longitud']

    );
}
if (isset($arrego_inte)){
    $arrego_inte ;
}else{
    $arrego_inte = 0;
}
//var_dump($arrego_inte);

/*---------------------------------------------------MejoraPlantaAnimal  */
$sql_mpAnimal ="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_semillas cas on cae.idEntrevista = cas.idAreaTrabajo 
where cas.MejoraPlantaAnimal =1 and cai.activo = 1;        
        ";

$result_mpAnimal = $conexion->query($sql_mpAnimal);

while ($row_mpAnimal = $result_mpAnimal ->fetch_array(MYSQLI_ASSOC)){
    $arrego_mpAnimal[]  = array(
        'id' => $row_mpAnimal['idEntrevista'],
        'lat' => $row_mpAnimal['Latitud'],
        'log' => $row_mpAnimal['Longitud']

    );
}
if (isset($arrego_mpAnimal)){
    $arrego_mpAnimal ;
}else{
    $arrego_mpAnimal = 0;
}
//var_dump($arrego_mpAnimal);

/*---------------------------------------------------ProduccionSemilla  */
$sql_proSem="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_semillas cas on cae.idEntrevista = cas.idAreaTrabajo 
where cas.ProduccionSemilla =1 and cai.activo = 1;        
        ";

$result_proSem = $conexion->query($sql_proSem);

while ($row_proSem = $result_proSem ->fetch_array(MYSQLI_ASSOC)){
    $arrego_proSem[]  = array(
        'id' => $row_proSem['idEntrevista'],
        'lat' => $row_proSem['Latitud'],
        'log' => $row_proSem['Longitud']

    );
}
if (isset($arrego_proSem)){
    $arrego_proSem ;
}else{
    $arrego_proSem = 0;
}
//var_dump($arrego_proSem);

/*---------------------------------------------------OtroSemillas  */
$sql_otroSem="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_semillas cas on cae.idEntrevista = cas.idAreaTrabajo 
where cas.OtroSemillas <> '' and cai.activo = 1       
        ";

$result_otroSem = $conexion->query($sql_otroSem);

while ($row_otroSem = $result_otroSem ->fetch_array(MYSQLI_ASSOC)){
    $arrego_otroSem[]  = array(
        'id' => $row_otroSem['idEntrevista'],
        'lat' => $row_otroSem['Latitud'],
        'log' => $row_otroSem['Longitud']

    );
}
if (isset($arrego_otroSem)){
    $arrego_otroSem ;
}else{
    $arrego_otroSem = 0;
}
//var_dump($arrego_otroSem);