<?php





/*---------------------------------------------------Manejo de recursos hídricos lluvias  */


$sql_MRH_lluvias ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_manejorecursoshidricos camrh on cae.idEntrevista = camrh.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where camrh.AlmacenAguaLluvia =1 and cai.activo = 1        
        ";

$result_MRH_lluvias = $conexion->query($sql_MRH_lluvias);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($row_MRH_lluvias = $result_MRH_lluvias ->fetch_array(MYSQLI_ASSOC)){
    $arrego_MRH_lluvias[]  = array(
        'id' => $row_MRH_lluvias['idEntrevista'],
        'lat' => $row_MRH_lluvias['Latitud'],
        'log' => $row_MRH_lluvias['Longitud']

    );
}
if (isset($arrego_MRH_lluvias)){
    //echo'tiene valores';
    $arrego_MRH_lluvias ;
}else{
    //echo 'no';
    $arrego_MRH_lluvias = 0;
}

//var_dump($arrego_MRH_lluvias);
/*---------------------------------------------------Manejo de recursos hídricos Microcuenta  */

$sql_MRH_microcuenta ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_manejorecursoshidricos camrh on cae.idEntrevista = camrh.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where camrh.EnfoqueMicrocuenca =1 and cai.activo = 1       
        ";

$result_MRH_microcuenta = $conexion->query($sql_MRH_microcuenta);
//$datos = $result->fetch_array(MYSQLI_ASSOC);
//var_dump($datos);
while ($row_MRH_microcuenta = $result_MRH_microcuenta ->fetch_array(MYSQLI_ASSOC)){
    $arrego_MRH_microcuenta[]  = array(
        'id' => $row_MRH_microcuenta['idEntrevista'],
        'lat' => $row_MRH_microcuenta['Latitud'],
        'log' => $row_MRH_microcuenta['Longitud']

    );
}
if (isset($arrego_MRH_microcuenta)){
    //echo'tiene valores';
    $arrego_MRH_microcuenta ;
}else{
    //echo 'no';
    $arrego_MRH_microcuenta = 0;
}

//var_dump($arrego_MRH_microcuenta);

/*---------------------------------------------------Manejo de recursos hídricos ecoTecnologias  */

$sql_MRH_tecnologias ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_manejorecursoshidricos camrh on cae.idEntrevista = camrh.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where camrh.ImplementacionEcotecnologicas =1 and cai.activo = 1        
        ";

$result_MRH_tecnologias = $conexion->query($sql_MRH_tecnologias);
//$datos = $result->fetch_array(MYSQLI_ASSOC);
//var_dump($datos);
while ($row_MRH_tecnologias = $result_MRH_tecnologias ->fetch_array(MYSQLI_ASSOC)){
    $arrego_MRH_tecnologias[]  = array(
        'id' => $row_MRH_tecnologias['idEntrevista'],
        'lat' => $row_MRH_tecnologias['Latitud'],
        'log' => $row_MRH_tecnologias['Longitud']

    );
}
if (isset($arrego_MRH_tecnologias)){
    //echo'tiene valores';
    $arrego_MRH_tecnologias ;
}else{
    //echo 'no';
    $arrego_MRH_tecnologias = 0;
}

//var_dump($arrego_MRH_tecnologias);

/*---------------------------------------------------Manejo de recursos hídricos agua  */

$sql_MRH_agua ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_manejorecursoshidricos camrh on cae.idEntrevista = camrh.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where camrh.ManejoComuniAgua =1 and cai.activo = 1        
        ";

$result_MRH_agua = $conexion->query($sql_MRH_agua);
//$datos = $result->fetch_array(MYSQLI_ASSOC);
//var_dump($datos);
while ($row_MRH_agua = $result_MRH_agua ->fetch_array(MYSQLI_ASSOC)){
    $arrego_MRH_agua[]  = array(
        'id' => $row_MRH_agua['idEntrevista'],
        'lat' => $row_MRH_agua['Latitud'],
        'log' => $row_MRH_agua['Longitud']

    );
}
if (isset($arrego_MRH_agua)){
    $arrego_MRH_agua ;
}else{
    $arrego_MRH_agua = 0;
}
//var_dump($arrego_MRH_agua);

/*---------------------------------------------------Manejo de recursos hídricos riesgo  */

$sql_MRH_riesgo ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_manejorecursoshidricos camrh on cae.idEntrevista = camrh.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where camrh.RiesgoDrenaje =1 and cai.activo = 1        
        ";

$result_MRH_riesgo = $conexion->query($sql_MRH_riesgo);
//$datos = $result->fetch_array(MYSQLI_ASSOC);
//var_dump($datos);
while ($row_MRH_riesgo = $result_MRH_riesgo ->fetch_array(MYSQLI_ASSOC)){
    $arrego_MRH_riesgo[]  = array(
        'id' => $row_MRH_riesgo['idEntrevista'],
        'lat' => $row_MRH_riesgo['Latitud'],
        'log' => $row_MRH_riesgo['Longitud']

    );
}
if (isset($arrego_MRH_riesgo)){
    $arrego_MRH_riesgo ;
}else{
    $arrego_MRH_riesgo = 0;
}

//var_dump($arrego_MRH_riesgo);


/*---------------------------------------------------Manejo de recursos hídricos TratamientoAgua */

$sql_MRH_tratamiento ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_manejorecursoshidricos camrh on cae.idEntrevista = camrh.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where camrh.TratamientoAgua =1 and cai.activo = 1        
        ";

$result_MRH_tratamiento = $conexion->query($sql_MRH_tratamiento);
//$datos = $result->fetch_array(MYSQLI_ASSOC);
//var_dump($datos);
while ($row_MRH_tratamiento = $result_MRH_tratamiento ->fetch_array(MYSQLI_ASSOC)){
    $arrego_MRH_tratamiento[]  = array(
        'id' => $row_MRH_tratamiento['idEntrevista'],
        'lat' => $row_MRH_tratamiento['Latitud'],
        'log' => $row_MRH_tratamiento['Longitud']

    );
}
if (isset($arrego_MRH_tratamiento)){
    $arrego_MRH_tratamiento ;
}else{
    $arrego_MRH_tratamiento = 0;
}

//var_dump($arrego_MRH_tratamiento);


/*---------------------------------------------------Manejo de recursos hídricos otro */

$sql_MRH_otro ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_manejorecursoshidricos camrh on cae.idEntrevista = camrh.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where camrh.OtroManejoH <> '' and cai.activo = 1        
        ";

$result_MRH_otro = $conexion->query($sql_MRH_tratamiento);
//$datos = $result->fetch_array(MYSQLI_ASSOC);
//var_dump($datos);
while ($row_MRH_otro = $result_MRH_otro ->fetch_array(MYSQLI_ASSOC)){
    $arrego_MRH_otro[]  = array(
        'id' => $row_MRH_otro['idEntrevista'],
        'lat' => $row_MRH_otro['Latitud'],
        'log' => $row_MRH_otro['Longitud']

    );
}
if (isset($arrego_MRH_otro)){
    $arrego_MRH_otro ;
}else{
    $arrego_MRH_otro = 0;
}

//var_dump($arrego_MRH_otro);






