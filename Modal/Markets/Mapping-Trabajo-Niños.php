<?php



/*---------------------------------------------------Trabajo con niños/juventud */


$sql_tnj_he ="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_trabajoninojuventud catnj on cae.idEntrevista = catnj.idAreaTrabajo
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where catnj.HuertoEscolar =1 and cai.activo = 1       
        ";

$result_tnj_he = $conexion->query($sql_tnj_he);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);

while ($row_tnj_he = $result_tnj_he ->fetch_array(MYSQLI_ASSOC)){
    $arrego_tnj_he[]  = array(
        'id' => $row_tnj_he['idEntrevista'],
        'lat' => $row_tnj_he['Latitud'],
        'log' => $row_tnj_he['Longitud']
    );
}

if (isset($arrego_tnj_he)){
    //echo'tiene valores';
    $arrego_tnj_he;
}else{
    //echo 'no';
    $arrego_tnj_he = 0;
}

//var_dump($arrego_tnj_he);

/*---------------------------------------------------Trabajo con niños/juventud educacion alimenticia */


$sql_tnj_ea ="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_trabajoninojuventud catnj on cae.idEntrevista = catnj.idAreaTrabajo
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where catnj.EducacionAlimenticia =1 and cai.activo = 1       
        ";

$result_tnj_ea = $conexion->query($sql_tnj_ea);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);

while ($row_tnj_ea = $result_tnj_ea ->fetch_array(MYSQLI_ASSOC)){
    $arrego_tnj_ea[]  = array(
        'id' => $row_tnj_ea['idEntrevista'],
        'lat' => $row_tnj_ea['Latitud'],
        'log' => $row_tnj_ea['Longitud']
    );
}

if (isset($arrego_tnj_ea)){
    //echo'tiene valores';
    $arrego_tnj_ea;
}else{
    //echo 'no';
    $arrego_tnj_ea = 0;
}

//var_dump($arrego_tnj_ea);

/*---------------------------------------------------Trabajo con niños/juventud  Otro*/


$sql_tnj_otra ="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_trabajoninojuventud catnj on cae.idEntrevista = catnj.idAreaTrabajo
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where catnj.OtroTNJ <> '' and cai.activo = 1       
        ";

$result_tnj_otra = $conexion->query($sql_tnj_otra);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);

while ($row_tnj_otra = $result_tnj_otra ->fetch_array(MYSQLI_ASSOC)){
    $arrego_tnj_otra[]  = array(
        'id' => $row_tnj_otra['idEntrevista'],
        'lat' => $row_tnj_otra['Latitud'],
        'log' => $row_tnj_otra['Longitud']
    );
}

if (isset($arrego_tnj_otra)){
    //echo'tiene valores';
    $arrego_tnj_otra;
}else{
    //echo 'no';
    $arrego_tnj_otra = 0;
}
//var_dump($arrego_tnj_otra);

