<?php



/*--------------------------------------------------- EduAmbiental  */
$sql_Edu_EduAmbiental ="        
 select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_educacion caed on cae.idEntrevista = caed.idAreaTrabajo 
where caed.EduAmbiental =1 and cai.activo = 1 ;     
        ";

$result_Edu_EduAmbiental = $conexion->query($sql_Edu_EduAmbiental);

while ($row_Edu_EduAmbiental = $result_Edu_EduAmbiental ->fetch_array(MYSQLI_ASSOC)){
    $array_Edu_EduAmbiental[]  = array(
        'id' => $row_Edu_EduAmbiental['idEntrevista'],
        'lat' => $row_Edu_EduAmbiental['Latitud'],
        'log' => $row_Edu_EduAmbiental['Longitud']

    );
}

if (isset($array_Edu_EduAmbiental)){
    $array_Edu_EduAmbiental;
}else{
    $array_Edu_EduAmbiental = 0;
}

//var_dump($array_Edu_EduAmbiental);

/*--------------------------------------------------- EscuelaCampesina  */
$sql_Edu_EscuelaCampesina ="        
 select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_educacion caed on cae.idEntrevista = caed.idAreaTrabajo 
where caed.EscuelaCampesina =1 and cai.activo = 1 ;     
        ";

$result_Edu_EscuelaCampesina = $conexion->query($sql_Edu_EscuelaCampesina);

while ($row_Edu_EscuelaCampesina = $result_Edu_EscuelaCampesina ->fetch_array(MYSQLI_ASSOC)){
    $array_Edu_EscuelaCampesina[]  = array(
        'id' => $row_Edu_EscuelaCampesina['idEntrevista'],
        'lat' => $row_Edu_EscuelaCampesina['Latitud'],
        'log' => $row_Edu_EscuelaCampesina['Longitud']

    );
}

if (isset($array_Edu_EscuelaCampesina)){
    $array_Edu_EscuelaCampesina;
}else{
    $array_Edu_EscuelaCampesina = 0;
}

//var_dump($array_Edu_EscuelaCampesina);

/*--------------------------------------------------- EscuelaIndigena  */
$sql_Edu_EscuelaIndigena ="        
 select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_educacion caed on cae.idEntrevista = caed.idAreaTrabajo 
where caed.EscuelaIndigena =1 and cai.activo = 1 ;     
        ";

$result_Edu_EscuelaIndigena = $conexion->query($sql_Edu_EscuelaIndigena);

while ($row_Edu_EscuelaIndigena = $result_Edu_EscuelaIndigena ->fetch_array(MYSQLI_ASSOC)){
    $array_Edu_EscuelaIndigena[]  = array(
        'id' => $row_Edu_EscuelaIndigena['idEntrevista'],
        'lat' => $row_Edu_EscuelaIndigena['Latitud'],
        'log' => $row_Edu_EscuelaIndigena['Longitud']

    );
}

if (isset($array_Edu_EscuelaIndigena)){
    $array_Edu_EscuelaIndigena;
}else{
    $array_Edu_EscuelaIndigena = 0;
}

//var_dump($array_Edu_EscuelaIndigena);

/*--------------------------------------------------- FormacionTecnico  */
$sql_Edu_FormacionTecnico ="        
 select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_educacion caed on cae.idEntrevista = caed.idAreaTrabajo 
where caed.FormacionTecnico =1 and cai.activo = 1 ;     
        ";

$result_Edu_FormacionTecnico = $conexion->query($sql_Edu_FormacionTecnico);

while ($row_Edu_FormacionTecnico = $result_Edu_FormacionTecnico ->fetch_array(MYSQLI_ASSOC)){
    $array_Edu_FormacionTecnico[]  = array(
        'id' => $row_Edu_FormacionTecnico['idEntrevista'],
        'lat' => $row_Edu_FormacionTecnico['Latitud'],
        'log' => $row_Edu_FormacionTecnico['Longitud']

    );
}

if (isset($array_Edu_FormacionTecnico)){
    $array_Edu_FormacionTecnico;
}else{
    $array_Edu_FormacionTecnico = 0;
}

//var_dump($array_Edu_FormacionTecnico);

/*--------------------------------------------------- ComuniAprende  */
$sql_Edu_ComuniAprende ="        
 select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_educacion caed on cae.idEntrevista = caed.idAreaTrabajo 
where caed.ComuniAprende =1 and cai.activo = 1 ;     
        ";

$result_Edu_ComuniAprende = $conexion->query($sql_Edu_ComuniAprende);

while ($row_Edu_ComuniAprende = $result_Edu_ComuniAprende ->fetch_array(MYSQLI_ASSOC)){
    $array_Edu_ComuniAprende[]  = array(
        'id' => $row_Edu_ComuniAprende['idEntrevista'],
        'lat' => $row_Edu_ComuniAprende['Latitud'],
        'log' => $row_Edu_ComuniAprende['Longitud']

    );
}

if (isset($array_Edu_ComuniAprende)){
    $array_Edu_ComuniAprende;
}else{
    $array_Edu_ComuniAprende = 0;
}

//var_dump($array_Edu_ComuniAprende);

/*--------------------------------------------------- IniciativaCampesina  */
$sql_Edu_IniciativaCampesina ="        
 select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_educacion caed on cae.idEntrevista = caed.idAreaTrabajo 
where caed.IniciativaCampesina =1 and cai.activo = 1 ;     
        ";

$result_Edu_IniciativaCampesina = $conexion->query($sql_Edu_IniciativaCampesina);

while ($row_Edu_IniciativaCampesina = $result_Edu_IniciativaCampesina ->fetch_array(MYSQLI_ASSOC)){
    $array_Edu_IniciativaCampesina[]  = array(
        'id' => $row_Edu_IniciativaCampesina['idEntrevista'],
        'lat' => $row_Edu_IniciativaCampesina['Latitud'],
        'log' => $row_Edu_IniciativaCampesina['Longitud']

    );
}

if (isset($array_Edu_IniciativaCampesina)){
    $array_Edu_IniciativaCampesina;
}else{
    $array_Edu_IniciativaCampesina = 0;
}

//var_dump($array_Edu_ComuniAprende);

/*--------------------------------------------------- Basicas  */
$sql_Edu_Basicas ="        
 select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_educacion caed on cae.idEntrevista = caed.idAreaTrabajo 
where caed.Basicas =1 and cai.activo = 1 ;     
        ";

$result_Edu_Basicas = $conexion->query($sql_Edu_Basicas);

while ($row_Edu_Basicas = $result_Edu_Basicas ->fetch_array(MYSQLI_ASSOC)){
    $array_Edu_Basicas[]  = array(
        'id' => $row_Edu_Basicas['idEntrevista'],
        'lat' => $row_Edu_Basicas['Latitud'],
        'log' => $row_Edu_Basicas['Longitud']

    );
}

if (isset($array_Edu_Basicas)){
    $array_Edu_Basicas;
}else{
    $array_Edu_Basicas = 0;
}

//var_dump($array_Edu_Basicas);

/*--------------------------------------------------- MediaSuperior  */
$sql_Edu_MediaSuperior ="        
 select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_educacion caed on cae.idEntrevista = caed.idAreaTrabajo 
where caed.MediaSuperior =1 and cai.activo = 1 ;     
        ";

$result_Edu_MediaSuperior = $conexion->query($sql_Edu_MediaSuperior);

while ($row_Edu_MediaSuperior = $result_Edu_MediaSuperior ->fetch_array(MYSQLI_ASSOC)){
    $array_Edu_MediaSuperior[]  = array(
        'id' => $row_Edu_MediaSuperior['idEntrevista'],
        'lat' => $row_Edu_MediaSuperior['Latitud'],
        'log' => $row_Edu_MediaSuperior['Longitud']

    );
}

if (isset($array_Edu_MediaSuperior)){
    $array_Edu_MediaSuperior;
}else{
    $array_Edu_MediaSuperior = 0;
}

//var_dump($array_Edu_MediaSuperior);

/*--------------------------------------------------- Superior  */
$sql_Edu_Superior ="        
 select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_educacion caed on cae.idEntrevista = caed.idAreaTrabajo 
where caed.Superior =1 and cai.activo = 1 ;     
        ";

$result_Edu_Superior = $conexion->query($sql_Edu_Superior);

while ($row_Edu_Superior = $result_Edu_Superior ->fetch_array(MYSQLI_ASSOC)){
    $array_Edu_Superior[]  = array(
        'id' => $row_Edu_Superior['idEntrevista'],
        'lat' => $row_Edu_Superior['Latitud'],
        'log' => $row_Edu_Superior['Longitud']

    );
}

if (isset($array_Edu_Superior)){
    $array_Edu_Superior;
}else{
    $array_Edu_Superior = 0;
}

//var_dump($array_Edu_Superior);

/*--------------------------------------------------- OtroEcudacion  */
$sql_Edu_OtroEcudacion ="        
 select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_educacion caed on cae.idEntrevista = caed.idAreaTrabajo 
where caed.OtroEcudacion <> '' and cai.activo = 1 ;     
        ";

$result_Edu_OtroEcudacion = $conexion->query($sql_Edu_OtroEcudacion);

while ($row_Edu_OtroEcudacion = $result_Edu_OtroEcudacion ->fetch_array(MYSQLI_ASSOC)){
    $array_Edu_OtroEcudacion[]  = array(
        'id' => $row_Edu_OtroEcudacion['idEntrevista'],
        'lat' => $row_Edu_OtroEcudacion['Latitud'],
        'log' => $row_Edu_OtroEcudacion['Longitud']

    );
}

if (isset($array_Edu_OtroEcudacion)){
    $array_Edu_OtroEcudacion;
}else{
    $array_Edu_OtroEcudacion = 0;
}

//var_dump($array_Edu_OtroEcudacion);








