<?php


/*---------------------------------------------------Derechos humanos dh  */


$sql_DH_dh ="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_derechos_humanos cadh on cae.idEntrevista = cadh.idAreaTrabajo 
where cadh.DerechosHumanos =1 and cai.activo = 1 and cae.bajaLogica = 0      
        ";

$result_DH_dh = $conexion->query($sql_DH_dh);



while ($row_DH_dh = $result_DH_dh ->fetch_array(MYSQLI_ASSOC)){
    $arrego_DH_dh[]  = array(
        'id' => $row_DH_dh['idEntrevista'],
        'lat' => $row_DH_dh['Latitud'],
        'log' => $row_DH_dh['Longitud']

    );
}

if (isset($arrego_DH_dh)){

    //echo'tiene valores';
    $arrego_DH_dh ;
}else{
    //echo 'no';
    $arrego_DH_dh = 0;
}

//var_dump($arrego_DH_dh);


/*---------------------------------------------------Derechos humanos de  */


$sql_DH_de ="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_derechos_humanos cadh on cae.idEntrevista = cadh.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where cadh.DerechosEconomicos =1 and cai.activo = 1        
        ";

$result_DH_de = $conexion->query($sql_DH_de);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($row_DH_de = $result_DH_de ->fetch_array(MYSQLI_ASSOC)){
    $arrego_DH_de[]  = array(
        'id' => $row_DH_de['idEntrevista'],
        'lat' => $row_DH_de['Latitud'],
        'log' => $row_DH_de['Longitud']

    );
}

if (isset($arrego_DH_de)){

    //echo'tiene valores';
    $arrego_DH_de;
}else{
    //echo 'no';
    $arrego_DH_de = 0;
}

//var_dump($arrego_DH_de);

/*---------------------------------------------------Derechos humanos derechos ambientales */


$sql_DH_da ="        
   select
distinct cae.idEntrevista,
cae.NombreGrupo,
cai.NombreIniciativa,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_derechos_humanos cadh on cae.idEntrevista = cadh.idAreaTrabajo 
where cadh.DerechosAmbientales = 1 and cai.activo = 1;       
        ";

$result_DH_da = $conexion->query($sql_DH_da);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);

while ($row_DH_da = $result_DH_da ->fetch_array(MYSQLI_ASSOC)){
    $arrego_DH_da[]  = array(
        'id' => $row_DH_da['idEntrevista'],
        'lat' => $row_DH_da['Latitud'],
        'log' => $row_DH_da['Longitud']
    );
}

if (isset($arrego_DH_da)){
    //echo'tiene valores';
    $arrego_DH_da;
}else{
    //echo 'no';
    $arrego_DH_da = 0;
}

//var_dump($arrego_DH_da);

