<?php


/*---------------------------------------------------AreaDestinoConservaSilvo */
$sql_sa_adcs ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemasagroforestales casfo  on cae.idEntrevista = casfo.idAreaTrabajo 
where casfo.AreaDestinoConservaSilvo =1 and cai.activo = 1     
        ";

$result_sa_adcs = $conexion->query($sql_sa_adcs);
while ($row_sa_adcs = $result_sa_adcs ->fetch_array(MYSQLI_ASSOC)){
    $arrego_sa_adcs[]  = array(
        'id' => $row_sa_adcs['idEntrevista'],
        'lat' => $row_sa_adcs['Latitud'],
        'log' => $row_sa_adcs['Longitud']

    );
}
if (isset($arrego_sa_adcs)){
    $arrego_sa_adcs ;
}else{
    $arrego_sa_adcs = 0;
}

//var_dump($arrego_sa_adcs);

/*---------------------------------------------------SistemaAgroforestal */
$sql_sa_sa ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemasagroforestales casfo  on cae.idEntrevista = casfo.idAreaTrabajo 
where casfo.SistemaAgroforestal =1 and cai.activo = 1     
        ";

$result_sa_sa = $conexion->query($sql_sa_sa);
while ($row_sa_sa = $result_sa_sa ->fetch_array(MYSQLI_ASSOC)){
    $arrego_sa_sa[]  = array(
        'id' => $row_sa_sa['idEntrevista'],
        'lat' => $row_sa_sa['Latitud'],
        'log' => $row_sa_sa['Longitud']

    );
}
if (isset($arrego_sa_sa)){
    $arrego_sa_sa ;
}else{
    $arrego_sa_sa = 0;
}
//var_dump($arrego_sa_sa);

/*---------------------------------------------------BosqueComestible */
$sql_sa_bc ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemasagroforestales casfo  on cae.idEntrevista = casfo.idAreaTrabajo 
where casfo.BosqueComestible =1 and cai.activo = 1     
        ";

$result_sa_bc = $conexion->query($sql_sa_bc);
while ($row_sa_bc = $result_sa_bc ->fetch_array(MYSQLI_ASSOC)){
    $arrego_sa_bc[]  = array(
        'id' => $row_sa_bc['idEntrevista'],
        'lat' => $row_sa_bc['Latitud'],
        'log' => $row_sa_bc['Longitud']

    );
}
if (isset($arrego_sa_bc)){
    $arrego_sa_bc ;
}else{
    $arrego_sa_bc = 0;
}
//var_dump($arrego_sa_bc);

/*---------------------------------------------------CercasVivas */
$sql_sa_cv ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemasagroforestales casfo  on cae.idEntrevista = casfo.idAreaTrabajo 
where casfo.CercasVivas =1 and cai.activo = 1     
        ";

$result_sa_cv = $conexion->query($sql_sa_cv);
while ($row_sa_cv = $result_sa_cv ->fetch_array(MYSQLI_ASSOC)){
    $arrego_sa_cv[]  = array(
        'id' => $row_sa_cv['idEntrevista'],
        'lat' => $row_sa_cv['Latitud'],
        'log' => $row_sa_cv['Longitud']

    );
}
if (isset($arrego_sa_cv)){
    $arrego_sa_cv ;
}else{
    $arrego_sa_cv = 0;
}
//var_dump($arrego_sa_cv);

/*---------------------------------------------------BarreraRompeVientos */
$sql_sa_brv ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemasagroforestales casfo  on cae.idEntrevista = casfo.idAreaTrabajo 
where casfo.BarreraRompeVientos =1 and cai.activo = 1     
        ";

$result_sa_brv = $conexion->query($sql_sa_brv);
while ($row_sa_brv = $result_sa_brv ->fetch_array(MYSQLI_ASSOC)){
    $arrego_sa_brv[]  = array(
        'id' => $row_sa_brv['idEntrevista'],
        'lat' => $row_sa_brv['Latitud'],
        'log' => $row_sa_brv['Longitud']

    );
}
if (isset($arrego_sa_brv)){
    $arrego_sa_brv ;
}else{
    $arrego_sa_brv = 0;
}
//var_dump($arrego_sa_brv);

/*---------------------------------------------------OtrosistemaAgro */
$sql_sa_otro ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_sistemasagroforestales casfo  on cae.idEntrevista = casfo.idAreaTrabajo 
where casfo.OtrosistemaAgro =1 and cai.activo = 1     
        ";

$result_sa_otro = $conexion->query($sql_sa_otro);
while ($row_sa_otro = $result_sa_otro ->fetch_array(MYSQLI_ASSOC)){
    $arrego_sa_otro[]  = array(
        'id' => $row_sa_otro['idEntrevista'],
        'lat' => $row_sa_otro['Latitud'],
        'log' => $row_sa_otro['Longitud']
    );
}
if (isset($arrego_sa_otro)){
    $arrego_sa_otro ;
}else{
    $arrego_sa_otro = 0;
}
//var_dump($arrego_sa_otro);

