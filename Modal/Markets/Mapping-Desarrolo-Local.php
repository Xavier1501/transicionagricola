<?php

/*--------------------------------------------------- desarrollo local AcomProyectos*/
$sql_AcomProyectos="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_desarollolocal cadl on cadl.idAreaTrabajo = cae.idEntrevista 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where cadl.AcomProyectos = 1 and cai.activo = 1      
        ";

$resultA_AcomProyectos= $conexion->query($sql_AcomProyectos);

while ($row_AcomProyectos = $resultA_AcomProyectos ->fetch_array(MYSQLI_ASSOC)){
    $arr_DL_AP []  = array(
        'id' => $row_AcomProyectos['idEntrevista'],
        'lat' => $row_AcomProyectos['Latitud'],
        'log' => $row_AcomProyectos['Longitud']
    );
}
if (isset($arr_DL_AP)){
    $arr_DL_AP ;
}else{
    $arr_DL_AP = 0;
}
//var_dump($arreglo_DL_AcomProyectos);

/*--------------------------------------------------- desarrollo local Desarrollo*/
$sql_dlDesarrollo="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_desarollolocal cadl on cadl.idAreaTrabajo = cae.idEntrevista 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where cadl.Desarollo = 1 and cai.activo = 1     
        ";

$resultA_dlDesarrollo= $conexion->query($sql_dlDesarrollo);

while ($row_dlDesarrollo= $resultA_dlDesarrollo ->fetch_array(MYSQLI_ASSOC)){
    $arr_dlDesarrollo []  = array(
        'id' => $row_dlDesarrollo['idEntrevista'],
        'lat' => $row_dlDesarrollo['Latitud'],
        'log' => $row_dlDesarrollo['Longitud']
    );
}
if (isset($arr_dlDesarrollo)){
    $arr_dlDesarrollo ;
}else{
    $arr_dlDesarrollo = 0;
}
//var_dump($arr_dlDesarrollo);

/*--------------------------------------------------- desarrollo local OrganizacionAgriculturales*/
$sql_dl_Org="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_desarollolocal cadl on cadl.idAreaTrabajo = cae.idEntrevista 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where cadl.OrganizacionAgriculturales = 1 and cai.activo = 1     
        ";
$resultA_dl_Org= $conexion->query($sql_dl_Org);
while ($row_dl_Org= $resultA_dl_Org ->fetch_array(MYSQLI_ASSOC)){
    $arr_dl_Org []  = array(
        'id' => $row_dl_Org['idEntrevista'],
        'lat' => $row_dl_Org['Latitud'],
        'log' => $row_dl_Org['Longitud']
    );
}
if (isset($arr_dl_Org)){
    $arr_dl_Org ;
}else{
    $arr_dl_Org = 0;
}
//var_dump($arr_dl_Org);


/*--------------------------------------------------- desarrollo local Gobierno*/
$sql_dl_Gob="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_desarollolocal cadl on cadl.idAreaTrabajo = cae.idEntrevista 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where cadl.ProgramaGobierno = 1 and cai.activo = 1      
        ";
$resultA_dl_Gob= $conexion->query($sql_dl_Gob);
while ($row_dl_Gob= $resultA_dl_Gob ->fetch_array(MYSQLI_ASSOC)){
    $arr_dl_Gob []  = array(
        'id' => $row_dl_Gob['idEntrevista'],
        'lat' => $row_dl_Gob['Latitud'],
        'log' => $row_dl_Gob['Longitud']
    );
}
if (isset($arr_dl_Gob)){
    $arr_dl_Gob ;
}else{
    $arr_dl_Gob = 0;
}
//var_dump($arr_dl_Gob);

/*--------------------------------------------------- desarrollo local Nucleos*/
$sql_dl_Nuc="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_desarollolocal cadl on cadl.idAreaTrabajo = cae.idEntrevista 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where cadl.NucleoAgrario = 1 and cai.activo = 1      
        ";
$resultA_dl_Nuc= $conexion->query($sql_dl_Nuc);
while ($row_dl_Nuc= $resultA_dl_Nuc ->fetch_array(MYSQLI_ASSOC)){
    $arr_dl_Nuc []  = array(
        'id' => $row_dl_Nuc['idEntrevista'],
        'lat' => $row_dl_Nuc['Latitud'],
        'log' => $row_dl_Nuc['Longitud']
    );
}
if (isset($arr_dl_Nuc)){
    $arr_dl_Nuc ;
}else{
    $arr_dl_Nuc = 0;
}
//var_dump($arr_dl_Gob);

/*--------------------------------------------------- desarrollo local Otros*/
$sql_dl_Otro="        
        select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_desarollolocal cadl on cadl.idAreaTrabajo = cae.idEntrevista 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where cadl.OtroDesaLocal <> '' and cai.activo = 1      
        ";
$resultA_dl_Otro= $conexion->query($sql_dl_Otro);
while ($row_dl_Otro= $resultA_dl_Otro ->fetch_array(MYSQLI_ASSOC)){
    $arr_dl_Otro []  = array(
        'id' => $row_dl_Otro['idEntrevista'],
        'lat' => $row_dl_Otro['Latitud'],
        'log' => $row_dl_Otro['Longitud']
    );
}
if (isset($arr_dl_Otro)){
    $arr_dl_Otro ;
}else{
    $arr_dl_Otro = 0;
}
//var_dump($arr_dl_Gob);
