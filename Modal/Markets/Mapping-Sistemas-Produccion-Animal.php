<?php


/*---------------------------------------------------Abejas Europeas */


$sql_span_ae ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.AbejaEuropeas =1 and cai.activo = 1    
        ";
$result_span_ae = $conexion->query($sql_span_ae);

while ($row_span_ae = $result_span_ae ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_ae[]  = array(
        'id' => $row_span_ae['idEntrevista'],
        'lat' => $row_span_ae['Latitud'],
        'log' => $row_span_ae['Longitud']

    );
}

if (isset($arrego_span_ae)){

    //echo'tiene valores';
    $arrego_span_ae ;
}else{
    //echo 'no';
    $arrego_span_ae = 0;
}

//var_dump($arrego_span_ae);

/*---------------------------------------------------Abejas Nativas */
$sql_span_an ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.AbejasNativas =1 and cai.activo = 1    
        ";
$result_span_an = $conexion->query($sql_span_an);
while ($row_span_an = $result_span_an ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_an[]  = array(
        'id' => $row_span_an['idEntrevista'],
        'lat' => $row_span_an['Latitud'],
        'log' => $row_span_an['Longitud']

    );
}
if (isset($arrego_span_an)){

    //echo'tiene valores';
    $arrego_span_an ;
}else{
    //echo 'no';
    $arrego_span_an = 0;
}
//var_dump($arrego_span_an);


/*---------------------------------------------------Acuacultura */
$sql_span_acua ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Acuacultura =1 and cai.activo = 1    
        ";

$result_span_acua = $conexion->query($sql_span_acua);
while ($row_span_acua = $result_span_acua ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_acua[]  = array(
        'id' => $row_span_acua['idEntrevista'],
        'lat' => $row_span_acua['Latitud'],
        'log' => $row_span_acua['Longitud']

    );
}
if (isset($arrego_span_acua)){
    $arrego_span_acua ;
}else{
    $arrego_span_acua = 0;
}
//var_dump($arrego_span_acua);

/*---------------------------------------------------Aves */
$sql_span_aves ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Aves =1 and cai.activo = 1    
        ";

$result_span_aves = $conexion->query($sql_span_aves);
while ($row_span_aves = $result_span_aves ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_aves[]  = array(
        'id' => $row_span_aves['idEntrevista'],
        'lat' => $row_span_aves['Latitud'],
        'log' => $row_span_aves['Longitud']

    );
}
if (isset($arrego_span_aves)){
    $arrego_span_aves ;
}else{
    $arrego_span_aves = 0;
}
//var_dump($arrego_span_aves);

/*---------------------------------------------------Bovinos */
$sql_span_bov ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Bovinos =1 and cai.activo = 1    
        ";

$result_span_bov = $conexion->query($sql_span_bov);
while ($row_span_bov = $result_span_bov ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_bov[]  = array(
        'id' => $row_span_bov['idEntrevista'],
        'lat' => $row_span_bov['Latitud'],
        'log' => $row_span_bov['Longitud']

    );
}
if (isset($arrego_span_bov)){
    $arrego_span_bov ;
}else{
    $arrego_span_bov = 0;
}
//var_dump($arrego_span_bov);

/*---------------------------------------------------Caprinos */
$sql_span_capr ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Caprinos =1 and cai.activo = 1    
        ";

$result_span_capr = $conexion->query($sql_span_capr);
while ($row_span_capr = $result_span_capr ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_capr[]  = array(
        'id' => $row_span_capr['idEntrevista'],
        'lat' => $row_span_capr['Latitud'],
        'log' => $row_span_capr['Longitud']

    );
}
if (isset($arrego_span_capr)){
    $arrego_span_capr ;
}else{
    $arrego_span_capr = 0;
}
//var_dump($arrego_span_capr);

/*---------------------------------------------------Cuniculturas */
$sql_span_cuni ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Cuniculturas =1 and cai.activo = 1    
        ";

$result_span_cuni = $conexion->query($sql_span_cuni);
while ($row_span_cuni = $result_span_cuni ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_cuni[]  = array(
        'id' => $row_span_cuni['idEntrevista'],
        'lat' => $row_span_cuni['Latitud'],
        'log' => $row_span_cuni['Longitud']

    );
}
if (isset($arrego_span_cuni)){
    $arrego_span_cuni ;
}else{
    $arrego_span_cuni = 0;
}
//var_dump($arrego_span_cuni);

/*---------------------------------------------------Equinos */
$sql_span_equi ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Equinos =1 and cai.activo = 1    
        ";

$result_span_equi = $conexion->query($sql_span_equi);
while ($row_span_equi = $result_span_equi ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_equi[]  = array(
        'id' => $row_span_equi['idEntrevista'],
        'lat' => $row_span_equi['Latitud'],
        'log' => $row_span_equi['Longitud']

    );
}
if (isset($arrego_span_equi)){
    $arrego_span_equi ;
}else{
    $arrego_span_equi = 0;
}
//var_dump($arrego_span_equi);

/*---------------------------------------------------Lombricultura */
$sql_span_lomb ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Lombricultura =1 and cai.activo = 1    
        ";

$result_span_lomb = $conexion->query($sql_span_lomb);
while ($row_span_lomb = $result_span_lomb ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_lomb[]  = array(
        'id' => $row_span_lomb['idEntrevista'],
        'lat' => $row_span_lomb['Latitud'],
        'log' => $row_span_lomb['Longitud']

    );
}
if (isset($arrego_span_lomb)){
    $arrego_span_lomb ;
}else{
    $arrego_span_lomb = 0;
}
//var_dump($arrego_span_lomb);

/*---------------------------------------------------Ovinos */
$sql_span_ovi ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Ovinos =1 and cai.activo = 1    
        ";

$result_span_ovi = $conexion->query($sql_span_ovi);
while ($row_span_ovi = $result_span_ovi ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_ovi[]  = array(
        'id' => $row_span_ovi['idEntrevista'],
        'lat' => $row_span_ovi['Latitud'],
        'log' => $row_span_ovi['Longitud']

    );
}
if (isset($arrego_span_ovi)){
    $arrego_span_ovi ;
}else{
    $arrego_span_ovi = 0;
}
//var_dump($arrego_span_ovi);

/*---------------------------------------------------Pesca */
$sql_span_pes ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Pesca =1 and cai.activo = 1    
        ";

$result_span_pes = $conexion->query($sql_span_pes);
while ($row_span_pes = $result_span_pes ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_pes[]  = array(
        'id' => $row_span_pes['idEntrevista'],
        'lat' => $row_span_pes['Latitud'],
        'log' => $row_span_pes['Longitud']

    );
}
if (isset($arrego_span_pes)){
    $arrego_span_pes ;
}else{
    $arrego_span_pes = 0;
}
//var_dump($arrego_span_pes);

/*---------------------------------------------------Porcinos */
$sql_span_porc ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Porcinos =1 and cai.activo = 1    
        ";

$result_span_porc = $conexion->query($sql_span_porc);
while ($row_span_porc = $result_span_porc ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_porc[]  = array(
        'id' => $row_span_porc['idEntrevista'],
        'lat' => $row_span_porc['Latitud'],
        'log' => $row_span_porc['Longitud']

    );
}
if (isset($arrego_span_porc)){
    $arrego_span_porc ;
}else{
    $arrego_span_porc = 0;
}
//var_dump($arrego_span_porc);

/*---------------------------------------------------Conejo */
$sql_span_conejo ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Conejo =1 and cai.activo = 1    
        ";

$result_span_conejo = $conexion->query($sql_span_conejo);
while ($row_span_conejo = $result_span_conejo ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_conejo[]  = array(
        'id' => $row_span_conejo['idEntrevista'],
        'lat' => $row_span_conejo['Latitud'],
        'log' => $row_span_conejo['Longitud']

    );
}
if (isset($arrego_span_conejo)){
    $arrego_span_conejo ;
}else{
    $arrego_span_conejo = 0;
}
//var_dump($arrego_span_conejo);

/*---------------------------------------------------MejoramientoGenetico */
$sql_span_mg ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.MejoramientoGenetico =1 and cai.activo = 1    
        ";

$result_span_mg = $conexion->query($sql_span_mg);
while ($row_span_mg = $result_span_mg ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_mg[]  = array(
        'id' => $row_span_mg['idEntrevista'],
        'lat' => $row_span_mg['Latitud'],
        'log' => $row_span_mg['Longitud']

    );
}
if (isset($arrego_span_mg)){
    $arrego_span_mg ;
}else{
    $arrego_span_mg = 0;
}
//var_dump($arrego_span_mg);

/*---------------------------------------------------NutricionAnimal */
$sql_span_nan ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.NutricionAnimal =1 and cai.activo = 1    
        ";

$result_span_nan = $conexion->query($sql_span_nan);
while ($row_span_nan = $result_span_nan ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_nan[]  = array(
        'id' => $row_span_nan['idEntrevista'],
        'lat' => $row_span_nan['Latitud'],
        'log' => $row_span_nan['Longitud']

    );
}
if (isset($arrego_span_nan)){
    $arrego_span_nan ;
}else{
    $arrego_span_nan = 0;
}
//var_dump($arrego_span_nan);

/*---------------------------------------------------PracticasSalud */
$sql_span_ps ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.PracticasSalud =1 and cai.activo = 1    
        ";

$result_span_ps = $conexion->query($sql_span_ps);
while ($row_span_ps = $result_span_ps ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_ps[]  = array(
        'id' => $row_span_ps['idEntrevista'],
        'lat' => $row_span_ps['Latitud'],
        'log' => $row_span_ps['Longitud']

    );
}
if (isset($arrego_span_ps)){
    $arrego_span_ps ;
}else{
    $arrego_span_ps = 0;
}
//var_dump($arrego_span_ps);

/*---------------------------------------------------VeterinariaNatural */
$sql_span_vn ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.VeterinariaNatural =1 and cai.activo = 1    
        ";

$result_span_vn = $conexion->query($sql_span_vn);
while ($row_span_vn = $result_span_vn ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_vn[]  = array(
        'id' => $row_span_vn['idEntrevista'],
        'lat' => $row_span_vn['Latitud'],
        'log' => $row_span_vn['Longitud']

    );
}
if (isset($arrego_span_vn)){
    $arrego_span_vn ;
}else{
    $arrego_span_vn = 0;
}
//var_dump($arrego_span_vn);

/*---------------------------------------------------Homeopatia */

$sql_span_hom ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Homeopatia =1 and cai.activo = 1    
        ";

$result_span_hom = $conexion->query($sql_span_hom);
while ($row_span_hom = $result_span_hom ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_hom[]  = array(
        'id' => $row_span_hom['idEntrevista'],
        'lat' => $row_span_hom['Latitud'],
        'log' => $row_span_hom['Longitud']

    );
}
if (isset($arrego_span_hom)){
    $arrego_span_hom ;
}else{
    $arrego_span_hom = 0;
}
//var_dump($arrego_span_hom);

/*---------------------------------------------------Construccio */

$sql_span_cons ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.Construccio =1 and cai.activo = 1    
        ";

$result_span_cons = $conexion->query($sql_span_cons);
while ($row_span_cons = $result_span_cons ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_cons[]  = array(
        'id' => $row_span_cons['idEntrevista'],
        'lat' => $row_span_cons['Latitud'],
        'log' => $row_span_cons['Longitud']

    );
}
if (isset($arrego_span_cons)){
    $arrego_span_cons ;
}else{
    $arrego_span_cons = 0;
}

//var_dump($arrego_span_cons);

/*---------------------------------------------------OtroProduccion */

$sql_span_otro ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista
left join co_agr_sistemaproduccionanimal caspa on cae.idEntrevista = caspa.idAreaTrabajo 
where caspa.OtroProduccion =1 and cai.activo = 1    
        ";

$result_span_otro = $conexion->query($sql_span_otro);
while ($row_span_otro = $result_span_otro ->fetch_array(MYSQLI_ASSOC)){
    $arrego_span_otro[]  = array(
        'id' => $row_span_otro['idEntrevista'],
        'lat' => $row_span_otro['Latitud'],
        'log' => $row_span_otro['Longitud']

    );
}
if (isset($arrego_span_otro)){
    $arrego_span_otro ;
}else{
    $arrego_span_otro = 0;
}

//var_dump($arrego_span_otro);
