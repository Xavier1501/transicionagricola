<?php




/*---------------------------------------------------Investigación agrícola y Extensión rural  comunicacion*/


$sql_IAER_c ="        
    select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_investigacionagricolarural caiar on cae.idEntrevista = caiar.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caiar.Comunicacion =1 and cai.activo = 1        
        ";

$result_IAER_c = $conexion->query($sql_IAER_c);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);

while ($row_IAER_c = $result_IAER_c ->fetch_array(MYSQLI_ASSOC)){
    $arrego_IAER_c[]  = array(
        'id' => $row_IAER_c['idEntrevista'],
        'lat' => $row_IAER_c['Latitud'],
        'log' => $row_IAER_c['Longitud']
    );
}

if (isset($arrego_IAER_c)){
    //echo'tiene valores';
    $arrego_IAER_c;
}else{
    //echo 'no';
    $arrego_IAER_c = 0;
}

//var_dump($arrego_IAER_c);


/*---------------------------------------------------Investigación agrícola y Extensión rural DiagAgroecosistema*/


$sql_IAER_d ="        
    select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_investigacionagricolarural caiar on cae.idEntrevista = caiar.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caiar.DiagAgroecosistema =1 and cai.activo = 1         
        ";

$result_IAER_d = $conexion->query($sql_IAER_d);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);

while ($row_IAER_d = $result_IAER_d ->fetch_array(MYSQLI_ASSOC)){
    $arrego_IAER_d[]  = array(
        'id' => $row_IAER_d['idEntrevista'],
        'lat' => $row_IAER_d['Latitud'],
        'log' => $row_IAER_d['Longitud']
    );
}

if (isset($arrego_IAER_d)){
    //echo'tiene valores';
    $arrego_IAER_d;
}else{
    //echo 'no';
    $arrego_IAER_d = 0;
}

//var_dump($arrego_IAER_d);

/*---------------------------------------------------Investigación agrícola y Extensión rural DiagAgroecosistema*/


$sql_IAER_e ="        
    select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_investigacionagricolarural caiar on cae.idEntrevista = caiar.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caiar.Experimentacion =1 and cai.activo = 1         
        ";

$result_IAER_e = $conexion->query($sql_IAER_e);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);

while ($row_IAER_e = $result_IAER_e ->fetch_array(MYSQLI_ASSOC)){
    $arrego_IAER_e[]  = array(
        'id' => $row_IAER_e['idEntrevista'],
        'lat' => $row_IAER_e['Latitud'],
        'log' => $row_IAER_e['Longitud']
    );
}

if (isset($arrego_IAER_e)){
    //echo'tiene valores';
    $arrego_IAER_e;
}else{
    //echo 'no';
    $arrego_IAER_e = 0;
}

//var_dump($arrego_IAER_e);

/*---------------------------------------------------Investigación agrícola y Extensión rural DiagAgroecosistema*/


$sql_IAER_fc ="        
    select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_investigacionagricolarural caiar on cae.idEntrevista = caiar.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caiar.FormacionCapacitacion =1 and cai.activo = 1         
        ";

$result_IAER_fc = $conexion->query($sql_IAER_fc);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);

while ($row_IAER_fc = $result_IAER_fc ->fetch_array(MYSQLI_ASSOC)){
    $arrego_IAER_fc[]  = array(
        'id' => $row_IAER_fc['idEntrevista'],
        'lat' => $row_IAER_fc['Latitud'],
        'log' => $row_IAER_fc['Longitud']
    );
}

if (isset($arrego_IAER_fc)){
    //echo'tiene valores';
    $arrego_IAER_fc;
}else{
    //echo 'no';
    $arrego_IAER_fc = 0;
}

//var_dump($arrego_IAER_fc);

/*---------------------------------------------------Investigación agrícola y Extensión rural DiagAgroecosistema*/


$sql_IAER_in ="        
    select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_investigacionagricolarural caiar on cae.idEntrevista = caiar.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caiar.Investigacion =1 and cai.activo = 1         
        ";

$result_IAER_in = $conexion->query($sql_IAER_in);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);

while ($row_IAER_in = $result_IAER_in ->fetch_array(MYSQLI_ASSOC)){
    $arrego_IAER_in[]  = array(
        'id' => $row_IAER_in['idEntrevista'],
        'lat' => $row_IAER_in['Latitud'],
        'log' => $row_IAER_in['Longitud']
    );
}

if (isset($arrego_IAER_in)){
    //echo'tiene valores';
    $arrego_IAER_in;
}else{
    //echo 'no';
    $arrego_IAER_in = 0;
}

//var_dump($arrego_IAER_in);

/*---------------------------------------------------Investigación agrícola y Extensión rural DiagAgroecosistema*/


$sql_IAER_orn ="        
    select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_investigacionagricolarural caiar on cae.idEntrevista = caiar.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caiar.OrdenTerritoral =1 and cai.activo = 1         
        ";

$result_IAER_orn = $conexion->query($sql_IAER_orn);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);

while ($row_IAER_orn = $result_IAER_orn ->fetch_array(MYSQLI_ASSOC)){
    $arrego_IAER_orn[]  = array(
        'id' => $row_IAER_orn['idEntrevista'],
        'lat' => $row_IAER_orn['Latitud'],
        'log' => $row_IAER_orn['Longitud']
    );
}

if (isset($arrego_IAER_orn)){
    //echo'tiene valores';
    $arrego_IAER_orn;
}else{
    //echo 'no';
    $arrego_IAER_orn = 0;
}

//var_dump($arrego_IAER_orn);

/*---------------------------------------------------Investigación agrícola y Extensión rural DiagAgroecosistema*/


$sql_IAER_otro ="        
    select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_investigacionagricolarural caiar on cae.idEntrevista = caiar.idAreaTrabajo 
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caiar.OtroInvestigacionRural <> '' and cai.activo = 1         
        ";

$result_IAER_otro = $conexion->query($sql_IAER_otro);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);

while ($row_IAER_otro = $result_IAER_otro ->fetch_array(MYSQLI_ASSOC)){
    $arrego_IAER_otro[]  = array(
        'id' => $row_IAER_otro['idEntrevista'],
        'lat' => $row_IAER_otro['Latitud'],
        'log' => $row_IAER_otro['Longitud']
    );
}

if (isset($arrego_IAER_otro)){
    //echo'tiene valores';
    $arrego_IAER_otro;
}else{
    //echo 'no';
    $arrego_IAER_otro = 0;
}

//var_dump($arrego_IAER_otro);

