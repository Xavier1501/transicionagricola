<?php



/*--------------------------------------------------- AutoConsumo  */
$sql_E_AutoConsumo ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.AutoConsumo =1 and cai.activo = 1 ;        
        ";

$result_E_AutoConsumo = $conexion->query($sql_E_AutoConsumo);

while ($row_E_AutoConsumo = $result_E_AutoConsumo ->fetch_array(MYSQLI_ASSOC)){
    $array_E_AutoConsumo[]  = array(
        'id' => $row_E_AutoConsumo['idEntrevista'],
        'lat' => $row_E_AutoConsumo['Latitud'],
        'log' => $row_E_AutoConsumo['Longitud']

    );
}

if (isset($array_E_AutoConsumo)){
    $array_E_AutoConsumo;
}else{
    $array_E_AutoConsumo = 0;
}

//var_dump($array_E_AutoConsumo);

/*--------------------------------------------------- CertificaParticipativa */
$sql_E_CertificaParticipativa ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.CertificaParticipativa =1 and cai.activo = 1 ;        
        ";

$result_E_CertificaParticipativa = $conexion->query($sql_E_CertificaParticipativa);

while ($row_E_CertificaParticipativa = $result_E_CertificaParticipativa ->fetch_array(MYSQLI_ASSOC)){
    $array_E_CertificaParticipativa[]  = array(
        'id' => $row_E_CertificaParticipativa['idEntrevista'],
        'lat' => $row_E_CertificaParticipativa['Latitud'],
        'log' => $row_E_CertificaParticipativa['Longitud']

    );
}

if (isset($array_E_CertificaParticipativa)){
    $array_E_CertificaParticipativa;
}else{
    $array_E_CertificaParticipativa = 0;
}
//var_dump($array_E_CertificaParticipativa);

/*--------------------------------------------------- CertificacionTerceraParte */
$sql_E_CTP ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.CertificacionTerceraParte =1 and cai.activo = 1 ;        
        ";

$result_E_CTP = $conexion->query($sql_E_CTP);

while ($row_E_CTP = $result_E_CTP ->fetch_array(MYSQLI_ASSOC)){
    $array_E_CTP[]  = array(
        'id' => $row_E_CTP['idEntrevista'],
        'lat' => $row_E_CTP['Latitud'],
        'log' => $row_E_CTP['Longitud']

    );
}

if (isset($array_E_CTP)){
    $array_E_CTP;
}else{
    $array_E_CTP = 0;
}
//var_dump($array_E_CTP);

/*--------------------------------------------------- ComercioLocal */
$sql_E_CL ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.ComercioLocal =1 and cai.activo = 1 ;        
        ";

$result_E_CL = $conexion->query($sql_E_CL);

while ($row_E_CL = $result_E_CL ->fetch_array(MYSQLI_ASSOC)){
    $array_E_CL[]  = array(
        'id' => $row_E_CL['idEntrevista'],
        'lat' => $row_E_CL['Latitud'],
        'log' => $row_E_CL['Longitud']

    );
}

if (isset($array_E_CL)){
    $array_E_CL;
}else{
    $array_E_CL = 0;
}
//var_dump($array_E_CL);

/*--------------------------------------------------- ComercioRegional */
$sql_E_CReg ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.ComercioRegional =1 and cai.activo = 1 ;        
        ";

$result_E_CReg = $conexion->query($sql_E_CReg);

while ($row_E_CReg = $result_E_CReg ->fetch_array(MYSQLI_ASSOC)){
    $array_E_CReg[]  = array(
        'id' => $row_E_CReg['idEntrevista'],
        'lat' => $row_E_CReg['Latitud'],
        'log' => $row_E_CReg['Longitud']

    );
}
if (isset($array_E_CReg)){
    $array_E_CReg;
}else{
    $array_E_CReg = 0;
}
//var_dump($array_E_CReg);

/*--------------------------------------------------- EmpresaProducInsumo */
$sql_E_EPIns ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.EmpresaProducInsumo =1 and cai.activo = 1 ;        
        ";

$result_E_EPIns = $conexion->query($sql_E_EPIns);

while ($row_E_EPIns = $result_E_EPIns ->fetch_array(MYSQLI_ASSOC)){
    $array_E_EPIns[]  = array(
        'id' => $row_E_EPIns['idEntrevista'],
        'lat' => $row_E_EPIns['Latitud'],
        'log' => $row_E_EPIns['Longitud']

    );
}
if (isset($array_E_EPIns)){
    $array_E_EPIns;
}else{
    $array_E_EPIns = 0;
}
//var_dump($array_E_EPIns);

/*--------------------------------------------------- IniciativaIntercambio */
$sql_E_II ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.IniciativaIntercambio =1 and cai.activo = 1 ;        
        ";

$result_E_II = $conexion->query($sql_E_II);

while ($row_E_II = $result_E_II ->fetch_array(MYSQLI_ASSOC)){
    $array_E_II[]  = array(
        'id' => $row_E_II['idEntrevista'],
        'lat' => $row_E_II['Latitud'],
        'log' => $row_E_II['Longitud']

    );
}
if (isset($array_E_II)){
    $array_E_II;
}else{
    $array_E_II = 0;
}
//var_dump($array_E_II);

/*--------------------------------------------------- MercadoAlternativo */
$sql_E_MAl ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.MercadoAlternativo =1 and cai.activo = 1 ;        
        ";

$result_E_MAl = $conexion->query($sql_E_MAl);

while ($row_E_MAl = $result_E_MAl ->fetch_array(MYSQLI_ASSOC)){
    $array_E_MAl[]  = array(
        'id' => $row_E_MAl['idEntrevista'],
        'lat' => $row_E_MAl['Latitud'],
        'log' => $row_E_MAl['Longitud']

    );
}
if (isset($array_E_MAl)){
    $array_E_MAl;
}else{
    $array_E_MAl = 0;
}
//var_dump($array_E_MAl);

/*--------------------------------------------------- SistemaGesta */
$sql_E_SGe ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.SistemaGesta =1 and cai.activo = 1 ;        
        ";

$result_E_SGe = $conexion->query($sql_E_SGe);

while ($row_E_SGe = $result_E_SGe ->fetch_array(MYSQLI_ASSOC)){
    $array_E_SGe[]  = array(
        'id' => $row_E_SGe['idEntrevista'],
        'lat' => $row_E_SGe['Latitud'],
        'log' => $row_E_SGe['Longitud']

    );
}
if (isset($array_E_SGe)){
    $array_E_SGe;
}else{
    $array_E_SGe = 0;
}
//var_dump($array_E_SGe);

/*--------------------------------------------------- TianguisOrganico */
$sql_E_TOrg ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.TianguisOrganico =1 and cai.activo = 1 ;        
        ";

$result_E_TOrg = $conexion->query($sql_E_TOrg);

while ($row_E_TOrg = $result_E_TOrg ->fetch_array(MYSQLI_ASSOC)){
    $array_E_TOrg[]  = array(
        'id' => $row_E_TOrg['idEntrevista'],
        'lat' => $row_E_TOrg['Latitud'],
        'log' => $row_E_TOrg['Longitud']

    );
}
if (isset($array_E_TOrg)){
    $array_E_TOrg;
}else{
    $array_E_TOrg = 0;
}
//var_dump($array_E_TOrg);

/*--------------------------------------------------- Consumo */
$sql_E_Con ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.Consumo =1 and cai.activo = 1 ;        
        ";

$result_E_Con = $conexion->query($sql_E_Con);

while ($row_E_Con = $result_E_Con ->fetch_array(MYSQLI_ASSOC)){
    $array_E_Con[]  = array(
        'id' => $row_E_Con['idEntrevista'],
        'lat' => $row_E_Con['Latitud'],
        'log' => $row_E_Con['Longitud']

    );
}
if (isset($array_E_Con)){
    $array_E_Con;
}else{
    $array_E_Con = 0;
}
//var_dump($array_E_Con);

/*--------------------------------------------------- Credito */
$sql_E_Credito ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.Credito =1 and cai.activo = 1 ;        
        ";

$result_E_Credito = $conexion->query($sql_E_Credito);

while ($row_E_Credito = $result_E_Credito ->fetch_array(MYSQLI_ASSOC)){
    $array_E_Credito[]  = array(
        'id' => $row_E_Credito['idEntrevista'],
        'lat' => $row_E_Credito['Latitud'],
        'log' => $row_E_Credito['Longitud']

    );
}
if (isset($array_E_Credito)){
    $array_E_Credito;
}else{
    $array_E_Credito = 0;
}
//var_dump($array_E_Credito);

/*--------------------------------------------------- Servicios */
$sql_E_Servicios ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.Servicios =1 and cai.activo = 1 ;        
        ";

$result_E_Servicios = $conexion->query($sql_E_Servicios);

while ($row_E_Servicios = $result_E_Servicios ->fetch_array(MYSQLI_ASSOC)){
    $array_E_Servicios[]  = array(
        'id' => $row_E_Servicios['idEntrevista'],
        'lat' => $row_E_Servicios['Latitud'],
        'log' => $row_E_Servicios['Longitud']

    );
}
if (isset($array_E_Servicios)){
    $array_E_Servicios;
}else{
    $array_E_Servicios = 0;
}
//var_dump($array_E_Servicios);

/*--------------------------------------------------- Semillas */
$sql_E_Semillas ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.Semillas =1 and cai.activo = 1 ;        
        ";

$result_E_Semillas = $conexion->query($sql_E_Semillas);

while ($row_E_Semillas = $result_E_Semillas ->fetch_array(MYSQLI_ASSOC)){
    $array_E_Semillas[]  = array(
        'id' => $row_E_Semillas['idEntrevista'],
        'lat' => $row_E_Semillas['Latitud'],
        'log' => $row_E_Semillas['Longitud']

    );
}
if (isset($array_E_Semillas)){
    $array_E_Semillas;
}else{
    $array_E_Semillas = 0;
}
//var_dump($array_E_Semillas);

/*--------------------------------------------------- Insumos */
$sql_E_Insumos ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.Insumos =1 and cai.activo = 1 ;        
        ";

$result_E_Insumos = $conexion->query($sql_E_Insumos);

while ($row_E_Insumos = $result_E_Insumos ->fetch_array(MYSQLI_ASSOC)){
    $array_E_Insumos[]  = array(
        'id' => $row_E_Insumos['idEntrevista'],
        'lat' => $row_E_Insumos['Latitud'],
        'log' => $row_E_Insumos['Longitud']

    );
}
if (isset($array_E_Insumos)){
    $array_E_Insumos;
}else{
    $array_E_Insumos = 0;
}
//var_dump($array_E_Insumos);

/*--------------------------------------------------- OtroEconomia */
$sql_E_OtroEconomia ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_economia caecon on cae.idEntrevista = caecon.idAreaTrabajo 
where caecon.OtroEconomia <> '' and cai.activo = 1 ;        
        ";

$result_E_OtroEconomia = $conexion->query($sql_E_OtroEconomia);

while ($row_E_OtroEconomia = $result_E_OtroEconomia ->fetch_array(MYSQLI_ASSOC)){
    $array_E_OtroEconomia[]  = array(
        'id' => $row_E_OtroEconomia['idEntrevista'],
        'lat' => $row_E_OtroEconomia['Latitud'],
        'log' => $row_E_OtroEconomia['Longitud']

    );
}
if (isset($array_E_OtroEconomia)){
    $array_E_OtroEconomia;
}else{
    $array_E_OtroEconomia = 0;
}
//var_dump($array_E_OtroEconomia);
