<?php

include '../ConexionBD.php';

require __DIR__ . "/vendor/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$objPHPExcel = new Spreadsheet();
$sheet = $objPHPExcel->getActiveSheet();

$objPHPExcel->createSheet();
include 'ExportacionHojas/Ficha-Tecnica.php';
include 'ExportacionHojas/Area.php';
include 'ExportacionHojas/colectivo.php';
include 'ExportacionHojas/iniciativas.php';
include_once 'ExportacionHojas/genero.php';
include 'ExportacionHojas/politica.php';
include 'ExportacionHojas/redes.php';
include 'ExportacionHojas/formacion.php';
include 'ExportacionHojas/Seguridad.php';
include 'ExportacionHojas/Contacto.php';



// Aquí
$filename = 'test.xlsx';

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');

$objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, 'Xlsx');
$objWriter->save('php://output');




