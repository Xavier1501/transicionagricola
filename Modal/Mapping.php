<?php

include 'ConexionBD.php';
include 'Markets/Mapping-SistemaProduccionAgricola.php';
include 'Markets/MappingActividadesNoAgricolas.php';
include 'Markets/Mapping-Desarrolo-Local.php';
include 'Markets/Mapping-Economia.php';
include 'Markets/Mapping-Derechos-Humanos.php';
include 'Markets/Mapping-Trabajo-Niños.php';
include 'Markets/Mappping-Investigacion-agricola-rural.php';
include 'Markets/Mapping-Manejo-Recursos-Hibridos.php';
include 'Markets/Mapping-Manejo-poscosecha.php';
include 'Markets/Mapping-Semillas.php';
include 'Markets/Mapping-Educacion.php';
include 'Markets/Mapping-Sistemas-Produccion-Animal.php';
include 'Markets/Mapping-Productos-forestales.php';
include 'Markets/Mapping-Sistemas-agroforestales.php';
include 'Markets/Mapping-iniciativas.php';


        $sqlazotea ="        
  select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud,
cai.activo 
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_agricultura_urbana caaur on  caaur.idEntrevistaFK = cae.idEntrevista
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caaur.Azotea =1 and cai.activo = 1;         
        ";
        $resultAzotea = $conexion->query($sqlazotea);
       //$datos = $result->fetch_array(MYSQLI_ASSOC);
        //var_dump($datos);
        while ($rowAzotea = $resultAzotea ->fetch_array(MYSQLI_ASSOC)){
            $datosMarkets[]  = array(
                'id' => $rowAzotea['idEntrevista'],
                'lat' => $rowAzotea['Latitud'],
                'log' => $rowAzotea['Longitud']

            );
        }
if (isset($datosMarkets)){
    //echo'tiene valores';
    $datosMarkets ;
}else{
    //echo 'no';
    $datosMarkets = 0;
}



//var_dump($datosMarkets);


/*--------------------------------------------------- AgriculturaUrbana banco  */


$sqlBalcon ="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.activo,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_agricultura_urbana caaur on  caaur.idEntrevistaFK = cae.idEntrevista
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caaur.Balcon =1 and cai.activo = 1;         
        ";

$resultBalcon = $conexion->query($sqlBalcon);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowBalcon = $resultBalcon ->fetch_array(MYSQLI_ASSOC)){
    $AgUrBalcon[]  = array(
        'id' => $rowBalcon['idEntrevista'],
        'lat' => $rowBalcon['Latitud'],
        'log' => $rowBalcon['Longitud'],
        'activo' => $rowBalcon['activo']

    );
}

if (isset($AgUrBalcon)){

    //echo'tiene valores';
    $AgUrBalcon ;
}else{
    //echo 'no';
    $AgUrBalcon = 0;
}

//var_dump($AgUrBalcon);


/*--------------------------------------------------- AgriculturaUrbana Parques  */


$sqlParques ="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_agricultura_urbana caaur on  caaur.idEntrevistaFK = cae.idEntrevista
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caaur.Parques=1 and cai.activo = 1;         
        ";

$resultParques = $conexion->query($sqlParques);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowParques = $resultParques ->fetch_array(MYSQLI_ASSOC)){
    $AgUrParques[]  = array(
        'id' => $rowParques['idEntrevista'],
        'lat' => $rowParques['Latitud'],
        'log' => $rowParques['Longitud']
    );
}

if (isset($AgUrParques)){

    //echo'tiene valores';
    $AgUrParques;
}else{
    //echo 'no';
    $AgUrParques = 0;
}


/*--------------------------------------------------- AgriculturaUrbana Traspatio */





$sqlTraspatio ="        
  select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_agricultura_urbana caaur on  caaur.idEntrevistaFK = cae.idEntrevista
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caaur.Traspatio=1 and cai.activo = 1;        
        ";

$resultTraspatio= $conexion->query($sqlTraspatio);
//$datos = $resultTraspatio->fetch_array(MYSQLI_ASSOC);
//$rowTraspatio = $resultTraspatio ->fetch_array(MYSQLI_ASSOC);
//var_dump($rowTraspatio);

while($rowTraspatio = $resultTraspatio ->fetch_array(MYSQLI_ASSOC)){
    $arregloTraspatio [] = array(
        'id' => $rowTraspatio['idEntrevista'],
        'lat' => $rowTraspatio['Latitud'],
        'log' => $rowTraspatio['Longitud'],

    );

}

if (isset($arregloTraspatio)){

    //echo'tiene valores';
    $arregloTraspatio ;
}else{
    //echo 'no';
    $arregloTraspatio = 0;
}


//var_dump($arregloTraspatio);


/*--------------------------------------------------- AgriculturaUrbana Hcomunitarios */


$sqlHcomunitarios ="        
 select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_agricultura_urbana caaur on  caaur.idEntrevistaFK = cae.idEntrevista
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caaur.Hcomunitarios =1 and cai.activo = 1;        
        ";

$resultHcomunitarios= $conexion->query($sqlHcomunitarios);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowHcomunitarios = $resultHcomunitarios ->fetch_array(MYSQLI_ASSOC)){
    $AgUrHcomunitarios[]  = array(
        'id' => $rowHcomunitarios['idEntrevista'],
        'lat' => $rowHcomunitarios['Latitud'],
        'log' => $rowHcomunitarios['Longitud'],

    );
}

if (isset($AgUrHcomunitarios)){

    //echo'tiene valores';
    $AgUrHcomunitarios ;
}else{
    //echo 'no';
    $AgUrHcomunitarios = 0;
}

//var_dump($AgUrHcomunitarios);



/*--------------------------------------------------- AgriculturaUrbana Hempresas*/


$sqlHempresas="        
 select
distinct cae.idEntrevista,
cae.NombreGrupo,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_agricultura_urbana caaur on  caaur.idEntrevistaFK = cae.idEntrevista
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caaur.Hempresas =1 and cai.activo = 1;       
        ";

$resultHempresas= $conexion->query($sqlHempresas);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowHempresas = $resultHempresas ->fetch_array(MYSQLI_ASSOC)){
    $AgUrHempresas[]  = array(
        'id' => $rowHempresas['idEntrevista'],
        'lat' => $rowHempresas['Latitud'],
        'log' => $rowHempresas['Longitud']

    );
}


if (isset($AgUrHempresas)){

    //echo'tiene valores';
    $AgUrHempresas ;
}else{
    //echo 'no';
    $AgUrHempresas = 0;
}
//var_dump($AgUrHempresas);


/*--------------------------------------------------- AgriculturaUrbana Hsociales*/


$sqlHsociales="        
 select
distinct cae.idEntrevista,
cae.NombreGrupo,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_agricultura_urbana caaur on  caaur.idEntrevistaFK = cae.idEntrevista
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caaur.HSociales =1 and cai.activo = 1;       
        ";

$resultHsociales= $conexion->query($sqlHsociales);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowHsociales = $resultHsociales ->fetch_array(MYSQLI_ASSOC)){
    $arregloHsociales[]  = array(
        'id' => $rowHsociales['idEntrevista'],
        'lat' => $rowHsociales['Latitud'],
        'log' => $rowHsociales['Longitud']

    );
}


if (isset($arregloHsociales)){

    //echo'tiene valores';
    $arregloHsociales ;
}else{
    //echo 'no';
    $arregloHsociales = 0;
}
//var_dump($arregloHsociales);


/*--------------------------------------------------- AgriculturaUrbana Histituciones */


$sqlHistituciones ="        
select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_agricultura_urbana caaur on  caaur.idEntrevistaFK = cae.idEntrevista
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caaur.HInstitucionales =1 and cai.activo = 1;        
        ";

$resultHistituciones= $conexion->query($sqlHistituciones);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowHistituciones = $resultHistituciones ->fetch_array(MYSQLI_ASSOC)){
    $arregloHistituciones[]  = array(
        'id' => $rowHistituciones['idEntrevista'],
        'lat' => $rowHistituciones['Latitud'],
        'log' => $rowHistituciones['Longitud'],

    );
}

if (isset($arregloHistituciones)){

    //echo'tiene valores';
    $arregloHistituciones ;
}else{
    //echo 'no';
    $arregloHistituciones = 0;
}

//var_dump($arregloHistituciones);


/*--------------------------------------------------- AgriculturaUrbana otros  */


$sqlOtros ="        
   select
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_agricultura_urbana caaur on  caaur.idEntrevistaFK = cae.idEntrevista
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caaur.HOtros <>  '' and cai.activo = 1;      
        ";

$resultOtros = $conexion->query($sqlOtros);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowOtros = $resultOtros ->fetch_array(MYSQLI_ASSOC)){
    $arregloHotros[]  = array(
        'id' => $rowOtros['idEntrevista'],
        'lat' => $rowOtros['Latitud'],
        'log' => $rowOtros['Longitud']

    );
}

if (isset($arregloHotros)){

    //echo'tiene valores';
    $arregloHotros ;
}else{
    //echo 'no';
    $arregloHotros = 0;
}

//var_dump($arregloHotros);

/*--------------------------------------------------- salud plantas medicinales homeopatia  */


$sqlSaludHomeopatia ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_saludplantasmedicinales caspm on cae.idEntrevista = caspm.idAreaTrabajo
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caspm.Homepatia =1 and cai.activo = 1;      
        ";

$resultSaludHomeopatia = $conexion->query($sqlSaludHomeopatia);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowSaludHomeopatia = $resultSaludHomeopatia ->fetch_array(MYSQLI_ASSOC)){
    $arregloSaludHomeopatia[]  = array(
        'id' => $rowSaludHomeopatia['idEntrevista'],
        'lat' => $rowSaludHomeopatia['Latitud'],
        'log' => $rowSaludHomeopatia['Longitud']

    );
}

if (isset($arregloSaludHomeopatia)){

    //echo'tiene valores';
    $arregloSaludHomeopatia ;
}else{
    //echo 'no';
    $arregloSaludHomeopatia = 0;
}

//var_dump($arregloSaludHomeopatia);

/*--------------------------------------------------- salud plantas medicinales medicina  */


$sqlSaludMedicina ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_saludplantasmedicinales caspm on cae.idEntrevista = caspm.idAreaTrabajo
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caspm.Medicina =1 and cai.activo = 1;       
        ";

$resultSaludMedicina = $conexion->query($sqlSaludMedicina);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowSaludMedicina = $resultSaludMedicina ->fetch_array(MYSQLI_ASSOC)){
    $arregloSaludMedicina[]  = array(
        'id' => $rowSaludMedicina['idEntrevista'],
        'lat' => $rowSaludMedicina['Latitud'],
        'log' => $rowSaludMedicina['Longitud']

    );
}

if (isset($arregloSaludMedicina)){

    //echo'tiene valores';
    $arregloSaludMedicina ;
}else{
    //echo 'no';
    $arregloSaludMedicina = 0;
}

//var_dump($arregloSaludMedicina);

/*--------------------------------------------------- salud plantas medicinales preparados botanicos  */


$sqlSaludPreparadosBotanicos ="        
select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_saludplantasmedicinales caspm on cae.idEntrevista = caspm.idAreaTrabajo
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caspm.PreparadosBotanicos =1 and cai.activo = 1;     
        ";

$resultSaludPreparadosBotanicos = $conexion->query($sqlSaludPreparadosBotanicos);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowSaludPreparadosBotanicos = $resultSaludPreparadosBotanicos ->fetch_array(MYSQLI_ASSOC)){
    $arregloSaludPreparadosBotanicos[]  = array(
        'id' => $rowSaludPreparadosBotanicos['idEntrevista'],
        'lat' => $rowSaludPreparadosBotanicos['Latitud'],
        'log' => $rowSaludPreparadosBotanicos['Longitud']

    );
}

if (isset($arregloSaludPreparadosBotanicos)){

    //echo'tiene valores';
    $arregloSaludPreparadosBotanicos ;
}else{
    //echo 'no';
    $arregloSaludPreparadosBotanicos = 0;
}

//var_dump($arregloSaludPreparadosBotanicos);


/*--------------------------------------------------- salud plantas medicinales remedios caseros  */


$sqlSaludRemedioCasero ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_saludplantasmedicinales caspm on cae.idEntrevista = caspm.idAreaTrabajo
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caspm.RemedioCasero =1 and cai.activo = 1;      
        ";



$resultSaludRemedioCasero = $conexion->query($sqlSaludRemedioCasero);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowSaludRemedioCasero = $resultSaludRemedioCasero ->fetch_array(MYSQLI_ASSOC)){
    $arregloSaludRemedioCasero[]  = array(
        'id' => $rowSaludRemedioCasero['idEntrevista'],
        'lat' => $rowSaludRemedioCasero['Latitud'],
        'log' => $rowSaludRemedioCasero['Longitud']

    );
}

if (isset($arregloSaludRemedioCasero)){

    //echo'tiene valores';
    $arregloSaludRemedioCasero ;
}else{
    //echo 'no';
    $arregloSaludRemedioCasero = 0;
}

//var_dump($arregloSaludRemedioCasero);

/*--------------------------------------------------- salud plantas medicinales nutricion  */


$sqlSaludNutricionAgri ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_saludplantasmedicinales caspm on cae.idEntrevista = caspm.idAreaTrabajo
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caspm.NutricionAgri =1 and cai.activo = 1;       
        ";

$resultSaludNutricionAgri = $conexion->query($sqlSaludNutricionAgri);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowSaludNutricionAgri = $resultSaludNutricionAgri ->fetch_array(MYSQLI_ASSOC)){
    $arregloSaludNutricionAgri[]  = array(
        'id' => $rowSaludNutricionAgri['idEntrevista'],
        'lat' => $rowSaludNutricionAgri['Latitud'],
        'log' => $rowSaludNutricionAgri['Longitud']

    );
}

if (isset($arregloSaludNutricionAgri)){

    //echo'tiene valores';
    $arregloSaludNutricionAgri ;
}else{
    //echo 'no';
    $arregloSaludNutricionAgri = 0;
}

//var_dump($arregloSaludNutricionAgri);

/*--------------------------------------------------- salud plantas medicinales otro */


$sqlSaludOtrosaludPlantas ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_saludplantasmedicinales caspm on cae.idEntrevista = caspm.idAreaTrabajo
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caspm.OtrosaludPlantas <> '' and cai.activo = 1;       
        ";

$resultSaludOtrosaludPlantas = $conexion->query($sqlSaludOtrosaludPlantas);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowSaludOtrosaludPlantas = $resultSaludOtrosaludPlantas ->fetch_array(MYSQLI_ASSOC)){
    $arregloSaludOtrosaludPlantas[]  = array(
        'id' => $rowSaludOtrosaludPlantas['idEntrevista'],
        'lat' => $rowSaludOtrosaludPlantas['Latitud'],
        'log' => $rowSaludOtrosaludPlantas['Longitud']

    );
}

if (isset($arregloSaludOtrosaludPlantas)){

    //echo'tiene valores';
    $arregloSaludOtrosaludPlantas ;
}else{
    //echo 'no';
    $arregloSaludOtrosaludPlantas = 0;
}

//var_dump($arregloSaludOtrosaludPlantas);

/*--------------------------------------------------- salud plantas medicinales otro */


$sqlSaludOtrosaludPlantas ="        
   select 
distinct cae.idEntrevista,
cai.Longitud,
cai.Latitud
from co_agr_entrevista cae 
left join co_agr_iniciativa cai on cae.idEntrevista = cai.idEntrevista 
left join co_agr_saludplantasmedicinales caspm on cae.idEntrevista = caspm.idAreaTrabajo
left join co_agr_ubicacion caub on caub.idEntrevista = cae.idEntrevista
left join co_agr_municipio camun on camun.id_municipio =  caub.idMunicipio
left join co_agr_estado caest on caest.id_estado = caub.idEstado
where caspm.OtrosaludPlantas <> '' and cai.activo = 1;       
        ";

$resultSaludOtrosaludPlantas = $conexion->query($sqlSaludOtrosaludPlantas);
//$datos = $result->fetch_array(MYSQLI_ASSOC);


//var_dump($datos);


while ($rowSaludOtrosaludPlantas = $resultSaludOtrosaludPlantas ->fetch_array(MYSQLI_ASSOC)){
    $arregloSaludOtrosaludPlantas[]  = array(
        'id' => $rowSaludOtrosaludPlantas['idEntrevista'],
        'lat' => $rowSaludOtrosaludPlantas['Latitud'],
        'log' => $rowSaludOtrosaludPlantas['Longitud']

    );
}

if (isset($arregloSaludOtrosaludPlantas)){

    //echo'tiene valores';
    $arregloSaludOtrosaludPlantas ;
}else{
    //echo 'no';
    $arregloSaludOtrosaludPlantas = 0;
}

//var_dump($arregloSaludOtrosaludPlantas);



